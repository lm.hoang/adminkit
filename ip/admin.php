<?php
session_start();
/*
 * @author Balaji
 */
require_once('config.php');
if($_SESSION['login'] == false)
{
    header("Location: login.php");
}
else
{ 
}
?>
<!DOCTYPE html>
<html> 
<head>
    <meta charset="utf-8">
    <title>Admin Section- IP Address Script</title>
    <!-- Sets initial viewport load and disables zooming  -->
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="favicon.png"/>
    <link rel="bookmark" href="favicon.png"/> 
    <!-- site css -->
    <link rel="stylesheet" href="css/site.min.css">
    <link rel="stylesheet" href="css/ad.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
    <!-- <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'> -->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="js/site.min.js"></script>
</head>

 <body style="background-color: #f1f2f6;">
    <div class="docs-header">
      <!--nav-->
      <nav class="navbar navbar-default navbar-custom" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img src="img/logo.png" height="40"></a>
          </div>
          <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
              <li><a class="nav-link" href="index.php">Go to Site</a></li>
              <li><a class="nav-link current" href="#top_area">General Settings</a></li>
              <li><a class="nav-link" href="logout.php">Logout</a></li>
            </ul>
          </div>
        </div>
      </nav>
      <!--header-->
  
      <div class="topic">
        <div class="container">
          <h3>Admin Section</h3>
          <h4>Manage your site!</h4>
        </div>
        <div class="topic__infos">
          <div class="container">
          An IP address (Internet Protocol Address) is an identifier for devices on a TCP/IP network.
          </div>
        </div>
      </div>
    </div>
    <!--documents-->

   
<div class="container documents">
<form method="POST" action="<?= $_SERVER['PHP_SELF'];?>">
<div class="top_area">

<?php
if($_SERVER['REQUEST_METHOD'] =='POST')
{
    if($_POST['email'] && $_POST['password'] == null)
    {
        echo "<br> <b> Unable to save the settings without admin email/password.... </b> <br><br>";
    }
    else
    {
    file_put_contents("config.php","<?php
/*
 * @author Balaji
 */

error_reporting(1);

//admin login details
define('EMAIL', '".$_POST['email']."');
define('PASSWORD', '".$_POST['password']."');

//Meta Tags
define('TITLE', '".$_POST['title']."');
define('DESCRIPTION','".$_POST['description']."');
define('KEYWORD', '".$_POST['keyword']."');

// Contact US E-mail ID
define('CONTACT_US','".$_POST['contact_us']."');
define('COMPANY_NAME','".$_POST['company_name']."');
define('OUR_ADDRESS_1','".$_POST['our_address_1']."');
define('OUR_ADDRESS_2','".$_POST['our_address_2']."');
define('OUR_ADDRESS_3','".$_POST['our_address_3']."');
define('OUR_ADDRESS_4','".$_POST['our_address_4']."');
define('OUR_ADDRESS_5','".$_POST['our_address_5']."');

//AD_UNIT
define('AD_UNIT_1','".$_POST['ad_unit_1']."');
define('AD_UNIT_2','".$_POST['ad_unit_2']."');

//About US Page
define('ABOUT_US','".$_POST['about_us']."');
?>");
    }
    echo "<br> <b> Settings saved successfully.... </b> <br><br>";
}
?>
<div class="row">
    <div class="col-md-6">
  <p><b>Admin details</b></p>
  Enter Admin Mail ID: <input type="text"  name="email" id="email" value="<?=EMAIL?>" class="form-control"> <br />
  Enter Admin Password: <input type="password"  name="password" id="password" value="<?=PASSWORD?>" class="form-control"><br />
  Enter Conatct Mail ID: <input type="text"  name="contact_us" id="contact_us" value="<?=CONTACT_US?>" class="form-control"><br />

 </div>
      <div class="col-md-6">
  <p><b>Site details</b></p>
  Enter Site Title: <input type="text"  name="title" id="title" value="<?=TITLE?>" class="form-control"><br />
  Enter Site Description: <input type="text"  name="description" id="description" value="<?=DESCRIPTION?>" class="form-control"><br />
  Enter Site Keywords: <input type="text"  name="keyword" id="keyword" value="<?=KEYWORD?>" class="form-control"><br />
  </div>
  </div>
  <br />
<div class="row">
    <div class="col-md-6">
  <p><b>Company Contact details:</b></p>
  Enter Company Name: <input type="text"  name="company_name" id="company_name" value="<?=COMPANY_NAME?>" class="form-control"><br />
  Enter Address Line 1: <input type="text"  name="our_address_1" id="our_address_1" value="<?=OUR_ADDRESS_1?>" class="form-control"><br /> 
  Enter Address Line 2: <input type="text"  name="our_address_2" id="our_address_2" value="<?=OUR_ADDRESS_2?>" class="form-control"><br /> 
  </div>
  <p><b>&nbsp;</b></p>
  <div class="col-md-6">
  Enter State / Region: <input type="text"  name="our_address_3" id="our_address_3" value="<?=OUR_ADDRESS_3?>" class="form-control"><br /> 
  Enter Country: <input type="text"  name="our_address_4" id="our_address_4" value="<?=OUR_ADDRESS_4?>" class="form-control"><br /> 
  Enter Phone No: <input type="text"  name="our_address_5" id="our_address_5" value="<?=OUR_ADDRESS_5?>" class="form-control"><br /> 
  </div>
  </div>
</div>
<div class="ad_section">
  <p><b>Ad Code details</b></p>
  Enter Ad Code 336x280: (JavaScript / HTML Tgas Only) <textarea name="ad_unit_1" id="ad_unit_1" class="form-control" rows="3"><?=AD_UNIT_1?></textarea><br /><br />

  Enter Ad Code 720x90: (JavaScript / HTML Tgas Only) <textarea name="ad_unit_2" id="ad_unit_2" class="form-control" rows="3"><?=AD_UNIT_2?></textarea><br /><br />
</div>
<div class="about_section">
  <p><b>About details</b></p>
    Enter about page details: (JavaScript / HTML Tgas Only) <textarea name="about_us" id="about_us" class="form-control" rows="3"><?=ABOUT_US?></textarea><br /><br />
</div>
<input type="submit" style="float:right;" class="btn btn-success" value="Save Settings" name="submit" id="submit"/> &nbsp;<br /> &nbsp; <br />
</form>
</div>

 &nbsp; <br />
   
         <div class="clearfix"></div>
            <div class="footer">
              <div class="container">
                <div class="clearfix">
                  <div class="footer-logo"><a href="#"><img src="img/logo.png" /></a></div>
                  <dl class="footer-nav">
                    <dt class="nav-title">Services</dt>
                    <dd class="nav-item"><a href="#">Web Design</a></dd>
                    <dd class="nav-item"><a href="#">WHMCS Intergration</a></dd>
                    <dd class="nav-item"><a href="#">Mobile Design</a></dd>
                    <dd class="nav-item"><a href="#">Box Billing Intergration</a></dd>
                    <dd class="nav-item"><a href="#">User Interface</a></dd>
                  </dl>
                  <dl class="footer-nav">
                    <dt class="nav-title">ABOUT</dt>
                    <dd class="nav-item"><a href="#">The Company</a></dd>
                    <dd class="nav-item"><a href="#">History</a></dd>
                    <dd class="nav-item"><a href="#">Vision</a></dd>
                  </dl>
                  <dl class="footer-nav">
                    <dt class="nav-title">Scripts</dt>
                    <dd class="nav-item"><a href="#">WHOIS Data</a></dd>
                    <dd class="nav-item"><a href="#">IP Locate</a></dd>
                    <dd class="nav-item"><a href="#">Down or Not</a></dd>
                    <dd class="nav-item"><a href="#">Custom OpenVPN GUI</a></dd>
                  </dl>
                  <dl class="footer-nav">
                    <dt class="nav-title">CONTACT</dt>
                    <dd class="nav-item"><a href="#">Basic Info</a></dd>
                    <dd class="nav-item"><a href="#">Map</a></dd>
                    <dd class="nav-item"><a href="http://script2.prothemes.biz/contact.php">Conctact Form</a></dd>
                  </dl>
                </div>
                <div class="footer-copyright text-center">Copyright &copy; 2014 <?=COMPANY_NAME?>. All rights reserved.<br />
                 Get IP Geolocation service from <a href="http://www.whatismyip.com/">whatismyip.com</a>
                </div>
              </div>
            </div>
</body>  
</html>