<?php
session_start();
error_reporting(1);

if($_SESSION['login'] == true)
{
    header("Location: admin.php");
    echo '<meta http-equiv="refresh" content="0;url=admin.php">';
    exit();
}

require_once('config.php');

if($_SERVER['REQUEST_METHOD'] =='POST')
{
if (htmlspecialchars(trim($_POST['email'])) == EMAIL && htmlspecialchars(trim($_POST['password'])) == PASSWORD)
  {
  $status = "Login Success";
  $_SESSION['login'] = true;
  header("Location: admin.php");
  echo '<meta http-equiv="refresh" content="0;url=admin.php">';
  exit();
  }
  else
  {
  $status = "Login Failed";
  $_SESSION['login'] = false;
  }
}
?>
<!DOCTYPE html>
<html> 
<head>
    <meta charset="utf-8">
    <title>Login Page - <?=TITLE?></title>
    <!-- Sets initial viewport load and disables zooming  -->
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="favicon.png"/>
    <link rel="bookmark" href="favicon.png"/> 
    <!-- site css -->
    <link rel="stylesheet" href="css/site.min.css">
    <link rel="stylesheet" href="css/ad.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
    <!-- <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'> -->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="js/site.min.js"></script>
</head>

 <body style="background-color: #f1f2f6;">
    <div class="docs-header">
      <!--nav-->
      <nav class="navbar navbar-default navbar-custom" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img src="img/logo.png" height="40"></a>
          </div>
          <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
              <li><a class="nav-link" href="index.php">Go to Site</a></li>
              <li><a class="nav-link current" href="login.php">Login</a></li>
            </ul>
          </div>
        </div>
      </nav>
      <!--header-->
  
      <div class="topic">
        <div class="container">
          <h3>Admin Section</h3>
          <h4>Manage your site!</h4>
        </div>
        <div class="topic__infos">
          <div class="container">
          An IP address (Internet Protocol Address) is an identifier for devices on a TCP/IP network.
          </div>
        </div>
      </div>
    </div>
    <!--documents-->

   
<div class="container documents">
<form method="POST" action="<?= $_SERVER['PHP_SELF'];?>">
<div class="top_area">
<?php
if($_SERVER['REQUEST_METHOD'] =='POST')
{
   if($status == 'Login Failed')
   {
    echo '       <div class="alert alert-danger">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <strong>Login Failed!</strong> Check username/password and try submitting again.
            </div>';
   } 
}
?>
<p> <b> Admin Login Section </b> </p>
      <div class="row">
              <div class="col-md-6">
              Admin Mail ID:
                <input type="text" name="email" id="email" class="form-control" placeholder="eg. admin@admin.com"><br />
               Admin Password ID:
                <input type="password" name="password" id="password" class="form-control" placeholder="eg. password"><br />
              <input type="submit" style="float:right;" class="btn btn-success" value="Login" name="submit" id="submit"/><br />
              </div>
              <div class="col-md-6">
              </div>
            </div>

 &nbsp;<br /> &nbsp; <br />
</form>
</div></div>

 &nbsp; <br />
   
         <div class="clearfix"></div>
            <div class="footer">
              <div class="container">
                <div class="clearfix">
                  <div class="footer-logo"><a href="#"><img src="img/logo.png" /></a></div>
                  <dl class="footer-nav">
                    <dt class="nav-title">Services</dt>
                    <dd class="nav-item"><a href="#">Web Design</a></dd>
                    <dd class="nav-item"><a href="#">WHMCS Intergration</a></dd>
                    <dd class="nav-item"><a href="#">Mobile Design</a></dd>
                    <dd class="nav-item"><a href="#">Box Billing Intergration</a></dd>
                    <dd class="nav-item"><a href="#">User Interface</a></dd>
                  </dl>
                  <dl class="footer-nav">
                    <dt class="nav-title">ABOUT</dt>
                    <dd class="nav-item"><a href="#">The Company</a></dd>
                    <dd class="nav-item"><a href="#">History</a></dd>
                    <dd class="nav-item"><a href="#">Vision</a></dd>
                  </dl>
                  <dl class="footer-nav">
                    <dt class="nav-title">Scripts</dt>
                    <dd class="nav-item"><a href="#">WHOIS Data</a></dd>
                    <dd class="nav-item"><a href="#">IP Locate</a></dd>
                    <dd class="nav-item"><a href="#">Down or Not</a></dd>
                    <dd class="nav-item"><a href="#">Custom OpenVPN GUI</a></dd>
                  </dl>
                  <dl class="footer-nav">
                    <dt class="nav-title">CONTACT</dt>
                    <dd class="nav-item"><a href="#">Basic Info</a></dd>
                    <dd class="nav-item"><a href="#">Map</a></dd>
                    <dd class="nav-item"><a href="http://script2.prothemes.biz/contact.php">Conctact Form</a></dd>
                  </dl>
                </div>
                <div class="footer-copyright text-center">Copyright &copy; 2014 <?=COMPANY_NAME?>. All rights reserved.<br />
                 Get IP Geolocation service from <a href="http://www.whatismyip.com/">whatismyip.com</a>
                </div>
              </div>
            </div>
</body>  
</html>