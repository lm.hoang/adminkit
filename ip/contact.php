<?php  
/*
 * @author Balaji
 */
error_reporting(1);
require_once('config.php');
?>
<?php

	// Set email to send messages to
	$emailTo = CONTACT_US;

	// Do not edit anything from here unless you know what you are doing
	$contactErrors = array();

	if ($_SERVER['REQUEST_METHOD'] == 'POST')
	{	
		if(trim($_POST['name']) === '')
		{
			$contactErrors['name'] = 'Your full name is required.';
		}
		else
		{
			$name = trim($_POST['name']);
		}
		
		if(trim($_POST['email']) === '')
		{
			$contactErrors['email'] = 'Your email address is required.';
		}
		else if (!preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^", trim($_POST['email'])))
		{
			$contactErrors['email'] = 'Your email address seems to be invalid.';
		}
		else
		{
			$email = trim($_POST['email']);
		}
		
		if(trim($_POST['message']) === '')
		{
			$contactErrors['message'] = 'Your message is required.';
		}
		else
		{
			if (function_exists('stripslashes'))
			{
				$message = stripslashes(trim($_POST['message']));
			}
			else
			{
				$message = trim($_POST['message']);
			}
		}
		
		if (empty($contactErrors) && trim($emailTo) !== '')
		{			
			$subject = '(Contact Form) From ' . $name;
			$body = "Name: $name \n\nEmail: $email \n\nMessage: $message";
			$headers = 'From: ' . $name . ' <' . $emailTo . '>' . "\r\n" . 'Reply-To: ' . $email;
			
			mail($emailTo, $subject, $body, $headers);
			$emailSent = true;
		}
	}

?>
<!DOCTYPE html>
<html> 
<head>
    <meta charset="utf-8">
    <title>Contact US - <?=TITLE?></title>
    <meta name="description" content="<?=DESCRIPTION?>">
    <meta name="keywords" content="<?=KEYWORD?>">
    <!-- Sets initial viewport load and disables zooming  -->
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="favicon.png"/>
    <link rel="bookmark" href="favicon.png"/> 
    <!-- site css -->
    <link rel="stylesheet" href="css/site.min.css">
    <link rel="stylesheet" href="css/ad.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
    <!-- <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'> -->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="js/site.min.js"></script>
   <script type="text/javascript" 
           src="http://maps.google.com/maps/api/js?sensor=false"></script>
</head>

  <body style="background-color: #f1f2f6;">
    <div class="docs-header">
      <!--nav-->
      <nav class="navbar navbar-default navbar-custom" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img src="img/logo.png" height="40"></a>
          </div>
          <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
              <li><a class="nav-link" href="index.php">Home</a></li>
              <li><a class="nav-link" href="ip_lookup.php">IP Lookup</a></li>
              <li><a class="nav-link current" href="contact.php">Contact US</a></li>
              <li><a class="nav-link" href="about.php">About US</a></li>
            </ul>
          </div>
        </div>
      </nav>
      <!--header-->
      
          <div class="topic">
        <div class="container">
          <h3>Contact Us</h3>
          <h4>We are always in touch with our clients</h4>
        </div>
        <div class="topic__infos">
          <div class="container">
          An IP address (Internet Protocol Address) is an identifier for devices on a TCP/IP network.
          </div>
        </div>
      </div>
    </div>
      <div class="container documents">  
      <?php
echo '&nbsp; <br /> &nbsp; <br />';

	if ($_SERVER['REQUEST_METHOD'] == 'POST')
	{
    if (isset($emailSent))
    {
        echo '	<h2>Message <span>Sent</span></h2>
						<p class="text">Thank you, we have received your message and will reply as soon as possible.</p>';
    }
    else
    {
        		echo '<h2>Error <span>- Try Again</span></h2> <br />
                        <b>Somthing went Wrong........</b>';
    }
    }
?>
                        
                         
     <div class="example">
        <div class="row example-modal">
          <div class="col-md-6">
            <h2 class="example-title">Contact US</h2>
            <div class="modal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <form method="POST" action="<?=$_SERVER['PHP_SELF'];?>">
                  <div class="modal-header">
                    <h4 class="modal-title">Contact</h4>
                  </div>
                  <div class="modal-body">
                    <p>Feel free to contact us for any issues you might have with our products.</p>
                    <div class="row">
                      <div class="col-xs-6">
                        <label>Name</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="Name">
                      </div>
                      <div class="col-xs-6">
                        <label>Email</label>
                        <input type="text" name="email" id="email" class="form-control" placeholder="Email">
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-xs-12">
                        <label>Message</label>
                        <textarea name="message" id="message" class="form-control" rows="3"> Hello, </textarea>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="submit" name="submit" id="submit" class="btn btn-success">Send</button>
                  </div>
                </form>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <h2 class="example-title">Our Address</h2>  
            <div class="well">&nbsp; <br /><b> Company Name: <?=COMPANY_NAME?></b> <br /> 
            Address Line 1: <?=OUR_ADDRESS_1?><br /> 
            Address Line 1: <?=OUR_ADDRESS_2?><br /> 
            State / Region: <?=OUR_ADDRESS_3?><br /> 
            Country: <?=OUR_ADDRESS_4?><br /> 
            Phone No: <?=OUR_ADDRESS_5?><br /> 
            Email ID: <?=CONTACT_US?><br /> &nbsp; <br />
            </div>
          </div>
        </div>
      </div>
      </div>
  &nbsp; <br />
   
         <div class="clearfix"></div>
            <div class="footer">
              <div class="container">
                <div class="clearfix">
                  <div class="footer-logo"><a href="#"><img src="img/logo.png" /></a></div>
                  <dl class="footer-nav">
                    <dt class="nav-title">Services</dt>
                    <dd class="nav-item"><a href="#">Web Design</a></dd>
                    <dd class="nav-item"><a href="#">WHMCS Intergration</a></dd>
                    <dd class="nav-item"><a href="#">Mobile Design</a></dd>
                    <dd class="nav-item"><a href="#">Box Billing Intergration</a></dd>
                    <dd class="nav-item"><a href="#">User Interface</a></dd>
                  </dl>
                  <dl class="footer-nav">
                    <dt class="nav-title">ABOUT</dt>
                    <dd class="nav-item"><a href="#">The Company</a></dd>
                    <dd class="nav-item"><a href="#">History</a></dd>
                    <dd class="nav-item"><a href="#">Vision</a></dd>
                  </dl>
                  <dl class="footer-nav">
                    <dt class="nav-title">Scripts</dt>
                    <dd class="nav-item"><a href="#">WHOIS Data</a></dd>
                    <dd class="nav-item"><a href="#">IP Locate</a></dd>
                    <dd class="nav-item"><a href="#">Down or Not</a></dd>
                    <dd class="nav-item"><a href="#">Custom OpenVPN GUI</a></dd>
                  </dl>
                  <dl class="footer-nav">
                    <dt class="nav-title">CONTACT</dt>
                    <dd class="nav-item"><a href="#">Basic Info</a></dd>
                    <dd class="nav-item"><a href="#">Map</a></dd>
                    <dd class="nav-item"><a href="http://script2.prothemes.biz/contact.php">Conctact Form</a></dd>
                  </dl>
                </div>
                <div class="footer-copyright text-center">Copyright &copy; 2014 <?=COMPANY_NAME?>. All rights reserved.<br />
                 Get IP Geolocation service from <a href="http://www.whatismyip.com/">whatismyip.com</a>
                </div>
              </div>
            </div>
</body>  
</html>