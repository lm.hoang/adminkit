<?php  
/*
 * @author Balaji
 */
error_reporting(1);
require_once('code_area.php');
require_once('config.php');
?>
<!DOCTYPE html>
<html> 
<head>
<meta charset="utf-8">
<?php
if($_SERVER['REQUEST_METHOD'] =='POST')
{
    $ip = htmlspecialchars(Trim($_POST['ip']));
    echo "<title>$ip - ".TITLE."</title>";
}else
{
    echo '<title>'.TITLE.'</title>';
}
?>

    <meta name="description" content="<?=DESCRIPTION?>">
    <meta name="keywords" content="<?=KEYWORD?>">
    <!-- Sets initial viewport load and disables zooming  -->
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="favicon.png"/>
    <link rel="bookmark" href="favicon.png"/> 
    <!-- site css -->
    <link rel="stylesheet" href="css/site.min.css">
    <link rel="stylesheet" href="css/ad.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
    <!-- <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'> -->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="js/site.min.js"></script>
   <script type="text/javascript" 
           src="http://maps.google.com/maps/api/js?sensor=false&key=AIzaSyAL3-E9Mjn3hKf69JIJaDp9DGfBnWvhUsY"></script>
</head>
<?php 

if($_SERVER['REQUEST_METHOD'] =='POST')
{
$ip= htmlspecialchars(Trim($_POST['ip']));
$agent = "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0";
$ip_data_url= 'http://api.prothemes.biz/myip/api.php?code=RPYORJMFKKJJEKKOFO&domain='.$_SERVER['HTTP_HOST'].'&ip='.$ip;
    
	$ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $ip_data_url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_USERAGENT, $agent );
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_MAXREDIRS,100);
	curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/html; charset=utf-8","Accept: */*"));
    curl_setopt($ch, CURLOPT_VERBOSE, True);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	$ip_data=curl_exec($ch);
    curl_close($ch);
    
    $city= explode('<div id="city">',$ip_data);
    $city = explode('</div>',$city[1]);
    $city = ucfirst($city[0]);
    
    $region =  explode('<div id="region">',$ip_data);
    $region = explode('</div>',$region[1]);
    $region = ucfirst($region[0]);
    
    $country_code = explode('<div id="country">',$ip_data);
    $country_code = explode('</div>',$country_code[1]);
    $country_code = strtoupper($country_code[0]);
    
    $isp = explode('<div id="isp">',$ip_data);
    $isp = explode('</div>',$isp[1]);
    $isp = ucfirst($isp[0]);
    
    $latitude = explode('<div id="latitude">',$ip_data);
    $latitude = explode('</div>',$latitude[1]);
    $latitude = $latitude[0];
    
    $longitude = explode('<div id="longitude">',$ip_data);
    $longitude = explode('</div>',$longitude[1]);
    $longitude = $longitude[0];
   
    $country = country_code_to_country(Trim($country_code));
    
}   
?> 
  <body style="background-color: #f1f2f6;">
    <div class="docs-header">
      <!--nav-->
      <nav class="navbar navbar-default navbar-custom" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img src="img/logo.png" height="40"></a>
          </div>
          <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
              <li><a class="nav-link" href="index.php">Home</a></li>
              <li><a class="nav-link current" href="ip_lookup.php">IP Lookup</a></li>
              <li><a class="nav-link" href="contact.php">Contact US</a></li>
              <li><a class="nav-link" href="about.php">About US</a></li>
            </ul>
          </div>
        </div>
      </nav>
      <!--header-->
      <div class="topic">
        <div class="container">
        <?php 
if($_SERVER['REQUEST_METHOD'] =='POST')
{
 echo"<h3>IP Address: $ip</h3>
          <h4>Country: <b>$country</b></h4>
          <h4>ISP: <b>$isp</b></h4>";
}  
else
{
   echo"<h3>Lookup any IP address</h3>";  
}

 ?>  
 
        </div>
        <div class="topic__infos">
          <div class="container">
          An IP address (Internet Protocol Address) is an identifier for devices on a TCP/IP network.
          </div>
        </div>
      </div>
    </div>
    <!--documents-->
    
    
 <div class="container documents">
<div class="top_area">

<div id="adunit" style="width: auto; height: 300px; float: right">

  <?=AD_UNIT_1?>
</div> 
              <div class="col-md-6">
                 <div><b> Enter a IP Address: </b></div> <br />
                <form method="POST" action="<?=$_SERVER['PHP_SELF']?>">
                <div class="input-group form-search"> 
                  <input type="text" name="ip" id="ip" placeholder="eg. 83.136.248.98" class="form-control search-query" value="">
                  <span class="input-group-btn">
                    <button type="submit" class="btn btn-primary" data-type="last">Search</button>
                  </span>
                </div>
                </form>
                
<br /><br />
            <div class="panel">
              <div class="tabbable tabs-below">
                <div id="myTabContent" class="tab-content">
                  <div class="tab-pane fade active in" id="home2">
                    <p>An Internet Protocol address (IP address) is a numerical label assigned to each device (e.g., computer, printer) participating in a computer network that uses the Internet Protocol for communication.</p>
                  </div>
                  <div class="tab-pane fade" id="profile2">
                    <p>Using the IP tracer or IP lookup tool will allow you to trace the origin of an IP address back to the original source. </p>
                  </div>
                  <div class="tab-pane fade" id="dropdown2-1">
                    <p>Our data service from: Get IP Geolocation service from whatismyip.com! Geo Map service from Google Maps! </p>
                  </div>
                </div>
                <ul id="myTab2" class="nav nav-tabs nav-justified">
                  <li class="active"><a href="#home2" data-toggle="tab">IP address</a></li>
                  <li><a href="#profile2" data-toggle="tab">IP lookup</a></li>
                  <li><a href="#dropdown2-1" data-toggle="tab">Data</a></li>
                </ul>
              </div>
              
              </div>


    </div>
        </div>
 <div class="clearfix"></div><div class="clearfix"></div>
<?php 

if($_SERVER['REQUEST_METHOD'] =='POST')
{
    echo '<div class="top_area1">
<div id="adunit" style="width: auto; height: 300px; float: left">

  <a href="index.php"><img src="img/ad.png" width="336" height="280"></a>
</div> 
   <div id="map" style="width: 760px; height: 280px; float: right"></div> 

   <script type="text/javascript"> 
      var myOptions = {
         zoom: 10,
         center: new google.maps.LatLng('.$latitude.', '.$longitude.'),
         mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      var map = new google.maps.Map(document.getElementById("map"), myOptions);
   </script> 
 </div>   
 
               <div class="clearfix"></div><div class="clearfix"></div>
               
               
<div class="example">
        <h2 class="example-title">More Information</h2>
        <div class="row">
          <div class="col-md-6">
            <ul class="list-group">
              <li class="list-group-item"><span class="badge badge-default">'.$ip.'</span>Your IP address is:</li>
              <li class="list-group-item"><span class="badge">'.$city.'</span>Your city is:</li>
              <li class="list-group-item"><span class="badge badge-primary">'.$region.'</span>Your state/region is:</li>
              <li class="list-group-item"><span class="badge badge-success">'.$country.'</span>Your country is: </li>
              <li class="list-group-item"><span class="badge badge-warning">'.$isp.'</span>Your ISP is: </li>
              <li class="list-group-item"><span class="badge badge-danger">'.$longitude.'</span><span class="badge badge-danger">'.$latitude.'</span>Your latitude/Longitude is:</li>
            </ul>
          </div>
  
          <div class="col-md-6">
            <div class="list-group">
              <a href="http://en.wikipedia.org/wiki/Special:Search?search='.$country.'" class="list-group-item">
                <h4 class="list-group-item-heading">Read about '.$country.'</h4>
                <p class="list-group-item-text">Read about the country population statistics, culture, religion, languages and economy details.</p>
              </a>
              <a href="http://en.wikipedia.org/wiki/Special:Search?search='.$isp.'" class="list-group-item">
                <h4 class="list-group-item-heading">About '.$isp.' (ISP)</h4>
                <p class="list-group-item-text">An Internet service provider (ISP) is an organization that provides services for accessing, using, or participating in the Internet.</p>
              </a>
              <a href="#" class="list-group-item">
                <h4 class="list-group-item-heading">Check IP Blacklisted or Not</h4>
                <p class="list-group-item-text">The blacklist test will check a server IP address against over 100 DNS based blacklists &amp; other spam details. </p>
              </a>
            </div>
          </div>
        
        
        </div>
      </div>
 ';
}
else
{
}
?>    

<div class="myipinfo">

<h4>Lookup IP Address Location</h4>
<p>
Every machine that is on a TCP/IP network ( a local network, or the Internet ) has a unique Internet Protocol ( IP ) address. <br /><br />

IP-Lookup helps you to find information about your current IP address or any other IP address. It supports both IPv4 and IPv6 addresses. <br /><br />

<h4>How accurate is the IP Lookup tool?</h4>

Your IP Location can be found using our IP Lookup tool. No IP Lookup tool is 100% accurate due to many different factors. <br /> <br />
Country - 99% <br />
State - 90% <br />
Region (25-mile radius) - 81% <br /> <br />

In other countries the accuracy for 25-mile radius within the region is 55%. <br /><br />

<h4>Information you get.</h4>
Your IP-address search will give you general details only about what is on the end of that IP address. Here's what you'll find out: <br /><br />

    The ISP and organization's name<br />
    The country it's in<br />
    The region/state<br />
    The city (a best guess)<br />
    The latitude and longitude of the location (a best guess)<br />
</p>
</div>
<div class="clearfix"></div>   

   &nbsp; <br />
 <div id="adunit1" style="width: auto; height: 80px;"> &nbsp; <br /> &nbsp; <br />
 <?=AD_UNIT_2?>
</div> 

   </div>
  &nbsp; <br />
   
         <div class="clearfix"></div>
            <div class="footer">
              <div class="container">
                <div class="clearfix">
                  <div class="footer-logo"><a href="#"><img src="img/logo.png" /></a></div>
                  <dl class="footer-nav">
                    <dt class="nav-title">Services</dt>
                    <dd class="nav-item"><a href="#">Web Design</a></dd>
                    <dd class="nav-item"><a href="#">WHMCS Intergration</a></dd>
                    <dd class="nav-item"><a href="#">Mobile Design</a></dd>
                    <dd class="nav-item"><a href="#">Box Billing Intergration</a></dd>
                    <dd class="nav-item"><a href="#">User Interface</a></dd>
                  </dl>
                  <dl class="footer-nav">
                    <dt class="nav-title">ABOUT</dt>
                    <dd class="nav-item"><a href="#">The Company</a></dd>
                    <dd class="nav-item"><a href="#">History</a></dd>
                    <dd class="nav-item"><a href="#">Vision</a></dd>
                  </dl>
                  <dl class="footer-nav">
                    <dt class="nav-title">Scripts</dt>
                    <dd class="nav-item"><a href="#">WHOIS Data</a></dd>
                    <dd class="nav-item"><a href="#">IP Locate</a></dd>
                    <dd class="nav-item"><a href="#">Down or Not</a></dd>
                    <dd class="nav-item"><a href="#">Custom OpenVPN GUI</a></dd>
                  </dl>
                  <dl class="footer-nav">
                    <dt class="nav-title">CONTACT</dt>
                    <dd class="nav-item"><a href="#">Basic Info</a></dd>
                    <dd class="nav-item"><a href="#">Map</a></dd>
                    <dd class="nav-item"><a href="http://script2.prothemes.biz/contact.php">Conctact Form</a></dd>
                  </dl>
                </div>
                <div class="footer-copyright text-center">Copyright &copy; 2014 <?=COMPANY_NAME?>. All rights reserved.<br />
                 Get IP Geolocation service from <a href="http://www.whatismyip.com/">whatismyip.com</a>
                </div>
              </div>
            </div>
</body>  
</html>