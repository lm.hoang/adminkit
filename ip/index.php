<?php  
/*
 * @author Balaji
 */
error_reporting(1);
require_once('code_area.php');
require_once('config.php');
?>
<!DOCTYPE html>
<html> 
<head>
    <meta charset="utf-8">
    <title><?=TITLE?></title>
    <meta name="description" content="<?=DESCRIPTION?>">
    <meta name="keywords" content="<?=KEYWORD?>">
    <!-- Sets initial viewport load and disables zooming  -->
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="favicon.png"/>
    <link rel="bookmark" href="favicon.png"/> 
    <!-- site css -->
    <link rel="stylesheet" href="css/site.min.css">
    <link rel="stylesheet" href="css/ad.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
    <!-- <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'> -->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="js/site.min.js"></script>
   <script type="text/javascript" 
           src="http://maps.google.com/maps/api/js?sensor=false&key=AIzaSyAL3-E9Mjn3hKf69JIJaDp9DGfBnWvhUsY"></script>
</head>
<?php 
$ip= $_SERVER['REMOTE_ADDR'];
$agent = "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0";
$ip_data_url= 'http://api.prothemes.biz/myip/api.php?code=RPYORJMFKKJJEKKOFO&domain='.$_SERVER['HTTP_HOST'].'&ip='.$ip;
    
	$ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $ip_data_url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_USERAGENT, $agent );
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_MAXREDIRS,100);
	curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/html; charset=utf-8","Accept: */*"));
    curl_setopt($ch, CURLOPT_VERBOSE, True);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	$ip_data=curl_exec($ch);
    curl_close($ch);
    
    $city= explode('<div id="city">',$ip_data);
    $city = explode('</div>',$city[1]);
    $city = ucfirst($city[0]);
    
    $region =  explode('<div id="region">',$ip_data);
    $region = explode('</div>',$region[1]);
    $region = ucfirst($region[0]);
    
    $country_code = explode('<div id="country">',$ip_data);
    $country_code = explode('</div>',$country_code[1]);
    $country_code = strtoupper($country_code[0]);
    
    $isp = explode('<div id="isp">',$ip_data);
    $isp = explode('</div>',$isp[1]);
    $isp = ucfirst($isp[0]);
    
    $latitude = explode('<div id="latitude">',$ip_data);
    $latitude = explode('</div>',$latitude[1]);
    $latitude = $latitude[0];
    
    $longitude = explode('<div id="longitude">',$ip_data);
    $longitude = explode('</div>',$longitude[1]);
    $longitude = $longitude[0];
   
    $country = country_code_to_country(Trim($country_code));
    
?> 
  <body style="background-color: #f1f2f6;">
    <div class="docs-header">
      <!--nav-->
      <nav class="navbar navbar-default navbar-custom" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img src="img/logo.png" height="40"></a>
          </div>
          <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
              <li><a class="nav-link current" href="index.php">Home</a></li>
              <li><a class="nav-link" href="ip_lookup.php">IP Lookup</a></li>
              <li><a class="nav-link" href="contact.php">Contact US</a></li>
              <li><a class="nav-link" href="about.php">About US</a></li>
            </ul>
          </div>
        </div>
      </nav>
      <!--header-->
  
      <div class="topic">
        <div class="container">
          <h3>Your IP Address: <?=$ip?></h3>
          <h4>Your Country: <b><?=$country?></b></h4>
          <h4>Your ISP: <b><?=$isp?></b></h4>
        </div>
        <div class="topic__infos">
          <div class="container">
          An IP address (Internet Protocol Address) is an identifier for devices on a TCP/IP network.
          </div>
        </div>
      </div>
    </div>
    <!--documents-->

   
         <div class="container documents">
<div class="top_area">
<div id="adunit" style="width: auto; height: 300px; float: left">

<?=AD_UNIT_1?>
</div> 
   <div id="map" style="width: 760px; height: 280px; float: right"></div> 

   <script type="text/javascript"> 
      var myOptions = {
         zoom: 10,
         center: new google.maps.LatLng(<?=$latitude?>, <?=$longitude?>),
         mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      var map = new google.maps.Map(document.getElementById("map"), myOptions);
   </script> 
 </div>   
 
               <div class="clearfix"></div><div class="clearfix"></div>
               
               
<div class="example">
        <h2 class="example-title">More Information</h2>
        <div class="row">
          <div class="col-md-6">
            <ul class="list-group">
              <li class="list-group-item"><span class="badge badge-default"><?=$ip?></span>Your IP address is:</li>
              <li class="list-group-item"><span class="badge"><?=$city?></span>Your city is:</li>
              <li class="list-group-item"><span class="badge badge-primary"><?=$region?></span>Your state/region is:</li>
              <li class="list-group-item"><span class="badge badge-success"><?=$country?></span>Your country is: </li>
              <li class="list-group-item"><span class="badge badge-warning"><?=$isp?></span>Your ISP is: </li>
              <li class="list-group-item"><span class="badge badge-danger"><?=$longitude?></span><span class="badge badge-danger"><?=$latitude?></span>Your latitude/Longitude is:</li>
            </ul>
          </div>
  
          <div class="col-md-6">
            <div class="list-group">
              <a href="http://en.wikipedia.org/wiki/Special:Search?search=<?=$country?>" class="list-group-item">
                <h4 class="list-group-item-heading">Read about <?=$country?></h4>
                <p class="list-group-item-text">Read about the country population statistics, culture, religion, languages and economy details.</p>
              </a>
              <a href="http://en.wikipedia.org/wiki/Special:Search?search=<?=$isp?>" class="list-group-item">
                <h4 class="list-group-item-heading">About <?=$isp?> (ISP)</h4>
                <p class="list-group-item-text">An Internet service provider (ISP) is an organization that provides services for accessing, using, or participating in the Internet.</p>
              </a>
              <a href="#" class="list-group-item">
                <h4 class="list-group-item-heading">Check IP Blacklisted or Not</h4>
                <p class="list-group-item-text">The blacklist test will check a server IP address against over 100 DNS based blacklists &amp; other spam details. </p>
              </a>
            </div>
          </div>
        
        
        </div>
      </div> 
      

   &nbsp; <br />
 <div id="adunit1" style="width: auto; height: 80px;">
<?=AD_UNIT_2?>

</div> 

   </div>
   &nbsp; <br />
   
         <div class="clearfix"></div>
            <div class="footer">
              <div class="container">
                <div class="clearfix">
                  <div class="footer-logo"><a href="#"><img src="img/logo.png" /></a></div>
                  <dl class="footer-nav">
                    <dt class="nav-title">Services</dt>
                    <dd class="nav-item"><a href="#">Web Design</a></dd>
                    <dd class="nav-item"><a href="#">WHMCS Intergration</a></dd>
                    <dd class="nav-item"><a href="#">Mobile Design</a></dd>
                    <dd class="nav-item"><a href="#">Box Billing Intergration</a></dd>
                    <dd class="nav-item"><a href="#">User Interface</a></dd>
                  </dl>
                  <dl class="footer-nav">
                    <dt class="nav-title">ABOUT</dt>
                    <dd class="nav-item"><a href="#">The Company</a></dd>
                    <dd class="nav-item"><a href="#">History</a></dd>
                    <dd class="nav-item"><a href="#">Vision</a></dd>
                  </dl>
                  <dl class="footer-nav">
                    <dt class="nav-title">Scripts</dt>
                    <dd class="nav-item"><a href="#">WHOIS Data</a></dd>
                    <dd class="nav-item"><a href="#">IP Locate</a></dd>
                    <dd class="nav-item"><a href="#">Down or Not</a></dd>
                    <dd class="nav-item"><a href="#">Custom OpenVPN GUI</a></dd>
                  </dl>
                  <dl class="footer-nav">
                    <dt class="nav-title">CONTACT</dt>
                    <dd class="nav-item"><a href="#">Basic Info</a></dd>
                    <dd class="nav-item"><a href="#">Map</a></dd>
                    <dd class="nav-item"><a href="http://script2.prothemes.biz/contact.php">Conctact Form</a></dd>
                  </dl>
                </div>
                <div class="footer-copyright text-center">Copyright &copy; 2014 <?=COMPANY_NAME?>. All rights reserved.<br />
                 Get IP Geolocation service from <a href="http://www.whatismyip.com/">whatismyip.com</a>
                </div>
              </div>
            </div>
</body>  
</html>