<?php

defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: Rainbow PHP Framework
 * @copyright © 2017 ProThemes.Biz
 *
 */
?>
<!-- Blog style -->
<link href="<?php themeLink('css/blog.css'); ?>" rel="stylesheet" />

<div class="container main-container">
    <div class="row">
        
    <?php
    if($themeOptions['general']['sidebar'] == 'left')
        require_once(THEME_DIR."sidebar.php");
    ?>   

    <!-- Blog Entries Column -->
   <div class="col-md-8 contentLayer blogLayer top40">
        
   <div class="col-md-12">
    
    <?php
    foreach($rsd as $row){
        if($row['access'] == 'registered'){
            if(!isset($_SESSION['username']))
                continue;
        }
        if($row['lang'] != 'all' && $row['lang'] != ACTIVE_LANG)
            continue;
        $post_title = $posted_by = $category = $tags = $page_content = $post_url = $myimage = $posted_date_raw = '';
        $post_title = shortCodeFilter($row['post_title']);
        $posted_by = shortCodeFilter($row['posted_by']);
        $category = shortCodeFilter($row['category']);
        $tags = shortCodeFilter($row['meta_tags']);
        $page_content = shortCodeFilter(htmlspecialchars_decode($row['post_content']));
        $post_url = $row['post_url'];$posted_date_raw=date_create($row['date']);   
        $myimage = trim($row['featured_image']) == '' ? $no_image_path: trim($row['featured_image']);
    ?>
        
    <div class="row mr_top30">
        <div class="col-md-12">
          <div>
            <div class="row">
              <div class="col-md-10 raTitle">
              <div class="title_bar">
                  <a href="<?php createLink('blog/'.$post_url); ?>">
                  <h3 class="premiumTitle csPageTitle"><?php echo ucfirst($post_title); ?></h3>
                  </a>
              </div>
              
              <div class="text_size12"> 
                  <span class="color_text_in"> 
                    <i class="fa fa-tag color-grey fz14"></i>in 
                    <b class="color_dark"><a href="<?php createLink('blog/category/'.str_replace(' ','-',$category)); ?>"><?php echo ucfirst($category); ?></a></b> 
                  </span> 
                  <span class="color_text_in">
                    <i class="fa fa-user color-grey fz14"></i> <?php trans('by',$lang['BL286']); ?> <b class="color_dark"> <?php echo ucfirst($posted_by); ?></b>
                  </span> 
              </div>
              </div>
                
              <div class="col-md-2 raDate">
                <div class="date_1 pull-right">
                  <div class="date_up2">
                    <p class="center2 feb2"><?php echo date_format($posted_date_raw,'M'); ?></p>
                  </div>
                  <div class="date_down2">
                    <p class="text_word"><?php echo date_format($posted_date_raw,'j'); ?></p>
                  </div>
                </div>
              </div>
            </div>
              <hr style="margin:20px 0;" />
            <div class="row">
              <div class="col-md-4">
                <div class="thumbnail blog-img"> 
                <a href="<?php createLink('blog/'.$post_url); ?>"><img src="<?php echo $baseURL. 'core/library/imagethumb.php?w=180&=180&src='.$myimage; ?>" alt="<?php echo $post_title; ?>" /></a> 
                </div>
               </div>
               
                <div class="col-md-8 pad_left">
                    
                    <p style="text-align: justify;" class="font_14 version">
                    <?php echo Trim(truncate(strip_tags($page_content), "90", $post_length)); ?>
                    <br /><a href="<?php createLink('blog/'.$post_url); ?>" class="btn btn-blog-success"><?php trans('Read More',$lang['BL287']); ?></a> </p>                    
                </div> 
                
            </div>
          </div>
        </div>
      </div>

      <div class="divider_h mr_top30"></div>
      
      <?php } ?>    
      
      <div class="row mr_top30 mr_bottom1">
        <div class="col-md-5"> </div>
        <div class="col-md-5">
            <br /> <?php echo $pg->process(); ?>
        </div>
      </div>
       
          </div>   		
        </div>
                    
        <?php
        if($themeOptions['general']['sidebar'] == 'right')
            require_once(THEME_DIR."sidebar.php");
        ?>  
    </div> 
</div> 
<br />