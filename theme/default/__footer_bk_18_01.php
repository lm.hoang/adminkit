<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
/*
 * @author Balaji
 * @name: A to Z SEO Tools - PHP Script
 * @theme: Default Style
 * @copyright © 2017 ProThemes.Biz
 *
 */
?>
<?php
	require_once(THEME_DIR."includes/benefit.php");
	require_once(THEME_DIR."includes/howtouse.php");
?>

<!--section-footer./start-->
<section class="section-links mega-footer">
    <div class="fill bg-fill"></div>
    <div class="container">
        <div class="wrap-links accordion" id="accordionExample">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <div class="box-text">
                            <div class="links-title">
                                <i class="add-more"></i>
                                <h2>Quick Links <span>(Explore popular categories)</span></h2>
                            </div>
                        </div>
                    </div>
                    <div class="show-links">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2dot4">
                                <div class="box-text">
                                    <h4>Server Performance Tools</h4>
                                    <p><a href="https://seotool.adminkit.net/my-ip-address">My IP Address</a></p>
                                    <p><a href="https://seotool.adminkit.net/server-status-checker">Server Status Checker</a></p>
                                    <p><a href="https://seotool.adminkit.net/reverse-ip-domain-checker">Reverse IP Domain Checker</a></p>
                                    <p><a href="https://seotool.adminkit.net/blacklist-lookup">Blacklist Lookup</a></p>
                                    <p><a href="https://seotool.adminkit.net/domain-hosting-checker">Domain Hosting Checker</a></p>
                                    <p><a href="https://seotool.adminkit.net/find-dns-records">Find DNS records</a></p>
                                    <p><a href="https://seotool.adminkit.net/domain-authority-checker">Domain Authority Checker</a></p>
                                    <p><a href="https://seotool.adminkit.net/domain-into-ip">Domain into IP</a></p>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2dot4">
                                <div class="box-text">
                                    <h4>SEO Performance Tools</h4>
                                    <p><a href="https://seotool.adminkit.net/article-rewriter">Article Rewriter</a></p>
                                    <p><a href="https://seotool.adminkit.net/backlink-maker">Backlink Maker</a></p>
                                    <p><a href="https://seotool.adminkit.net/meta-tag-generator">Meta Tag Generator</a></p>
                                    <p><a href="https://seotool.adminkit.net/meta-tags-analyzer">Meta Tags Analyzer</a></p>
                                    <p><a href="https://seotool.adminkit.net/keyword-position-checker">Keyword Position Checker</a></p>
                                    <p><a href="https://seotool.adminkit.net/robots-txt-generator">Robots.txt Generator</a></p>
                                    <p><a href="https://seotool.adminkit.net/xml-sitemap-generator">XML Sitemap Generator</a></p>
                                    <p><a href="https://seotool.adminkit.net/backlink-checker">Backlink Checker</a></p>
                                    <p><a href="https://seotool.adminkit.net/alexa-rank-checker">Alexa Rank Checker</a></p>
                                    <p><a href="https://seotool.adminkit.net/url-rewriting-tool">URL Rewriting Tool</a></p>
                                    <p><a href="https://seotool.adminkit.net/google-index-checker">Google Index Checker</a></p>
                                    <p><a href="https://seotool.adminkit.net/website-links-count-checker">Website Links Count Checker</a></p>
                                    <p><a href="https://seotool.adminkit.net/spider-simulator">Search Engine Spider Simulator</a></p>
                                    <p><a href="https://seotool.adminkit.net/keywords-suggestion-tool">Keywords Suggestion Tool</a></p>
                                    <p><a href="https://seotool.adminkit.net/page-authority-checker">Page Authority Checker</a></p>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2dot4">
                                <div class="box-text">
                                    <h4>Web Peformance Tools</h4>
                                    <p><a href="https://seotool.adminkit.net/page-size-checker">Page Size Checker</a></p>
                                    <p><a href="https://seotool.adminkit.net/website-screenshot-generator">Website Screenshot Generator</a></p>
                                    <p><a href="https://seotool.adminkit.net/get-source-code-of-webpage">Get Source Code of Webpage</a></p>
                                    <p><a href="https://seotool.adminkit.net/page-speed-checker">Page Speed Checker</a></p>
                                    <p><a href="https://seotool.adminkit.net/code-to-text-ratio-checker">Code to Text Ratio Checker</a></p>
                                    <p><a href="https://seotool.adminkit.net/broken-links-finder">Broken Links Finder</a></p>
                                    <p><a href="https://seotool.adminkit.net/pagespeed-insights-checker">Pagespeed Insights Checker</a></p>
                                    <p><a href="https://seotool.adminkit.net/get-http-headers">Get HTTP Headers</a></p>
                                    <p><a href="https://seotool.adminkit.net/mobile-friendly-test">Mobile Friendly Test</a></p>
                                    <p><a href="https://seotool.adminkit.net/website-reviewer">Website Reviewer</a></p>
                                    <p><a href="https://seotool.adminkit.net/keyword-density-checker">Keyword Density Checker</a></p>
                                    <p><a href="https://seotool.adminkit.net/check-gzip-compression">Check GZIP compression</a></p>
                                    <p><a href="https://seotool.adminkit.net/class-c-ip-checker">Class C Ip Checker</a></p>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2dot4">
                                <div class="box-text">
                                    <h4>Admin Tool Kits</h4>
                                    <p><a href="https://adminkit.net/dnsbl.aspx">BlackList Check</a></p>
                                    <p><a href="https://adminkit.net/mxlookup.aspx">MX Lookup</a></p>
                                    <p><a href="https://adminkit.net/dnsrecords.aspx">DNS Lookup</a></p>
                                    <p><a href="https://adminkit.net/telnet.aspx">Telnet</a></p>
                                    <p><a href="https://adminkit.net/ip2location.aspx">IP 2 Location</a></p>
                                    <p><a href="https://adminkit.net/ping.aspx">Ping</a></p>
                                    <p><a href="https://adminkit.net/dnsbl_monitor.aspx">BlackList Monitor</a></p>
                                    <p><a href="https://adminkit.net/smtp.aspx">SMTP Test Tool</a></p>
                                    <p><a href="https://adminkit.net/my_ip_address.aspx">My IP Adress</a></p>
                                    <p><a href="https://adminkit.net/traceroute.aspx">Trace route</a></p>
                                    <p><a href="https://adminkit.net/whois.aspx">Whois</a></p>
                                    <p><a href="https://adminkit.net/applications.aspx">Desktop ToolsOther Tools</a></p>
                                </div>
                            </div>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2dot4">
                                <div class="box-text">
                                    <h4>Other Tools</h4>
                                    <p><a href="https://seotool.adminkit.net/plagiarism-checker">Plagiarism Checker</a></p>
                                    <p><a href="https://seotool.adminkit.net/word-counter">Word Counter</a></p>
                                    <p><a href="https://seotool.adminkit.net/link-analyzer-tool">Link Analyzer</a></p>
                                    <p><a href="https://seotool.adminkit.net/domain-age-checker">Domain Age Checker</a></p>
                                    <p><a href="https://seotool.adminkit.net/whois-checker">Whois Checker</a></p>
                                    <p><a href="https://seotool.adminkit.net/www-redirect-checker">www Redirect Checker</a></p>
                                    <p><a href="https://seotool.adminkit.net/mozrank-checker">Mozrank Checker</a></p>
                                    <p><a href="https://seotool.adminkit.net/url-encoder-decoder">URL Encoder / Decoder</a></p>
                                    <p><a href="https://seotool.adminkit.net/suspicious-domain-checker">Suspicious Domain Checker</a></p>
                                    <p><a href="https://seotool.adminkit.net/link-price-calculator">Link Price Calculator</a></p>
                                    <p><a href="https://seotool.adminkit.net/online-md5-generator">Online Md5 Generator</a></p>
                                    <p><a href="https://seotool.adminkit.net/what-is-my-browser">What is my Browser</a></p>
                                    <p><a href="https://seotool.adminkit.net/email-privacy">Email Privacy</a></p>
                                    <p><a href="https://seotool.adminkit.net/google-cache-checker">Google Cache Checker</a></p>
                                    <p><a href="https://seotool.adminkit.net/social-stats-checker">Social Stats Checker</a></p>
                                    <p><a href="https://seotool.adminkit.net/domain-availability-checker">Bulk Domain Availability Checker</a></p>
                                    <p><a href="https://seotool.adminkit.net/grammar-checker">Grammar Checker</a></p>
                                    <p><a href="https://seotool.adminkit.net/flag-counter">Flag Counter</a></p>
                                    <p><a href="https://seotool.adminkit.net/google-malware-checker">Google Malware Checker</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--footer./start-->
<button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>
<style>
#myBtn {
    font-size: 14px;
    border: none;
    outline: none;
    z-index: 9;
    position: fixed;
    right: 15px;
    bottom: 100px;
    background: #f7931d;
    color: #fff;
    width: 40px;
    height: 40px;
    padding-top: 9px;
    text-align: center;
    border-radius: 4px;
    cursor: pointer;
	line-height: 40px;
}
#myBtn:before {
    content: "\f077";
    font-family: 'FontAwesome';
    position: absolute;
    top: 8px;
    left: 50%;
    transform: translate(-50%, -50%);
}
</style>
<script>
//Get the button
var mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
</script>
<footer class="mega-footer">
    <div class="fill bg-fill"></div>
    <div class="container">
        <div class="footer-top">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                    <div class="icon-box">
                        <div class="box-images">
                            <a href="#">
                                <img src="../img/logo-netbase.png" alt="logo-footer">
                            </a>
                        </div>
                        <div class="box-text">
                            <p class="info-netbase">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fames id condimentum a neque, id quam tempus eget nulla. At faucibus et lorem ac.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
                    <div class="row">
                        <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                            <div class="box-text">
                                <h4>Netbase JSC</h4>
                                <p><a href="#">Terms</a></p>
                                <p><a href="#">Licenses</a></p>
                                <p><a href="#">Market API</a></p>
                                <p><a href="#">Become an affiliate</a></p>
                            </div>
                        </div>
                        <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                            <div class="box-text">
                                <h4>Netbase JSC</h4>
                                <p><a href="#">Terms</a></p>
                                <p><a href="#">Licenses</a></p>
                                <p><a href="#">Market API</a></p>
                                <p><a href="#">Become an affiliate</a></p>
                            </div>
                        </div>
                        <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                            <div class="box-text">
                                <h4>Adminkit</h4>
                                <p><a href="#">Terms</a></p>
                                <p><a href="#">Licenses</a></p>
                                <p><a href="#">Market API</a></p>
                                <p><a href="#">Become an affiliate</a></p>
								<p><a href="https://adminkit.net/privacypolicy.aspx">Privacy</a></p>
								<p><a href="https://adminkit.net/contactus.aspx">Contact us</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="row">
                <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                    <div class="Library-images">
                        <span class="icon-lib"></span>
                        <span class="icon-lib"></span>
                        <span class="icon-lib"></span>
                        <span class="icon-lib"></span>
                        <span class="icon-lib"></span>
                    </div>
                </div>
                <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">

                </div>
                <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                    <div class="social-network">
                        <span class="icon-social icon-skype"></span>
                        <span class="icon-social icon-twitter"></span>
                        <span class="icon-social icon-facebook"></span>
                        <span class="icon-social icon-google"></span>
                        <span class="icon-social icon-instagram"></span>
                        <span class="icon-social icon-youtube"></span>
                    </div>
                </div>
            </div>
            <div class="wrap-footer-bottom">
                <div class="row">
                    <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
			<div class="netbase-about">
				Netbase JSC,. Copyright Â© 2010 - 2020 by <a href="#">Netbase</a>. All Rights Reserved
			</div>
		    </div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4"></div>
                    <div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
                        <ul class="links-about">
                            <li><a href="#">About Netbase JSC</a></li>
                            <li><a href="#">Careers</a></li>
                            <li><a href="#">Privacy</a></li>
                            <li><a href="#">PolicySitemap</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>         
<!--sidenav-fixed./start-->
<section class="sidenav-fixed">
   <div class="logo">
      <a href="#">
      <img class="img-logo-adminkit" src="../img/logo.png" alt="" />
      </a>
   </div>
   <ul id="shortcuts-var3">
      <li title="" title-right="true">
         <a href="https://netbasejsc.com/" title="About Cmsmart">
            <div class="sprite"><img src="../img/Seo_Tool.png" /></div>
         </a>
      </li>
      <li title="" title-right="true">
         <a href="#" title="Project Support">
            <div class="sprite"><img src="../img/Website_Reviewer.png" /></div>
         </a>
      </li>
      <li title="" title-right="true">
         <a href="#" title="Printshop Solutions">
            <div class="sprite"><img src="../img/Pingdom.png" /></div>
         </a>
      </li>
      <li title="" title-right="true">
         <a href="#" title="Booking Solution">
            <div class="sprite"><img src="../img/Contract_Sign.png" /></div>
         </a>
      </li>
      <li title="" title-right="true">
         <a href="#" title="Marketplace Solutions">
            <div class="sprite"><img src="../img/PDF_Tool.png" /></div>
         </a>
      </li>
      <li title="" title-right="true">
         <a href="#" title="E-commerce Services">
            <div class="sprite"><img src="../img/File_Sharing.png" /></div>
         </a>
      </li>
      <li title="" title-right="true">
         <a href="#" title="Cmsmart Community">
            <div class="sprite"><img src="../img/Help_Desk.png" /></div>
         </a>
      </li>
      <li title="" title-right="true">
         <a href="https://netbasejsc.com/" title="About Cmsmart">
            <div class="sprite"><img src="../img/Micro.png" /></div>
         </a>
      </li>
      <li title="" title-right="true">
         <a href="https://netbasejsc.com/" title="About Cmsmart">
            <div class="sprite"><img src="../img/Domain.png" /></div>
         </a>
      </li>
      <li title="" title-right="true">
         <a href="https://netbasejsc.com/" title="About Cmsmart">
            <div class="sprite"><img src="../img/admin_tool.png" /></div>
         </a>
      </li>
   </ul>
</section>
<!-- Bootstrap -->
<script src="<?php themeLink('js/bootstrap.min.js'); ?>" type="text/javascript"></script>

<!-- Sweet Alert -->
<script type='text/javascript' src='<?php themeLink('js/sweetalert.min.js'); ?>'></script>

<!-- App JS -->
<script src="<?php themeLink('js/app.js'); ?>" type="text/javascript"></script>

<!-- Master JS -->
<script src="<?php createLink('rainbow/master-js'); ?>" type="text/javascript"></script>

<!-- Sign in -->
<!-- enable reg -->
<?php if($enable_reg):?>
<div class="modal fade loginme" id="signin" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><?php trans('Sign In',$lang['RF57']); ?></h4>
			</div>
            <form method="POST" action="<?php createLink('account/login'); ?>" class="loginme-form">
			<div class="modal-body">
				<div class="alert alert-warning">
					<button type="button" class="close dismiss">&times;</button><span></span>
				</div>
                <?php if($enable_oauth){ ?>
				<div class="form-group connect-with">
					<div class="info"><?php trans('Sign in using social network',$lang['RF58']); ?></div>
					<a href="<?php createLink('facebook/login'); ?>" class="connect facebook" title="<?php trans('Sign in using Facebook',$lang['RF59']); ?>"><?php trans('Facebook',$lang['RF62']); ?></a>
		        	<a href="<?php createLink('google/login'); ?>" class="connect google" title="<?php trans('Sign in using Google',$lang['RF60']); ?>"><?php trans('Google',$lang['RF63']); ?></a>  	
		        	<a href="<?php createLink('twitter/login'); ?>" class="connect twitter" title="<?php trans('Sign in using Twitter',$lang['RF61']); ?>"><?php trans('Twitter',$lang['RF64']); ?></a>		        
			    </div>
                <?php } ?>
   				<div class="info"><?php trans('Sign in with your username',$lang['RF65']); ?></div>
				<div class="form-group">
					<label><?php trans('Username',$lang['RF66']); ?> <br />
						<input type="text" name="username" class="form-input width96" />
					</label>
				</div>	
				<div class="form-group">
					<label><?php trans('Password',$lang['RF67']); ?> <br />
						<input type="password" name="password" class="form-input width96" />
					</label>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary pull-left"><?php trans('Sign In',$lang['RF57']); ?></button>
				<div class="pull-right align-right">
				    <a href="<?php createLink('account/forget'); ?>"><?php trans('Forgot Password',$lang['RF68']); ?></a><br />
					<a href="<?php createLink('account/resend'); ?>"><?php trans('Resend Activation Email',$lang['RF69']); ?></a>
				</div>
			</div>
			 <input type="hidden" name="signin" value="<?php echo md5($date.$ip); ?>" />
             <input type="hidden" name="quick" value="<?php echo md5(randomPassword()); ?>" />
			</form> 
		</div>
	</div>
</div>  

<!-- Sign up -->
<div class="modal fade loginme" id="signup" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><?php trans('Sign Up',$lang['RF70']); ?></h4>
			</div>
			<form action="<?php createLink('account/register'); ?>" method="POST" class="loginme-form">
			<div class="modal-body">
				<div class="alert alert-warning">
					<button type="button" class="close dismiss">&times;</button><span></span>
				</div>
                <?php if($enable_oauth){ ?>
				<div class="form-group connect-with">
					<div class="info"><?php trans('Sign in using social network',$lang['RF58']); ?></div>
					<a href="<?php createLink('facebook/login'); ?>" class="connect facebook" title="<?php trans('Sign in using Facebook',$lang['RF59']); ?>"><?php trans('Facebook',$lang['RF62']); ?></a>
		        	<a href="<?php createLink('google/login'); ?>" class="connect google" title="<?php trans('Sign in using Google',$lang['RF60']); ?>"><?php trans('Google',$lang['RF63']); ?></a>  	
		        	<a href="<?php createLink('twitter/login'); ?>" class="connect twitter" title="<?php trans('Sign in using Twitter',$lang['RF61']); ?>"><?php trans('Twitter',$lang['RF64']); ?></a>		        
			    </div>
                <?php } ?>
   				<div class="info"><?php trans('Sign up with your email address',$lang['RF71']); ?></div>
				<div class="form-group">
					<label><?php trans('Username',$lang['RF66']); ?> <br />
						<input type="text" name="username" class="form-input width96" />
					</label>
				</div>	
				<div class="form-group">
					<label><?php trans('Email',$lang['RF73']); ?> <br />
						<input type="text" name="email" class="form-input width96" />
					</label>
				</div>
				<div class="form-group">
					<label><?php trans('Full Name',$lang['RF72']); ?> <br />
						<input type="text" name="full" class="form-input width96" />
					</label>
				</div>
				<div class="form-group">
					<label><?php trans('Password',$lang['RF67']); ?> <br />
						<input type="password" name="password" class="form-input width96" />
					</label>
				</div>
				</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary"><?php trans('Sign Up',$lang['RF70']); ?></button>	
			</div>
			<input type="hidden" name="signup" value="<?php echo md5($date.$ip); ?>" />
            <input type="hidden" name="quick" value="<?php echo md5(randomPassword()); ?>" />
			</form>
		</div>
	</div>
</div>

<!-- XD Box -->
<div class="modal fade loginme" id="xdBox" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button id="xdClose" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="xdTitle"></h4>
			</div>
			<div class="modal-body" id="xdContent">

            </div>
		</div>
	</div>
</div>
<?php endif;?>
<?php if(isset($footerAddArr)){ 
    foreach($footerAddArr as $footerCodes)
        echo $footerCodes;
} ?>
		<!-- styles JS -->
		<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-R78QZDBK6F"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-R78QZDBK6F');
</script>
		<script type="text/javascript" src="../js/styles-detail.js"></script>
		<!-- Bootrap Js -->
		<script type="text/javascript" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
		<script type="text/javascript">
		$.holdReady( true );
			$.getScript( "../js/search.js", function() {
			$.holdReady( false );
		});
		$('.page-detail').on('click', '.openMore', function(event) {
			window.location.href = "https://adminkit.net" + $(this).attr('href');
		});
		</script>
		<!-- Pixel Code for https://leadee.ai/leadflows/ -->
			<script async src="https://leadee.ai/leadflows/pixel/b02js1k3jstkp458lx3jgx0w1oqreswf"></script>
		<!-- END Pixel Code -->
</body>
</html>