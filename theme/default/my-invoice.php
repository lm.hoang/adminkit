<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
/*
 * @author Balaji
 * @name: A to Z SEO Tools - PHP Script
 * @Theme: Default Style
 * @copyright � 2018 ProThemes.Biz
 *
 */
?>
<link href="<?php themeLink('premium/css/premium.css'); ?>" rel="stylesheet" type="text/css" />

<!-- DATA TABLES -->
<link href="<?php themeLink('css/datatables/dataTables.bootstrap.css'); ?>" rel="stylesheet" type="text/css" />
    
<div class="container main-container">
    <div class="row">
  	    <?php
        if($themeOptions['general']['sidebar'] == 'left')
            require_once(THEME_DIR."sidebar.php");
        ?>
        <div class="col-md-8 main-index">                             
        <h2 class="premiumTitle"><?php echo $lang['AD739']; ?></h2>
           <br />
          <table id="invoiceTable" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th><?php trans('Invoice #', $lang['AD807']); ?></th>
                <th><?php trans('Invoice Date', $lang['AD803']); ?></th>
                <th><?php trans('Due Date', $lang['AD804']); ?></th>
                <th><?php trans('Detail', $lang['AD805']); ?></th>
                <th><?php trans('Total', $lang['AD806']); ?></th>
                <th><?php trans('Status', $lang['149']); ?></th>
              </tr>
            </thead>
            <tbody>
            <?php foreach($invoices as $invoice){                               
                echo '<tr style="cursor: pointer;" onclick="openInvoice('.$invoice[6].');">
                <td>'.$invoice[0].'</td>
                <td>'.$invoice[1].'</td>
                <td>'.$invoice[2].'</td>
                <td>'.$invoice[3].'</td>
                <td>'.$invoice[4].'</td>
                <td>'.$invoice[5].'</td>
              </tr>';
            }
            ?>
            </tbody>
          </table>
            
        <br />

        </div>
        <?php
        if($themeOptions['general']['sidebar'] == 'right')
            require_once(THEME_DIR."sidebar.php");
        ?>
    </div>
</div>
<br />
<!-- DATA TABES SCRIPT -->
<script src="<?php themeLink('js/plugins/datatables/jquery.dataTables.js'); ?>" type="text/javascript"></script>
<script src="<?php themeLink('js/plugins/datatables/dataTables.bootstrap.js'); ?>" type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('#invoiceTable').dataTable({
        "aaSorting": [[ 0, "desc" ]]
    });
});

function openInvoice(id){
    //window.location.replace('/invoice/'+id);
    window.open('<?php createLink('invoice'); ?>/'+id+'/view', '_blank');
}
</script>