<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
/*
 * @author Balaji
 * @name: Rainbow PHP Framework
 * @Theme: Default Style
 * @copyright � 2018 ProThemes.Biz
 *
 */
?>

<div class="col-md-4" id="rightCol">   
	
    <div class="well">  
        <?php if(isSelected($themeOptions['general']['sSearch'])){ ?>   
        <div id="sidebarSc" class="col-md-12">
            <div class="form-group">
                <div class="input-group green shadow">
                    <div class="input-group-addon"><i class="fa fa-search"></i></div>
                        <input type="text" class="form-control" placeholder="<?php echo $lang['AS39']; ?>" autocomplete="off" id="sidebarsearch" />
                    </div>
               	</div>
            <div class="search-results" id="sidebar-results"></div>
        </div>
        <?php } ?>
        
        <?php if(!isset($_SESSION[N_APP.'premiumClient'])) { ?>   
        <div class="sideXd">
        <?php echo $ads_250x300; ?>
        </div>
                
        <div class="sideXd">
        <?php echo $ads_250x125; ?>
        </div>
        <?php } ?>
    </div>
    
    <?php if(isset($_SESSION[N_APP.'premiumClient'])) { ?>    
     
    <div class="hisData">
        <div class="titleBox"><?php trans('Lastest Analyzed Websites', $lang['AD888']); ?></div>
        <ul class="hisUl">
            <?php foreach(getUserRecentSites($con) as $domain){ ?>
                <li><a href="<?php createLink('domain/'.$domain); ?>"><img class="favicon" alt="img" src="https://s2.googleusercontent.com/s2/favicons?domain_url=<?php echo $domain; ?>" /><?php echo ucfirst($domain); ?></a></li>
            <?php } ?>
        </ul>
    </div>
    
    <?php } ?>
</div>