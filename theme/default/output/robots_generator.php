<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name A to Z SEO Tools v2 - PHP Script
 * @copyright © 2017 ProThemes.Biz
 *
 */
?>
<script>
var msg1 = "<?php makeJavascriptStr($lang['163'],true); ?>";
var msg2 = "# <?php makeJavascriptStr($lang['164'],true); ?>\n";
</script>
<script src='<?php createLink('core/library/robots.js',false,true); ?>'></script>
<div class="container main-container">
<div class="row">
        <?php
        if($themeOptions['general']['sidebar'] == 'left')
            require_once(THEME_DIR."sidebar.php");
        ?>
      	<div class="col-md-12 main-index">
        
        <div class="xd_top_box">
         <?php echo $ads_720x90; ?>
        </div>
            <div class="img-mainDetail">
                <img src="<?php echo "../theme/default/".$data['icon_name']; ?>" alt="<?php echo $data['tool_name']; ?>">
                <h2 id="title"><?php echo $data['tool_name']; ?></h2>
            </div>
            <div class="box_art_Rew">
                <?php if ($pointOut != 'output') { ?>
                    <form class="form-robots">
                        <div class="robot-top">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-6">
                                    <select size="1" name="allow" id="allow" class="form-control" >
                                        <option selected=""><?php echo $lang['150']; ?></option>
                                        <option value=" "><?php echo $lang['151']; ?></option>
                                        <option value=" /"><?php echo $lang['152']; ?></option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <select size="1" name="delay" id="delay" class="form-control">
                                        <option value="" selected=""><?php echo $lang['154']; ?></option>
                                        <option value="5">5 Seconds</option>
                                        <option value="10">10 Seconds</option>
                                        <option value="20">20 Seconds</option>
                                        <option value="60">60 seconds</option>
                                        <option value="120">120 Seconds</option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <input type="text" value="<?php echo $lang['155']; ?> <?php echo $lang['156']; ?>" placeholder="http://www.example.com/sitemap.xml" name="sitemap" class="form-control" />
                                </div>
                            </div>
                        </div>
                        <div class="robot-center">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box-text">
                                        <h2 class="title-robots">
                                            <?php echo $lang['157']; ?>
                                        </h2>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <select size="1" name="google" id="google" class="form-control">
                                        <option selected="">Google</option>
                                        <option value="" ><?php echo $lang['158']; ?></option>
                                        <option value=" "><?php echo $lang['151']; ?></option>
                                        <option value=" /"><?php echo $lang['152']; ?></option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <select size="1" name="gimage" id="gimage" class="form-control">
                                        <option selected="">Google Image</option>
                                        <option value="" ><?php echo $lang['158']; ?></option>
                                        <option value=" "><?php echo $lang['151']; ?></option>
                                        <option value=" /"><?php echo $lang['152']; ?></option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <select size="1" name="gmobile" id="gmobile" class="form-control">
                                        <option selected="">Google Mobible</option>
                                        <option value=""><?php echo $lang['158']; ?></option>
                                        <option value=" "><?php echo $lang['151']; ?></option>
                                        <option value=" /"><?php echo $lang['152']; ?></option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <select size="1" name="msn" id="msn" class="form-control">
                                        <option >MSN Search</option>
                                        <option value="" selected=""><?php echo $lang['158']; ?></option>
                                        <option value=" "><?php echo $lang['151']; ?></option>
                                        <option value=" /"><?php echo $lang['152']; ?></option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <select size="1" name="yahoo" id="yahoo" class="form-control">
                                        <option selected="">Yahoo</option>
                                        <option value="" ><?php echo $lang['158']; ?></option>
                                        <option value=" "><?php echo $lang['151']; ?></option>
                                        <option value=" /"><?php echo $lang['152']; ?></option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <select size="1" name="ymm" id="ymm" class="form-control">
                                        <option selected="">Yahoo MM</option>
                                        <option value="" ><?php echo $lang['158']; ?></option>
                                        <option value=" "><?php echo $lang['151']; ?></option>
                                        <option value=" /"><?php echo $lang['152']; ?></option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <select size="1" name="blogs" id="blogs" class="form-control">
                                        <option selected="">Yahoo Blogs</option>
                                        <option value="" ><?php echo $lang['158']; ?></option>
                                        <option value=" "><?php echo $lang['151']; ?></option>
                                        <option value=" /"><?php echo $lang['152']; ?></option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <select size="1" name="teoma" id="teoma" class="form-control">
                                        <option selected="">Ask/Teoma</option>
                                        <option value="" ><?php echo $lang['158']; ?></option>
                                        <option value=" "><?php echo $lang['151']; ?></option>
                                        <option value=" /"><?php echo $lang['152']; ?></option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <select size="1" name="gigablast" id="gigablast" class="form-control">
                                        <option selected="">GigaBlast</option>
                                        <option value="" ><?php echo $lang['158']; ?></option>
                                        <option value=" "><?php echo $lang['151']; ?></option>
                                        <option value=" /"><?php echo $lang['152']; ?></option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <select size="1" name="dmoz" id="dmoz" class="form-control">
                                        <option selected="">DMOZ Checker</option>
                                        <option value="" ><?php echo $lang['158']; ?></option>
                                        <option value=" "><?php echo $lang['151']; ?></option>
                                        <option value=" /"><?php echo $lang['152']; ?></option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <select size="1" name="nutch" id="nutch" class="form-control">
                                        <option selected="">Nutch</option>
                                        <option value="" ><?php echo $lang['158']; ?></option>
                                        <option value=" "><?php echo $lang['151']; ?></option>
                                        <option value=" /"><?php echo $lang['152']; ?></option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <select size="1" name="alexa" id="alexa" class="form-control">
                                        <option selected="">Alexa/Wayback</option>
                                        <option value="" ><?php echo $lang['158']; ?></option>
                                        <option value=" "><?php echo $lang['151']; ?></option>
                                        <option value=" /"><?php echo $lang['152']; ?></option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <select size="1" name="baidu" id="baidu" class="form-control">
                                        <option selected="">Baidu</option>
                                        <option value="" ><?php echo $lang['158']; ?></option>
                                        <option value=" "><?php echo $lang['151']; ?></option>
                                        <option value=" /"><?php echo $lang['152']; ?></option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <select size="1" name="naver" id="naver" class="form-control">
                                        <option selected="">Naver</option>
                                        <option value="" ><?php echo $lang['158']; ?></option>
                                        <option value=" "><?php echo $lang['151']; ?></option>
                                        <option value=" /"><?php echo $lang['152']; ?></option>
                                    </select>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <select size="1" name="psbot" id="psbot" class="form-control">
                                        <option selected="">MSN PicSearch</option>
                                        <option value="" ><?php echo $lang['158']; ?></option>
                                        <option value=" "><?php echo $lang['151']; ?></option>
                                        <option value=" /"><?php echo $lang['152']; ?></option>
                                    </select>
                                </div>
                            </div>
                            </div>
                        <div class="robot-bottom">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box-text">
                                        <h2 class="title-robots-bottom">
                                            <?php echo $lang['159']; ?>
                                        </h2>
                                        <p class="info-robots-bottom">
                                            <?php echo $lang['160']; ?>
                                        </p>

                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <input type="text" value="/cgi-bin/" size="70" name="dir1" class="form-control" />
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <input type="text" size="70" name="dir2" class="form-control" />
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <input type="text" size="70" name="dir3" class="form-control" />
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <input type="text" size="70" name="dir4" class="form-control" />
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <input type="text" size="70" name="dir5" class="form-control" />
                                </div>
                                <div class="col-12 col-sm-12 col-md-6">
                                    <input type="text" size="70" name="dir6" class="form-control" />
                                </div>
                                <div class="col-md-12">
                                    <div class="box-btn-robots">
                                        <input type="button" class="btn btn-success" onclick="genRobots(this.form,msg1,msg2)" value="<?php echo $lang['161']; ?>" />
                                        <input type="button" class="btn btn-danger" onclick="genRobots(this.form,msg1,msg2,true)" value="Create and Save as Robots.txt" />
                                        <input type="reset" class="btn btn-primary" value="Clear" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="robot-apsolut">
                            <div class="row">
                                <div class="col-md-12">
                                    <p>
                                        <textarea class="form-control" readonly="" style="height: 270px;" rows="3" id="robolist" name="robolist"></textarea>
                                    </p>
                                    <p class="info-robots-bottom"><?php echo $lang['162']; ?></p>
                                </div>
                            </div>
                        </div>
                    </form>
                    <?php
                }
                ?>
            </div>
            <div class="info-page">
                <div class="box-text">
                    <h2 id="sec1" class="about_tool"><?php echo $lang['11'].' '.$data['tool_name']; ?></h2>
                    <p>
                        <?php echo $data['about_tool']; ?>
                    </p>
                </div>
            </div>
        </div>
        <?php
        if($themeOptions['general']['sidebar'] == 'right')
//            require_once(THEME_DIR."sidebar.php");
        ?>      		
    </div>
</div> <br />