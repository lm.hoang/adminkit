<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
/*
* @author Balaji
* @name A to Z SEO Tools - PHP Script
* @copyright 2018 ProThemes.Biz
*
*/
?>
<style>
.small-box {
border-radius: 11px;
color: #fff;
cursor: pointer;
}
.small-box:hover > .social-icon i {
transition: all 0.35s ease 0s;
transform: rotate(360deg);
}
.small-box h3 {
font-size: 28px;
}
.small-box .social-icon {
font-size: 60px;
bottom: 3px;
color: rgba(0, 0, 0, 0.25);
position: absolute;
right: 5px;
top: auto;
z-index: 0;
}
.small-box > .inner {
border-radius: 11px;
color: #fff;
}
.bg-facebook {
background-color: #3B5998 !important;
}
.bg-twitter {
background-color: #00ABF0 !important;
}
.bg-google {
background-color: #DB514F !important;
}
.bg-pinterest {
background-color: #DA232A !important;
}
.bg-linkedin {
background-color: #1C86BC !important;
}
.bg-stumbleupon {
background-color: #EA4B24 !important;
}
</style>
<div class="container main-container">
	<div class="row">
		<?php
		if($themeOptions['general']['sidebar'] == 'left')
		require_once(THEME_DIR."sidebar.php");
		?>
		<div class="col-md-12 main-index">
			<div class="img-mainDetail">
				<img src="<?php echo "../theme/default/".$data['icon_name']; ?>" alt="<?php echo $data['tool_name']; ?>">
				<h2 id="title"><?php echo $data['tool_name']; ?></h2>
			</div>
			<div class="box_art_Rew">
				<?php if ($pointOut != 'output') { ?>
				<form method="POST" action="<?php echo $toolOutputURL;?>" onsubmit="return fixURL();">
					<input type="text" name="url" id="url" value="" class="form-control" placeholder="<?php echo $lang['23']; ?>"/>
					<br />
					<?php if ($toolCap) echo $captchaCode; ?>
					<div class="text-center">
						<input class="btn btn-info" type="submit" value="<?php echo $lang['8']; ?>" name="submit"/>
					</div>
				</form>
				
				<?php
				} else {
				//Output Block
				if(isset($error)) {
				
				echo '<br/><br/><div class="alert alert-error">
					<strong>Alert!</strong> '.$error.'
				</div><br/><br/>
				<div class="text-center"><a class="btn btn-info" href="'.$toolURL.'">'.$lang['12'].'</a>
			</div><br/>';
			
			} else {
			?>
			<br /><hr />
			<div class="text-center">
				<p style="font-size: 17px;"><?php trans('Social Stats for', $lang['AD91']); ?> <?php echo $myHost; ?></p>
			</div>
			<hr />  <br />
			
			
			<!-- Small boxes (Stat box) -->
			<div class="row">
				<div class="col-lg-6 col-xs-12">
					<!-- small box -->
					<div class="small-box bg-facebook">
						<div class="inner">
							<h3>
							<?php echo number_format($facebook_like); ?>
							</h3>
							<p>
								<?php trans('Facebook Likes', $lang['AD92']); ?>
							</p>
						</div>
						<div class="social-icon">
							<i class="fa fa-facebook"></i>
						</div>
						
					</div>
					</div><!-- ./col -->
					<div class="col-lg-6 col-xs-12">
						<!-- small box -->
						<div class="small-box bg-facebook">
							<div class="inner">
								<h3>
								<?php echo number_format($facebook_comment); ?>                                    </h3>
								<p>
									<?php trans('Facebook Followers', $lang['AD93']); ?>
								</p>
							</div>
							<div class="social-icon">
								<i class="fa fa-facebook"></i>
							</div>
						</div>
						</div><!-- ./col -->
						</div><!-- /.row -->
						<!-- Small boxes (Stat box) -->
						<div class="row">
							<div class="col-lg-6 col-xs-12">
								<!-- small box -->
								<div class="small-box bg-google">
									<div class="inner">
										<h3>
										<?php echo number_format($gplus_count); ?>                                    </h3>
										<p>
											<?php trans('PlusOne', $lang['AD95']); ?>
										</p>
									</div>
									<div class="social-icon">
										<i class="fa fa-google-plus"></i>
									</div>
								</div>
								</div><!-- ./col -->
								<div class="col-lg-6 col-xs-12">
									<!-- small box -->
									<div class="small-box bg-twitter">
										<div class="inner">
											<h3>
											<?php echo number_format($tweets_count); ?>                                    </h3>
											<p>
												<?php trans('Tweets', $lang['AD98']); ?>
											</p>
										</div>
										<div class="social-icon">
											<i class="fa fa-twitter"></i>
										</div>
									</div>
									</div><!-- ./col -->
									</div><!-- /.row -->
									<hr />
									<div class="text-center">
										<br /> &nbsp; <br />
										<a class="btn btn-info" href="<?php echo $toolURL; ?>"><?php echo $lang['27']; ?></a>
										<br />
									</div>
									<?php } } ?>
								</div>
								
								<br />
								<div class="xd_top_box">
									<?php echo $ads_720x90; ?>
								</div>
								<div class="info-page">
									<div class="box-text">
										<h2 id="sec1" class="about_tool"><?php echo $lang['11'].' '.$data['tool_name']; ?></h2>
										<p>
											<?php echo $data['about_tool']; ?>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div> <br />