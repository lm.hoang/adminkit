<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
/*
* @author Balaji
* @name A to Z SEO Tools - PHP Script
* @copyright © 2018 ProThemes.Biz
*
*/
?>
<style>
table {
table-layout: fixed; width: 100%;
}
td {
word-wrap: break-word;
}
</style>
<div class="container main-container">
  <div class="row">
    <?php
    if($themeOptions['general']['sidebar'] == 'left')
    require_once(THEME_DIR."sidebar.php");
    ?>
    <div class="col-md-12 main-index">
      <div class="img-mainDetail">
        <img src="<?php echo "../theme/default/".$data['icon_name']; ?>" alt="<?php echo $data['tool_name']; ?>">
        <h2 id="title"><?php echo $data['tool_name']; ?></h2>
      </div>
      <div class="box_art_Rew">
        <?php if ($pointOut != 'output') { ?>
        <form method="POST" action="<?php echo $toolOutputURL;?>" onsubmit="return fixURL();">
          <input type="text" name="url" id="url" value="" class="form-control" placeholder = "<?php echo $lang['23']; ?>"/>
          <br />
          <?php if ($toolCap) echo $captchaCode; ?>
          <div class="text-center">
            <input class="btn btn-info" type="submit" value="<?php echo $lang['8']; ?>" name="submit"/>
          </div>
        </form>
        
        <?php
        } else {
        //Output Block
        if(isset($error)) {
        
        echo '<br/><br/><div class="alert alert-error">
          <strong>Alert!</strong> '.$error.'
        </div><br/><br/>
        <div class="text-center"><a class="btn btn-info" href="'.$toolURL.'">'.$lang['12'].'</a>
      </div><br/>';
      
      } else {
      ?>
      <br />
      <div class="widget-body">
        <div class="widget-main">
          <br />
          <table class="table table-hover table-bordered table-striped">
            <thead>
              <tr>
                <th class="heading">
                  <h4 class="text-center"><?php trans('HTTP Headers', $lang['AD78']); ?></h4>
                </th>
              </tr>
            </thead>
            <tbody>
              <?php
              $myLines = preg_split("/\r\n|\n|\r/", $outData);
              foreach($myLines as $line){
              if(!empty($line))
              echo '<tr><td>'.$line.'</td></tr>';
              }
              ?>
            </tbody>
          </table>
          
          </div><!-- /.widget-main -->
          </div><!-- /.widget-body -->
          
          <div class="text-center">
            <br /> &nbsp; <br />
            <a class="btn btn-info" href="<?php echo $toolURL; ?>"><?php echo $lang['27']; ?></a>
            <br />
          </div>
          <?php } } ?>
        </div>
        <div class="xd_top_box">
          <?php echo $ads_720x90; ?>
        </div>
        <div class="info-page">
          <div class="box-text">
            <h2 id="sec1" class="about_tool"><?php echo $lang['11'].' '.$data['tool_name']; ?></h2>
            <p>
              <?php echo $data['about_tool']; ?>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div> <br />