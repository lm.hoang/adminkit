<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name A to Z SEO Tools - PHP Script
 * @copyright � 2018 ProThemes.Biz
 *
 */

//Fix
$server_path = 'h';
?>

<!-- Bootstrap Color Picker CSS -->
<link href="<?php themeLink('css/colorpicker/bootstrap-colorpicker.min.css'); ?>" rel="stylesheet"/>

<style>
.loadBox{
    display: none;
}
.legendDiv li {
    border-radius: 5px;
    cursor: default;
    display: block;
    font-size: 14px;
    margin-bottom: 4px;
    padding: 2px 8px 2px 28px;
    position: relative;
    transition: background-color 200ms ease-in-out 0s;
}
.legendDiv li span {
    border-radius: 5px;
    display: block;
    height: 100%;
    left: 0;
    position: absolute;
    top: 0;
    width: 20px;
}
.tableZ{
    margin-bottom: 10px !important;
    margin-top: 10px !important;
}
</style>
  <div class="container main-container">
	<div class="row">
      	    <?php
            if($themeOptions['general']['sidebar'] == 'left')
                require_once(THEME_DIR."sidebar.php");
            ?>
          	<div class="col-md-12 main-index">
            
            <div class="xd_top_box">
             <?php echo $ads_720x90; ?>
            </div>
            <div class="img-mainDetail">
                <img src="<?php echo "../theme/default/".$data['icon_name']; ?>" alt="<?php echo $data['tool_name']; ?>">
                <h2 id="title"><?php echo $data['tool_name']; ?></h2>
            </div>
		<div class="box_art_Rew">
			                <?php if ($pointOut == '') { ?>
                <div id="gen_flag"></div>
                <br />

                <?php
                if ($toolCap){
                    echo '<form method="POST" onsubmit="return captchaCodeCheckMsg();" action="'.$toolURL.'/generate">';
                }else {
                    echo '<form method="POST" action="'.$toolURL.'/generate">';
                }
                ?>

    <div id="gen_flag_box">
    <div class="row">
        <div class="col-md-6">
        <div class="form-group">
        <!----<label><?php trans('Maximum Flags to Show', $lang['AD304']); ?></label>--->
        <select class="form-control" name="maximumFlag" id="maximumFlag">
		<option selected=''>Maximum Flags to Show</option>
        <?php 
            for ($loop=1; $loop<=250; $loop++)
            {
                $limit_results = isset($limit_results) ? $limit_results : '20';
				
                if ($loop == $limit_results)
                {
                    echo "<option>$loop</option>";
                }
                else
                {
                      echo "<option>$loop</option>";
                }
            } 
        ?>
        </select>
        </div>
        <div class="form-group">
        <!----<label><?php trans('Columns of Flags', $lang['AD305']); ?></label>--->
        <select class="form-control" name="columnsLimit" id="columnsLimit">
			<option selected=''>Columns of Flags</option>
          <?php 
            for ($loop=1; $loop<=8; $loop++)
            {
                $columns = isset($columns) ? $columns : '2';
                if ($loop == $columns)
                {
                    echo "<option>$loop</option>";
                }
                else
                {
                      echo "<option>$loop</option>";
                }
            } 
        ?>
        </select>
        </div>
        <div class="form-group">
        <!---<label><?php trans('Flag Size', $lang['AD306']); ?></label>-->
        <select class="form-control" name="flagSize" id="flagSize">
		<option selected=''>Flag Size</option>
        <?php
			$selSmall  = $selMedium = $selLarge = "";
			$flagSize = strtolower($flagSize);
			if($flagSize  == 'small') 
			$selSmall = "selected=''"; 
			elseif($flagSize  == 'medium') 
			$selMedium = "selected=''";     
			elseif($flagSize  == 'large') 
			$selLarge = "selected=''";  
        ?>                                             
            <option <?php echo $selSmall; ?>><?php trans('Small', $lang['AD307']); ?></option>
            <option  <?php echo $selMedium; ?>><?php trans('Medium', $lang['AD308']); ?></option>
            <option  <?php echo $selLarge; ?>><?php trans('Large', $lang['AD309']); ?></option>
        </select>
        </div>
                                                                    
        <div class="form-group">
        <!---<label for="counterName"><?php trans('Label on Top of Counter', $lang['AD310']); ?></label>--->
        <input type="text" value="<?php echo $flag_title; ?>" placeholder="<?php trans('Enter label name for counter..', $lang['AD311']); ?>" name="counterName" id="counterName" class="form-control" />
        </div>
        
    <?php 
    if($countryCode == true)
    $dIs = 'checked=""';
    else
    $dIs = '';
    ?>            
    <div class="form-group"> 
      <div class="checkbox checkbox-flag">
            <label style="font-weight: bold;">
            <input type="checkbox" <?php echo $dIs; ?> name="showLabel" id="showLabel" />  <?php trans('Show Flag Labels', $lang['AD312']); ?>
            </label>
      </div>
    </div>   
    </div>
    <div class="col-md-6 color-flag-counter">
        <div class="form-group flag-group">
                        <label><?php trans('Background Color', $lang['AD313']); ?></label>
                        <div class="input-group my-colorpicker1 colorpicker-element">                                            
                            <input type="text" value="#<?php echo $background_color; ?>" class="form-control" name="backColor" id="backColor" />
                            <div class="input-group-addon">
                               <i style="background-color: #<?php echo $background_color; ?>;"></i>
                            </div>
                        </div><!-- /.input group -->
                    </div>

        <div class="form-group flag-group">
                        <label><?php trans('Border Color', $lang['AD314']); ?></label>
                        <div class="input-group my-colorpicker2 colorpicker-element">                                            
                            <input type="text" value="#<?php echo $border_color; ?>" class="form-control"  name="borderColor" id="borderColor" />
                            <div class="input-group-addon">
                                <i style="background-color: #<?php echo $border_color; ?>;"></i>
                            </div>
                        </div><!-- /.input group -->
                    </div>

        <div class="form-group flag-group">
                        <label><?php trans('Title Text Color', $lang['AD315']); ?></label>
                        <div class="input-group my-colorpicker3 colorpicker-element">                                            
                            <input type="text" value="#<?php echo  $title_color; ?>" class="form-control" name="titleColor" id="titleColor" />
                            <div class="input-group-addon">
                                <i style="background-color: #<?php echo  $title_color; ?>;"></i>
                            </div>
                        </div><!-- /.input group -->
                    </div>

        <div class="form-group flag-group">
                        <label><?php trans('Other Text Color', $lang['AD316']); ?></label>
                        <div class="input-group my-colorpicker4 colorpicker-element">                                            
                            <input type="text" value="#<?php echo $textColor; ?>" class="form-control" name="textColor" id="textColor" />
                            <div class="input-group-addon">
                                <i style="background-color: #<?php echo $textColor; ?>;"></i>
                            </div>
                        </div><!-- /.input group -->
                    </div>
            <?php 
            if($showPage == true)
            $dIs = 'checked=""';
            else
            $dIs = '';
            ?>
            <div class="form-group"> 
            <div class="checkbox checkbox-flag">
            <label style="font-weight: bold;">
            <input type="checkbox" <?php echo $dIs; ?> name="pageView" id="pageView" />  <?php trans('Show Pageview Count', $lang['AD317']); ?>
            </label>
            </div>
             </div>    
    </div>

       <?php if ($toolCap)  echo $captchaCode; ?>    
            
        <div class ="more-flag" style="text-align: center;">
        <a class="btn btn-danger" id="preItNow"><?php trans('Preview', $lang['AD319']); ?></a>
        <input type="submit" class="btn btn-success" id="generateFlag1" value="<?php trans('Get your Flag Counter', $lang['AD318']); ?>" />
        </div>
    
   </div></div>
     
        <hr class="small" id="prev_flag" />
        <div class="row text-left">
            <div class="col-lg-12">
                <h3 class="title-fg-counter"><?php trans('Preview your Flag Counter', $lang['AD320']); ?></h3>
                <p class="info-fg-counter"><?php trans('Not looking good? Customize it again!', $lang['AD321']); ?></p>
                    <div class="well" id="prev_flag_box">
                    <br />
                    <div class="loadBox"><img src="<?php themeLink('img/load.gif'); ?>" /><br /></div>
                    <div id="prv_img_box">
                    <img src="<?php echo $server_path.substr($toolURL, 1); ?>/load/if/" />
                    </div>
                    <div style="margin-top: 15px;">
                        <a class="btn btn-warning" id="cusItNow"><?php trans('Customize again!', $lang['AD322']); ?></a>
                        <input type="submit" class="btn btn-success" id="generateFlag2" value="<?php trans('Get your Flag Counter', $lang['AD318']); ?>" />
                    </div>
                    </div>
            </div>
        </div>
        </form>
                          
               <?php 
               } elseif($pointOut == 'generate') { 
               //Output Block
               if(isset($error)) {
                
                echo '<br/><br/><div class="alert alert-error">
                <strong>Alert!</strong> '.$error.'
                </div><br/><br/>
                <div class="text-center"><a class="btn btn-info" href="'.$toolURL.'">'.$lang['12'].'</a>
                </div><br/>';
                
               } else {
               ?>
            <?php 
            if (isset($error)) {
            echo '<div class="alert alert-danger alert-dismissable">        
                                                    <b>Alert!</b> '.$msg.'
                                                </div>';
                                                
            } else {
                echo '<div class="alert alert-success alert-dismissable">
                                                    <b>Alert!</b> '.$msg.'
                                                </div>';                       
            ?>
           <br />   

            <h3 class="page-header text-center"><?php trans('Your Widget Code', $lang['AD323']); ?></h3>
           <p>
            <?php trans('Flag Counter are just images, so they will work on any website. The first is an HTML code for your websites / blog and the second is a code for posting to forums. Just copy and paste this to your website\'s source code wherever you\'d like your Flag Counter to appear.  ', $lang['AD324']); ?>
           </p>
              <br />          
           <h4><?php trans('HTML (website / blogs)', $lang['AD325']); ?>:</h4>
           <input type="text" value="<?php echo htmlspecialchars($html_code); ?>" name="html_code" class="form-control" />

           <h4><?php trans('BBCode (message boards &amp; forums)', $lang['AD326']); ?>:</h4>
           <input type="text" value="<?php echo $bb_code; ?>" name="bb_code" class="form-control" />
           
           <h4><?php trans('Direct Link', $lang['AD327']); ?>:<small> (Image) </small></h4>
           <input type="text" value="<?php echo $image_link; ?>" name="image_link" class="form-control" />
           <br />
           <h3 class="page-header"><?php trans('Preview your Flag Counter', $lang['AD328']); ?></h3>
           <div class="text-center">
           <small><?php trans('(Below preview image will be resized for best fit!)', $lang['AD329']); ?></small>
           <br />
           </div>
           <div class="text-center">
           <img style="max-width: 100%;" src="<?php echo $image_link; ?>" alt="<?php trans('Flag Counter', $lang['AD330']); ?>" title="<?php trans('Flag Counter', $lang['AD330']); ?>"/>
           </div>
           <br />
           <p><?php trans('Since you are the first visitor, you have added the first flag. Best of luck in collecting many more flags!', $lang['AD331']); ?> </p>
           <br />
           <div class="text-center">
            <?php
            //Ads - 468x70
            echo $ads_3;
            ?>
            </div>
           <h3 class="page-header"><?php trans('Other Info', $lang['AD332']); ?></h3>

           <h4><?php trans('Statics Link', $lang['AD333']); ?>:</h4>
           <input type="text" value="<?php echo htmlspecialchars($statics_link); ?>" name="statics_link" class="form-control" />
           
           <h4><?php trans('Customize your Flag Counter again', $lang['AD334']); ?>:<small> <?php trans('(Visit the below link!)', $lang['AD335']); ?></small></h4>
           <input type="text" value="<?php echo htmlspecialchars($re_generate_link); ?>" name="re_generate_link" class="form-control" />
           
           <h4><?php trans('Password', $lang['AD336']); ?>:</h4>
           <input type="text" value="<?php echo $passWord; ?>" name="password" class="form-control" />
           <br /><strong>
           <?php trans('Note: Future customization of your flag counter need password. So store the password on safe place!', $lang['AD337']); ?></strong>
           <br /> <hr /> <br />
           <div class="text-center">
           <a href="<?php echo $toolURL; ?>" id="createNew" class="btn btn-danger" title="<?php trans('Create a New Flag Counter', $lang['AD338']); ?>"><?php trans('Create a New Flag Counter', $lang['AD338']); ?></a>
           </div>
           <br />
           <style>
           #mailSuc, #mailFail {
            display:none;
           }
           </style>
           <script>
           jQuery(document).ready(function(){
               jQuery("#mailItNow").click(function()
                {
                var userMailID = $('input[name=mailId]').val();
                if (userMailID==null || userMailID=="") {
                alert("Enter your mail ID!");
                return false;
                }
                jQuery("#mailItNow").fadeOut(); 
                jQuery(".loadBox").css({"display":"block"});
   	            jQuery(".loadBox").show();
   	            jQuery(".loadBox").fadeIn();   
                jQuery("#mailIdBox").fadeOut();    
   	            jQuery("#mailIdBox").css({"display":"none"});
                jQuery("#mailIdBox").hide();    
                var flagPassword =jQuery.trim('<?php echo $passWord; ?>');  
                var flagCounterId = jQuery.trim('<?php echo $last_id; ?>');  
                jQuery.post('../php/ajax_mail.php',{flag_Password:flagPassword, flag_Counter_Id:flagCounterId, user_email:userMailID},function(data){
                var outData = jQuery.trim(data);
                jQuery(".loadBox").fadeOut();    
   	            jQuery(".loadBox").css({"display":"none"});
                jQuery(".loadBox").hide();
                if (outData == '1')
                {
                jQuery("#mailSuc").css({"display":"block"});
   	            jQuery("#mailSuc").show();
   	            jQuery("#mailSuc").fadeIn();  
                }
                else
                {
                jQuery("#mailFail").css({"display":"block"});
   	            jQuery("#mailFail").show();
   	            jQuery("#mailFail").fadeIn();    
                }
                });
                });
           });
           </script>
        <?php } ?>
        
        <?php } } elseif($pointOut == 'details'){ ?>
        
        <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab_1"><?php trans('Overview', $lang['AD339']); ?></a></li>
                <li class=""><a href="<?php echo $server_path.substr($toolURL, 1); ?>/details/<?php echo $code_id; ?>/live-traffic/"><?php trans('Live Traffic', $lang['AD340']); ?></a></li>
                <li class=""><a href="<?php echo $server_path.substr($toolURL, 1); ?>/details/<?php echo $code_id; ?>/today/"><?php trans('Today', $lang['AD341']); ?></a></li>
                <li class=""><a href="<?php echo $server_path.substr($toolURL, 1); ?>/details/<?php echo $code_id; ?>/yesterday/"><?php trans('Yesterday', $lang['AD342']); ?></a></li>
                <li class=""><a href="<?php echo $server_path.substr($toolURL, 1); ?>/details/<?php echo $code_id; ?>/last-30-days/"><?php trans('Last 30 Days', $lang['AD343']); ?></a></li>
                <li class=""><a href="<?php echo $server_path.substr($toolURL, 1); ?>/details/<?php echo $code_id; ?>/all-details/"><?php trans('Details', $lang['AD344']); ?></a></li>
            </ul>
            <div class="tab-content">
                <div id="tab_1" class="tab-pane active">
                   <br />      <br /> 
                   <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <td> 
                        <strong>
                        <?php trans('Last New Visitor from', $lang['AD345']); ?> <?php echo $lastCountry; ?><hr class="tableZ"/>
                        <?php trans('Date', $lang['AD346']); ?>:  <?php echo $lastDate; ?> <hr class="tableZ"/>
                        <?php trans('Browser', $lang['AD347']); ?>: <?php echo $lastBrowser; ?> <hr class="tableZ"/>
                        <?php trans('OS', $lang['AD348']); ?>: <?php echo $lastOS; ?>
                        </strong>
                        </td>
                        <td><center><img src="<?php echo $baseURL.'/'.$default_flag_path.'128/'.strtolower($lastCountry).'.png'; ?>"/></center></td>
                    </tr>
                </tbody></table>
          
                                                                                        
              
<div class="row">
<h3 class="page-header text-center">
<?php trans('Overall', $lang['AD349']); ?>
<small><?php echo $totalCount; ?>  <?php trans('flags collected so far', $lang['AD350']); ?></small>
</h3>

<center> 
<!-- Jvectormap -->
<link rel="stylesheet" href="<?php themeLink('css/jquery-jvectormap-2.0.1.css'); ?>" type="text/css" media="screen"/>
<script src="<?php themeLink('js/jquery-jvectormap-2.0.1.min.js'); ?>"></script>
<script src="<?php themeLink('js/jquery-jvectormap-world-mill-en.js'); ?>"></script>
<div id="world-map" style="width: 600px; height: 400px"></div>
</center>     
<p>

            <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <td style="width: 70px">1.</td>
                        <td><?php trans('Total Unique Visitors', $lang['AD351']); ?></td>
                        <td><span class="badge bg-red"><?php echo number_format($totalHit); ?></span></td>
                    </tr>
                    <tr>
                        <td style="width: 70px">2.</td>
                        <td><?php trans('Total Pageviews', $lang['AD352']); ?></td>
                        <td><span class="badge bg-light-blue"><?php echo number_format($pageview); ?></span></td>
                    </tr>

                </tbody></table>
                

    
                    </p>
                    <div class="text-center">
                     <?php
                     //Ads - 468x70
                     echo $ads_3;
                     ?>
                    </div>
                    
</div>             
<!-- ChartJs -->
<script src="<?php themeLink('js/chart.min.js'); ?>"></script> 

<div class="row">
<h3 class="page-header text-center">
<?php trans('Top 5 Countries', $lang['AD353']); ?>
<small><?php trans('Visited your page', $lang['AD354']); ?></small>
</h3>
<div class="col-md-4">
<span class="legendDiv" id="legendDiv"></span>
</div>
<div class="col-md-4">
<span id="canvas-holder">
 <canvas id="chart-area" width="300" height="300"/>
</span>
</div>
</div>

         
<div class="tableX">
<table class="table table-bordered">
<thead>
<tr>
<th>#</th>
<th><?php echo $lang['18']; ?></th>
<th><?php trans('Total Visitors', $lang['AD355']); ?></th>
</tr>
</thead>
<tbody>
<?php 
$loop = 1;
foreach($dataCountry as $country=>$hits)
{
$country = country_code_to_country($country);
if ($loop==6)
{
    break;
}
$file = 'flags/default/16/'.strtolower(Trim($country)).'.png';
if (!file_exists(ROOT_DIR.$file))
$file = 'icons/unknown.png';
echo '
         <tr>
         <td style="width: 50px">'.$loop.'.</td>
         <td><img src="'.createLink($file,true,true).'" alt="'.$country.'" />  '.$country.'</td>
         <td>'.$hits.'</td>
         </tr>
';
$loop++;
}
?>
           

                </tbody></table>
</div>

<div class="row">
<h3 class="page-header text-center">
<?php trans('Top 5 Browsers ', $lang['AD356']); ?>
<small><?php trans('Used by your Visitors', $lang['AD357']); ?></small>
</h3>
<div class="col-md-4">
<span class="legendDiv" id="legendDiv2"></span>
</div>
<div class="col-md-4">
<span id="canvas-holder">
 <canvas id="chart-area2" width="300" height="300"/>
</span>
</div>
</div>
<div class="tableX">
<table class="table table-bordered">
<thead>
<tr>
<th>#</th>
<th><?php echo $lang['AD347']; ?></th>
<th><?php echo $lang['AD355']; ?></th>
</tr>
</thead>
<tbody>
<?php 
$loop = 1;
foreach($dataBrowser as $browser=>$hits)
{
if ($browser == 'Googlebot-Image')
$browser = 'Googlebot';
if ($browser == 'MSIE')
$browser = 'IE';
if ($loop==6)
{
    break;
}
$file = 'icons/'.strtolower(Trim($browser)).'.png';
if (!file_exists(ROOT_DIR.$file))
$file = 'icons/unknown.png';
echo '
         <tr>
         <td style="width: 50px">'.$loop.'.</td>
         <td><img src="'.createLink($file,true,true).'" alt="'.$browser.'" />  '.$browser.'</td>
         <td>'.$hits.'</td>
         </tr>
';
$loop++;
}
?>
           

                </tbody></table>
</div>

<div class="row">
<h3 class="page-header text-center">
<?php trans('Top 5 Platforms', $lang['AD358']); ?>
<small><?php trans('Used by your Visitors', $lang['AD359']); ?></small>
</h3>
<div class="col-md-4">
<span class="legendDiv" id="legendDiv1"></span>
</div>
<div class="col-md-4">
<span id="canvas-holder">
 <canvas id="chart-area1" width="300" height="300"/>
</span>
</div>
</div>

<div class="tableX">
<table class="table table-bordered">
<thead>
<tr>
<th>#</th>
<th><?php echo $lang['AD348']; ?></th>
<th><?php echo $lang['AD355']; ?></th>
</tr>
</thead>
<tbody>
<?php 
$loop = 1;
foreach($dataOs as $os=>$hits)
{
if ($loop==6)
{
    break;
}
$file = 'icons/'.strtolower(Trim($os)).'.png';
if (!file_exists(ROOT_DIR.$file))
$file = 'icons/unknown.png';
echo '
         <tr>
         <td style="width: 50px">'.$loop.'.</td>
         <td><img src="'.createLink($file,true,true).'" alt="'.$os.'" />  '.$os.'</td>
         <td>'.$hits.'</td>
         </tr>
';
$loop++;
}
?>
           

                </tbody></table>
</div>


            


                </div><!-- /.tab-pane -->

            </div><!-- /.tab-content -->
        </div><!-- nav-tabs-custom -->
                        
<script>
    $(function(){
       var visitorsData = {
        <?php 
        $loop = 1;
        foreach($dataCountry as $country=>$hits)
        {
            if ($totalCount == $loop)
            echo '"'.$country.'": '.$hits;
            else
            echo '"'.$country.'": '.$hits.',';
            $loop++;
        }
        ?>
    };
    //World map by jvectormap
    $('#world-map').vectorMap({
        map: 'world_mill_en',
        backgroundColor: "#fff",
        regionStyle: {
            initial: {
                fill: '#e4e4e4',
                "fill-opacity": 1,
                stroke: 'none',
                "stroke-width": 0,
                "stroke-opacity": 1
            }
        },
        series: {
            regions: [{
                    values: visitorsData,
                    scale: ["#3c8dbc", "#2D79A6"], //['#3E5E6B', '#A6BAC2'],
                    normalizeFunction: 'polynomial'
                }]
        },
        onRegionTipShow: function(e, el, code) {
            if (typeof visitorsData[code] != "undefined")
                el.html(el.html() + ': ' + visitorsData[code] + ' <?php makeJavascriptStr($lang['AD360'],true); ?>');
        }
    });
    });
        var pieOptions = {
    //Boolean - Whether we should show a stroke on each segment
    segmentShowStroke : true,

    //String - The colour of each segment stroke
    segmentStrokeColor : "#fff",

    //Number - The width of each segment stroke
    segmentStrokeWidth : 2,

    //Number - The percentage of the chart that we cut out of the middle
    percentageInnerCutout : 50, // This is 0 for Pie charts

    //Number - Amount of animation steps
    animationSteps : 100,

    //String - Animation easing effect
    animationEasing : "easeOutBounce",

    //Boolean - Whether we animate the rotation of the Doughnut
    animateRotate : true,

    //Boolean - Whether we animate scaling the Doughnut from the centre
    animateScale : false,
    
    tooltipTemplate : "<%if (label){%><%=label%>: <%}%><%= value %>%",
    
    //String - A legend template
    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"

    }

var pieData = [
        
                <?php 
                    $loop = 1;
                    $totalCoHit = 0;
                    foreach($dataCountry as $country=>$hits)
                    {
                        if ($loop == 6)
                        {
                        break;
                        } 
                        $totalCoHit = $totalCoHit + (int)$hits;
                    }
                    $avgCo = $totalCoHit / 100;
                    
                    $loop = 1;
                    foreach($dataCountry as $country=>$hits)
                    {
                        $country = country_code_to_country($country);
                        $country = ($country == "United States of America" ? "USA" : $country);
                        if ($loop == 6)
                        {
                        break;
                        }
                        if ($loop == 1)
                        {
                        echo '{
                        value: '.ceil($hits/$avgCo).',
   					    color:"#F7464A",
                        highlight: "#FF5A5E",
                        label: "'.$country.'"
                        },';
                        }
                        if ($loop == 2)
                        {
                        echo '{
                        value: '.ceil($hits/$avgCo).',
				        color: "#46BFBD",
                        highlight: "#5AD3D1",
                        label: "'.$country.'"
                        },';
                        }
                        if ($loop == 3)
                        {
                        echo '{
                        value: '.ceil($hits/$avgCo).',
					    color: "#FDB45C",
					    highlight: "#FFC870",
                        label: "'.$country.'"
                        },';
                        }
                        if ($loop == 4)
                        {
                        echo '{
                        value: '.ceil($hits/$avgCo).',
   					 	color: "#949FB1",
					    highlight: "#A8B3C5",
                        label: "'.$country.'"
                        },';
                        }
                        if ($loop == 5)
                        {
                        echo '{
                        value: '.ceil($hits/$avgCo).',
                        color: "#4D5360",
					    highlight: "#616774",
                        label: "'.$country.'"
                        }';
                        }
                        
                        $loop++;
                    }
                ?>

			];


var pieData1 = [
        
                <?php 
                    $loop = 1;
                    $totalOsHit = 0;
                    foreach($dataOs as $os=>$hits)
                    {
                        if ($loop == 6)
                        {
                        break;
                        } 
                        $totalOsHit = $totalOsHit + (int)$hits;
                    }
                    $avgOs = $totalOsHit / 100;
                    
                    $loop = 1;
                    foreach($dataOs as $os=>$hits)
                    {
                        if ($loop == 6)
                        {
                        break;
                        }
                        if ($loop == 1)
                        {
                        echo '{
                        value: '.ceil($hits/$avgOs).',
   					    color:"#2980b9",
                        highlight: "#2a83be",
                        label: "'.$os.'"
                        },';
                        }
                        if ($loop == 2)
                        {
                        echo '{
                        value: '.ceil($hits/$avgOs).',
				        color: "#e74c3c",
                        highlight: "#eb4a39",
                        label: "'.$os.'"
                        },';
                        }
                        if ($loop == 3)
                        {
                        echo '{
                        value: '.ceil($hits/$avgOs).',
					    color: "#95a5a6",
					    highlight: "#97a2a3",
                        label: "'.$os.'"
                        },';
                        }
                        if ($loop == 4)
                        {
                        echo '{
                        value: '.ceil($hits/$avgOs).',
   					 	color: "#f39c12",
					    highlight: "#f39b0f",
                        label: "'.$os.'"
                        },';
                        }
                        if ($loop == 5)
                        {
                        echo '{
                        value: '.ceil($hits/$avgOs).',
                        color: "#1abc9c",
					    highlight: "#1fc2a2",
                        label: "'.$os.'"
                        }';
                        }
                        
                        $loop++;
                    }
                ?>

			];
            
var pieData2 = [
        
                <?php 
                    $loop = 1;
                    $totalBrowserHit = 0;
                    foreach($dataBrowser as $browser=>$hits)
                    {
                        if ($loop == 6)
                        {
                        break;
                        } 
                        $totalBrowserHit = $totalBrowserHit + (int)$hits;
                    }
                    $avgBrowser = $totalBrowserHit / 100;
                    
                    $loop = 1;
                    foreach($dataBrowser as $browser=>$hits)
                    {
                        if ($browser == 'Googlebot-Image')
                        $browser = 'Googlebot';
                        if ($browser == 'MSIE')
                        $browser = 'IE';
                        if ($loop == 6)
                        {
                        break;
                        }
                        if ($loop == 1)
                        {
                        echo '{
                        value: '.ceil($hits/$avgBrowser).',
   					    color:"#ba7faf",
                        highlight: "#B48EAD",
                        label: "'.$browser.'"
                        },';
                        }
                        if ($loop == 2)
                        {
                        echo '{
                        value: '.ceil($hits/$avgBrowser).',
                        color: "#cd8068",
					    highlight: "#D08770",
                        label: "'.$browser.'"
                        },';
                        }
                        if ($loop == 3)
                        {
                        echo '{
                        value: '.ceil($hits/$avgBrowser).',
					    color: "#9db885",
					    highlight: "#A3BE8C",
                        label: "'.$browser.'"
                        },';
                        }
                        if ($loop == 4)
                        {
                        echo '{
                        value: '.ceil($hits/$avgBrowser).',
   					 	color: "#558dbf",
					    highlight: "#5B90BF",
                        label: "'.$browser.'"
                        },';
                        }
                        if ($loop == 5)
                        {
                        echo '{
                        value: '.ceil($hits/$avgBrowser).',
	                    color: "#c0392b",
                        highlight: "#c45044",
                        label: "'.$browser.'"
                        }';
                        }
                        
                        $loop++;
                    }
                ?>

			];
			window.onload = function(){
                    var ctx = document.getElementById("chart-area").getContext("2d");
				    var lineChart = new Chart(ctx).Doughnut(pieData,pieOptions);
                    //then you just need to generate the legend
                    var legend = lineChart.generateLegend();

                    //and append it to your page somewhere
                    $('#legendDiv').append(legend);
        
				    var ctx1 = document.getElementById("chart-area1").getContext("2d");
				    var lineChart1 = new Chart(ctx1).Doughnut(pieData1,pieOptions);
                    //then you just need to generate the legend
                    var legend1= lineChart1.generateLegend();

                    //and append it to your page somewhere
                    $('#legendDiv1').append(legend1);
                    
                    var ctx2 = document.getElementById("chart-area2").getContext("2d");
				    var lineChart2 = new Chart(ctx2).Doughnut(pieData2,pieOptions);
                    //then you just need to generate the legend
                    var legend2= lineChart2.generateLegend();

                    //and append it to your page somewhere
                    $('#legendDiv2').append(legend2);
			};


  </script>
    
        <?php } elseif($pointOut == 'live-traffic'){ ?>
        <br /> <br />
<script>
function liveTraffic(codeID) { 
jQuery.post('<?php createLink('?route=flag-ajax/ajax-traffic',false,true); ?>',{code_id:codeID},function(data){
jQuery("#liveTrafficID").html(data);
window.setTimeout("liveTraffic('<?php echo $code_id; ?>')", 5000);
});
}
$(window).load(function() {
window.setTimeout("liveTraffic('<?php echo $code_id; ?>')", 2000);
});
</script>

<!-- Custom Tabs -->
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class=""><a href="<?php echo $server_path.substr($toolURL, 1); ?>/details/<?php echo $code_id; ?>"><?php echo $lang['AD339']; ?></a></li>
        <li class="active"><a data-toggle="tab" href="<?php echo $server_path.substr($toolURL, 1); ?>/details/<?php echo $code_id; ?>/live-traffic/"><?php echo $lang['AD340']; ?></a></li>
        <li class=""><a href="<?php echo $server_path.substr($toolURL, 1); ?>/details/<?php echo $code_id; ?>/today/"><?php echo $lang['AD341']; ?></a></li>
        <li class=""><a href="<?php echo $server_path.substr($toolURL, 1); ?>/details/<?php echo $code_id; ?>/yesterday/"><?php echo $lang['AD342']; ?></a></li>
        <li class=""><a href="<?php echo $server_path.substr($toolURL, 1); ?>/details/<?php echo $code_id; ?>/last-30-days/"><?php echo $lang['AD343']; ?></a></li>
        <li class=""><a href="<?php echo $server_path.substr($toolURL, 1); ?>/details/<?php echo $code_id; ?>/all-details/"><?php echo $lang['AD344']; ?></a></li>
    </ul>
    <div class="tab-content">
        <div id="tab_2" class="tab-pane active">
    <h3 class="text-center">
        <?php trans('You are Watching Live Traffic of your WebPage', $lang['AD361']); ?>
    </h3>
    <br />
    <div class="preLoder text-center"> 
    <img src="<?php createLink('icons/loader.gif',false,true); ?>" alt="loader" /> <?php trans('Watching for unique visitors', $lang['AD362']); ?> <br />
    </div>
    
    <div class="tableX" id="liveTrafficID">
      <table class="table table-bordered">
      <thead>
      <tr>
      <th>#</th>
      <th><?php echo $lang['18']; ?></th>
      <th><?php echo $lang['AD347']; ?></th>
      <th style="width: 120px;"><?php echo $lang['AD363']; ?></th>
      <th><?php trans('Time', $lang['AD364']); ?></th>
      </tr>
      </thead>
      <tbody>
            <?php
            $loop = 1;
            $arrLoop = 0;
            foreach($country as $user_country)
            {
                $countryFile = 'flags/default/16/'.strtolower(Trim($user_country)).'.png';
                if (!file_exists(ROOT_DIR.$countryFile))
                $countryFile = 'icons/unknown.png';
                $browser = $browserArr[$arrLoop];
                if ($browser == 'Googlebot-Image')
                $browser = 'Googlebot';
                if ($browser == 'MSIE')
                $browser = 'IE';
                $browserFile = 'icons/'.strtolower(Trim($browser)).'.png';
                if (!file_exists(ROOT_DIR.$browserFile))
                $browserFile = 'icons/unknown.png';
                $osFile = 'icons/'.strtolower(Trim($os[$arrLoop])).'.png';
                if (!file_exists(ROOT_DIR.$osFile))
                $osFile = 'icons/unknown.png';
                echo'<tr>
                 <td style="width: 50px">'.$loop.'.</td>
                 <td><img alt="'.$user_country.'" src="'.createLink($countryFile,true,true).'">  '.$user_country.'</td>
                 <td><img alt="'.$browser.'" src="'.createLink($browserFile,true,true).'">  '.ucfirst($browser).'</td>
                 <td><img alt="'.$os[$arrLoop].'" src="'.createLink($osFile,true,true).'">  '.ucfirst($os[$arrLoop]).'</td>
                 <td>'.$visTime[$arrLoop].'</td>
                 </tr>';
                 $arrLoop++;
                 $loop++;
            }
            ?>

                                       

                        </tbody></table>
    </div>
                        </div><!-- /.tab-pane -->
                    </div><!-- /.tab-content -->
                </div><!-- nav-tabs-custom -->
        
        <?php } elseif($pointOut == 'today' || $pointOut == 'yesterday' || $pointOut == 'last-30-days') { ?>
 <br /> <br />
<link rel="stylesheet" type="text/css" href="<?php themeLink('css/jquery.dataTables.css'); ?>" />
<script type="text/javascript" language="javascript" src="<?php themeLink('js/jquery.dataTables.js'); ?>"></script>
    
<script type="text/javascript" language="javascript" class="init">
$(document).ready(function() {
	$('#mySitesTable').dataTable( {
		"processing": true,
		"serverSide": true,
        <?php if ($page_id == 'last-30-days')
            echo '"ajax": "'.createLink('?route=flag-ajax/ajax-data&code_id='.$code_id.'&date=upto30',true,true).'"';
        else
            echo '"ajax": "'.createLink('?route=flag-ajax/ajax-data&code_id='.$code_id.'&date='.$date,true,true).'"';
		?>
	} );
} );
</script>
    
<!-- Custom Tabs -->
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class=""><a href="<?php echo $server_path.substr($toolURL, 1); ?>/details/<?php echo $code_id; ?>"><?php echo $lang['AD339']; ?></a></li>
        <li class=""><a href="<?php echo $server_path.substr($toolURL, 1); ?>/details/<?php echo $code_id; ?>/live-traffic"><?php echo $lang['AD340']; ?></a></li>
        <?php if ($page_id == 'today') { ?>
        <li class="active"><a data-toggle="tab" href="#tab_3"><?php echo $lang['AD341']; ?></a></li>
        <li class=""><a href="<?php echo $server_path.substr($toolURL, 1); ?>/details/<?php echo $code_id; ?>/yesterday"><?php echo $lang['AD342']; ?></a></li>
        <li class=""><a href="<?php echo $server_path.substr($toolURL, 1); ?>/details/<?php echo $code_id; ?>/last-30-days"><?php echo $lang['AD343']; ?></a></li>
        <?php } elseif ($page_id == 'yesterday') { ?>
        <li class=""><a href="<?php echo $server_path.substr($toolURL, 1); ?>/details/<?php echo $code_id; ?>/today"><?php echo $lang['AD341']; ?></a></li>
        <li class="active"><a data-toggle="tab" href="#tab_4">Yesterday</a></li>
        <li class=""><a href="<?php echo $server_path.substr($toolURL, 1); ?>/details/<?php echo $code_id; ?>/last-30-days"><?php echo $lang['AD343']; ?></a></li>
         <?php } elseif ($page_id == 'last-30-days') { ?>
        <li class=""><a href="<?php echo $server_path.substr($toolURL, 1); ?>/details/<?php echo $code_id; ?>/today"><?php echo $lang['AD341']; ?></a></li>
        <li class=""><a href="<?php echo $server_path.substr($toolURL, 1); ?>/details/<?php echo $code_id; ?>/yesterday"><?php echo $lang['AD342']; ?></a></li>
        <li class="active"><a data-toggle="tab" href="#tab_5"><?php echo $lang['AD343']; ?></a></li>
        <?php } ?>

         <li class=""><a href="<?php echo $server_path.substr($toolURL, 1); ?>/details/<?php echo $code_id; ?>/all-details"><?php echo $lang['AD344']; ?></a></li>
    </ul>
    <div class="tab-content">
        <div id="tab" class="tab-pane active">
                
         
                 <div class="row">
<h3 class="page-header text-center">
<?php echo $p_title; ?>
</h3>

<center> 
<!-- Jvectormap -->
<link rel="stylesheet" href="<?php themeLink('css/jquery-jvectormap-2.0.1.css'); ?>" type="text/css" media="screen"/>
<script src="<?php themeLink('js/jquery-jvectormap-2.0.1.min.js'); ?>"></script>
<script src="<?php themeLink('js/jquery-jvectormap-world-mill-en.js'); ?>"></script>
<div id="world-map" style="width: 600px; height: 400px"></div>
</center>     
<p>

</p>

</div>        
                
<div class="box box-primary">
<div class="box-header">
    <!-- tools box -->
</div>
<fieldset>     

           
    <br /> <br /> <div class="box-body">
<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="mySitesTable">
<thead>
<tr>
<th><?php trans('ID', $lang['AD365']); ?></th>
<th><?php echo $lang['18']; ?></th>
<th><?php echo $lang['AD347']; ?></th>
<th><?php echo $lang['AD363']; ?></th>
<th><?php trans('Visited Time', $lang['AD366']); ?></th>
</tr>
</thead>         
<tbody>
        
</tbody>
</table>


    </div><!-- /.box-body -->

    <div class="box-footer">

    </div>
</div>  

        </div><!-- /.tab-pane -->
    </div><!-- /.tab-content -->
</div><!-- nav-tabs-custom -->

<script>
    $(function(){
       var visitorsData = {
        <?php 
        $loop = 1;
        foreach($dataCountry as $country=>$hits)
        {
            if ($totalCount == $loop)
            echo '"'.$country.'": '.$hits;
            else
            echo '"'.$country.'": '.$hits.',';
            $loop++;
        }
        ?>
    };
    //World map by jvectormap
    $('#world-map').vectorMap({
        map: 'world_mill_en',
        backgroundColor: "#fff",
        regionStyle: {
            initial: {
                fill: '#e4e4e4',
                "fill-opacity": 1,
                stroke: 'none',
                "stroke-width": 0,
                "stroke-opacity": 1
            }
        },
        series: {
            regions: [{
                    values: visitorsData,
                    scale: ["#3c8dbc", "#2D79A6"], //['#3E5E6B', '#A6BAC2'],
                    normalizeFunction: 'polynomial'
                }]
        },
        onRegionTipShow: function(e, el, code) {
            if (typeof visitorsData[code] != "undefined")
                el.html(el.html() + ': ' + visitorsData[code] + ' <?php makeJavascriptStr($lang['AD366'],true); ?>');
        }
    });
    });
    </script>
    
    <?php } elseif($pointOut == 'all-details') { ?>
   	
    <link rel="stylesheet" type="text/css" href="<?php themeLink('css/jquery.dataTables.css'); ?>" />
	<script type="text/javascript" language="javascript" src="<?php themeLink('js/jquery.dataTables.js'); ?>"></script>
    
	<script type="text/javascript" language="javascript" class="init">
    $(document).ready(function() {
    	$('#mySitesTable').dataTable( {
    		"processing": true,
    		"serverSide": true,
    		"ajax": "<?php createLink('?route=flag-ajax/ajax-all-data&code_id='.$code_id,false,true); ?>"
    	} );
    } );
	</script>
        
  <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class=""><a href="<?php echo $server_path.substr($toolURL, 1); ?>/details/<?php echo $code_id; ?>"><?php echo $lang['AD339']; ?></a></li>
            <li class=""><a href="<?php echo $server_path.substr($toolURL, 1); ?>/details/<?php echo $code_id; ?>/live-traffic"><?php echo $lang['AD340']; ?></a></li>
            <li class=""><a href="<?php echo $server_path.substr($toolURL, 1); ?>/details/<?php echo $code_id; ?>/today"><?php echo $lang['AD341']; ?></a></li>
            <li class=""><a href="<?php echo $server_path.substr($toolURL, 1); ?>/details/<?php echo $code_id; ?>/yesterday"><?php echo $lang['AD342']; ?></a></li>
            <li class=""><a href="<?php echo $server_path.substr($toolURL, 1); ?>/details/<?php echo $code_id; ?>/last-30-days"><?php echo $lang['AD343']; ?></a></li>
            <li class="active"><a data-toggle="tab" href="#tab_6"><?php echo $lang['AD344']; ?></a></li>
        </ul>
        <div class="tab-content">
            <div id="tab" class="tab-pane active">
                    
                     <br /> <br />
                    
		         <div class="box box-primary">
        <div class="box-header">
            <!-- tools box -->
        </div>
<fieldset>     

               
    <div class="box-body">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="mySitesTable">
        <thead>
        <tr>
        <th><?php echo $lang['AD365']; ?></th>
        <th><?php echo $lang['AD346']; ?></th>
        <th><?php echo $lang['18']; ?></th>
        <th><?php echo $lang['AD347']; ?></th>
        <th><?php echo $lang['AD363']; ?></th>
        <th><?php echo $lang['AD366']; ?></th>
        </tr>
        </thead>         
        <tbody>
                    
        </tbody>
        </table>


        </div><!-- /.box-body -->

        <div class="box-footer">

        </div>
    </div>  

            </div><!-- /.tab-pane -->
        </div><!-- /.tab-content -->
    </div><!-- nav-tabs-custom -->
        
    <?php } elseif($pointOut == 'regenerate') { ?>
    
<style> 

#editFlagBox{ display:none; } #c_alert2{ display:none; }</style>   
<script>
function editForm()
{
var xmlhttp;  
var passWord = $('input[name=password]').val();
var userId = $('input[name=user_id]').val();
if (passWord == '')
{
    alert('<?php makeJavascriptStr($lang['AD367'],true); ?>');
    return false;
}
else {
if (window.XMLHttpRequest)
{
// code for IE7+, Firefox, Chrome, Opera, Safari
xmlhttp=new XMLHttpRequest();
}
else
{
// code for IE6, IE5
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
}
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    }
  }
$.post("<?php createLink('flag-ajax/ajaxPass'); ?>", {password:passWord,user_id:userId}, function(results){
if (results == 1) {
     jQuery("#passBox").fadeOut();
     $("#editFlagBox").show();
     $('input[name=Userpass]').val(passWord);
     $('input[name=Userid]').val(userId);
     jQuery("#editFlagBox").fadeIn();
     return true;
}
else
{
     $("#c_alert2").show();
     alert(results);
     return false;
}
});
}
}
</script> 

    <span id="gen_flag"></span>
    <br />
    
    <div id="passBox">
    <form method="POST" action="#">
<div class="modal-body">
<?php trans('Your Flag Counter ID', $lang['AD368']); ?>: <strong><?php echo $code_id; ?></strong>
<br /><br />
<?php trans('Created Date', $lang['AD369']); ?>: <strong><?php echo $date; ?></strong>
<br /><br />
<?php trans('For edting the flag counter you need password, that can be generated on inital generation of flag counter.', $lang['AD370']); ?>  
<br /><br />
                        <div id="c_alert2">
                                <div class="alert alert-danger alert-dismissable">
       <i class="fa fa-ban"></i>
       <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
         <b>Alert!</b> <?php trans('Error - Try Again (Password Wrong)', $lang['AD371']); ?> </div>
                        </div>
                        
    
        <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><?php echo $lang['RF67']; ?>:</span>
                    <input type="password" placeholder="<?php echo $lang['AD367']; ?>" class="form-control" id="password" name="password">
                </div>
            </div>
        </div>
        
            <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>" />
        <div class="modal-footer clearfix">
        <br />
        <button class="btn btn-primary" onclick="return editForm()" type="button"><i class="fa fa-edit"></i> <?php trans('Edit', $lang['AD372']); ?></button>
        </div>
    </form> 

                </div>  
                  
                  <div id="editFlagBox">
                  <?php trans('Your Flag Counter ID', $lang['AD373']); ?>: <strong><?php echo $code_id; ?></strong>
<br /><br />
<?php echo $lang['AD369']; ?>: <strong><?php echo $date; ?></strong>
<br /><br />
    <form method="POST" action="generate">
    <input type="hidden" value="" id="Userpass" name="Userpass"/>
    <input type="hidden" value="" id="Userid" name="Userid"/>
    <div class="well" id="gen_flag_box" >
    <div class="row">
    
        <div class="col-md-6">
        
        <div class="form-group">
        <label><?php echo $lang['AD304']; ?></label>
        <select class="form-control" name="maximumFlag" id="maximumFlag">
        <?php 
            for ($loop=1; $loop<=250; $loop++)
            {
                if ($loop == (int)$flagLimit)
                {
                    echo "<option selected=''>$loop</option>";
                }
                else
                {
                      echo "<option>$loop</option>";
                }
          
            } 
        ?>
        </select>
        </div>
        
        <div class="form-group">
        <label><?php echo $lang['AD305']; ?></label>
        <select class="form-control" name="columnsLimit" id="columnsLimit">
        <?php 
            for ($loop=1; $loop<=8; $loop++)
            {
                if ($loop == (int)$columnLimit)
                {
                    echo "<option selected=''>$loop</option>";
                }
                else
                {
                      echo "<option>$loop</option>";
                }
          
            } 
        ?>
        </select>
        </div>
        
        <div class="form-group">
        <label><?php echo $lang['AD306']; ?></label>
        <select class="form-control" name="flagSize" id="flagSize">
          <?php 
          if (strtolower($flagSize) == 'small')
          echo '<option selected="">'.$lang['AD307'].'</option>';
          else
          echo '<option>'.$lang['AD307'].'</option>';
          if (strtolower($flagSize) == 'medium')
          echo '<option selected="">'.$lang['AD308'].'</option>';
          else
          echo '<option>'.$lang['AD308'].'</option>';  
          if (strtolower($flagSize) == 'large')
          echo '<option selected="">'.$lang['AD309'].'</option>';
          else
          echo '<option>'.$lang['AD309'].'</option>';   
          ?>
        </select>
        </div>
                                                                    
        <div class="form-group">
        <label for="counterName"><?php echo $lang['AD310']; ?></label>
        <input type="text" value="<?php echo $topLabel; ?>" placeholder="<?php echo $lang['AD311']; ?>" name="counterName" id="counterName" class="form-control">
        </div>
        
                
    <div class="form-group"> 
      <div class="checkbox">
            <label style="font-weight: bold;">
            <?php 
            if($countryCode == true)
            $dIs = 'checked=""';
            else
            $dIs = '';
            ?>
            <input type="checkbox" <?php echo $dIs; ?> name="showLabel" id="showLabel" />  <?php echo $lang['AD312']; ?>
            </label>
      </div>
    </div>
            
            
    </div>
    <div class="col-md-6">
        <div class="form-group">
                        <label><?php echo $lang['AD313']; ?></label>
                        <div class="input-group my-colorpicker1 colorpicker-element">                                            
                            <input type="text" value="#<?php echo $backColor; ?>" class="form-control" name="backColor" id="backColor">
                            <div class="input-group-addon">
                                <i style="background-color: rgb(255, 255, 255);"></i>
                            </div>
                        </div><!-- /.input group -->
                    </div>

        <div class="form-group">
                        <label><?php echo $lang['AD314']; ?></label>
                        <div class="input-group my-colorpicker2 colorpicker-element">                                            
                            <input type="text" value="#<?php echo $borderColor; ?>" class="form-control"  name="borderColor" id="borderColor">
                            <div class="input-group-addon">
                                <i style="background-color: rgb(207, 207, 207);"></i>
                            </div>
                        </div><!-- /.input group -->
                    </div>

        <div class="form-group">
                        <label><?php echo $lang['AD315']; ?></label>
                        <div class="input-group my-colorpicker3 colorpicker-element">                                            
                            <input type="text" value="#<?php echo $titleColor; ?>" class="form-control" name="titleColor" id="titleColor">
                            <div class="input-group-addon">
                                <i style="background-color: rgb(0, 0, 0);"></i>
                            </div>
                        </div><!-- /.input group -->
                    </div>

        <div class="form-group">
                        <label><?php echo $lang['AD316']; ?></label>
                        <div class="input-group my-colorpicker4 colorpicker-element">                                            
                            <input type="text" value="#<?php echo $textColor; ?>" class="form-control" name="textColor" id="textColor">
                            <div class="input-group-addon">
                                <i style="background-color: rgb(0, 0, 0);"></i>
                            </div>
                        </div><!-- /.input group -->
                    </div>
            
            <div class="form-group"> 
            <div class="checkbox">
            <label style="font-weight: bold;">
            <?php 
            if($showPage == true)
            $dIs = 'checked=""';
            else
            $dIs = '';
            ?>
            <input type="checkbox" <?php echo $dIs; ?> name="pageView" id="pageView">  <?php echo $lang['AD317']; ?>
            </label>
            </div>
             </div>
            
    
    </div>
              

    <div style="text-align: center;">
    <a class="btn btn-danger" id="preItNow"><?php echo $lang['AD319']; ?></a>
    <a class="btn btn-success" id="saveItNow"><?php trans('Save the Settings', $lang['AD374']); ?></a>
    </div>
    
   </div></div>
     
    <hr class="small" id="prev_flag">
    <div class="row text-left">
        <div class="col-lg-12">
            <h3 class="title-flag"><?php echo $lang['AD328']; ?></h3>
            <p><?php echo $lang['AD321']; ?></p>
                <div class="well" id="prev_flag_box">
                <br />
                <div class="loadBox"><img src="<?php themeLink('img/load.gif'); ?>" /><br /></div>
                <div id="prv_img_box">
                <img style="max-width: 100%;" src="<?php echo $server_path.substr($toolURL,1); ?>/load/<?php echo $code_id; ?>/" />
                </div>
                <div style="margin-top: 15px;">
                    <a class="btn btn-warning" id="cusItNow"><?php echo $lang['AD322']; ?></a>
                    <a class="btn btn-success" id="saveItNow2"><?php trans('Save the Settings', $lang['AD374']); ?></a>
                </div>
                </div>
        </div>
    </div>
    </form>  </div>
    
    
    <?php } ?>
		</div>


<br />

<div class="xd_top_box">
<?php echo $ads_720x90; ?>
</div>
<div class="info-page">
	<div class="box-text">
		<h2 id="sec1" class="about_tool"><?php echo $lang['11'].' '.$data['tool_name']; ?></h2>
<p>
<?php echo $data['about_tool']; ?>
</p>
	</div>
</div>

</div>               		
        </div>
    </div> <br />

<!-- JavaScript -->
<script src="<?php themeLink('js/plugins/colorpicker/bootstrap-colorpicker.min.js'); ?>" type="text/javascript"></script>


<script>
jQuery(document).ready(function(){
    jQuery("#getItNow").click(function()
    {
    var pos = $('#gen_flag').offset();
    $('body,html').animate({ scrollTop: pos.top });
    });
    jQuery("#cusItNow").click(function()
    {
    var pos = $('#gen_flag').offset();
    $('body,html').animate({ scrollTop: pos.top });
    });
    jQuery("#preItNow").click(function()
    {
    jQuery(".loadBox").css({"display":"block"});
   	jQuery(".loadBox").show();
   	jQuery(".loadBox").fadeIn();    
    var maximumFlag=jQuery.trim($('#maximumFlag :selected').text());  
    var columnsLimit=jQuery.trim($('#columnsLimit :selected').text());  
    var flagSize = jQuery.trim($('#flagSize :selected').text());  
    var counterName = jQuery.trim(jQuery("#counterName").val());  
    var showLabel = $('#showLabel').is(':checked');
    var backColor = jQuery.trim(jQuery("#backColor").val());  
    var borderColor = jQuery.trim(jQuery("#borderColor").val());  
    var titleColor = jQuery.trim(jQuery("#titleColor").val());  
    var textColor = jQuery.trim(jQuery("#textColor").val());  
    var pageView = $('#pageView').is(':checked');
    var pos = $('#prev_flag').offset();
    var rndVal = Math.floor((Math.random() * 999999999) + 1);
    $('body,html').animate({ scrollTop: pos.top });
    jQuery.post('<?php createLink('flag-ajax/ajaxDemo'); ?>',{page_view:pageView, show_label:showLabel, flag_size:flagSize, flag_limit:maximumFlag, column_limit:columnsLimit, top_label:counterName, back_color:backColor, title_color:titleColor, text_color:textColor, border_color:borderColor},function(data){
    jQuery(".loadBox").fadeOut();    
   	jQuery(".loadBox").css({"display":"none"});
    jQuery(".loadBox").hide();
    if (data == '0')
    {
        jQuery("#prv_img_box").html('<b>Failed</b>'); 
    }
    else
    {
    jQuery("#prv_img_box").html('<img style="max-width: 100%;" src="<?php echo $toolURL; ?>/load/ig?'+rndVal+'" alt="Flag Counter" title="Flag Counter" />');  
    }
    });
    });
    jQuery("#saveItNow").click(function()
    {
    var passWord = $('input[name=password]').val();
    var userId = $('input[name=user_id]').val();   
    var maximumFlag=jQuery.trim($('#maximumFlag :selected').text());  
    var columnsLimit=jQuery.trim($('#columnsLimit :selected').text());  
    var flagSize = jQuery.trim($('#flagSize :selected').text());  
    var counterName = jQuery.trim(jQuery("#counterName").val());  
    var showLabel = $('#showLabel').is(':checked');
    var backColor = jQuery.trim(jQuery("#backColor").val());  
    var borderColor = jQuery.trim(jQuery("#borderColor").val());  
    var titleColor = jQuery.trim(jQuery("#titleColor").val());  
    var textColor = jQuery.trim(jQuery("#textColor").val());  
    var pageView = $('#pageView').is(':checked');
    jQuery.post('<?php createLink('flag-ajax/ajaxSave'); ?>',{user_id:userId, password:passWord, page_view:pageView, show_label:showLabel, flag_size:flagSize, flag_limit:maximumFlag, column_limit:columnsLimit, top_label:counterName, back_color:backColor, title_color:titleColor, text_color:textColor, border_color:borderColor},function(data){
    if (data == 1) {
        sweetAlert('<?php makeJavascriptStr($lang['143'],true); ?>', '<?php makeJavascriptStr($lang['AD375'],true); ?>' , "success");
    }
    else
    {
        alert(data);
    }
    });
    });
    jQuery("#saveItNow2").click(function()
    {
    var passWord = $('input[name=password]').val();
    var userId = $('input[name=user_id]').val();   
    var maximumFlag=jQuery.trim($('#maximumFlag :selected').text());  
    var columnsLimit=jQuery.trim($('#columnsLimit :selected').text());  
    var flagSize = jQuery.trim($('#flagSize :selected').text());  
    var counterName = jQuery.trim(jQuery("#counterName").val());  
    var showLabel = $('#showLabel').is(':checked');
    var backColor = jQuery.trim(jQuery("#backColor").val());  
    var borderColor = jQuery.trim(jQuery("#borderColor").val());  
    var titleColor = jQuery.trim(jQuery("#titleColor").val());  
    var textColor = jQuery.trim(jQuery("#textColor").val());  
    var pageView = $('#pageView').is(':checked');
    jQuery.post('<?php createLink('flag-ajax/ajaxSave'); ?>',{user_id:userId, password:passWord, page_view:pageView, show_label:showLabel, flag_size:flagSize, flag_limit:maximumFlag, column_limit:columnsLimit, top_label:counterName, back_color:backColor, title_color:titleColor, text_color:textColor, border_color:borderColor},function(data){
    if (data == 1) {
        sweetAlert('<?php makeJavascriptStr($lang['143'],true); ?>', '<?php makeJavascriptStr($lang['AD375'],true); ?>' , "success");
    }
    else
    {
        alert(data);
    }
    });
    });
    jQuery("#generateFlag").click(function()
    {
    //Generate Flag Counter
    });
});

//Colorpicker
$(".my-colorpicker1").colorpicker();
$(".my-colorpicker2").colorpicker();
$(".my-colorpicker3").colorpicker();
$(".my-colorpicker4").colorpicker();

</script>