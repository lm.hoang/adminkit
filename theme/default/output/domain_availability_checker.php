<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name A to Z SEO Tools - PHP Script
 * @copyright � 2018 ProThemes.Biz
 *
 */
?>
<style>
table {
    table-layout: fixed; width: 100%;
}
td {
  word-wrap: break-word;
}
</style>
<script>
function processLoadBar() {
var myUrl= jQuery("#data").val();
if (myUrl==null || myUrl=="") {
}else{
    jQuery("#mainbox").fadeOut();
    jQuery("#percentimg").css({"display":"block"});
    var pos = $('#homeBox').offset();
    $('body,html').animate({ scrollTop: pos.top },800);
}
}
</script>
  <div class="container main-container">
	<div class="row">
      	    <?php
            if($themeOptions['general']['sidebar'] == 'left')
                require_once(THEME_DIR."sidebar.php");
            ?>
          	<div class="col-md-8 main-index">
            <div id="homeBox"></div>
            <div class="xd_top_box">
             <?php echo $ads_720x90; ?>
            </div>
            <div class="img-mainDetail">
                <img src="<?php echo "../theme/default/".$data['icon_name']; ?>" alt="<?php echo $data['tool_name']; ?>">
                <h2 id="title"><?php echo $data['tool_name']; ?></h2>
            </div>
		<div class="box_art_Rew">
			<?php if ($pointOut != 'output' && $pointOut != 'whois') { ?>
               <br />
               <div id="mainbox">
               <form method="POST" action="<?php echo $toolOutputURL;?>" onsubmit="return fixData();"> 
               <p><?php echo $lang['320']; ?>
               </p>
               <textarea class="form-control" name="data" id="data" rows="3" style="height: 270px;"></textarea>
               <br />
               <?php if ($toolCap) echo $captchaCode; ?>
               <div class="text-center">
               <input class="btn btn-info" onclick="processLoadBar();" type="submit" value="<?php echo $lang['8']; ?>" name="submit"/>
               </div>
               </form>     
               </div>
               <div id="percentimg" class="text-center" style="display:none;">
                    <br /><br />
                    <img src="<?php themeLink('img/load.gif'); ?>" />
                    <br /><br />
                    <?php echo $lang['146']; ?>...
                    <br /><br />
               </div>  
                       
               <?php 
               } elseif($pointOut == 'whois') {
               //WHOIS Block
               ?>
                <br />
                <div class="widget-box">
                    <div class="widget-header">
                        <h4 class="widget-title lighter smaller">
                         <i class="fa fa-thumb-tack blue"></i>
                            &nbsp;&nbsp; <?php echo $myHost; ?>
                        </h4>
                    </div>

                    <div class="widget-body">
                        <div class="widget-main">

                                <br />
                                <table class="table table-hover table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th class="heading">
                                                <h4 class="text-center"><?php echo $lang['85']; ?></h4>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $myLines = preg_split("/\r\n|\n|\r/", $whoisData);
                                    foreach($myLines as $line){
                                        if(!empty($line))
                                        echo '<tr><td>'.$line.'</td></tr>';
                                    }
                                    ?>
                                    </tbody>
                                </table>
                                
                              </div><!-- /.widget-main -->
                    </div><!-- /.widget-body -->
            </div>
            <?php }else { 
               //Output Block
               if(isset($error)) {
                
                echo '<br/><br/><div class="alert alert-error">
                <strong>Alert!</strong> '.$error.'
                </div><br/><br/>
                <div class="text-center"><a class="btn btn-info" href="'.$toolURL.'">'.$lang['12'].'</a>
                </div><br/>';
                
               } else {
               ?>
            <br />

                   
            <h4><?php echo $lang['64']; ?></h4>
			<table class="table table-hover table-bordered table-striped" style="margin-bottom: 30px;">
				<thead>
					<tr>
						<th style="width: 10%;">No.</th>
						<th><?php echo $lang['98']; ?></th>
                        <th><?php echo $lang['69']; ?></th>
                        <th style="width: 20%;"><?php trans('WHOIS', $lang['AD65']); ?></th>
				    </tr>
				</thead>
                <tbody>
                <?php for($loop=0; $loop<$count; $loop++) { ?>
                <tr>
                    <td><?php echo $loop+1; ?></td>
                    <td><?php echo $myHost[$loop]; ?></td>
                    <td><b style="color: <?php echo $statusColors[$loop];?>;"><?php echo $stats[$loop]; ?></b></td>
                    <td><?php echo $whoisLinks[$loop];?></td>
                </tr>
                <?php } ?>
				</tbody>
			</table>
            <br />
   
    
    <div class="text-center">
    <br /> &nbsp; <br />
    <a class="btn btn-info" href="<?php echo $toolURL; ?>"><?php echo $lang['27']; ?></a>
    <br />
    </div>

<?php } } ?>

		</div>
               
<br />

<div class="xd_top_box">
<?php echo $ads_720x90; ?>
</div>
 	<div class="info-page">
	     <div class="box-text">
		<h2 id="sec1" class="about_tool"><?php echo $lang['11'].' '.$data['tool_name']; ?></h2>
		<p>
		<?php echo $data['about_tool']; ?>
		</p>
	     </div>
	</div>

</div>              
            
        <?php
        if($themeOptions['general']['sidebar'] == 'right')
            require_once(THEME_DIR."sidebar.php");
        ?>       		
        </div>
    </div> <br />