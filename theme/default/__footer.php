<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
/*
 * @author Balaji
 * @name: A to Z SEO Tools - PHP Script
 * @theme: Default Style
 * @copyright � 2017 ProThemes.Biz
 *
 */
?>
<!-- custom -->
    <footer id="page-footer">
        <div class="span7">
            <span class="text-copyright">Copyright © Princart.com Inc.All Rights Reserved.</span>
        </div>
        <div class="social-right-ft">
            <span class="facebook iconm-facebook" </span>
            <span class="twitter iconm-twitter"></span>
            <span class="linkedin iconm-linkedin"></span>
            <span class="slack iconm-slack"></span>
        </div>
    </footer>
<!-- custom -->

<!-- Bootstrap -->
<script src="<?php themeLink('js/bootstrap.min.js'); ?>" type="text/javascript"></script>

<!-- Sweet Alert -->
<script type='text/javascript' src='<?php themeLink('js/sweetalert.min.js'); ?>'></script>

<!-- App JS -->
<script src="<?php themeLink('js/app.js'); ?>" type="text/javascript"></script>

<!-- Master JS -->
<script src="<?php createLink('rainbow/master-js'); ?>" type="text/javascript"></script>

<!-- Sign in -->
<div class="modal fade loginme" id="signin" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><?php trans('Sign In',$lang['RF57']); ?></h4>
			</div>
            <form method="POST" action="<?php createLink('account/login'); ?>" class="loginme-form">
			<div class="modal-body">
				<div class="alert alert-warning">
					<button type="button" class="close dismiss">&times;</button><span></span>
				</div>
                <?php if($enable_oauth){ ?>
				<div class="form-group connect-with">
					<div class="info"><?php trans('Sign in using social network',$lang['RF58']); ?></div>
					<a href="<?php createLink('facebook/login'); ?>" class="connect facebook" title="<?php trans('Sign in using Facebook',$lang['RF59']); ?>"><?php trans('Facebook',$lang['RF62']); ?></a>
		        	<a href="<?php createLink('google/login'); ?>" class="connect google" title="<?php trans('Sign in using Google',$lang['RF60']); ?>"><?php trans('Google',$lang['RF63']); ?></a>  	
		        	<a href="<?php createLink('twitter/login'); ?>" class="connect twitter" title="<?php trans('Sign in using Twitter',$lang['RF61']); ?>"><?php trans('Twitter',$lang['RF64']); ?></a>		        
			    </div>
                <?php } ?>
   				<div class="info"><?php trans('Sign in with your username',$lang['RF65']); ?></div>
				<div class="form-group">
					<label><?php trans('Username',$lang['RF66']); ?> <br />
						<input type="text" name="username" class="form-input width96" />
					</label>
				</div>	
				<div class="form-group">
					<label><?php trans('Password',$lang['RF67']); ?> <br />
						<input type="password" name="password" class="form-input width96" />
					</label>
				</div>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary pull-left"><?php trans('Sign In',$lang['RF57']); ?></button>
				<div class="pull-right align-right">
				    <a href="<?php createLink('account/forget'); ?>"><?php trans('Forgot Password',$lang['RF68']); ?></a><br />
					<a href="<?php createLink('account/resend'); ?>"><?php trans('Resend Activation Email',$lang['RF69']); ?></a>
				</div>
			</div>
			 <input type="hidden" name="signin" value="<?php echo md5($date.$ip); ?>" />
             <input type="hidden" name="quick" value="<?php echo md5(randomPassword()); ?>" />
			</form> 
		</div>
	</div>
</div>  

<!-- Sign up -->
<div class="modal fade loginme" id="signup" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"><?php trans('Sign Up',$lang['RF70']); ?></h4>
			</div>
			<form action="<?php createLink('account/register'); ?>" method="POST" class="loginme-form">
			<div class="modal-body">
				<div class="alert alert-warning">
					<button type="button" class="close dismiss">&times;</button><span></span>
				</div>
                <?php if($enable_oauth){ ?>
				<div class="form-group connect-with">
					<div class="info"><?php trans('Sign in using social network',$lang['RF58']); ?></div>
					<a href="<?php createLink('facebook/login'); ?>" class="connect facebook" title="<?php trans('Sign in using Facebook',$lang['RF59']); ?>"><?php trans('Facebook',$lang['RF62']); ?></a>
		        	<a href="<?php createLink('google/login'); ?>" class="connect google" title="<?php trans('Sign in using Google',$lang['RF60']); ?>"><?php trans('Google',$lang['RF63']); ?></a>  	
		        	<a href="<?php createLink('twitter/login'); ?>" class="connect twitter" title="<?php trans('Sign in using Twitter',$lang['RF61']); ?>"><?php trans('Twitter',$lang['RF64']); ?></a>		        
			    </div>
                <?php } ?>
   				<div class="info"><?php trans('Sign up with your email address',$lang['RF71']); ?></div>
				<div class="form-group">
					<label><?php trans('Username',$lang['RF66']); ?> <br />
						<input type="text" name="username" class="form-input width96" />
					</label>
				</div>	
				<div class="form-group">
					<label><?php trans('Email',$lang['RF73']); ?> <br />
						<input type="text" name="email" class="form-input width96" />
					</label>
				</div>
				<div class="form-group">
					<label><?php trans('Full Name',$lang['RF72']); ?> <br />
						<input type="text" name="full" class="form-input width96" />
					</label>
				</div>
				<div class="form-group">
					<label><?php trans('Password',$lang['RF67']); ?> <br />
						<input type="password" name="password" class="form-input width96" />
					</label>
				</div>
				</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary"><?php trans('Sign Up',$lang['RF70']); ?></button>	
			</div>
			<input type="hidden" name="signup" value="<?php echo md5($date.$ip); ?>" />
            <input type="hidden" name="quick" value="<?php echo md5(randomPassword()); ?>" />
			</form>
		</div>
	</div>
</div>

<!-- XD Box -->
<div class="modal fade loginme" id="xdBox" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button id="xdClose" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="xdTitle"></h4>
			</div>
			<div class="modal-body" id="xdContent">

            </div>
		</div>
	</div>
</div>
<?php if(isset($footerAddArr)){ 
    foreach($footerAddArr as $footerCodes)
        echo $footerCodes;
} ?>
</body>
</html>