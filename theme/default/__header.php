<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
/*
 * @author Balaji
 * @name: A to Z SEO Tools - PHP Script
 * @theme: Default Style
 * @copyright � 2018 ProThemes.Biz
 *
 */
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="Content-Language" content="<?php echo (ACTIVE_LANG); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <link rel="icon" type="image/png" href="<?php echo $themeOptions['general']['favicon']; ?>" />

        <!-- Meta Data-->
        <title><?php echo $metaTitle; ?></title>
                
        <meta property="site_name" content="<?php echo $site_name; ?>"/>
        <meta name="description" content="<?php echo $des; ?>" />
        <meta name="keywords" content="<?php echo $keyword; ?>" />
        <meta name="author" content="Balaji" />
        
        <!-- Open Graph -->
        <meta property="og:title" content="<?php echo $metaTitle; ?>" />
        <meta property="og:site_name" content="<?php echo $site_name; ?>" />
        <meta property="og:type" content="website" />
        <meta property="og:description" content="<?php echo $des; ?>" />
        <meta property="og:image" content="<?php echo (substr($server_path,-1) == '/' ? substr($server_path,0,-1) : $server_path).$logo_path?>"/>
        <meta property="og:url" content="<?php echo $currentLink; ?>" />
        
        <?php genCanonicalData($baseURL, $currentLink, $loadedLanguages); ?>
        
        <!-- Main style -->
        <link href="<?php themeLink('css/theme.css'); ?>" rel="stylesheet" />
        
        <!-- Font-Awesome -->
        <link href="<?php themeLink('css/font-awesome.min.css'); ?>" rel="stylesheet" />
                
        <!-- Custom Theme style -->
        <link href="<?php themeLink('css/custom.css'); ?>" rel="stylesheet" type="text/css" />
        
        <?php if($isRTL) echo '<link href="'.themeLink('css/rtl.css',true).'" rel="stylesheet" type="text/css" />'; ?>
        
        <!-- jQuery 1.10.2 -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <style>
            .navbar-nav > li.dropdown .dropdown-menu {
                box-shadow: 0 0 3px rgba(0, 0, 0, 0.09) !important;
                border-top: 0 !important;
            }
            .navbar-brand .icon {
                background: none !important;
            }
            .navbar-nav > li.dropdown .dropdown-menu a:hover {
                    background: rgba(255, 255, 255, 0.02) !important;
                    color: #989ea8;
            }
            .reviewIn{
                height: 44px !important;
                box-shadow: none !important;
                font-size: 15px !important;
                padding: 8px 10px 5px !important;
                color: #333 !important;
            }
            .reviewIn:focus {
                border-color: #51BAD6;
                box-shadow: 0 0 5px rgba(255, 255, 255, 0.4) !important;
            }
            .reviewBox{
                width: 100%;
            }
            .premiumBox h2{
               color: #e0e8eb;
               font-size: 22px;
            }
            .newH3{
               color: #989ea8;
               font-size: 20px;
               font-weight: 400;
            }
            .hisData {
                background: #fff none repeat scroll 0 0;
                border: 1px solid #e3e3e3;
                border-radius: 4px;
                margin-bottom: 20px;
                padding: 0 !important;
            }
            .hisData .titleBox {
                background: #f3f3f5 none repeat scroll 0 0;
                border-bottom: 1px solid #d7dbde;
                border-top-left-radius: 4px;
                border-top-right-radius: 4px;
                color: #2c3e50;
                font-size: 15px;
                font-weight: bold;
                padding: 15px 50px 15px 20px;
                position: relative;
                text-transform: uppercase;
            }
            .hisUl{
                list-style: outside none none;
                margin: 0 !important; 
                padding: 0 !important;
            }
            .hisUl li{
                border-bottom: 1px solid #dcdcdc;
                list-style: outside none none;
                margin: 0 !important; 
                padding: 0 !important;
            }
            .hisUl li a {
                color: #1f313f;
                display: block;
                padding: 10px 20px;
                position: relative;
            }
            .hisUl li a:hover {
                background: #fbfcfc none repeat scroll 0 0;
                color: #ec4060;
            }
            .favicon {
                margin-right: 8px;
                vertical-align: -2px;
            }
        </style>
        <?php if($themeOptions['custom']['css'] != '') echo '<style>'.htmlPrint($themeOptions['custom']['css'],true).'</style>'; ?>
    </head>

<body>   
   <nav class="navbar navbar-default navbar-static-top" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#collapse-menu">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php createLink(); ?>">
                        <?php echo $themeOptions['general']['themeLogo']; ?>
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="collapse-menu">
                    <ul class="nav navbar-nav navbar-right">
                        <?php 
                            foreach($headerLinks as $headerLink)
                                echo $headerLink[1];
                            
                            if(file_exists(CON_DIR."premium.php")){  if(!isset($_SESSION[N_APP.'premiumClient'])){ ?>
                            <li <?php if($controller == "premium") echo 'class="active"'; ?>> 
                                <a href="<?php createLink('premium'); ?>"><?php trans('Pricing', $lang['AD868']); ?></a>
                            </li>
                            <?php } else { ?>
                            <script>$(document).ready(function() {  $(".dropdown-toggle").dropdown(); });</script>    
                            <li class="dropdown">
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle"><?php trans('Account', $lang['AD869']); ?> <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php createLink('my-dashboard'); ?>"><?php trans('Dashboard', $lang['AD870']); ?></a></li>
                                    <li><a href="<?php createLink('my-profile'); ?>"><?php trans('Profile', $lang['AD871']); ?></a></li>
                                    <li><a href="<?php createLink('settings'); ?>"><?php trans('PDF Settings', $lang['AD872']); ?></a></li>
                                    <li><a href="<?php createLink('my-subscription'); ?>"><?php trans('Subscription', $lang['AD873']); ?></a></li>
                                    <li><a href="<?php createLink('my-invoice'); ?>"><?php trans('My Invoices', $lang['AD874']); ?></a></li>
                                </ul>
                            </li>
                            <?php } }
                            echo $loginNav; 
                            if(isSelected($themeOptions['general']['langSwitch'])){ ?>
        					<li class="dropdown">
        						<a href="javascript:void(0)" data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false"><i class="fa fa-globe fa-lg"></i> &nbsp; <?php echo strtoupper(ACTIVE_LANG); ?></a>
        						<ul class="dropdown-menu">
                                    <?php foreach($loadedLanguages as $language){
        							      echo '<li><a href="'.customLangLink($_SERVER["REQUEST_URI"], $language[2], $subPath, true).'">'.$language[3].'</a></li>';
                                    }?>
        						</ul>
        					</li>
                           <?php } ?>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container -->
    </nav><!--/.navbar-->  

<?php  if($controller == "main"){ ?>
<div class="masthead">
    <div class="container">
        <div class="row">

        <div class="col-md-6 seobannerBig">
        
            <div class="premiumBox">
            <h1 class="seobannerh1"><?php trans('Instant Website Review', $lang['AD875']); ?></h1>
            
            <h2><?php trans('From SEO to Digital Marketing, using our site to analyze and optimize unlimited websites.', $lang['AD876']); ?></h2>
            <form method="POST" action="<?php createLink('domain'); ?>" onsubmit="return fixURL();">
            <div class="input-group reviewBox">
                <div class="input-container">
                    <input type="text" tabindex="1" placeholder="<?php echo $lang['AD865']; ?>" id="url" name="url" class="form-control reviewIn"/>
                </div>
                
                <div class="input-group-btn">
                    <button tabindex="2" type="submit" name="generate" class="btn btn-primary btn-lg">
                        <span class="ready"><?php echo isset($_SESSION[N_APP.'premiumToken']) ? $lang['AD866'] : $lang['AD877']; ?></span>
                    </button>
                </div>
            </div>
            </form>
            </div>
        </div>
        
        <div class="col-md-6">
            <img class="visible-lg visible-md" alt="<?php echo $lang['317']; ?>" src="<?php themeLink('img/seobanner.png'); ?>" />
        </div>

        </div>            
    </div>
</div>
<?php } else { ?>
<div class="submasthead">
<div class="container">

        <div class="col-md-6 seobannerSmall">
        
            <h1 class="sub_seobannerh1"><?php echo $pageTitle; ?></h1>

        </div>
        
        <div class="col-md-6">
            <img class="visible-lg visible-md" alt="<?php echo $lang['317']; ?>" src="<?php themeLink('img/seobanner_mini.png'); ?>" />
        </div>
        

</div>
</div>
<?php } if(isSelected($other['other']['maintenance'])){ ?>
    <div class="alert alert-error text-center" style="margin: 35px 140px -10px 140px;">
    <strong>Alert!</strong> &nbsp; Your website is currently set to be closed.
    </div>
<?php } ?>