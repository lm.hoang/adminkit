<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name A to Z SEO Tools - PHP Script
 * @copyright © 2017 ProThemes.Biz
 *
 */

?> 
    <section class="section-toUse <?php echo ($data['toUse_show'] != 'yes') ? 'section-hidden':''; ?>">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="wrap-use">
                        <div class="box-text">
                            <h2 class="title-use">How to Use</h2>
                            <div class="info-use">
                                <p><?php echo htmlspecialchars_decode($data['about_toUse']); ?></p>
                            </div>
                        </div>
                        <?php $steps = unserialize($data['toUses']);?>
                            <div class="owl-slide-use owl-carousel owl-theme owl-loaded owl-drag">
                                <div class="owl-stage-outer">
                                <?php if ($steps[0] == '' ): ?>
                                    <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1687px;">
                                        <div class="owl-item active" style="width: 386.667px; margin-right: 35px;">
                                            <div class="item">
                                                <div class="icon-box">
                                                    <div class="box-images">
                                                        <img src="../images/To_use_1.png" class="img-use" alt="">
                                                        <p class="step-use">STEP 1</p>
                                                        <h2 class="headline-use">Choose a suitable tool</h2>
                                                    </div>
                                                    <div class="box-text">
                                                        <p class="description-use">Choose a tool that fits your purpose in our tools above</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="owl-item active" style="width: 386.667px; margin-right: 35px;">
                                            <div class="item">
                                                <div class="icon-box">
                                                    <div class="box-images">
                                                            <img src="../images/To_use_2.png" class="img-use" alt="">
                                                            <p class="step-use">STEP 2</p>
                                                            <h2 class="headline-use">Read description</h2>
                                                    </div>
                                                    <div class="box-text">
                                                        <p class="description-use">Read all of the descriptions we wrote below each tool to better understand the tool you are using</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="owl-item active" style="width: 386.667px; margin-right: 35px;">
                                            <div class="item">
                                                <div class="icon-box">
                                                    <div class="box-images">
                                                            <img src="../images/To_use_3.png" class="img-use" alt="">
                                                            <p class="step-use">STEP 3</p>
                                                            <h2 class="headline-use">Check and monitor</h2>
                                                    </div>
                                                    <div class="box-text">
                                                        <p class="description-use">Enter your Domain Name or IP address into the tool you want to check. Then press enter or the test buttons and you're done</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="owl-item" style="width: 386.667px; margin-right: 35px;">
                                            <div class="item">
                                                <div class="icon-box">
                                                    <div class="box-images">
                                                            <img src="../images/To_use_4.png" class="img-use" alt="">
                                                            <p class="step-use">STEP 4</p>
                                                            <h2 class="headline-use">Choose to add another tool</h2>
                                                    </div>
                                                    <div class="box-text">
                                                        <p class="description-use">Combine the tool you just selected with some of our others for the best results</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1687px;">
                                        <?php foreach( $steps as $key => $step ): ?>
                                        <div class="owl-item active" style="width: 386.667px; margin-right: 35px;">
                                            <div class="item">
                                                <div class="icon-box">
                                                    <div class="box-images">
                                                        <a href="#">
                                                            <img src="<?php echo $baseURL.$steps[$key]['image']; ?>" class="img-use" alt="">
                                                            <p class="step-use"><?php echo $steps[$key]['title']; ?></p>
                                                            <h2 class="headline-use"><?php echo $steps[$key]['short']; ?></h2>
                                                        </a>
                                                    </div>
                                                    <div class="box-text">
                                                        <p class="description-use"><?php echo $steps[$key]['detail']; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endforeach;?>
                                    </div>
                                <?php endif; ?>
                                <div class="owl-nav">
                                    <button type="button" role="presentation" class="owl-prev disabled"><span aria-label="Previous"></span></button>
                                    <button type="button" role="presentation" class="owl-next"><span aria-label="Next"></span></button>
                                </div>
                                <div class="owl-dots disabled"> </div>
                            </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
<style type="text/css">
    .section-hidden {
        display: none;
    }
</style>