<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name A to Z SEO Tools - PHP Script
 * @copyright � 2017 ProThemes.Biz
 *
 */

?>
<section class="section-benefit">
    <div class="container">
        <div class="row">
            <div class="box-text">
                <h2 class="title-benefit">The Benefit</h2>
                <p class="info-benefit"><?php echo htmlspecialchars_decode($data['about_benefit']); ?></p>
            </div>
            <?php $datas = unserialize($data['bnfs']);?>
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="wrap-benefit">
                    <?php if ($datas != '' ):?>
                        <div class="grid">
                            <?php foreach( $datas as $key => $dt ): ?>
                                <div class="gir-item">
                                    <div class="icon-box">
                                        <div class="box-images">
                                            <a href="#">
                                                <img src="<?php echo $baseURL.$datas[$key]['image']; ?>" alt="check if server ip is blacklisted">
                                                <h4 class="txt-title"><?php echo $datas[$key]['name']; ?></h4>
                                            </a>
                                        </div>
                                        <div class="box-text">
                                            <p class="info-txt"><?php echo $datas[$key]['des']; ?></p>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach;?>
                        </div>
                    <?php else: ?>
                        <div class="grid">
                            <div class="gir-item">
                                <div class="icon-box">
                                    <div class="box-images">
                                        <a href="#">
                                            <img src="/theme/default/img/icon.jpg" alt="check if server ip is blacklisted">
                                            <h4 class="txt-title">BlackList Check</h4>
                                        </a>
                                    </div>
                                    <div class="box-text">
                                        <p class="info-txt">Check to see if your hostname or IP addresses are listed on major anti-spam databases (blacklists). </p>
                                    </div>
                                </div>
                            </div>
                            <div class="gir-item">
                                <div class="icon-box">
                                    <div class="box-images">
                                        <a href="#">
                                            <img src="/theme/default/img/icon.jpg" alt="check if server ip is blacklisted">
                                            <h4 class="txt-title">BlackList Check</h4>
                                        </a>
                                    </div>
                                    <div class="box-text">
                                        <p class="info-txt">Check to see if your hostname or IP addresses are listed on major anti-spam databases (blacklists). </p>
                                    </div>
                                </div>
                            </div>
                            <div class="gir-item">
                                <div class="icon-box">
                                    <div class="box-images">
                                        <a href="#">
                                            <img src="/theme/default/img/icon.jpg" alt="check if server ip is blacklisted">
                                            <h4 class="txt-title">BlackList Check</h4>
                                        </a>
                                    </div>
                                    <div class="box-text">
                                        <p class="info-txt">Check to see if your hostname or IP addresses are listed on major anti-spam databases (blacklists). </p>
                                    </div>
                                </div>
                            </div>
                            <div class="gir-item">
                                <div class="icon-box">
                                    <div class="box-images">
                                        <a href="#">
                                            <img src="/theme/default/img/icon.jpg" alt="check if server ip is blacklisted">
                                            <h4 class="txt-title">BlackList Check</h4>
                                        </a>
                                    </div>
                                    <div class="box-text">
                                        <p class="info-txt">Check to see if your hostname or IP addresses are listed on major anti-spam databases (blacklists). </p>
                                    </div>
                                </div>
                            </div>
                            <div class="gir-item">
                                <div class="icon-box">
                                    <div class="box-images">
                                        <a href="#">
                                            <img src="/theme/default/img/icon.jpg" alt="check if server ip is blacklisted">
                                            <h4 class="txt-title">BlackList Check</h4>
                                        </a>
                                    </div>
                                    <div class="box-text">
                                        <p class="info-txt">Check to see if your hostname or IP addresses are listed on major anti-spam databases (blacklists). </p>
                                    </div>
                                </div>
                            </div>
                            <div class="gir-item">
                                <div class="icon-box">
                                    <div class="box-images">
                                        <a href="#">
                                            <img src="/theme/default/img/icon.jpg" alt="check if server ip is blacklisted">
                                            <h4 class="txt-title">BlackList Check</h4>
                                        </a>
                                    </div>
                                    <div class="box-text">
                                        <p class="info-txt">Check to see if your hostname or IP addresses are listed on major anti-spam databases (blacklists). </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
</section>