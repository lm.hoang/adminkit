<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
/*
* @author Balaji
* @name A to Z SEO Tools - PHP Script
* @copyright © 2017 ProThemes.Biz
*
*/
?>
<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
$name = raino_trim($_POST['contact_name']);
$from = $replyTo = raino_trim($_POST['contact_email']);
$sub = raino_trim($_POST['contact_sub']);
$message = raino_trim($_POST['contact_message']);
// $company = raino_trim($_POST['contact_comp']);
// $company_size = raino_trim($_POST['contact_comp_size']);
$url = raino_trim($_POST['contact_url']);
$type = raino_trim($_POST['contact_type']);
$budget = raino_trim($_POST['contact_budget']);
$nowDate = date('m/d/Y h:i:sA');
if(isset($_FILES['contact_file']) && $_FILES['contact_file']['name'] != ''){
	$files = $_FILES['contact_file'];
	}
if(isset($_SESSION[N_APP.'Username']))
$username = $_SESSION[N_APP.'Username'];
else
$username = $lang['RF26'];
if(!isset($error)){
//No Error - Continue
if ($name != null && $replyTo != null && $sub != null && $message != null && $adminEmail != null){
	if($type == '3' || $type == '4'){
		if($type == '3'){
			$for = 'I want to submit a request';
			$bud = 'Unknown';
		} else if($type == '4'){
			$for = 'Other';
		}
	} else if ($type == '1' || $type == '2'){
	if ($type == '2'){
		$for = 'I want to ask about Adminkit';
		$bud = 'Unknown';
	} else if ($type == '1'){
		$for = 'I need website optimization service';
		switch ($budget) {
			case 'u500':
				$bud = 'Under $500 USD';
				break;
			case '500':
				$bud = '$500 - $1,000 USD';
				break;
			case 'o1000':
				$bud = 'Over $1,000 USD';
				break;
			default:
				$bud = 'Unknown';
				break;
		}
	}
	}
$htmlMessage = file_get_contents(THEME_DIR.'includes/notify-temp.html');
$htmlMessage = str_replace('%nowDate%', $nowDate, $htmlMessage);
			$htmlMessage = str_replace('%admin_mail%', $adminEmail, $htmlMessage);
			$htmlMessage = str_replace('%contact_name%', $name, $htmlMessage);
			$htmlMessage = str_replace('%contact_mail%', $replyTo, $htmlMessage);
			$htmlMessage = str_replace('%for%', $for, $htmlMessage);
			$htmlMessage = str_replace('%message%', $message, $htmlMessage);
			$htmlMessage = str_replace('%username%', $username, $htmlMessage);
			// $htmlMessage = str_replace('%company%', $company, $htmlMessage);
			// $htmlMessage = str_replace('%comp_size%', $comp_size, $htmlMessage);
			$htmlMessage = str_replace('%url%', $url, $htmlMessage);
			$htmlMessage = str_replace('%budget%', $bud, $htmlMessage);
//Load Mail Settings
extract(loadMailSettings($con));
if($protocol == '1'){
//PHP Mail
if(default_mail($adminEmail, $name, $replyTo, $name, $adminEmail, $sub, $htmlMessage,$files)){
//Your message has been sent successfully
$success = $lang['RF27'];
$htmlMessage = file_get_contents(THEME_DIR.'includes/thank-temp.html');
	$htmlMessage = str_replace('%nowDate%', $nowDate, $htmlMessage);
					$htmlMessage = str_replace('%contact_name%', $name, $htmlMessage);
					$htmlMessage = str_replace('%contact_mail%', $replyTo, $htmlMessage);
					$htmlMessage = str_replace('%for%', $for, $htmlMessage);
					$htmlMessage = str_replace('%message%', $message, $htmlMessage);
default_mail($from, $site_name, $replyTo, $site_name, $replyTo, $sub, $htmlMessage,$files);
$message = $body = $from = $name = $sub = '';
}else{
//Failed to send your message
$error = $lang['RF28'];
}
}else{
//SMTP Mail
if(smtp_mail($smtp_host, $smtp_port, isSelected($smtp_auth), $smtp_username, $smtp_password, $smtp_socket,
$adminEmail, $name, $replyTo, $name, $adminEmail, $sub, $htmlMessage,$files)){
//Your message has been sent successfully
$success = $lang['RF27'];
	$htmlMessage = file_get_contents(THEME_DIR.'includes/thank-temp.html');
	$htmlMessage = str_replace('%nowDate%', $nowDate, $htmlMessage);
					$htmlMessage = str_replace('%contact_name%', $name, $htmlMessage);
					$htmlMessage = str_replace('%contact_mail%', $replyTo, $htmlMessage);
					$htmlMessage = str_replace('%for%', $for, $htmlMessage);
					$htmlMessage = str_replace('%message%', $message, $htmlMessage);
	smtp_mail($smtp_host, $smtp_port, isSelected($smtp_auth), $smtp_username, $smtp_password, $smtp_socket,$from, $name, $replyTo, $name, $replyTo, $sub, $htmlMessage,$files);
$message = $body = $from = $name = $sub = '';
}else{
//Failed to send your message
$error = $lang['RF28'];
}
}
}else{
$error = $lang['RF25'];
}
}
}
?>
<section class="section-popup">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12">
				<div class="box-text">
					<h2 class="title-benefit">We'd love to hear from you</h2>
					<p class="note">Find a solution to solve the problem, contact US immediately</p>
					<div class="button-wrapper">
						<a class="support-button"><span>SUBMIT A REQUEST</span></a>
					</div>
				</div>
				<div class="modal-popup">
					<div class="bg-popup"></div>
					<div class="popup-content">
						<div class="popup-wrapper">
							<div class="form-contact-us">
								<div class="container">
									<div class="row">
										<div class="col-12 col-sm-12 col-md-12">
											<div class="box-text">
												<div class="page-form-content">
													<h3 class="txt-form-type">
													How can we help you?
													</h3>
													<form id="contact-form-nb" method="post" action="#" class="contact-form-nb" enctype="multipart/form-data">
														<div class="row">
															<div class="col-12 col-sm-12 col-md-12">
																<div class="form-group">
																	<div class="box-text">
																		<select class="sl-type-contact" id="sl-type-contact" name="contact_type">
																			<option class="default" value="0" selected>Please select an option for you</option>
																			<option value="1">&nbsp;&nbsp;I need website optimization service</option>
																			<option value="2">&nbsp;&nbsp;I want to ask about Adminkit</option>
																			<option value="3">&nbsp;&nbsp;I want to submit a request</option>
																			<option value="4">&nbsp;&nbsp;I would like to contact the consultant directly</option>
																		</select>
																	</div>
																</div>
															</div>
															<div class="col-12 col-sm-12 col-md-12">
																<div class="row form-body">
																	<div class="col-12 col-sm-12 col-md-12 col-lg-6">
																		<div class="form-group">
																			<!-- <label for="contact_name"><?php trans('Name *',$lang['RF21']); ?></label> -->
																			<input value="<?php echo $name; ?>" id="contact_name" type="text" name="contact_name" class="form-control" placeholder="Name*" data-error="<?php trans('Fullname is required',$lang['RF12']); ?>" />
																			<div class="help-block with-errors"></div>
																		</div>
																	</div>
																	<div class="col-12 col-sm-12 col-md-12 col-lg-6" style="">
																		<div class="form-group">
																			<!-- <label for="contact_email"><?php trans('Email *',$lang['RF22']); ?></label> -->
																			<input value="<?php echo $from; ?>" id="contact_email" type="email" name="contact_email" class="form-control" required="required" placeholder="Email*" data-error="<?php trans('Valid email is required',$lang['RF14']); ?>" />
																			<div class="help-block with-errors"></div>
																		</div>
																	</div>
																	<div class="col-12 col-sm-6 col-md-6 optional">
																		<div class="form-group">
																			<!-- <label for="contact_url">URL</label> -->
																			<input value="" id="contact_url" type="text" name="contact_url" class="form-control" placeholder="URL" data-error="Valid URL is required" />
																			<div class="help-block with-errors"></div>
																		</div>
																	</div>
																	<div class="col-12 col-sm-6 col-md-6 optional">
																		<div class="field field-budget form-group">
																			<select class="sl-budget" name="contact_budget">
																				<option value="0" disabled selected="">Please select your working budget…</option>
																				<option value="u500">Under $500 USD</option>
																				<option value="500">$500 - $1,000 USD</option>
																				<option value="o1000">Over $1,000 USD</option>
																			</select>
																		</div>
																	</div>
																	<div class="col-12 col-sm-12 col-md-12">
																		<div class="form-group">
																			<!-- <label for="contact_sub"><?php trans('Subject *',$lang['RF23']); ?></label> -->
																			<input value="<?php echo $sub; ?>" id="contact_sub" type="text" name="contact_sub" class="form-control" placeholder="Subject" data-error="<?php trans('Subject is required',$lang['RF15']); ?>" />
																			<div class="help-block with-errors"></div>
																		</div>
																	</div>
																	<div class="col-12 col-sm-12 col-md-12" style="">
																		<div class="form-group">
																			<!-- <label for="contact_message"><?php trans('Message *',$lang['RF24']); ?></label> -->
																			<textarea id="contact_message" name="contact_message" class="form-control" rows="4" required="required" placeholder="Message*" data-error="<?php trans('Please leave some message',$lang['RF18']); ?>"><?php echo $message; ?></textarea>
																			<div class="help-block with-errors"></div>
																		</div>
																	</div>
																	<div class="col-12 col-sm-12 col-md-12" style="">
																		<div class="form-group">
																			<div class="file-upload">
																				<!-- <label>Attached File</label> -->
																				<!-- <input type="file" name="contact_file" id='contact_file' class="nbCustom-file-input"> -->
																				<label for="contact_file" class="form-control contact_file">
																				    <i class="fa fa-paperclip" aria-hidden="true"></i>Attach a file (optional)
																				</label>
																				<input id="contact_file" name='contact_file' type="file" style="display:none;">
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-12 col-sm-12 col-md-12">
																<div class="row">
																	<div class="col-12 col-sm-12 col-md-12">
																		<div class="form-direct" style="display: none;">
																			<div class="project-inner consultant">
																				<div class="consultant-inner custom-consultant-inner">
																					<h3>Consultant team</h3>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col-12 col-sm-6 col-md-6">
																		<div class="form-direct" style="display: none;">
																			<div class="project-inner consultant">
																				<div class="consultant-inner custom-consultant-inner">
																					<div class="icon-box">
																						<div class="box-images">
																							<img class="img-responsive" src="https://d151fqqb1tev7m.cloudfront.net/images/banners/consultant01.jpg" alt="Ms. Janet Tran">
																						</div>
																						<div class="box-text">
																							<div class="txt-consultant">
																								<p>Ms. Janet Tran</p>
																								<p><span>Skype:</span> janet@cmsmart.net</p>
																								<p><span>Email:</span> janet@cmsmart.net</p>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="col-12 col-sm-6 col-md-6">
																		<div class="form-direct" style="display: none;">
																			<div class="project-inner consultant">
																				<div class="consultant-inner custom-consultant-inner">
																					<div class="icon-box">
																						<div class="box-images">
																							<img class="img-responsive" src="https://d151fqqb1tev7m.cloudfront.net/images/banners/consultant02.png" alt="Mr. Vincent Ray">
																						</div>
																						<div class="box-text">
																							<div class="txt-consultant">
																								<p>Mr. Vincent Ray</p>
																								<p><span>Skype:</span> live:vincent_4281</p>
																								<p><span>Email:</span> vincent@cmsmart.net</p>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<div class="col-12 col-sm-12 col-md-12 submit-box">
																<div class="box-text">
																	<div class="form-submit-nb">
																		<button type="submit" class="btn-submit-nb">Send</button>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="close">
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
							<path d="M6.692 6L10.5 9.808l-.692.692L6 6.692 2.192 10.5 1.5 9.808 5.308 6 1.5 2.192l.692-.692L6 5.308 9.808 1.5l.692.692z"></path></svg>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</section>
<div id="success-popup" class="success-popup">
	<div class="bg-popup"></div>
	<div class="popup-content">
		<div class="popup-wrapper">
			<div class="alert alert-success">
			    <div id="stage" class="form-all">
					<p style="text-align:center;"><img src="https://cdn.jotfor.ms/img/check-icon.png" alt="" width="128" height="128"></p>
					<p style="text-align:center;">&nbsp;</p>
					<div style="text-align:center;">
						<h1 style="text-align:center;"><span style="font-size:36pt;">Thank You!</span></h1>
						<p style="color:#4c4c4c;font-family:Roboto, 'Helvetica Neue', 'Arial Narrow', sans-serif;font-size:22px;text-align:left;"><span style="font-size:12pt;">We have received your message and would like to thank you for writing to us. If your inquiry is urgent, please use the Skype, Email listed below to talk to one of our staff members. Otherwise, we will reply by email as soon as possible.</span></p>
						<p style="color:#4c4c4c;font-family:Roboto, 'Helvetica Neue', 'Arial Narrow', sans-serif;font-size:22px;text-align:left;">Talk to you soon,&nbsp;</p>
					</div>
					<div class="col-12 col-sm-12 col-md-12">
						<div class="row">
							<div class="col-12 col-sm-12 col-md-12">
								<div class="form-direct">
									<div class="project-inner consultant">
										<div class="consultant-inner custom-consultant-inner">
											<h3 style="text-align:left;">&nbsp;</h3>
											<h3 style="text-align:left;">Consultant team</h3>
										</div>
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-6 col-md-6" style="text-align:left;">
								<div class="form-direct">
									<div class="project-inner consultant">
										<div class="consultant-inner custom-consultant-inner">
											<div class="icon-box">
												<div class="box-images">&nbsp;<img class="img-responsive" src="https://d151fqqb1tev7m.cloudfront.net/images/banners/consultant01.jpg" alt="Ms. Janet Tran">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<img class="img-responsive" src="https://d151fqqb1tev7m.cloudfront.net/images/banners/consultant02.png" alt="Mr. Vincent Ray"></div>
												<div class="box-text">
													<div class="txt-consultant">
														<p>Ms. Janet Tran&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Mr. Vincent Ray</p>
														<p>Skype: <a href="mailto:janet@cmsmart.net" target="_blank">janet@cmsmart.net</a>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Skype: live:vincent_4281&nbsp;</p>
														<p>Email: <a href="mailto:janet@cmsmart.net" target="_blank">janet@cmsmart.net</a>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Email: <a href="mailto:vincent@cmsmart.net" target="_blank">vincent@cmsmart.net</a></p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-12 col-sm-6 col-md-6">
								<div class="form-direct">
									<div class="project-inner consultant">
										<div class="consultant-inner custom-consultant-inner">
											<div class="icon-box">
												<div class="box-images" style="text-align:left;">&nbsp;</div>
												<div class="box-text">
													<div class="txt-consultant">
														<p style="text-align:left;">&nbsp;</p>
														<p style="text-align:left;">&nbsp;</p>
														<p style="text-align:left;">&nbsp;</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
	        </div>
        </div>
        <div class="close-succ">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
			<path d="M6.692 6L10.5 9.808l-.692.692L6 6.692 2.192 10.5 1.5 9.808 5.308 6 1.5 2.192l.692-.692L6 5.308 9.808 1.5l.692.692z"></path></svg>
		</div>
	</div>
</div>
<style type="text/css">
	.section-popup {
		padding: 100px 0;
		background-color: #dee2e6;
		position: relative;
		/*z-index: 99999;*/
		display: inline-block;
		width: 100%;
	}
	.txt-consultant {
		width: 100%;
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
		padding: 0 15px;
	}
	.txt-consultant p {
		font-family: Roboto;
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		line-height: 24px;
		text-align: left;
		color: #666666;
	}
	.txt-consultant p:nth-child(1) {
		font-weight: bold;
		cursor: pointer;
	}
	.txt-consultant p:nth-child(n+2) {
		font-size: 14px;
	}
	.txt-consultant p span {
		font-weight: bold;
		cursor: pointer;
	}
	.custom-consultant-inner .box-text {
		text-align: left;
		width: 100%;
		position: relative;
	}
	.section-popup .note {
		font-weight: 400;
		font-size: 16px;
		line-height: 24px;
		color: #333;
		text-align: center;
		margin-right: auto;
		margin-left: auto;
		margin-top: 30px;
	}
	.section-popup .button-wrapper {
		text-align: center;
	}
	.section-popup .suggest-button, .section-popup .support-button {
		border: 2px solid #778088;
		line-height: 44px;
		padding: 0 15px;
		font-size: 16px;
		font-weight: 700;
		text-transform: uppercase;
		border-radius: 5px;
		text-align: center;
		color: #333;
		letter-spacing: .1em;
		-webkit-transition-property: color;
		-o-transition-property: color;
		transition-property: color;
		-webkit-transition-duration: .2s;
		-o-transition-duration: .2s;
		transition-duration: .2s;
		cursor: pointer;
		display: inline-block;
		margin: 20px 8px 0;
	}
	 .support-button span {
	   cursor: pointer;
	   display: inline-block;
	   position: relative;
	   transition: 0.4s;
	 }
	 .support-button span:after {
	   content: '\00bb';
	   position: absolute;
	   opacity: 0;
	   top: 0;
	   right: -20px;
	   transition: 0.5s;
	 }
	 .support-button:hover span {
	   padding-right: 25px;
	 }
	 .support-button:hover span:after {
	   opacity: 1;
	   right: 0;
	 }
	.modal-popup {
		display: none;
		position: fixed;
		z-index: 600;
		top: 0px;
		left: 0px;
		flex-direction: column;
		-webkit-box-pack: center;
		justify-content: center;
		-webkit-box-align: center;
		align-items: center;
		width: 100%;
		height: 100%;
	}
	.popup-content {
		position: relative;
/*		max-height: calc(100vh - 72px);*/
		max-width: 950px;
		box-shadow: rgb(26 26 26 / 20%) 0px 0px 24px;
		display: flex;
		flex-direction: column;
		z-index: 600;
		background-color: rgb(255, 255, 255);
		box-sizing: content-box;
		overflow: hidden;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
		border-radius: 5px;
	}
	.bg-popup {
		position: fixed;
		z-index: 1;
		background-color: rgba(0, 0, 0, 0.4);
		inset: 0px;
	}
	.close {
		cursor: pointer;
		color: rgb(33, 33, 33);
		position: absolute;
		top: 0px;
		right: 0px;
		padding: 12px;
		box-sizing: content-box;
	}
	.close > svg {
		display: flex;
		flex: 1 1 auto;
		width: 20px;
		height: 20px;
	}
	.close:hover,.close:focus {
		color: #333;
		text-decoration: none;
		cursor: pointer;
	}
	.popup-wrapper {
		padding: 50px;
	}
	.consultant{
		text-align: left;
	}
	.consultant h3 {
		text-align: left;
		font-size: 20px;
		font-weight: 600;
		color: #333;
		padding-bottom: 15px;
		font-family: 'Roboto';
	}
	.custom-consultant-inner {
		width: 100%;
		display: flex;
	}
	.custom-consultant-inner .icon-box{
		width: 100%;
		display: flex;
		padding: 9px 15px;
		margin-bottom: 15px;
		border-radius: 5px;
		background: #FFFFFF;
		box-shadow: 0px 4px 30px rgb(28 28 28 / 10%);
		position: relative;
	}
	.txt-form-type {
		margin-bottom: 40px;
	font-family: Roboto;
	font-style: normal;
	font-weight: 500;
	font-size: 18px;
	line-height: 26px;
	color: #333333;
	}
	.sl-type-contact:focus {
	border-color: unset;
	color: #444;
	}
	.sl-type-contact option:nth-child(1) {
		color: #afafaf;
	}
	.sl-type-contact option:nth-child(n+2) {
		padding-left: 15px;
	}
	.contact-form-nb label {
		cursor: pointer;
		font-size: 16px;
		font-weight: 400;
		font-family: 'Roboto';
		margin-bottom: 2px;
		color: #646464;
		display: block;
	}
	.contact-form-nb label i {
		padding-right: 10px;
	}
	.form-required {
		color: #c72525;
		padding-left: 2px;
	}
	.contact-form-nb input[type="text"],
	.contact-form-nb select,
	.contact-form-nb textarea {
		width: 100%;
	}
	.sl-budget:focus  {
		border-color: unset;
	color: #444;
	}
	.contact-form-nb textarea {
		display: block;
		resize: vertical;
	}
	.btn-submit-nb {
		border: none;
		font-size: 22px;
		font-weight: 500;
		padding: 10px 30px 12px;
		background: #2E68B2;
		color: #fff;
		transition: background-color .25s ease-out;
		outline: none;
		border-radius: 5px;
	}
	.btn-submit-nb:focus {
		outline: none;
	}
	.btn-submit-nb:hover {
		background: #2e7db4;
	}
	.form-submit-nb {
		margin-top: 16px;
		text-align: left;
	}
	.nbCustom-file-input {
	color: transparent;
	width: 100%;
	position: relative;
	}
	.nbCustom-file-input:after {
		content: "\f0c6";
		font-family: 'FontAwesome';
		width: 10px;
		display: inline-block;
		position: absolute;
		color: #999;
		font-size: 18px;
		left: 25px;
		top: 50%;
		transform: translate(-50%, -50%);
	}
	.nbCustom-file-input::-webkit-file-upload-button {
	visibility: hidden;
	}
	.nbCustom-file-input::before {
	content: 'Attach a file (optional)';
	color: #999;
	display: inline-block;
	background: #F4F7F8;
	border-radius: 5px;
	line-height: 22px;
	outline: none;
	white-space: nowrap;
	-webkit-user-select: none;
	cursor: pointer;
	text-shadow: 1px 1px #fff;
	font-weight: normal;
	font-size: 14px;
	padding: 9px 50px;
	font-family: 'Roboto';
	border: none;
	width: 100%;
	}
	.nbCustom-file-input:active {
	outline: 0;
	}
	.nbCustom-file-input:active::before {
	background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9);
	}
	input[type=file]:focus {
		outline: none;
	}
	.img-responsive {
		border-radius: 5px;
	}
	@media only screen and (max-width: 1200px){
		.popup-content {
			width: 80%;
			overflow: hidden auto;
		}
	}
	@media only screen and (max-width: 1100px){
		.popup-wrapper {
		    padding: 20px;
		}
		.popup-content {
			width: 70%;
		}
		.txt-form-type {
			margin-bottom: 20px;
		}
		.form-group {
    		margin-bottom: 10px;
		}
		.form-submit-nb {
    		margin-top: 10px;
    		text-align: left;
		}
	}
	@media only screen and (max-width: 991px){
		.custom-consultant-inner .icon-box {
			display: block;
		}
		.txt-consultant {
			position: relative;
			transform: unset;
			top: unset;
			left: unset;
			padding: unset;
			margin-top: 15px;
		}
		.img-responsive {
			margin: 0 auto;
		}
		.txt-consultant p {
			text-align: center;
		}
	}
	@media only screen and (max-width: 768px){
		.popup-wrap
		.popup-wraper {
		    padding: 15px;
		}
		.popup-content {
			width: 80%;
		}
	}
	@media only screen and (max-width: 414px){
		.popup-wrapper {
		    padding: 10px;
		}
		#myBtn {
			padding-top: 5px!important;
		}
		#myBtn:before {
			font-size: 10px;
		}
	}
	#success-popup {
		display: none;
		position: fixed;
		z-index: 600;
		top: 0px;
		left: 0px;
		flex-direction: column;
		-webkit-box-pack: center;
		justify-content: center;
		-webkit-box-align: center;
		align-items: center;
		width: 100%;
		height: 100%;
	}
	.close-succ {
		cursor: pointer;
		color: rgb(33, 33, 33);
		position: absolute;
		top: 0px;
		right: 0px;
		padding: 12px;
		box-sizing: content-box;
	}
	.close-succ > svg {
		display: flex;
		flex: 1 1 auto;
		width: 20px;
		height: 20px;
	}
	.close-succ:hover,.close-succ:focus {
		color: #333;
		text-decoration: none;
		cursor: pointer;
	}
</style>
<script >
	$('.js-hide').hide();
	$('#sl-type-contact').change(function() {
		var sl_key = $('#sl-type-contact').val();
		$("#sl-type-contact option[value='0']").attr("disabled", "disabled");
		switch (sl_key)
		{
			case '1' : {
				$('#contact-form-nb .form-body').show();
				$('#contact-form-nb .form-body .optional').show();
				$('#contact-form-nb .form-direct').hide();
				$('#contact-form-nb .submit-box').show();
			}
			break;
			case '2' :
			case '3' :{
				$('#contact-form-nb .form-body').show();
				$('#contact-form-nb .submit-box').show();
				$('#contact-form-nb .form-body .optional').hide();
				$('#contact-form-nb .form-direct').hide();
			}
			break;
			case '4' : {
				$('#contact-form-nb .form-body').hide();
				$('#contact-form-nb .form-direct').show();
				$('#contact-form-nb .submit-box').hide();
			}
			break;
			default : {
				$('#contact-form-nb .form-body').hide();
				$('#contact-form-nb .submit-box').hide();
			}
		}
	});
	$('.modal-popup').hide();
	$('.support-button').click(function () {
		$('.modal-popup').show();
		$('#sl-type-contact').val('3');
		$("#sl-type-contact option[value='0']").attr("disabled", "disabled");
		$('#contact-form-nb .form-body .optional').hide();
		$('html').css('overflow', 'hidden');
	});
	$('.close').click(function () {
		$('.modal-popup').hide();
		$('html').css('overflow', 'auto');
	});
	$(window).on('click', function (e) {
	if ($(e.target).is('.bg-popup')) {
	$('.modal-popup').hide();
	$('html').css('overflow', 'auto');
		}
	});
	$('#contact_file').change(function() {
	  var i = $(this).prev('label').clone();
	  var file = $('#contact_file')[0].files[0].name;
	  $(this).prev('label').text(file);
	  $('label.contact_file').css('color','#007bff');
	});
	//
	//$('.success-popup').hide();
	$('.close-succ').click(function () {
		$('.success-popup').hide();
	});
	<?php if($success):?>
		$('.success-popup').show();
		setTimeout(function(){
			$(".success-popup .close-succ").trigger("click"); 
		},2000);
	<?php endif; ?>
</script>