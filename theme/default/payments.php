<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name A to Z SEO Tools - PHP Script
 * @copyright © 2018 ProThemes.Biz
 *
 */
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta charset="utf-8" />
	<title><?php echo $pageTitle; ?></title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" />
</head>
<body onLoad="doPayment();">
<div class="container" style="margin-top: 100px;">
<br />
<hr />
<br />
    <div style="text-align: center;">
        <?php echo $themeOptions['general']['themeLogo']; ?>
        <?php if($type == 'cancel'){ ?>
            <h4 style="color: #c43b2d;"><?php trans('Transaction Cancelled', $lang['AD832']); ?></h4>
            <br/><img src="<?php themeLink('premium/img/loader.gif'); ?>" alt="<?php trans('Loader', $lang['AD830']); ?>" /><br/>
            <?php echo $paymentData; ?>  
        <?php }else if($type == 'process'){ ?>
            <h5><?php trans('Please wait, your order is being processed and you will be redirected to the', $lang['AD833']); ?> <?php echo $gatewayName; ?> <?php trans('website', $lang['AD834']); ?>.</h5>
            <br/><img src="<?php themeLink('premium/img/loader.gif'); ?>" alt="<?php trans('Loader', $lang['AD830']); ?>" /><br/>
            <?php echo $paymentData; ?>
        <?php }else if($type == 'success'){ ?>
            <h4 style="color: #27ae60;"><?php trans('Payment Transaction Done Successfully', $lang['AD831']); ?></h4>
            <br/><img src="<?php themeLink('premium/img/loader.gif'); ?>" alt="<?php trans('Loader', $lang['AD830']); ?>" /><br/>
            <?php echo $paymentData; ?> 
        <?php } ?>
    </div>
<br />
<hr />
<br />
</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>