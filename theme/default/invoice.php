<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
/*
 * @author Balaji
 * @name: A to Z SEO Tools - PHP Script
 * @Theme: Default Style
 * @copyright � 2018 ProThemes.Biz
 *
 */
?>
<link href="<?php themeLink('premium/css/premium.css'); ?>" rel="stylesheet" type="text/css" />

    <div class="container main-container">
        <div class="row">
      	    <?php
            if($themeOptions['general']['sidebar'] == 'left')
                require_once(THEME_DIR."sidebar.php");
            ?>
            <div class="col-md-8 main-index">
                  
                <?php if($activateMsg != '') { ?>
                <br />
                    <div class="alert alert-<?php echo $activateClass; ?> alert-dismissable alert-premium">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                    <?php echo $activateMsg; ?>
                    </div>
                <br />
                <?php } if($refreshMyUrl){ ?>
                    <meta http-equiv="refresh" content="10" />
                <?php } ?>
                                                
                <br />
                
                <div id="pageInvoice">
            	<div class="status <?php echo $invoiceClass; ?>">
           		<p><?php echo $invoiceStats; ?></p>
            	</div>
            		
            	<p class="recipient-address">
            	<strong><?php echo $lang['AD878']; ?>:</strong><br />
                <?php echo $userAddress; ?></p>
            	
            	<h1><?php echo $lang['AD879']; ?>: <?php echo $orderInfo['invoice_prefix']; ?></h1>
            	<p class="terms"><strong><?php trans('Date', $lang['AD887']); ?>: <?php echo $invoiceDate; ?></strong><br/>
           	    <?php echo $lang['AD804']; ?>: <?php echo $dueDate; ?></p>
            	
            	<img src="<?php echo $invoice_logo; ?>" alt="yourlogo" class="company-logo" />
            	<p class="company-address">
                        <?php echo html_entity_decode(strEOL($company_add)); ?>
                </p>
                
                <?php if($orderInfo['payment_status'] == 'pending'){ if($activateMsg == '') { ?>
                <form method="POST" action="#" style="margin-top: -20px; color: #a1a7ac; position: absolute; right: 40px; text-align: right; width: 200px;">
                    <strong class="payMethod"><?php echo $lang['AD880']; ?>:</strong><br/>
                    <select class="form-control select-inline" name="gateway">
                        <?php echo $paymentGatewayData; ?>
                    </select>
                    <input type="hidden" name="paynow" value="1" />
                    <button type="submit" class="btn btn-success btn-sm"><?php echo $lang['AD881']; ?></button>
                    <br /><br />
                </form>
                <?php } } ?>

            	<table id="table" class="tablesorter" cellspacing="0"> 
            	<thead> 
                <tr> 
            	    <th class="header"><?php echo $lang['AD882']; ?></th> 
            	    <th class="header"><?php echo $lang['AD883']; ?></th> 
            	    <th class="header right" style="width:22%;"><?php echo $lang['AD884']; ?></th> 
            	    <th class="header right" style="width:18%;"><?php echo $lang['AD885']; ?></th> 
            	</tr>
            	</thead> 
            	<tbody> 
            	<tr class="odd"> 
            	    <td>#<?php echo $orderInfo['plan_id']; ?></td> 
            	    <td><?php echo $orderInfo['plan_name']; ?></td> 
            	    <td class="right"><?php echo $currencySymbol[0].$amount; ?></td> 
            	    <td class="right"><?php echo $currencySymbol[0].$amount; ?></td> 
            	</tr> 
            	</tbody> 
            	</table> 
                <div id="invoiceTotals">
                    <table class="totalTable" style="width:100%;" cellspacing="0">
                        <tbody>
                        <tr>
                            <td>&nbsp;</td>
                            <td style="width:22%;" class="text-right"><?php echo $lang['AD886']; ?></td>
                            <td style="width:18%;" class="text-right"><?php echo $currencySymbol[0].$amount; ?></td>
                        </tr>
                        <?php foreach($taxArrData as $taxCurData){ ?>
                            <tr>
                                <td></td>
                                <td class="text-right"><?php echo $taxCurData[0]; ?></td>
                                <td class="text-right"><?php echo $currencySymbol[0].$taxCurData[1]; ?></td>
                            </tr>
                        <?php } ?>
                        
                        <tr class="invoiceTotal">
                            <td></td>
                            <td class="even text-right"><?php echo $lang['AD885']; ?></td>
                            <td class="even text-right"><?php echo $currencySymbol[0].$totalAmount.' '.$orderInfo['currency_type']; ?></td>
                        </tr>
                        
                        </tbody>
                    </table>
                </div>
                
            	<div class="total-due">
            		<div class="total-heading"><p><?php echo $lang['AD884']; ?> <?php echo $invoiceStats; ?></p></div>
            		<div class="total-amount"><p><?php echo $currencySymbol[0].$totalAmount; ?></p></div>
            	</div>
            	
            	<div class="invoiceClear"></div>
                <?php echo html_entity_decode(strEOL($invoice_footer)); ?>                 	
            </div>
                
            <br />

            </div>
            <?php
            if($themeOptions['general']['sidebar'] == 'right')
                require_once(THEME_DIR."sidebar.php");
            ?>    
        </div>
    </div>
    <br />