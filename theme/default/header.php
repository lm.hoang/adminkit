<?php
   defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
   /*
   * @author Balaji
   * @name: A to Z SEO Tools - PHP Script
   * @theme: Default Style
   * @copyright 2018 ProThemes.Biz
   *
   */
   ?>
<!DOCTYPE html>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta http-equiv="Content-Language" content="<?php echo (ACTIVE_LANG); ?>" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name ="author" content="adminkit.net"
      <link rel="icon" href="https://adminkit.net/favicon.ico" type="image/x-icon" /><link rel="shortcut icon" href="https://adminkit.net/favicon.ico" type="image/x-icon" />
      <!-- Meta Data-->
      <title>Seo Tools Adminkit - Improve the visibility of Website in search engines.</title>
      <meta property="site_name" content="<?php echo $site_name; ?>"/>
      <meta name="description" content="<?php echo $des; ?>" />
      <meta name="keywords" content="<?php echo $keyword; ?>" />
      <meta name="author" content="Balaji" />
      <!-- Open Graph -->
      <meta property="og:title" content="Seo Tools AdminKit - Improve the visibility of Website in search engines" />
      <meta property="og:site_name" content="<?php echo $site_name; ?>" />
      <meta property="og:type" content="website" />
      <meta property="og:description" content="<?php echo $des; ?>" />
      <meta property="og:image" content="<?php echo (substr($server_path,-1) == '/' ? substr($server_path,0,-1) : $server_path).$logo_path?>"/>
      <meta property="og:url" content="<?php echo $currentLink; ?>" />
      <?php genCanonicalData($baseURL, $currentLink, $loadedLanguages, false, isSelected($themeOptions['general']['langSwitch'])); ?>
      <!-- Main style -->
      <link href="<?php themeLink('css/theme.css'); ?>" rel="stylesheet" />
      <link href="<?php themeLink('css/custom-detail.css'); ?>" rel="stylesheet" />
      <!-- Font-Awesome -->
      <link href="<?php themeLink('css/font-awesome.min.css'); ?>" rel="stylesheet" />
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" crossorigin="anonymous">
      <!-- Owl-carousel -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
      <!-- bootstrap -->
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <!-- Custom Theme style -->
      <link href="<?php themeLink('css/custom.css'); ?>" rel="stylesheet" type="text/css" />
      <link href="<?php themeLink('css/top_header.css'); ?>" rel="stylesheet" type="text/css" />
      <link href="<?php themeLink('css/side_nav_fixed.css'); ?>" rel="stylesheet" type="text/css" />
      <link href="<?php themeLink('css/newsletter.css'); ?>" rel="stylesheet" type="text/css" />
      <?php if($isRTL) echo '<link href="'.themeLink('css/rtl.css',true).'" rel="stylesheet" type="text/css" />'; ?>
      <?php if($themeOptions['custom']['css'] != '') echo '<style>'.htmlPrint($themeOptions['custom']['css'],true).'</style>'; ?>
      <!-- jQuery 1.10.2 -->
      <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
      <script type="text/javascript" src="../js/owlUse.js"></script>
      <!-- Owl-carousel JS -->
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
      <!-- sytle-js -->
      <script type="text/javascript" src="../js/top_header.js"></script>
      <script type="text/javascript" src="../js/styles.js"></script>
      <script type="text/javascript" src="../js/search.js"></script>
      <script type="text/javascript" src="../js/header_login.js"></script>
   </head>
   <body class="page-detail">
      <!-- nav./start -->
      <nav class="navbar" role="navigation" aria-label="main navigation">
         <div class="wrap-admin container">
            <div class="navbar-brand span4l">
               <a class="navbar-item" href="https://adminkit.net">
               <img src="../../images/adminkit-logo.png" title="Logo adminkit.net" alt="Logo adminkit.net">
               <span class="logo-mobile"></span>
               </a>
               <div class="nav-seller-global">
                  <div class="border-nav-seller-global">
                     <div class="button-nav-gmenu">
                        <div class="border-button-nav-gmenu">
                           <svg width="18" height="12" viewBox="0 0 18 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                              <path d="M0 2V0H18V2H0Z" fill="#444"></path>
                              <path d="M0 7H18V5H0V7Z" fill="#444"></path>
                              <path d="M0 12H18V10H0V12Z" fill="#444"></path>
                           </svg>
                           <span class="menu-main">Menu</span>
                        </div>
                        <!-- menu-table./Start -->
                        <div class="menu-table">
                           <div class="wrap-menu">
                              <div class="container">
                                 <div class="row">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-2dot4">
                                       <div class="menu-item">
                                          <div class="txt-flex-title">
                                             <h2 class="title-menu">Admin Tool Kits</h2>
                                             <i class="ic-mobile">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                   <path d="M6 7.494L11.392 3l.608.729-6 5-6-5L.608 3z"></path>
                                                </svg>
                                             </i>
                                          </div>
                                          <div class="box-menu box-one">
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/admin-tools/BackList_Check.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://adminkit.net/dnsbl.aspx">BlackList Check</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/admin-tools/MX_Lookup.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://adminkit.net/mxlookup.aspx">MX Lookup</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/admin-tools/DNS_Lookup.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://adminkit.net/dnsrecords.aspx">DNS Lookup</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/admin-tools/Telnet.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://adminkit.net/telnet.aspx">Telnet</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/admin-tools/IP_2_Location.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://adminkit.net/ip2location.aspx">IP 2 Location</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/admin-tools/Ping.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://adminkit.net/ping.aspx">Ping</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/admin-tools/BlackList_Monitor.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://adminkit.net/dnsbl_monitor.aspx">BlackList Monitor</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/admin-tools/SMTP_Test_Tool.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://adminkit.net/smtp.aspx">SMTP Test Tool</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/admin-tools/My_IP_Address.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://adminkit.net/my_ip_address.aspx">My IP Adress</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/admin-tools/Trace_route.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://adminkit.net/traceroute.aspx">Trace route</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox box-hidden">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/admin-tools/Whois.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://adminkit.net/whois.aspx">Whois</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox box-hidden">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/admin-tools/Desktop_Tools.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://adminkit.net/applications.aspx">Desktop Tools</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="box-more">
                                                <a class="openMore" href="#admintool">Open More..</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-2dot4">
                                       <div class="menu-item">
                                          <div class="txt-flex-title">
                                             <h2 class="title-menu">Server Performance Tools</h2>
                                             <i class="ic-mobile">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                   <path d="M6 7.494L11.392 3l.608.729-6 5-6-5L.608 3z"></path>
                                                </svg>
                                             </i>
                                          </div>
                                          <div class="box-menu box-two">
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/server-tools/Online_Ping_Website_Tool.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/online-ping-website-tool">Online Ping Website Tool</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/server-tools/My_IP_Address.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/my-ip-address">My IP Address</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/server-tools/Server_Status_Checker.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/server-status-checker">Server Status Checker</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/server-tools/Reverse_IP_Domain_Checker.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/reverse-ip-domain-checker">Reverse IP Domain Checker</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/server-tools/Blacklist_Lookup.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/blacklist-lookup">Blacklist Lookup</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/server-tools/Domain_Hosting_Checker.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/domain-hosting-checker">Domain Hosting Checker</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/server-tools/Find_DNS_records.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/find-dns-records">Find DNS records</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/server-tools/Domain_Authority_Checker.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/domain-authority-checker">Domain Authority Checker</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/server-tools/Domain_intro_IP.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/domain-into-ip">Domain into IP</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="box-more" style="display: none">
                                                <a class="openMore" href="#seotool">Open More..</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-2dot4">
                                       <div class="menu-item">
                                          <div class="txt-flex-title">
                                             <h2 class="title-menu">Web Peformance Tools</h2>
                                             <i class="ic-mobile">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                   <path d="M6 7.494L11.392 3l.608.729-6 5-6-5L.608 3z"></path>
                                                </svg>
                                             </i>
                                          </div>
                                          <div class="box-menu box-three">
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/web-tools/Webpage_Screen_Resolution.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/webpage-screen-resolution-simulator">Webpage Screen Resolution</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/web-tools/Page_Size_Checker.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/page-size-checker">Page Size Checker</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/web-tools/Website_Screenshot_Generator.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/website-screenshot-generator">Website Screenshot Generator</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/web-tools/Get_Source_Code_of_Webpage.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/get-source-code-of-webpage">Get Source Code of Webpage</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/web-tools/Page_Speed_Checker.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/page-speed-checker">Page Speed Checker</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/web-tools/Code_to_Text_Ratio_Checker.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/code-to-text-ratio-checker">Code to Text Ratio Checker</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/web-tools/Broken_Links_Finder.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/broken-links-finder">Broken Links Finder</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/web-tools/Pagespeed_insights_Checker.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/pagespeed-insights-checker">Pagespeed Insights Checker</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/web-tools/Get_HTTP_Headers.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/get-http-headers">Get HTTP Headers</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/web-tools/Mobile_Friendly_Test.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/mobile-friendly-test">Mobile Friendly Test</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox box-hidden">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/web-tools/Website_Reviewer.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/website-reviewer">Website Reviewer</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox box-hidden">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/web-tools/Keyword_Density_Checker.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/keyword-density-checker">Keyword Density Checker</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox box-hidden">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/web-tools/Check_GZIP_compression.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/check-gzip-compression">Check GZIP compression</a></p>
                                                      <p>Check GZIP compression</p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox box-hidden">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/web-tools/Class_C_Ip_Checker.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/class-c-ip-checker">Class C Ip Checker</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="box-more">
                                                <a class="openMore" href="#webPeforTools">Open More..</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-2dot4">
                                       <div class="menu-item">
                                          <div class="txt-flex-title">
                                             <h2 class="title-menu">SEO Performance Tools</h2>
                                             <i class="ic-mobile">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                   <path d="M6 7.494L11.392 3l.608.729-6 5-6-5L.608 3z"></path>
                                                </svg>
                                             </i>
                                          </div>
                                          <div class="box-menu box-four">
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/seo-tools/Article_Rewriter.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/article-rewriter">Article Rewriter</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/seo-tools/Backlink_Checker.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/backlink-maker">Backlink Maker</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/seo-tools/Meta_Tag_Generator.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/meta-tag-generator">Meta Tag Generator</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/seo-tools/Meta_Tags_Analyzer.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/meta-tags-analyzer">Meta Tags Analyzer</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/seo-tools/Keyword_Position_Checker.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/keyword-position-checker">Keyword Position Checker</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/seo-tools/Robots.txt_Generator.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/robots-txt-generator">Robots.txt Generator</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/seo-tools/XML_Sitemap_Generator.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/xml-sitemap-generator">XML Sitemap Generator</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/seo-tools/Backlink_Maker.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/backlink-checker">Backlink Checker</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/seo-tools/Alexa Rank_Checker.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/alexa-rank-checker">Alexa Rank Checker</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/seo-tools/URL_Rewriting_Tool.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/url-rewriting-tool">URL Rewriting Tool</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox box-hidden">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/seo-tools/Google_Index_Checker.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/google-index-checker">Google Index Checker</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox box-hidden">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/seo-tools/Website_Links_Count_Checker.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/website-links-count-checker">Website Links Count Checker</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox box-hidden">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/seo-tools/Search_Engine_Spider_Simulator.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/spider-simulator">Search Engine Spider Simulator</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox box-hidden">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/seo-tools/Keyword_Position_Checker.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/keywords-suggestion-tool">Keywords Suggestion Tool</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox box-hidden">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/seo-tools/Page_Authority_Checker.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/page-authority-checker">Page Authority Checker</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="box-more">
                                                <a class="openMore" href="#seotool">Open More..</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-2dot4">
                                       <div class="menu-item">
                                          <div class="txt-flex-title">
                                             <h2 class="title-menu">Other Tools</h2>
                                             <i class="ic-mobile">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                                                   <path d="M6 7.494L11.392 3l.608.729-6 5-6-5L.608 3z"></path>
                                                </svg>
                                             </i>
                                          </div>
                                          <div class="box-menu box-five">
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/other-tools/Plagiarism_Checker.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/plagiarism-checker">Plagiarism Checker</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/other-tools/Word_Counter.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/word-counter">Word Counter</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/other-tools/Link_Analyzer.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/link-analyzer-tool">Link Analyzer</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/other-tools/Domain_Age_Checker.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/domain-age-checker">Domain Age Checker</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/other-tools/Whois_Checker.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/whois-checker">Whois Checker</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/other-tools/www_Redirect_Checker.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/www-redirect-checker">www Redirect Checker</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/other-tools/Mozrank_Checker.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/mozrank-checker">Mozrank Checker</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/other-tools/URL_Encoder_Decoder.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/url-encoder-decoder">URL Encoder / Decoder</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/other-tools/Suspicious_Domain_Checker.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/suspicious-domain-checker">Suspicious Domain Checker</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/other-tools/Link_Price_Calculator.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/link-price-calculator">Link Price Calculator</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox box-hidden">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/other-tools/Online_Md5_Generator.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/online-md5-generator">Online Md5 Generator</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox box-hidden">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/other-tools/What_is_my_Browser.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/what-is-my-browser">What is my Browser</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox box-hidden">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/other-tools/Email_Privacy.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/email-privacy">Email Privacy</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox box-hidden">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/other-tools/Google_Cache_Checker.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/google-cache-checker">Google Cache Checker</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox box-hidden">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/other-tools/Bulk_Domain_Availability.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/domain-availability-checker">Bulk Domain Availability Checker</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox box-hidden">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/other-tools/Grammar_Checker.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/grammar-checker">Grammar Checker</a></p>
                                                      <p>Grammar Checker</p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox box-hidden">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/other-tools/Flag Counter.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/flag-counter">Flag Counter</a></p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="moreBox box-hidden">
                                                <div class="icon-box">
                                                   <div class="box-images">
                                                      <img src="../images/menus/other-tools/Google_Malware_Checker.svg" alt="">
                                                   </div>
                                                   <div class="box-text">
                                                      <p><a href="https://seotool.adminkit.net/google-malware-checker">Google Malware Checker</a></p>
                                                      <p>Google Malware Checker</p>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="box-more">
                                                <a class="openMore" href="#OtherTool">Open More..</a>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="search-wrapper span4l tm-right">
               <div class="search-container">
                  <span id="btn-search-mobile" rel=".desktop-device-nav">
                     <i class="iconm-search">
                        <svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                           <path d="M9.93945 0.931274C4.98074 0.931274 0.941406 4.9706 0.941406 9.92934C0.941406 14.888 4.98074 18.9352 9.93945 18.9352C12.0575 18.9352 14.0054 18.193 15.5449 16.9606L19.293 20.7067C19.4821 20.888 19.7347 20.988 19.9967 20.9853C20.2587 20.9827 20.5093 20.8775 20.6947 20.6924C20.8801 20.5072 20.9856 20.2569 20.9886 19.9949C20.9917 19.7329 20.892 19.4801 20.7109 19.2907L16.9629 15.5427C18.1963 14.0008 18.9395 12.0498 18.9395 9.92934C18.9395 4.9706 14.8982 0.931274 9.93945 0.931274ZM9.93945 2.9313C13.8173 2.9313 16.9375 6.0515 16.9375 9.92934C16.9375 13.8072 13.8173 16.9352 9.93945 16.9352C6.06162 16.9352 2.94141 13.8072 2.94141 9.92934C2.94141 6.0515 6.06162 2.9313 9.93945 2.9313Z" fill="#92A7B4"></path>
                        </svg>
                     </i>
                  </span>
                  <form autocomplete="off" action="" id="searchForm" class="input">
                     <div class="autocomplete">
                        <div class="close-button">
                           <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                              <path d="M2 12L6 16L6 13L22 13L22 11L6 11L6 8L2 12Z" fill="#2E3A59"></path>
                           </svg>
                        </div>
                        <input class="search-main" type="text" name="my-search" id="myInput" placeholder="Enter Search Words">
                     </div>
                     <button class="btn-search-desktop" type="submit">
                     <i class="fa fa-search" aria-hidden="true"></i>
                     </button>
                  </form>
               </div>
            </div>
            <div class="header-right span4l">
               <ul class="nav-right">
               	  <li class="link-to">
               	  	<a href="https://seotool.adminkit.net/analyze"><span>Analyze Website</span></a>
               	  </li>
                  <li class="nav-button-account">
                     <a class="today-login">
                     <img src="../../images/default_avarta.jpg">
                     </a>
                  </li>
               </ul>
            </div>
         </div>
      </nav>
      <?php  if($controller == "main"){ ?>
      <div class="masthead">
         <div class="container">
            <div class="row">
               <div class="col-md-6 seobannerBig">
                  <h1 class="seobannerh1"><?php echo $lang['317']; ?></h1>
                  <p class="seobannerp"><?php echo $lang['319']; ?></p>
                  <button class="btn btn-default" id="getStarted"><?php echo $lang['318']; ?></button>
               </div>
               <div class="col-md-6">
                  <img class="visible-lg visible-md" alt="<?php echo $lang['317']; ?>" src="<?php themeLink('img/seobanner.png'); ?>" />
               </div>
            </div>
         </div>
      </div>
      <?php } else { ?>
      <?php } if(isSelected($other['other']['maintenance'])){ ?>
      <div class="alert alert-error text-center" style="margin: 35px 140px -10px 140px;">
         <strong>Alert!</strong> &nbsp; Your website is currently set to be closed.
      </div>
      <?php } ?>