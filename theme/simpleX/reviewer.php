<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name A to Z SEO Tools - PHP Script
 * @copyright � 2018 ProThemes.Biz
 *
 */
$solveMsg = array($lang['AN5'],$lang['AN6'],$lang['AN7'],$lang['AN8']);
$loadingBar = '                        <div class="text-center">
                        <img src="'.themeLink('img/load.gif',true).'" />
                        <br /><br />
                        '.$lang['AN9'].'...
                        <br /><br />
                        </div>';
?>

<script>
var hashCode = '<?php echo $hashCode; ?>';
var inputHost = '<?php echo $my_url_host; ?>';
var isOnline = '<?php echo $isOnline; ?>';
</script>

<script src="<?php themeLink('js/circle-progress.js'); ?>"></script>  
<link href="<?php themeLink('css/www.css'); ?>" rel="stylesheet" />

  <div class="container main-container">
	<div class="row">
      	    <?php
            if($themeOptions['general']['sidebar'] == 'left')
                require_once(THEME_DIR."sidebar.php");
            ?>
          	<div class="col-md-8 main-index">
            
            <div class="xd_top_box">
             <?php echo $ads_720x90; ?>
            </div>
            
              	<h2 id="title"><?php echo $data['tool_name']; ?></h2>

               <?php if ($pointOut != 'output') { ?>
               <br />
               <p><?php echo $lang['23']; ?>
               </p>
               <form method="POST" action="<?php createLink('domain');?>" onsubmit="return fixURL();"> 
               <input type="text" name="url" id="url" value="" class="form-control"/>
               <br />
               <?php if ($toolCap) echo $captchaCode; ?>
               <div class="text-center">
               <input class="btn btn-info" type="submit" value="<?php echo $lang['8']; ?>" name="submit"/>
               </div>
               </form>     
                          
               <?php 
               } else { 
               //Output Block
               if(isset($error)) {
                
                echo '<br/><br/><div class="alert alert-error">
                <strong>Alert!</strong> '.$error.'
                </div><br/><br/>
                <div class="text-center"><a class="btn btn-info" href="'.$toolURL.'">'.$lang['12'].'</a>
                </div><br/>';
                
               } else {
               ?>
               <br />
                  
            <br /> <br />

            <div id="scoreBoard" class="row">
                <div class="col-md-4 screenBox">
                    <div id="screenshot">
                        <div id="screenshotData">
                        <div class="loader">
                          <div class="side"></div>
                          <div class="side"></div>
                          <div class="side"></div>
                          <div class="side"></div>
                          <div class="side"></div>
                          <div class="side"></div>
                          <div class="side"></div>
                          <div class="side"></div>
                        </div>
                        <div class="loaderLabel"><?php echo $lang['AN82'] ; ?></div>
                        </div>
                        <div class="computer"></div>
                    </div>
                  </div>
                <div class="col-md-5 levelBox">
                <div>
                <a href="http://<?php echo $my_url_host; ?>" target="_blank" rel="nofollow" class="mainLink"><?php echo ucfirst($my_url_host); ?></a>
                </div>
                <div class="timeBox">
                <?php echo date('F, j Y h:i:s A'); ?>
                </div>
                <div class="progressBox">
                <span class="scoreProgress-label passedBox"><?php trans('Passed', $lang['AD79']); ?></span>
                <div class="scoreProgress scoreProgress-xs scoreProgress-success">
                    <div id="passScore" aria-valuemax="100" aria-valuenow="0" aria-valuemin="0" role="progressbar" class="scoreProgress-bar">
                        <span class="scoreProgress-value">0%</span>
                    </div>
                </div>
                </div>
                
                <div class="progressBox">
                <span class="scoreProgress-label improveBox"><?php trans('To Improve', $lang['AD80']); ?></span>
                <div class="scoreProgress scoreProgress-xs scoreProgress-warning">
                    <div id="improveScore" aria-valuemax="100" aria-valuenow="0" aria-valuemin="0" role="progressbar" class="scoreProgress-bar">
                        <span class="scoreProgress-value">0%</span>
                    </div>
                </div>
                </div>
                
                <div class="progressBox">
                <span class="scoreProgress-label errorBox"><?php trans('Errors', $lang['AD81']); ?></span>
                <div class="scoreProgress scoreProgress-xs scoreProgress-danger">
                    <div id="errorScore" aria-valuemax="100" aria-valuenow="0" aria-valuemin="0" role="progressbar" class="scoreProgress-bar">
                        <span class="scoreProgress-value">0%</span>
                    </div>
                </div>
                </div>
            </div>
            <div class="col-md-2 circleBox">
            <div class="second circle" data-size="130" data-thickness="5"><canvas width="130" height="130"></canvas>
            <strong id="overallscore">0<i>%</i></strong>
            </div>
            </div>
        </div>

                    
                <div class="clearfix"></div>
                               
               <div class="seoBox" onclick="javascript:showSuggestion('seoBox1');">
                    <?php outHeadBox($lang['AN1'],$solveMsg,1); ?>
                    <div class="contentBox" id="seoBox1">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox2');">
                    <?php outHeadBox($lang['AN2'],$solveMsg,1); ?>
                    <div class="contentBox" id="seoBox2">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox3');">
                    <?php outHeadBox($lang['AN3'],$solveMsg,1); ?>
                    <div class="contentBox" id="seoBox3">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>  
                
                <div class="seoBox headingResult" onclick="javascript:showSuggestion('seoBox4');">
                    <?php outHeadBox($lang['AN16'],$solveMsg,2); ?>
                    <div class="contentBox" id="seoBox4">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>  
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox5');">
                    <?php outHeadBox($lang['AN17'],$solveMsg,4); ?>
                    <div class="contentBox" id="seoBox5">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>  
                
                <div class="seoBox altImgResult" onclick="javascript:showSuggestion('seoBox6');">
                    <?php outHeadBox($lang['AN20'],$solveMsg,1); ?>
                    <div class="contentBox" id="seoBox6">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>  
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox7');">
                    <?php outHeadBox($lang['AN28'],$solveMsg,4); ?>
                    <div class="contentBox" id="seoBox7">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>  
                
                <div class="seoBox keyConsResult" onclick="javascript:showSuggestion('seoBox8');">
                    <?php outHeadBox($lang['AN30'],$solveMsg,1); ?>
                    <div class="contentBox" id="seoBox8">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>  
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox9');">
                    <?php outHeadBox($lang['AN35'],$solveMsg,2); ?>
                    <div class="contentBox" id="seoBox9">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div> 
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox10');">
                    <?php outHeadBox($lang['AN40'],$solveMsg,2); ?>
                    <div class="contentBox" id="seoBox10">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div> 
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox11');">
                    <?php outHeadBox($lang['AN45'],$solveMsg,1); ?>
                    <div class="contentBox" id="seoBox11">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div> 
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox12');">
                    <?php outHeadBox($lang['AN48'],$solveMsg,1); ?>
                    <div class="contentBox" id="seoBox12">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div> 
                
                <div class="seoBox inPage" onclick="javascript:showSuggestion('seoBox13');">
                    <?php outHeadBox($lang['AN51'],$solveMsg,1); ?>
                    <div class="contentBox" id="seoBox13">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div> 
                
                <div class="seoBox brokenLinks" onclick="javascript:showSuggestion('seoBox14');">
                    <?php outHeadBox($lang['AN58'],$solveMsg,1); ?>
                    <div class="contentBox" id="seoBox14">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div> 
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox15');">
                    <?php outHeadBox($lang['AN59'],$solveMsg,1); ?>
                    <div class="contentBox" id="seoBox15">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div> 
                        
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox16');">
                    <?php outHeadBox($lang['AN60'],$solveMsg,1); ?>
                    <div class="contentBox" id="seoBox16">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div> 
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox17');">
                    <?php outHeadBox($lang['AN61'],$solveMsg,2); ?>
                    <div class="contentBox" id="seoBox17">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div> 
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox18');">
                    <?php outHeadBox($lang['AN62'],$solveMsg,3); ?>
                    <div class="contentBox" id="seoBox18">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox19');">
                    <?php outHeadBox($lang['AN63'],$solveMsg,1); ?>
                    <div class="contentBox" id="seoBox19">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox20');">
                    <?php outHeadBox($lang['AN76'],$solveMsg,1); ?>
                    <div class="contentBox" id="seoBox20">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox21');">
                    <?php outHeadBox($lang['AN81'],$solveMsg,4); ?>
                    <div class="contentBox" id="seoBox21">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox whois" onclick="javascript:showSuggestion('seoBox22');">
                    <?php outHeadBox($lang['AN83'],$solveMsg,4); ?>
                    <div class="contentBox" id="seoBox22">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox23');">
                    <?php outHeadBox($lang['AN91'],$solveMsg,3); ?>
                    <div class="contentBox" id="seoBox23">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox24');">
                    <?php outHeadBox($lang['AN92'],$solveMsg,4); ?>
                    <div class="contentBox" id="seoBox24">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox25');">
                    <?php outHeadBox($lang['AN93'],$solveMsg,2); ?>
                    <div class="contentBox" id="seoBox25">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox26');">
                    <?php outHeadBox($lang['AN94'],$solveMsg,4); ?>
                    <div class="contentBox" id="seoBox26">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox27');">
                    <?php outHeadBox($lang['AN95'],$solveMsg,1); ?>
                    <div class="contentBox" id="seoBox27">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox28');">
                    <?php outHeadBox($lang['AN96'],$solveMsg,1); ?>
                    <div class="contentBox" id="seoBox28">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox29');">
                    <?php outHeadBox($lang['AN97'],$solveMsg,2); ?>
                    <div class="contentBox" id="seoBox29">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox30');">
                    <?php outHeadBox($lang['AN98'],$solveMsg,3); ?>
                    <div class="contentBox" id="seoBox30">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox31');">
                    <?php outHeadBox($lang['AN99'],$solveMsg,1); ?>
                    <div class="contentBox" id="seoBox31">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox32');">
                    <?php outHeadBox($lang['AN100'],$solveMsg,4); ?>
                    <div class="contentBox" id="seoBox32">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox33');">
                    <?php outHeadBox($lang['AN101'],$solveMsg,4); ?>
                    <div class="contentBox" id="seoBox33">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox34');">
                    <?php outHeadBox($lang['AN102'],$solveMsg,1); ?>
                    <div class="contentBox" id="seoBox34">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox35');">
                    <?php outHeadBox($lang['AN103'],$solveMsg,1); ?>
                    <div class="contentBox" id="seoBox35">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox36');">
                    <?php outHeadBox($lang['AN104'],$solveMsg,4); ?>
                    <div class="contentBox" id="seoBox36">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox37');">
                    <?php outHeadBox($lang['AN105'],$solveMsg,3); ?>
                    <div class="contentBox" id="seoBox37">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox38');">
                    <?php outHeadBox($lang['AN106'],$solveMsg,1); ?>
                    <div class="contentBox" id="seoBox38">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox39');">
                    <?php outHeadBox($lang['AN107'],$solveMsg,2); ?>
                    <div class="contentBox" id="seoBox39">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox40');">
                    <?php outHeadBox($lang['AN108'],$solveMsg,4); ?>
                    <div class="contentBox" id="seoBox40">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox41');">
                    <?php outHeadBox($lang['AN109'],$solveMsg,1); ?>
                    <div class="contentBox" id="seoBox41">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox42');">
                    <?php outHeadBox($lang['AN110'],$solveMsg,2); ?>
                    <div class="contentBox" id="seoBox42">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox43');">
                    <?php outHeadBox($lang['AN111'],$solveMsg,4); ?>
                    <div class="contentBox" id="seoBox43">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox44');">
                    <?php outHeadBox($lang['AN112'],$solveMsg,3); ?>
                    <div class="contentBox" id="seoBox44">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox45');">
                    <?php outHeadBox($lang['AN113'],$solveMsg,4); ?>
                    <div class="contentBox" id="seoBox45">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox46');">
                    <?php outHeadBox($lang['AN114'],$solveMsg,3); ?>
                    <div class="contentBox" id="seoBox46">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                
                <div class="seoBox" onclick="javascript:showSuggestion('seoBox47');">
                    <?php outHeadBox($lang['AN115'],$solveMsg,4); ?>
                    <div class="contentBox" id="seoBox47">
                        <?php echo $loadingBar; ?>
                    </div>
                    <?php outQuestionBox($lang['AN4']); ?>
	            </div>
                        
                <div class="text-center">
                <br /> &nbsp; <br />
                <a class="btn btn-info" href="<?php echo $toolURL; ?>"><?php echo $lang['27']; ?></a>
                <br />
                </div>

<?php } } ?>

<br />

<div class="xd_top_box">
<?php echo $ads_720x90; ?>
</div>

<h2 id="sec1" class="about_tool"><?php echo $lang['11'].' '.$data['tool_name']; ?></h2>
<p>
<?php echo $data['about_tool']; ?>
</p> <br />
</div>              
            
        <?php
        if($themeOptions['general']['sidebar'] == 'right')
            require_once(THEME_DIR."sidebar.php");
        ?>      		
        </div>
    </div> <br />
                    
<!-- App -->
<script src="<?php themeLink('js/www.js'); ?>" type="text/javascript"></script>