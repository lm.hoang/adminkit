<?php

defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: Rainbow PHP Framework
 * @copyright © 2017 ProThemes.Biz
 *
 */
?>
<!-- Blog style -->
<link href="<?php themeLink('css/blog.css'); ?>" rel="stylesheet" />

<div class="container main-container top30">
    <div class="row">
    
    <?php
    if($themeOptions['general']['sidebar'] == 'left')
        require_once(THEME_DIR."sidebar.php");
    ?>  
    
    <div class="col-md-8 contentLayer top40">
    <div class="row">
      <div class="col-md-1 raDate">
        <div class="date_1">
          <div class="date_up2">
            <div class="center2 feb2"><?php echo $post_month; ?></div>
          </div>
          <div class="date_down2">
            <div class="text_word"><?php echo $post_day; ?></div>
          </div>
        </div>
      </div>
      <div class="col-md-11">
      
      <div class="romantic_free">
        <h4 class="csPageTitle"><?php echo ucfirst($post_title); ?></h4>
      </div>
      
      <div class="text_size12 mr_top8 clock"> 
        <span class="color_text_in"> 
            <i class="fa fa-clock-o color-grey fz14"></i><b class="color_op"><?php echo $posted_date; ?></b></span> 
        
        <span class="color_text_in">
            <i class="fa fa-user color-grey fz14"></i> <?php trans('by',$lang['BL286']); ?> <b class="color_op"><?php echo $posted_by; ?></b></span>
        
        <span class="color_text_in"> 
            <i class="fa fa-tag color-grey fz14"></i><?php trans('in',$lang['BL291']); ?> <b class="color_op"><a href="<?php createLink('blog/category/'.str_replace(' ','-',$category)); ?>"><?php echo ucfirst($category); ?></a></b> 
        </span>  
      </div>
      
      </div>
    </div>
    
    <hr /><br />
        
    <?php echo $post_content; ?>
        
    <br />
    <div class="xd_top_box text-center">
        <?php echo $text_ads; ?>
    </div>
    
    <?php if($enable_related){ ?>
    
    <div id="relatedPosts">
        <h2><span><?php trans('RELATED POSTS',$lang['BL290']); ?>:</span></h2>
        <?php echo $relatedBlogPosts; ?>
    </div>
    <div class="clearfix"></div> 

    <?php } if($showComment){  ?>
    <div class="divider_h mr_top30"></div>
        <div class="row">
            <div class="col-xs-12">
            <?php if($post_allow_comment){ ?>
            <div class="panel panel-default">
              <div class="panel-heading leave">
              <div class="posts_1">
                    <h5><?php trans('leave',$lang['BL292']); ?><span> <?php trans('a comment',$lang['BL293']); ?></span></h5>
                    <span class="font_14 comment-here"><?php trans('Please post your comments here.',$lang['BL288']); ?></span>
              </div>
              </div>
              <div class="panel-body">
                <?php echo $discuss_id; ?>                
              </div>
          </div>
            <?php } else { ?>
            <br />
            <div class="alert alert-error">
            <?php trans('Comments are disabled for this post!',$lang['BL289']); ?>
            </div>
            <br /><br />
            <?php } ?>
          </div>
       </div>
                    
        <div class="xd_top_box text-center">
            <?php echo $ads_720x90; ?>
        </div>
    
        <br />
     <?php } ?>
    </div>
    
    <?php
    if($themeOptions['general']['sidebar'] == 'right')
        require_once(THEME_DIR."sidebar.php");
    ?>
    </div>
</div>
<br />