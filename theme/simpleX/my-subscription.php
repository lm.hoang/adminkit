<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
/*
 * @author Balaji
 * @name: A to Z SEO Tools - PHP Script
 * @Theme: Default Style
 * @copyright � 2018 ProThemes.Biz
 *
 */
?>

<link href="<?php themeLink('premium/css/premium.css'); ?>" rel="stylesheet" type="text/css" />

<div class="container main-container">
    <div class="row">
  	    <?php
        if($themeOptions['general']['sidebar'] == 'left')
            require_once(THEME_DIR."sidebar.php");
        ?>
        <div class="col-md-8 main-index">
        
        <?php if($pointOut == 'cancel'){ if(isset($_POST['cancel'])){ ?>
        
        <h2 class="text-center premiumTitle"><?php trans('You\'ve Canceled Your Subscription', $lang['AD808']); ?></h2> 
                                           
        <br />
        
        <p>
        <div class="canMsg">
        <b><?php trans('Subscription Canceled!', $lang['AD809']); ?></b><br />
        <?php trans('All the products and related services will be inactive now!', $lang['AD810']); ?>
        </div>
        </p>
       
        <meta http-equiv="refresh" content="5;url=<?php createLink(); ?>" />
        <br /><br />
        <?php } else { ?>
        <h2 class="premiumTitle"><?php trans('Cancel Subscription', $lang['AD811']); ?></h2> 
                                           
        <br />
        
        <p><?php trans('Would you like to cancel this subscription? All the products and related services on this contract will be inactive. This operation cannot be undone.', $lang['AD812']); ?></p>
        
        <p> <input name="canSub" type="checkbox"/>
        <label> &nbsp; <?php trans('Yes, cancel this subscription.', $lang['AD813']); ?></label>
        </p>
        <br />
        <form method="POST" action="#" onsubmit="return doCanSub()">
            <label><?php trans('Reason for Cancelling', $lang['AD814']); ?></label>
            <input class="form-control" type="text" name="can_msg" placeholder="<?php trans('(optional)', $lang['AD815']); ?>" />
            <br />
            <input type="hidden" value="1" name="cancel" />
            <input class="btn btn-danger"  type="submit" value="<?php trans('Confirm Cancellation', $lang['AD816']); ?>" />
            <a class="btn btn-success" href="<?php echo '/'.$controller; ?>"><?php trans('Go Back', $lang['AD817']); ?></a>
        </form>
        <br /><br />
        <script>
        function doCanSub(){
        if ($('input[name=canSub]:checked').length > 0){
            return true;
        }else{
            sweetAlert(oopsStr, '<?php makeJavascriptStr($lang['AD818'], true); ?>' , "error");
            return false;
        }
        }
        </script>
        <?php } } else { ?>
        
        <h2 class="premiumTitle"><?php trans('Subscription Details', $lang['AD819']); ?></h2> 
                                           
        <br />
        
        <table class="table table-hover table-bordered premiumTable">
        <tbody>
            <tr>
                <td style="width: 200px;"><?php trans('Subscribed Plan', $lang['AD820']); ?></td>
                <td><span><?php echo $planName; ?></span></td>
            </tr> 
            <tr>
                <td style="width: 200px;"><?php trans('Billing Type', $lang['AD821']); ?></td>
                <td><span><?php echo $billType; ?></span></td>         
            </tr> 
            <tr>
                <td style="width: 200px;"><?php trans('Expires On', $lang['AD822']); ?></td>
                <td><span><?php echo $expDate; ?></span></td>         
            </tr> 
            <tr>
                <td style="width: 200px;"><?php trans('Renew My Subscription', $lang['AD823']); ?></td>
                <td><span><?php echo $renewBtn; ?></span></td>         
            </tr> 
            <tr>
                <td style="width: 200px;"><?php trans('Cancel My Subscription', $lang['AD824']); ?></td>
                <td><span><a title="<?php trans('Cancelling my subscription', $lang['AD825']); ?>" href="<?php echo $cancelBtn; ?>" class="text-center btn btn-danger"><?php trans('Cancel', $lang['AD826']); ?></a></span></td>         
            </tr> 
            <tr>
                <td style="width: 200px;"><?php trans('Downloadable PDF Reports', $lang['AD827']); ?></td>
                <td><span><?php echo $allowPdf; ?></span></td>         
            </tr> 
            <?php if($totalLimit != '0'){ ?>
            <tr>
                <td style="width: 200px;"><?php trans('Remaining PDF Reports(Today)', $lang['AD828']); ?></td>
                <td><span><?php echo $remCountMsg; ?></span></td>         
            </tr> 
            <?php } ?>
            <tr>
                <td style="width: 200px;"><?php trans('Premium Tools Access', $lang['AD829']); ?></td>
                <td><ul><?php foreach($tools as $tool)
                    echo '<li>'.$tool[0].'</li>';
                ?></ul></td>         
            </tr> 
        </tbody>
        </table>
        <?php } ?>          
        <br />

        </div>
        <?php
        if($themeOptions['general']['sidebar'] == 'right')
            require_once(THEME_DIR."sidebar.php");
        ?>
    </div>
</div>
<br />