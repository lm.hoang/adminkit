<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
/*
 * @author Balaji
 * @name: A to Z SEO Tools - PHP Script
 * @Theme: Default Style
 * @copyright � 2018 ProThemes.Biz
 *
 */
?>

<link href="<?php themeLink('premium/css/premium.css'); ?>" rel="stylesheet" type="text/css" />

<div class="container main-container">
    <div class="row">
  	    <?php
        if($themeOptions['general']['sidebar'] == 'left')
            require_once(THEME_DIR."sidebar.php");
        ?>
        <div class="col-md-8 main-index">
        
        <h2 class="premiumTitle"><?php trans('Customize PDF report', $lang['AD839']); ?>:</h2>     
                                       
        <br />
        <?php if(isset($msg))  echo $msg.'<br>'; ?>
        
          <div id="pdf" >
          
            <div class="row">
              
           <form name="PDFBox" method="POST" action="#" enctype="multipart/form-data"> 
            <?php if(!$isBranded) { ?>
            <div class="alert alert-danger alert-dismissable alert-premium" style="margin: 5px;">
                 <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                 <strong><?php echo $lang['RF20']; ?></strong> <?php trans('Your subscribed plan not allowed customizable PDF Report', $lang['AD840']); ?>
            </div> <br />
            <?php } ?>
            
            <div class="col-md-6">
               <h4 style="margin-bottom: 15px; font-weight: 500;"><?php trans('Header Logo', $lang['AD841']); ?>:</h4>
                <img class="headerLogoBox" id="headerLogoBox" alt="<?php trans('Header Logo', $lang['AD841']); ?>" src="<?php echo $headerLogo; ?>" />   
                <br />
                <?php trans('Recommended Size: (JPEG 150x75px)', $lang['AD842']); ?>
                <input <?php echo $banClass; ?> type="file" name="headerUpload" id="headerUpload" class="btn btn-default" />
                <br /><br />
            </div><!-- /.col-md-6 -->
            
            <div class="col-md-6">
                <h4 style="margin-bottom: 15px; font-weight: 500;"><?php trans('Footer Logo', $lang['AD844']); ?>:</h4>
                <img class="footerLogoBox" id="footerLogoBox" alt="<?php trans('Footer Logo', $lang['AD844']); ?>" src="<?php echo $footerLogo; ?>" />   
                <br />
                <?php trans('Recommended Size: (JPEG 100x50px)', $lang['AD843']); ?>
                <input <?php echo $banClass; ?> type="file" name="footerUpload" id="footerUpload" class="btn btn-default" />
                <br /><br />  
            </div><!-- /.col-md-6 -->
            
            <div class="col-md-12">  
                <div class="form-group">
                    <h4 style="margin-bottom: 15px; font-weight: 500;"><?php trans('Copyright Text', $lang['AD845']); ?>:</h4>
    				<input <?php echo $banClass; ?> value="<?php echo $pdfCopyright; ?>" placeholder="<?php trans('Enter your copyright text', $lang['AD846']); ?>" type="text" name="copyright" class="form-control"  style="width: 96%;"/>
    			</div>	
                <br />
                
               <h4 style="margin-bottom: 15px; font-weight: 500;"><?php trans('Introduction Message', $lang['AD847']); ?>: <small><?php trans('(HTML/CSS Supported)', $lang['AD848']); ?></small></h4>
               <textarea name="introductionCode" <?php echo $banClass; ?> rows="7" class="form-control"><?php echo stripslashes(strEOL($introductionCode)); ?></textarea>
              
               <br />
               
               <h4 style="margin-bottom: 15px; font-weight: 500;"><?php trans('Footer PDF Code', $lang['AD849']); ?>: <small><?php trans('(HTML/CSS Supported)', $lang['AD848']); ?></small></h4>
               <textarea name="footerCode" <?php echo $banClass; ?> rows="8" class="form-control"><?php echo stripslashes(strEOL($footerCode)); ?></textarea>
               <br /><h4 style="margin-bottom: 15px; font-weight: 500;"><?php trans('Replacement Code Reference', $lang['AD850']); ?>:</h4>
            </div>
            
            <div class="col-md-6">
                    <div class="well alert-warning">
                    <b>{(CopyRight Text)}</b> - <?php trans('Returns your Copyright text', $lang['AD851']); ?><br /><br />
                    <b>{(InputSite)}</b> - <?php trans('Returns your current domain name', $lang['AD852']); ?><br /><br />
                    <b>{(CurrentPageNumber)}</b> - <?php trans('Returns current page number', $lang['AD853']); ?><br /><br />
                    <b>{(TotalPageNumber)}</b> - <?php trans('Returns total page numbers', $lang['AD854']); ?><br /><br />
                    <b>{(HeaderLogo)}</b> - <?php trans('Returns your header logo', $lang['AD855']); ?>
                    </div>
                <br /><br />  
               
            </div><!-- /.col-md-6 -->
            
            <div class="col-md-6">
                    <div class="well alert-warning">
                    <b>{(FooterLogo)}</b> - <?php trans('Returns your footer logo', $lang['AD856']); ?><br /><br />
                    <b>{(Date)}</b> - <?php trans('Returns today date', $lang['AD857']); ?><br /><br />
                    <b>{(Time)}</b> - <?php trans('Returns current time', $lang['AD858']); ?><br /><br />
                    <b>{(DateTime)}</b> - <?php trans('Returns today date and current time', $lang['AD859']); ?><br /><br />
                    </div>
                    <br /><br />  
            </div><!-- /.col-md-6 -->
              
            <div class="col-md-12 text-center">  
                <br />
                <input type="submit" value="<?php trans('Submit', $lang['AD860']); ?>" class="btn btn-success" />
                <input type="hidden" name="statestr" value="1" />
                <input type="hidden" name="user" value="1" />
                <br />
            </div>
            
            </form>
          </div>
      
          </div>
        <br />

        </div>
        <?php
        if($themeOptions['general']['sidebar'] == 'right')
            require_once(THEME_DIR."sidebar.php");
        ?>
    </div>
</div>
<br />

<script>
function readURL(input,idBox){
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(idBox).attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#headerUpload").change(function(){
    readURL(this,'#headerLogoBox');
});
$("#footerUpload").change(function(){
    readURL(this,'#footerLogoBox');
});
</script>