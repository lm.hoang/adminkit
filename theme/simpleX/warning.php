<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: Rainbow PHP Framework
 * @copyright © 2018 ProThemes.Biz
 *
 */
?>
<div class="container main-container">
    <div class="row">
  	    <?php
        if($themeOptions['general']['sidebar'] == 'left')
            require_once(THEME_DIR."sidebar.php");
        ?>
        <div class="col-md-8 main-index">
                            
            <div class="xd_top_box">
                <?php echo $ads_720x90; ?>
            </div>
            <br />
            
            <div class="alert alert-error">
            <br />
            <strong><?php echo $lang['RF20']; ?></strong> <?php echo $warningMsg; ?>
            <br /><br />
            </div>
            <br />
        </div>
        <?php
        if($themeOptions['general']['sidebar'] == 'right')
            require_once(THEME_DIR."sidebar.php");
        ?>  
    </div>
</div>
<br />