<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name A to Z SEO Tools - PHP Script
 * @copyright © 2018 ProThemes.Biz
 *
 */
?>

<style>
table {
    table-layout: fixed; width: 100%;
}
td {
  word-wrap: break-word;
}
</style>

  <div class="container main-container">
	<div class="row">
      	    <?php
            if($themeOptions['general']['sidebar'] == 'left')
                require_once(THEME_DIR."sidebar.php");
            ?>
          	<div class="col-md-8 main-index">
            
            <div class="xd_top_box">
             <?php echo $ads_720x90; ?>
            </div>
            
              	<h2 id="title"><?php echo $data['tool_name']; ?></h2>

               <?php if ($pointOut != 'output') { ?>
               <br />
               <p><?php echo $lang['23']; ?>
               </p>
               <form method="POST" action="<?php echo $toolOutputURL;?>" onsubmit="return fixURL();"> 
               <input type="text" name="url" id="url" value="" class="form-control"/>
               <br />
               <?php if ($toolCap) echo $captchaCode; ?>
               <div class="text-center">
               <input class="btn btn-info" type="submit" value="<?php echo $lang['8']; ?>" name="submit"/>
               </div>
               </form>     
                          
               <?php 
               } else { 
               //Output Block
               if(isset($error)) {
                
                echo '<br/><br/><div class="alert alert-error">
                <strong>Alert!</strong> '.$error.'
                </div><br/><br/>
                <div class="text-center"><a class="btn btn-info" href="'.$toolURL.'">'.$lang['12'].'</a>
                </div><br/>';
                
               } else {
               ?><hr />
                        <br />
                    
                    <div class="text-center">
                    <?php if($isGzip){ ?>
                            <p style="color:#2cc36b; font-size: 20px; font-weight: bold;"><?php trans('Wow! It\'s GZIP Enabled.', $lang['AD71']); ?></p>
                    <?php } else { ?>
                            <p style="color:#c0392b; font-size: 20px; font-weight: bold;"><?php trans('Oh No! GZIP is not enabled :(', $lang['AD72']); ?></p>
                    <?php } ?>
                    </div>
                    <br />
                    <div class="row">
                    
                    <div class="col-md-6">

                             <table class="table table-bordered table-striped">
                                        <tbody>
                                        <tr>
                                            <td><strong><?php echo $lang['220']; ?></strong></td>
                                            <td><?php echo $myHost; ?></td>
                                        </tr>
                                        <tr>
                                            <td><strong><?php trans('Compressed size', $lang['AD73']); ?></strong></td>
                                            <?php if($isGzip){ ?>
                                            <td><?php echo $comSize; ?> bytes (~<?php echo size_as_kb($comSize); ?> KB)</td>
                                            <?php } else { ?>
                                            <td><?php echo $gzdataSize; ?> bytes (~<?php echo size_as_kb($gzdataSize); ?> KB)</td>
                                            <?php } ?>
                                        </tr>
                                        <tr>
                                            <td><strong><?php trans('Uncompressed size', $lang['AD74']); ?></strong></td>
                                            <td><?php echo $unComSize; ?> bytes (~<?php echo size_as_kb($unComSize); ?> KB)</td>
                                        </tr>
                             </tbody></table>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="row">
                            
                            <div class="col-md-5 text-center">
                            <input type="text" data-readonly="true" data-fgcolor="#46B0CF" data-height="110" data-width="110" value="<?php echo $percentage; ?>" class="knob" readonly="readonly" style="width: 49px; height: 30px; position: absolute; vertical-align: middle; margin-top: 30px; margin-left: -69px; border: 0px none; background: none repeat scroll 0% 0% transparent; font: bold 18px Arial; text-align: center; color: rgb(60, 141, 188); padding: 0px;" />
                            </div>
                            
                            <div class="col-md-6">
                            <?php if($isGzip){ ?>
                            <p style="padding-top: 25px; font-size: 15px;"><?php trans('Was saved by compressing this page with GZIP.', $lang['AD75']); ?></p>
                            <?php } else { ?>
                            <p style="padding-top: 25px; font-size: 15px;"><?php trans('Could be saved by compressing this page with GZIP.', $lang['AD76']); ?></p>
                            <?php } ?>
                            
                            </div>
                            
                        </div>
                    <div></div>
                    
                    </div>
					
			 	    </div>
                    <br />
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th class="heading">
                                            <h4 class="text-center"><?php trans('Header Information', $lang['AD77']); ?></h4>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                $myLines = preg_split("/\r\n|\n|\r/", $header);
                                foreach($myLines as $line){
                                    if(!empty($line))
                                    echo '<tr><td>'.$line.'</td></tr>';
                                }
                                ?>
                                </tbody>
                            </table>
    
    <div class="text-center">
    <br /> &nbsp; <br />
    <a class="btn btn-info" href="<?php echo $toolURL; ?>"><?php echo $lang['27']; ?></a>
    <br />
    </div>

<?php } } ?>

<br />

<div class="xd_top_box">
<?php echo $ads_720x90; ?>
</div>

<h2 id="sec1" class="about_tool"><?php echo $lang['11'].' '.$data['tool_name']; ?></h2>
<p>
<?php echo $data['about_tool']; ?>
</p> <br />
</div>              
            
        <?php
        if($themeOptions['general']['sidebar'] == 'right')
            require_once(THEME_DIR."sidebar.php");
        ?>     		
        </div>
    </div> <br />

<script>
jQuery(document).ready(function(){
    $('.knob').knob();
    $(".knob").css('font-size','18px');
    $('.knob').val("<?php echo $percentage; ?>%");
});
</script>

<!-- jQuery Knob Chart -->
<script src="<?php themeLink('js/plugins/jqueryKnob/jquery.knob.js'); ?>" type="text/javascript"></script>