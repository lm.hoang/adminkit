﻿<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name A to Z SEO Tools - PHP Script
 * @copyright 2018 ProThemes.Biz
 *
 */
?>
<link href="<?php themeLink('css/grammar.css'); ?>" rel="stylesheet" />
<link rel="stylesheet" href="<?php themeLink('css/jquery.fileupload.css'); ?>" />
<link href="https://www.languagetool.org/vendors/dropkick/dropkick.css" rel="stylesheet">
<link href="https://www.languagetool.org/css/vex.css" rel="stylesheet">
<link href="https://www.languagetool.org/css/vex-theme-default.css" rel="stylesheet">

<script>
    var languageToText = [];
    languageToText['en'] = 'Paste your own text here and click the \'Check Text\' button. Click the colored phrases for details on potential errors. or use this text too see an few of of the problems that LanguageTool can detecd. What do you thinks of grammar checkers? Please not that they are not perfect. Style issues get a blue marker: It\'s 5 P.M. in the afternoon. LanguageTool was released on Thursday, 21 April 2018.';
    var checkDefaultLangWithCountry = "en-US";
</script>

<div class="container main-container">
    <div class="row">
        <?php
        if($themeOptions['general']['sidebar'] == 'left')
            require_once(THEME_DIR."sidebar.php");
        ?>
        <div class="col-md-8 main-index">

            <div class="xd_top_box">
                <?php echo $ads_720x90; ?>
            </div>

            <h2 id="title"><?php echo $data['tool_name']; ?></h2>

            <br />

            <div>
                <p><?php trans('Enter your text', $lang['AD393']); ?>:</p>
                <div class="text-center">(or)</div>
                <p><?php trans('Upload a document: (Supported Format: .doc, .docx, .txt)', $lang['AD394']); ?></p>
                <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span><?php trans('Select file', $lang['AD395']); ?></span>
                    <!-- The file input field used as target for the file upload widget -->
                    <input id="fileupload" type="file" name="files[]" multiple >
                    </span>
                <br />
                <br />

                <!-- The global progress bar -->
                <div id="progress" class="progress">
                    <div class="progress-bar progress-bar-success progress-bar-striped"></div>
                </div>
            </div>

            <div class="inner">
                <div id="editor">
                    <div class="inner">
                        <noscript class="warning">Please turn on Javascript to use this website</noscript>
                        <form id="checkform" class="" name="checkform" action="#" method="post">
                            <div id="handle">
                                <div id="feedbackMessage"></div>
                            </div>
                            <div class="window">
                                <div class="fullscreen-toggle">
                                    <a href="#" title="toggle fullscreen mode"
                                       onClick="fullscreen_toggle();return false;"></a>
                                </div>
                                <p id="checktextpara" style="margin: 0">
                                <textarea id="checktext" name="text" style="width: 100%"
                                          rows="10">Paste your own text here and click the 'Check Text' button. Click the colored phrases for details on potential errors. or use this text too see an few of of the problems that LanguageTool can detecd. What do you thinks of grammar checkers? Please not that they are not perfect. Style issues get a blue marker: It's 5 P.M. in the afternoon. LanguageTool was released on Thursday, 21 April 2018.</textarea>
                                </p>
                                <div id="editor_controls">
                                    <div id="feedbackErrorMessage"></div>
                                    <div id="sentenceContributionMessage"></div>

                                    <div class="editor_controls_items">
                                        <div class="dropdown editor_controls_group">
                                            <select class="dropkick editor_controls_group_item hide" style="width: 100%" name="lang"
                                                    id="lang">
                                                <option value="en-US"  selected='selected'>English</option>
                                            </select>

                                            <div id="subLangDropDown" class="editor_controls_group_item hide"
                                                 style="display: flex;">
                                                <select class="dropkick" name="subLang" id="subLang"
                                                        style="width: 100%">
                                                    <option>US</option>
                                                    <option>GB</option>
                                                    <option>AU</option>
                                                    <option>CA</option>
                                                    <option>NZ</option>
                                                    <option>ZA</option>
                                                </select>
                                            </div>

                                        </div>


                                        <div class="submit editor_controls_group">
                                            <button class="btn btn-warning" type="submit"
                                                    onClick="tinyMCE.activeEditor.setContent('');tinyMCE.get('checktext').focus();return false;"
                                                    title="Delete text">
                                                <i class="fa fa-trash-o fa-lg" aria-hidden="true"></i>
                                            </button>

                                            <button class="btn btn-success" type="submit" name="_action_checkText"
                                                    onClick="doit(true);return false;"
                                                    title="Check Text - or use Ctrl+Return">
                                                Check Text                                    </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


            <br />

            <div class="xd_top_box">
                <?php echo $ads_720x90; ?>
            </div>

            <h2 id="sec1" class="about_tool"><?php echo $lang['11'].' '.$data['tool_name']; ?></h2>
            <p>
                <?php echo $data['about_tool']; ?>
            </p> <br />
        </div>

        <?php
        if($themeOptions['general']['sidebar'] == 'right')
            require_once(THEME_DIR."sidebar.php");
        ?>
    </div>
</div> <br />

<script src="https://www.languagetool.org/js/vex.combined.min.js"></script>
<script src="https://www.languagetool.org/vendors/tiny_mce/tiny_mce.js"></script>
<script src="https://www.languagetool.org/vendors/tiny_mce/plugins/atd-tinymce/editor_plugin2.js"></script>
<script src="https://www.languagetool.org/vendors/dropkick/jquery.dropkick.js"></script>
<script src='<?php createLink('core/library/grammar.js',false,true); ?>'></script>
<script src="<?php themeLink('js/vendor/jquery.ui.widget.js'); ?>"></script>
<script src="<?php themeLink('js/jquery.iframe-transport.js'); ?>"></script>
<script src="<?php themeLink('js/jquery.fileupload.js'); ?>"></script>

<script>
    /*jslint unparam: true */
    /*global window, $ */
    $(function () {
        'use strict';
        // Change this to the location of your server-side upload handler:
        var url = baseUrl+'core/upload/';
        $('#fileupload').fileupload({
            url: url,
            dataType: 'json',
            done: function (e, data) {
                $.each(data.result.files, function (index, file) {
                    // Completed
                    var file_name = file.name;
                    jQuery.post(baseUrl+'core/upload/process.php',{fileName:file_name},function(data){
                        $("#checktext_ifr").contents().find("body").html('<p>'+data+'</p>');
                    });
                });
            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .progress-bar').css(
                    'width',
                    progress + '%'
                );
            }
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
    });
</script>