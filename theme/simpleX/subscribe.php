<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools - PHP Script
 * @Theme: Default Style
 * @copyright � 2018 ProThemes.Biz
 *
 */
?>

<link href="<?php themeLink('premium/css/premium.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?php themeLink('premium/css/bootstrap-formhelpers.min.css'); ?>" rel="stylesheet" type="text/css" />

<script>
var currencySymbol = '<?php echo $currencySymbol; ?>';
var currencyType = '<?php echo $currencyType; ?>';
var oneTimeStr = '<?php makeJavascriptStr($lang['AD750'], true); ?>';
var errStr = '<?php makeJavascriptStr($lang['AD890'], true); ?>';
</script>

<div class="container main-container">
    <div class="row">
  	    <?php
        if($themeOptions['general']['sidebar'] == 'left')
            require_once(THEME_DIR."sidebar.php");
        ?>
        <div class="col-md-8 main-index">
            <br />
            <?php if(!isset($_SESSION[N_APP.'premiumClient'])){ ?>                                
    		<div class="progress-meter-container step1">
             <div class="progress-meter"><div class="meter"></div></div> 
             <div class="premium-bg-1"></div>
             <div class="premium-1"><h5 class="sclass"><?php trans('ACCOUNT', $lang['AD891']); ?></h5></div><div class="premium-1-inner tagactive"><i class="fa fa-user activeiciion"></i></div>
             <div class="premium-bg-2"></div>
             <div class="premium-2"><h5 class="sclass dff"><?php trans('BILLING', $lang['AD892']); ?></h5></div><div class="premium-2-inner"><i class="fa fa-building-o notactiveicon"></i></div>
    	     <div class="premium-bg-3"></div>
    	     <div class="premium-3"><h5 class="sclass dff2"><?php trans('CHECKOUT', $lang['AD893']); ?></h5></div><div class="premium-3-inner"><i class="fa fa-credit-card notactiveicon abdioo"></i></div>
    	     <div class="premium-bg-4"></div>
    	     <div class="premium-4"><h5 class="sclass dff3"><?php trans('CONFIRMATION', $lang['AD894']); ?></h5></div><div class="premium-4-inner"><i class="fa fa-check notactiveicon abdioo"></i></div>
            </div>
	
	
	    <!-- page row -->
	    <div class="row">

         <!-- Step 1 -->   
         <div id="step1">

	      <h4 class="h4dd"> <?php trans('ACCOUNT INFORMATION', $lang['AD895']); ?></h4>
          
	      <p class="pdd"><?php trans('Here, type your login information to subscribe into new premium membership', $lang['AD896']); ?></p>

		  <h4 class="h4information"><span>1</span> <?php trans('Account Information', $lang['AD897']); ?> :</h4>
          
 		 <?php if(!isset($_SESSION[N_APP.'UserToken'])){ ?>
        <?php if($enable_oauth){ ?>
		<div class="premiumLogin text-center">
			<div class="info"><?php trans('Sign in using social network',$lang['RF58']); ?></div>
			<a href="<?php createLink('facebook/login'); ?>" class="connect facebook" title="<?php trans('Sign in using Facebook',$lang['RF59']); ?>"><?php trans('Facebook',$lang['RF62']); ?></a>
        	<a href="<?php createLink('google/login'); ?>" class="connect google" title="<?php trans('Sign in using Google',$lang['RF60']); ?>"><?php trans('Google',$lang['RF63']); ?></a>  	
        	<a href="<?php createLink('twitter/login'); ?>" class="connect twitter" title="<?php trans('Sign in using Twitter',$lang['RF61']); ?>"><?php trans('Twitter',$lang['RF64']); ?></a>		        
	    </div>
        <?php } ?>
         
        <div class="col-md-6">
        
			<form action="<?php createLink('account/register'); ?>" method="POST" class="loginme-form">
			<div class="premium-body">
	        <h5 class="h5dd"> <?php trans('New Customer', $lang['AD898']); ?></h5>

				<div class="text-center"><?php echo $lang['277']; ?></div>
                <br />
				<div class="form-group">
					<label><?php trans('Username',$lang['RF66']); ?> <br />
						<input type="text" name="username" class="form-input width96" />
					</label>
				</div>	
				<div class="form-group">
					<label><?php trans('Email',$lang['RF73']); ?> <br />
						<input type="text" name="email" class="form-input width96" />
					</label>
				</div>
				<div class="form-group">
					<label><?php trans('Full Name',$lang['RF72']); ?> <br />
						<input type="text" name="full" class="form-input width96" />
					</label>
				</div>
				<div class="form-group">
					<label><?php trans('Password',$lang['RF67']); ?> <br />
						<input type="password" name="password" class="form-input width96" />
					</label>
				</div>
				</div>
			<div class="premium-footer"> <br />
            	<div class="pull-right align-right">
				<button type="submit" class="btn btn-primary"><?php echo $lang['264']; ?></button>	
                	</div>
			</div>
			 <input type="hidden" name="signup" value="<?php echo md5($date.$ip); ?>" />
			</form>
            
	    </div><!-- /.col-md-6 -->
                
        <div class="col-md-6">
        
            <form method="POST" action="<?php createLink('account/login'); ?>" class="loginme-form">
			<div class="premium-body">
	        <h5 class="h5dd"> <?php trans('Returning Customer', $lang['AD899']); ?></h5>
          
				<div class="text-center"><?php echo $lang['270']; ?></div>
                <br />
				<div class="form-group">
					<label><?php trans('Username',$lang['RF66']); ?> <br />
						<input type="text" name="username" class="form-input width96" />
					</label>
				</div>	
				<div class="form-group">
					<label><?php trans('Password',$lang['RF67']); ?> <br />
						<input type="password" name="password" class="form-input width96" />
					</label>
				</div>
			</div>
			<div class="premium-footer"> <br />
				<div class="pull-right align-right">
                    <br /><br />
				    <a href="<?php createLink('account/forget'); ?>"><?php trans('Forgot Password',$lang['RF68']); ?></a><br />
					<a href="<?php createLink('account/resend'); ?>"><?php trans('Resend Activation Email',$lang['RF69']); ?></a>
				</div>

				<div class="pull-right align-right">
                    <button type="submit" class="btn btn-primary  pull-left"><?php echo $lang['263']; ?></button>
				</div>
			</div>
			 <input type="hidden" name="signin" value="<?php echo md5($date.$ip); ?>" />
			</form> 
            
	    </div><!-- /.col-md-6 -->
        <?php } else {?>
        
         <div class="lockscreen-wrapper">
     
          <div class="lockscreen-item">

            <div class="lockscreen-image text-center">
              <img style="width: 120px; height: 120px;" alt="<?php trans('User Image', $lang['AD900']); ?>" src="<?php echo $userLogo; ?>" />
            </div>

             <div class="form-group">
                <?php echo $lang['RF66']; ?> <br />
				<input value="<?php echo $userInfo['username']; ?>" disabled="" type="text" name="username" class="form-control"  style="width: 96%;"/>
			 </div>
             
             <div class="form-group">
				<?php echo $lang['RF73']; ?> <br />
				<input value="<?php echo $userInfo['email_id']; ?>" disabled="" type="text" name="email" class="form-control"  style="width: 96%;"/>
			 </div>
            <div class="lockscreen-image text-center">
                <a href="#" id="conBtn" class="text-center btn btn-primary"><i class="fa fa-arrow-right"></i> <?php trans('CONTINUE', $lang['AD901']); ?></a>
                <br /><br />
                <a class="text-center" href="<?php createLink('&logout'); ?>" title="Logout"><?php trans('Or sign in as a different user', $lang['AD902']); ?></a>
			</div>        
          </div>
        </div>
   
        <?php } ?>
        </div>
        
        <!-- Step 2 -->
        <div id="step2" class="hide">
        <h4 class="h4dd"> <?php trans('BILLING DETAILS', $lang['AD903']); ?> </h4>
          
        <p class="pdd"><?php trans('Here, type your full personal information used for billing/invoice.', $lang['AD904']); ?></p>

        <h4 class="h4information"><span>2</span> <?php trans('Your Personal Details', $lang['AD905']); ?> :</h4>
        
        <form> 
        <div class="col-md-6">

 				
                 <div class="form-group">
					<?php echo $lang['RF120']; ?> <br />
					<input value="<?php echo $firstname; ?>" placeholder="<?php trans('Enter your first name', $lang['RF148']); ?>" type="text" name="firstname" class="form-control"  style="width: 96%;"/>
				</div>	
                
                 <div class="form-group">
					<?php echo $lang['RF121']; ?> <br />
					<input value="<?php echo $lastname; ?>" placeholder="<?php trans('Enter your last name', $lang['RF147']); ?>" type="text" name="lastname" class="form-control"  style="width: 96%;"/>
				</div>
                
                <div class="form-group">
			         <?php echo $lang['RF122']; ?> <br />
					 <input value="<?php echo $company; ?>" placeholder="<?php echo $lang['RF146']; ?>" type="text" name="company" class="form-control"  style="width: 96%;"/>
				</div>  
                
                <div class="form-group">
			          <?php echo $lang['RF129']; ?> <br />
					 <input value="<?php echo $telephone; ?>" placeholder="<?php echo $lang['RF145']; ?>" type="text" name="telephone" class="form-control bfh-phone" data-country="country" style="width: 96%;" />
				</div>
                
                <div class="form-group">
			         <?php echo $lang['RF127']; ?> <br />
					 <select id="country" name="country" class="form-control bfh-countries" data-country="<?php echo $country != '' ? $country : 'IN'; ?>" style="width: 96%;"></select>
				</div>
                
        </div><!-- /.col-md-6 -->
        
        <div class="col-md-6">
           
            <div class="form-group">
		          <?php echo $lang['RF123']; ?> <br />
				 <input value="<?php echo $address1; ?>" placeholder="<?php echo $lang['RF144']; ?>" type="text" name="address1" class="form-control"  style="width: 96%;"/>
			</div>    
            
            <div class="form-group">
		         <?php echo $lang['RF124']; ?> <br />
				 <input value="<?php echo $address2; ?>" placeholder="<?php echo $lang['RF143']; ?>" type="text" name="address2" class="form-control"  style="width: 96%;"/>
			</div> 
            
            <div class="form-group">
		        <?php echo $lang['RF125']; ?> <br />
	 	        <input value="<?php echo $city; ?>" placeholder="<?php echo $lang['RF142']; ?>" type="text" name="city" class="form-control"  style="width: 96%;"/>
			</div>
            
            <div class="form-group">
		         <?php echo $lang['RF128']; ?> <br />
				 <input value="<?php echo $postcode; ?>" placeholder="<?php echo $lang['RF141']; ?>" type="text" name="postcode" class="form-control"  style="width: 96%;"/>
			</div>  
            
            <div class="form-group">
		          <?php echo $lang['RF133']; ?> <br />
                  <?php $state = ($state != '' ? 'data-state="'.$state.'"' : ''); ?>
				 <select name="state" class="form-control bfh-states" data-country="country" <?php echo $state; ?> style="width: 96%;"></select>
			</div>
                
        </div><!-- /.col-md-6 -->
        </form>
        </div>
        
        <!-- Step 3 -->
        <div id="step3" class="hide">
        <h4 class="h4dd"> <?php trans('CHECKOUT DETAILS', $lang['AD906']); ?> </h4>
          
        <p class="pdd"><?php trans('Here, select your payment type and also you will find information about this plan', $lang['AD907']); ?></p>

        <h4 class="h4information"><span>3</span> <?php trans('Checkout Details', $lang['AD908']); ?> :</h4>
        
        <table class="table table-bordered table-striped" align="center" style="width: 94%;">
            <thead>
                <th><?php echo $lang['AD883']; ?></th>
                <th><?php trans('Premium SEO Tools', $lang['AD909']); ?></th>
                <th><?php echo $lang['AD747']; ?></th>
                <th><?php trans('Branded PDF', $lang['AD910']); ?></th>
            </thead>
                    <tbody>
                    <tr>
                        <td><?php echo $data['plan_name']; ?></td>
                        <td><ul>
                        <?php 
                        foreach($tools as $myTool)
                            echo '<li>'.$myTool[0].'</li>';
                        ?>
                        </ul></td>
                        <td><?php echo $allowPDF; ?></td>
                        <td><?php echo $brandPDF; ?></td>
                    </tr>
                    </tbody>
         </table>

        <table class="table table-bordered" align="center" style="width: 94%;">
            <thead>
                <th><?php trans('Billing Type', $lang['AD911']); ?></th>
                <th><?php trans('Amount', $lang['AD912']); ?></th>
            </thead>
                    <tbody>
                    <tr>
                        <td><?php echo $billingType . $recCheckBoxData; ?></td>
                        <td id="planAm"><?php echo $planAmount; ?></td>
                    </tr>
                    </tbody>
         </table>
         
         <div id="paymentGateway" style=" margin: 0 auto; width: 94%;">
            <h4 class="bold"><?php trans('Payment Type', $lang['AD913']); ?>:</h4>
            <?php trans('Please select the preferred payment method to use on this order:', $lang['AD914']); ?>
                <?php echo $paymentGatewayData; ?>
         </div>
         
         <?php if($checkOutTerms){ ?>
            
            <div id="checkOut" style=" margin: 0 auto; width: 94%;">
                <br />
                <input type="checkbox" id="agree" name="agree" />
                <?php trans('I have read and agree to the', $lang['AD916']); ?> <a target="_blank" href="<?php echo $checkOutPage; ?>"><?php trans('Terms and Conditions', $lang['AD915']); ?></a>.
            </div>
            
         <?php } ?>

        </div>
        
        <!-- Step 4 -->
        <div id="step4" class="hide">
        <h4 class="h4dd"> <?php trans('CONFIRM ORDER', $lang['AD917']); ?> </h4>
          
        <p class="pdd"><?php trans('Final confirmation before making into payment', $lang['AD918']); ?></p>

        <h4 class="h4information"><span>4</span> <?php trans('Plan CONFIRMATION', $lang['AD919']); ?> :</h4>
        
        <table class="table table-bordered table-striped" align="center" style="width: 94%;">
            <thead>
                <th><?php trans('Plan ID', $lang['AD920']); ?></th>
                <th><?php echo $lang['AD883']; ?></th>
                <th><?php echo $lang['AD911']; ?> </th>
                <th><?php trans('Price', $lang['AD921']); ?></th>
            </thead>
                    <tbody>
                    <tr>
                        <td>#<?php echo $data['id']; ?></td>
                        <td><?php echo $data['plan_name']; ?></td>
                        <td id="bill_Val"></td>
                        <td id="bill_Price"></td>
                    </tr>
                    </tbody>
         </table>
         
         <div class="text-center" style="width: 94%;">
             <div class="totalLeft">
             <div class="bill6 gray16">
             <?php trans('Billing Address', $lang['AD922']); ?>
             </div>
             <div id="invoiceAddress"></div>
             </div>
             <div class="totalRight" id="ajaxTotal">
             </div>
         </div>
        
        </div>
        
        <input type="hidden" name="stage" value="1" />
        <input type="hidden" name="eToken" value="<?php echo $eToken; ?>" />
        <input type="hidden" name="one_amount" value="<?php echo $planAmount; ?>" />
        <input type="hidden" name="billVal" value="<?php echo $data['payment_type']; ?>" />
        <input type="hidden" name="planID" value="<?php echo $data['id']; ?>" />       
        </div><!-- /.row -->
	  
	  
	  	<div class="nextPrev row botton-row">
		     <div class="leftfloat"> <a href="#" id="prevBtn" class="action-button shadow animate green"><i class="fa fa-arrow-left iprev"></i><?php trans('PREV', $lang['AD923']); ?></a></div>
             <div class="rightfloat"><a href="#" id="nextBtn" class="action-button shadow animate blue"><i class="fa fa-arrow-right"></i> <?php trans('NEXT', $lang['AD924']); ?></a></div>			 
		</div><!-- /.botton-row -->
        
        <?php } else {  ?>
        
        <div class="text-center">
        <div class="alert alert-error">
            <h3><?php trans('You have an active subscription!', $lang['AD925']); ?></h3>
        </div>
            <p><?php trans('If you\'ve already bought a subscription and you can\'t make new subscription without cancelling it.', $lang['AD926']); ?></p>
            
            <a class="text-center btn btn-success" href="<?php createLink('my-dashboard'); ?>" title="<?php echo $lang['AD870']; ?>"><?php echo $lang['AD730']; ?></a>
            <a class="text-center btn btn-danger" href="<?php createLink('my-subscription/cancel'); ?>" title="<?php trans('Cancelling my subscription', $lang['AD927']); ?>"><?php trans('Cancel my subscription', $lang['AD928']); ?></a>
            <a class="text-center btn btn-warning" href="<?php createLink('&logout'); ?>" title="<?php echo $lang['RF104']; ?>"><?php trans('Sign in as a different user', $lang['AD929']); ?></a>
        </div>
        <?php } ?>
                <br /><br /><br /><br />
    </div>
    <?php
    if($themeOptions['general']['sidebar'] == 'right')
        require_once(THEME_DIR."sidebar.php");
    ?>
</div>
</div>
<br />
<script src="<?php themeLink('premium/js/bootstrap-formhelpers.min.js'); ?>" type="text/javascript"></script>
<script src="<?php themeLink('premium/js/subscribe.js'); ?>" type="text/javascript"></script>