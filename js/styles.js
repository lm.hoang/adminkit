$(document).ready(function() {
    //Browse More Tools
    var w = $(window).width();
    if (w >= 1139) {
        $(".admintool .box-hidden").slice(0, 10).show();
        $(".section-seotool .box-hidden").slice(0, 10).show();
        $(".webPeforTools .box-hidden").slice(0, 10).show();
        $(".othertool .box-hidden").slice(0, 10).show();
    }
    if (w >= 992 && w < 1139) {
        $(".admintool .box-hidden").slice(0, 8).show();
        $(".section-seotool .box-hidden").slice(0, 8).show();
        $(".webPeforTools .box-hidden").slice(0, 8).show();
        $(".othertool .box-hidden").slice(0, 8).show();
    } else if (w >= 768 && w < 992) {
        $(".admintool .box-hidden").slice(0, 9).show();
        $(".section-seotool .box-hidden").slice(0, 9).show();
        $(".webPeforTools .box-hidden").slice(0, 9).show();
        $(".server-tool .box-hidden").slice(0, 9).show();
        $(".othertool .box-hidden").slice(0, 9).show();
    } else {
        $(".admintool .box-hidden").slice(0, 8).show();
        $(".section-seotool .box-hidden").slice(0, 9).show();
        $(".webPeforTools .box-hidden").slice(0, 8).show();
        $(".othertool .box-hidden").slice(0, 8).show();
    }
    $(".btn-more").on('click', function(e) {
        var section = $(this).parents('section');
        e.preventDefault();
        section.find(".box-hidden:hidden").slice(0, 10).slideDown();
        $(this).css('display', 'none');
    });
    //set up owl of banner
    $('.owl-carousel-banner').owlCarousel({
        loop: true,
        margin: 0,
        nav: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });
    //add-more links-title
    $('.links-title').click(function() {
        $('.links-title .add-more').toggleClass('add-more-change');
        $('.show-links').toggleClass('show-links-change');
    });

    //Open-more	
    $("a[href^='#']").click(function(e) {
        e.preventDefault();
        
        var position = $($(this).attr("href")).offset().top;

        $("body, html").animate({
            scrollTop: position
        },3, function() {
                $('.menu-table').addClass('d-none');
            setTimeout(function() {
                $('.menu-table').removeClass('d-none');
            }, 1000);
        });
    });

    
});