$(document).ready(function(){
      $('.owl-slide-use').owlCarousel({
        loop:false,
        margin:35,
        nav:true,
        dots:true,
        responsive:{
            0:{
                items:1
            },
            769:{
                items:2
            },
            1139: {
                items: 2
            },
            1140: {
                items: 3
            }
        }
    })
});