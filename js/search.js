function autocomplete(inp, arr) {
  var currentFocus;
  var inp = document.getElementById('myInput');
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      a = document.createElement("DIV");
      a.setAttribute("class", "wrap-suggest");
      this.parentNode.appendChild(a);
      for (i = 0; i < arr.length; i++) {
        if (arr[i]['title'].substr(0, val.length).toUpperCase() == val.toUpperCase() || arr[i]['description'].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          // create element a
          b = document.createElement("a");
          b.setAttribute("class", "icon-box");
          a.setAttribute("id", this.id + "autocomplete-list");
          var links_a = arr[i]['links-a'];
          b.setAttribute("href", links_a);
          b.innerHTML = '<div class="box-images"><img src="'+ arr[i]['links-img'] +'" alt=""></div>';
          if (arr[i]['title'].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
              b.innerHTML += '<div class="box-text"><h2 class="title-suggest"><strong>' + arr[i]["title"].substr(0, val.length) + '</strong>'+ arr[i]["title"].substr(val.length) +'</h2> <p class="desc-suggest">'+ arr[i]["description"]+'</p></div>';
          }else {
              b.innerHTML += '<div class="box-text"><h2 class="title-suggest">'+ arr[i]["title"]+'</h2> <p class="desc-suggest"><strong>' + arr[i]["description"].substr(0, val.length) + '</strong>'+ arr[i]["description"].substr(val.length) +'</p></div>';

          }
          b.innerHTML += "<input type='hidden' value='" + arr[i]['title'] + "'>";
          b.addEventListener("click", function(e) {
              inp.value = this.getElementsByTagName("input")[0].value;
              closeAllLists();
          });
          a.appendChild(b);
        }
      }
  });
  //event Keydown
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("a");
      if (e.keyCode == 40) { //down
        currentFocus++;
        addActive(x);
        if(($(".autocomplete-active").offset().top + $("#" + this.id + "autocomplete-list").offset().top) > $( '#' +  "myInputautocomplete-list" ).height()){
            $( '#' +  "myInputautocomplete-list" ).scrollTop( ($("#" + this.id + "autocomplete-list").scrollTop() + $(".autocomplete-active").height()) + 20);
          }
        if (currentFocus == 0) {
            $('#' + "myInputautocomplete-list").animate({
                scrollTop: 0
            }, 'slow');
            return;
        }
      } else if (e.keyCode == 38) { //up
        currentFocus--;
        addActive(x);
        if($(".autocomplete-active").offset().top > $( '#' +  "myInputautocomplete-list" ).height()) {
           $('#' + "myInputautocomplete-list").animate({
                scrollTop: $(".autocomplete-active").offset().top
            }, 'slow');
        }
        if($(".autocomplete-active").offset().top < $("#" + this.id + "autocomplete-list").offset().top) {
          $("#" + this.id + "autocomplete-list").scrollTop(($("#" + this.id + "autocomplete-list").scrollTop() - $(".autocomplete-active").height() - 30 ));

        }
      } else if (e.keyCode == 13) { //enter
        e.preventDefault();
        if (currentFocus > -1) {
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    if (!x) return false;
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    var x = document.getElementsByClassName("wrap-suggest");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
  });
}
function readTextFile(file) {
	console.log(file);
    var rawFile = new XMLHttpRequest(); // XMLHttpRequest (often abbreviated as XHR) is a browser object accessible in JavaScript that provides data in XML, JSON, but also HTML format, or even a simple text using HTTP requests.
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4) // readyState = 4: request finished and response is ready
        {
            if(rawFile.status === 200) // status 200: "OK"
            {
                var allText = rawFile.responseText; //  Returns the response data as a string
                var countries = JSON.parse(allText);
                autocomplete(document.getElementById("myInput"), countries);
            }
        }
    }
  rawFile.open("GET", file); // open with method GET the file with the link file ,  false (synchronous)
    rawFile.send(null); //Sends the request to the server Used for GET requests with param null
}

readTextFile("../data/search.txt"); //<= Call function ===== don't need "file:///..." just the path