$(document).ready(function() {
	$(".today-login").click(function() {
		$(".section-login").css({
			right: '0',
			transition: 'all 0.5s ease 0s'
		});
		$('.bg-default').addClass("bg-login");
		$('html').css('overflow', 'hidden');
		$('html').css('overflow', 'hidden');
	})
	$(".btn-close-login").click(function() {
		$(".section-login").css({
			right: '-100%',
		});
		$('html').css('overflow', 'auto');
		$('.bg-default').removeClass("bg-login");
	});

	$('.bg-default').on('click', function() {
		event.preventDefault();
		$('.btn-close-login').trigger('click');
	});
   	// password
	$('#showPassword').on('click', function(){
	    var passwordField = $('#password');
	    var passwordFieldType = passwordField.attr('type');
	    if(passwordFieldType == 'password')
	    {
	        passwordField.attr('type', 'text');
	        $(this).val('Hide');
	        $('.show-eye').hide();
	        $('.hide-eye').show();
	    } else {
	        passwordField.attr('type', 'password');
	        $(this).val('Show');
	        $('.show-eye').show();
	        $('.hide-eye').show();
	    }

  });
	// Create-signup
	$('.Create-signup-cb').click(function() {
		$('.title-Signup').show();
		$('.intro-signup').show();
		$('.privacy-Terms').show();
		$('.btn-signup').show();
		$('.create-login').show();
		$('.confirm_pass').show();
		$('.pass-login').show();
		$('.checkbox-login').show();
		$('.name-login').show();

		$('.title-login').hide();
		$('.login-socical').hide();
		$('.btn-login-popup').hide();
		$('.forgot-pass').hide();
		$('.create-signup').hide();
		$('.cmback-login').hide();
		$('.cmbank-signup').hide();
		$('.btn-forgot').hide();
		$('.title-forgot').hide();
		$('.intro-forgot').hide();
		$('.checkbox-signup').hide();
		$('.des-login').hide();


	});

	//Create-login
	$('.Create-login-cb').click(function() {
		$('.title-Signup').hide();
		$('.intro-signup').hide();
		$('.privacy-Terms').hide();
		$('.btn-signup').hide();
		$('.create-login').hide();
		$('.checkbox-login ').hide();
		$('.confirm_pass').hide();
		$('.cmback-login').hide();
		$('.cmbank-signup').hide();
		$('.btn-forgot').hide();
		$('.title-forgot').hide();
		$('.intro-forgot').hide();
		$('.name-login').hide();
		$('.name-login').hide();

		$('.title-login').show();
		$('.login-socical').show();
		$('.btn-login-popup').show();
		$('.forgot-pass').show();
		$('.create-signup').show();
		$('.checkbox-signup').show();
		$('.pass-login').show();
		$('.pass-login').show();

	});

	$('.forgot-pass').on('click', function() {
		$('.title-forgot').show();
		$('.intro-forgot').show();
		$('.btn-forgot').show();
		$('.cmback-login').show();
		$('.cmbank-signup').show();


		$('.title-Signup').hide();
		$('.des-login').hide();
		$('.title-login').hide();
		$('.login-socical').hide();
		$('.name-login').hide();
		$('.pass-login').hide();
		$('.checkbox-login').hide();
		$('.forgot-pass').hide();
		$('.create-signup').hide();
		$('.btn-signup').hide();
		$('.btn-login-popup').hide();
		$('.checkbox-signup').hide();
	});
});
