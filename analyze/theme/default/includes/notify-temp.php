<html>
    <body>
    	<div style="height: 865px;width: 775px;background: #DCE7EE;position: relative;left: 30%;">
	    	<div style="padding: 80px 80px 15px 80px;">
	    		<p style="padding-bottom: 10px;">Time : %nowDate%</p>
		        <p>Dear <b>%admin_mail%</b></p>
		        <p>Our system has received a message from <b>%contact_name%</b> by %contact_mail%</p>
		    </div>
		    <div style="display: block;padding:0 80px;font-size: 16px; ">
		    	<p style="text-align: center;border-style: double;font-weight: bold;">Customer Contact Notification</p>
		    	<div style="padding:0 10px;">
		    		<p><b>Contact topic: </b>%for%</p>
				    <p>
				       	<b>User message:</b>
				        <div>%message%</div>
					</p>
				    <p>
				        <b>Additional Infomartion:</b> 
				        <br>
				        User name: %username%
				        <br>
				        Company Name: %company%
				        <br>
				        Company Size: %comp_size%
				        <br>
				        Customer Site URL: %url%
				        <br>
				        Budget: %budget%
				    </p>
		    	</div>
			</div>
			<div style="padding: 10px 80px;">
				<p>You are receiving this message because your preferences
				<br>are set to receive these notifications</p>
				<br>
				<p>Thank you,The Adminkit team</p>
			</div>
		</div>
    </body>
</html>