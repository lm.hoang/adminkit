<html>
    <body>
    	<div style="height: 865px;width: 775px;background: #DCE7EE;position: relative;left: 30%;">
    		<!-- header -->
    		<div style="padding: 80px 80px 15px 80px;">
    			<p style="padding-bottom: 10px;">Time : %nowDate%</p>
    			<p>Dear <b>%contact_name%</b>,</p>
	        	<p>We are happy to receive a message from yours by %contact_mail% with this topic.</p>
    		</div>
	        <!-- body -->
	       	<div style="display: block;padding:0 80px;font-size: 16px; ">
	       		<div style="padding:0 10px;">
			        <p><b>Topic: </b>%for%</p>
			        <p>
			        	<b>User message:</b>
			        	<div>%message%</div>
				    </p>
				</div>
		    </div>
		    <!-- footer -->
		    <div style="padding: 10px 80px;">
		        <p>Thank you for your communication.</p>
		        <p>We will provide you an answer for your topic as soon as possible.</p>
		        <p>Best regards,<br>
			        <b style="font-size: 17px;">Admitkit Team</b><br>
			        <small>https://adminkit.net/</small>
			    </p>
		    </div>
	    </div>
    </body>
</html>