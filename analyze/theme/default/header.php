<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
/*
* @author Balaji
* @Theme: Default Style
* @copyright 2018 ProThemes.Biz
*
*/
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="icon" type="image/png" href="<?php echo $themeOptions['general']['favicon']; ?>" />
    <?php
    $_uri = str_replace("/"," ",$_SERVER['REQUEST_URI']);
    $_pagename = str_replace("-"," ",ucfirst($_uri));
    ?>
    <title><?php echo  $_pagename.' | '?>AdminKit – Freemium Tools & Consultancy Services for Website and IT Admin</title>
    <meta property="site_name" content="<?php echo $site_name; ?>"/>
    <meta name="description" content="<?php echo $des; ?>" />
    <meta name="keywords" content="<?php echo $keyword; ?>" />
    <meta name="author" content="Balaji" />
    <?php genCanonicalData($baseURL, $currentLink, $loadedLanguages, false, isSelected($themeOptions['general']['langSwitch'])); ?>
    <meta property="og:title" content="AdminKit – Freemium Tools & Consultancy Services for Website and IT Admin" />
    <meta property="og:site_name" content="<?php echo $site_name; ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:description" content="<?php echo $des; ?>" />
    <link href="<?php themeLink('css/bootstrap.min.css'); ?>" rel="stylesheet" media="print" onload="this.onload=null;this.removeAttribute('media');" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" media="print" onload="this.onload=null;this.removeAttribute('media');">
    <?php if($isRTL) echo '<link href="'.themeLink('css/rtl.css',true).'" rel="stylesheet"/>'; ?>
    <link href="<?php themeLink('css/custom.css'); ?>" rel="stylesheet"/>
    <noscript>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </noscript>
    <!-- jQuery 1.10.2 -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" async></script>
    <?php if($themeOptions['custom']['css'] != '') echo '<style>'.htmlPrint($themeOptions['custom']['css'],true).'</style>'; ?>
  </head>
  <body data-spy="scroll" data-target="#scroll-menu" id="top" class="analyze-homepage">
    <!-- nav./start -->
    <nav class="navbar" aria-label="main navigation">
      <div class="wrap-admin container">
        <div class="navbar-brand span4l">
          <a class="navbar-item" href="https://adminkit.net">
            <img loading="lazy" src="/images/adminkit-logo.png" title="Logo adminkit.net" alt="Logo adminkit.net" width="166" height="34">
            <span class="logo-mobile"></span>
          </a>
          <div class="nav-seller-global">
            <div class="border-nav-seller-global">
              <div class="button-nav-gmenu">
                <div class="border-button-nav-gmenu">
                  <svg width="18" height="12" viewBox="0 0 18 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M0 2V0H18V2H0Z" fill="#444"></path>
                <path d="M0 7H18V5H0V7Z" fill="#444"></path>
              <path d="M0 12H18V10H0V12Z" fill="#444"></path>
            </svg>
            <span class="menu-main">Menu</span>
          </div>
          <!-- menu-table./Start -->
          <div class="menu-table">
            <div class="wrap-menu">
              <div class="container">
                <div class="row">
                  <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-2dot4">
                    <div class="menu-item">
                      <div class="txt-flex-title">
                        <h2 class="title-menu">Admin Tool Kits</h2>
                        <i class="ic-mobile">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                        <path d="M6 7.494L11.392 3l.608.729-6 5-6-5L.608 3z"></path>
                      </svg>
                      </i>
                    </div>
                    <div class="box-menu box-one">
                      <div class="moreBox">
                        <div class="icon-box">
                          <div class="box-images">
                            <img loading="lazy" src="/images/menus/admin-tools/BackList_Check.svg" alt="">
                          </div>
                          <div class="box-text">
                            <p><a href="https://adminkit.net/dnsbl.aspx">BlackList Check</a></p>
                          </div>
                        </div>
                      </div>
                      <div class="moreBox">
                        <div class="icon-box">
                          <div class="box-images">
                            <img loading="lazy" src="/images/menus/admin-tools/MX_Lookup.svg" alt="">
                          </div>
                          <div class="box-text">
                            <p><a href="https://adminkit.net/mxlookup.aspx">MX Lookup</a></p>
                          </div>
                        </div>
                      </div>
                      <div class="moreBox">
                        <div class="icon-box">
                          <div class="box-images">
                            <img loading="lazy" src="/images/menus/admin-tools/DNS_Lookup.svg" alt="">
                          </div>
                          <div class="box-text">
                            <p><a href="https://adminkit.net/dnsrecords.aspx">DNS Lookup</a></p>
                          </div>
                        </div>
                      </div>
                      <div class="moreBox">
                        <div class="icon-box">
                          <div class="box-images">
                            <img loading="lazy" src="/images/menus/admin-tools/Telnet.svg" alt="">
                          </div>
                          <div class="box-text">
                            <p><a href="https://adminkit.net/telnet.aspx">Telnet</a></p>
                          </div>
                        </div>
                      </div>
                      <div class="moreBox">
                        <div class="icon-box">
                          <div class="box-images">
                            <img loading="lazy" src="/images/menus/admin-tools/IP_2_Location.svg" alt="">
                          </div>
                          <div class="box-text">
                            <p><a href="https://adminkit.net/ip2location.aspx">IP 2 Location</a></p>
                          </div>
                        </div>
                      </div>
                      <div class="moreBox">
                        <div class="icon-box">
                          <div class="box-images">
                            <img loading="lazy" src="/images/menus/admin-tools/Ping.svg" alt="">
                          </div>
                          <div class="box-text">
                            <p><a href="https://adminkit.net/ping.aspx">Ping</a></p>
                          </div>
                        </div>
                      </div>
                      <div class="moreBox">
                        <div class="icon-box">
                          <div class="box-images">
                            <img loading="lazy" src="/images/menus/admin-tools/BlackList_Monitor.svg" alt="">
                          </div>
                          <div class="box-text">
                            <p><a href="https://adminkit.net/dnsbl_monitor.aspx">BlackList Monitor</a></p>
                          </div>
                        </div>
                      </div>
                      <div class="moreBox">
                        <div class="icon-box">
                          <div class="box-images">
                            <img loading="lazy" src="/images/menus/admin-tools/SMTP_Test_Tool.svg" alt="">
                          </div>
                          <div class="box-text">
                            <p><a href="https://adminkit.net/smtp.aspx">SMTP Test Tool</a></p>
                          </div>
                        </div>
                      </div>
                      <div class="moreBox">
                        <div class="icon-box">
                          <div class="box-images">
                            <img loading="lazy" src="/images/menus/admin-tools/My_IP_Address.svg" alt="">
                          </div>
                          <div class="box-text">
                            <p><a href="https://adminkit.net/my_ip_address.aspx">My IP Adress</a></p>
                          </div>
                        </div>
                      </div>
                      <div class="moreBox">
                        <div class="icon-box">
                          <div class="box-images">
                            <img loading="lazy" src="/images/menus/admin-tools/Trace_route.svg" alt="">
                          </div>
                          <div class="box-text">
                            <p><a href="https://adminkit.net/traceroute.aspx">Trace route</a></p>
                          </div>
                        </div>
                      </div>
                      <div class="moreBox box-hidden">
                        <div class="icon-box">
                          <div class="box-images">
                            <img loading="lazy" src="/images/menus/admin-tools/Whois.svg" alt="">
                          </div>
                          <div class="box-text">
                            <p><a href="https://adminkit.net/whois.aspx">Whois</a></p>
                          </div>
                        </div>
                      </div>
                      <div class="moreBox box-hidden">
                        <div class="icon-box">
                          <div class="box-images">
                            <img loading="lazy" src="/images/menus/admin-tools/Desktop_Tools.svg" alt="">
                          </div>
                          <div class="box-text">
                            <p><a href="https://adminkit.net/applications.aspx">Desktop Tools</a></p>
                          </div>
                        </div>
                      </div>
                      <div class="box-more">
                        <a class="openMore" href="#admintool">Open More..</a>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-2dot4">
                  <div class="menu-item">
                    <div class="txt-flex-title">
                      <h2 class="title-menu">Server Performance Tools</h2>
                      <i class="ic-mobile">
                      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                      <path d="M6 7.494L11.392 3l.608.729-6 5-6-5L.608 3z"></path>
                    </svg>
                    </i>
                  </div>
                  <div class="box-menu box-two">
                    <div class="moreBox">
                      <div class="icon-box">
                        <div class="box-images">
                          <img loading="lazy" src="/images/menus/server-tools/Online_Ping_Website_Tool.svg" alt="">
                        </div>
                        <div class="box-text">
                          <p><a href="https://seotool.adminkit.net/online-ping-website-tool">Online Ping Website Tool</a></p>
                        </div>
                      </div>
                    </div>
                    <div class="moreBox">
                      <div class="icon-box">
                        <div class="box-images">
                          <img loading="lazy" src="/images/menus/server-tools/My_IP_Address.svg" alt="">
                        </div>
                        <div class="box-text">
                          <p><a href="https://seotool.adminkit.net/my-ip-address">My IP Address</a></p>
                        </div>
                      </div>
                    </div>
                    <div class="moreBox">
                      <div class="icon-box">
                        <div class="box-images">
                          <img loading="lazy" src="/images/menus/server-tools/Server_Status_Checker.svg" alt="">
                        </div>
                        <div class="box-text">
                          <p><a href="https://seotool.adminkit.net/server-status-checker">Server Status Checker</a></p>
                        </div>
                      </div>
                    </div>
                    <div class="moreBox">
                      <div class="icon-box">
                        <div class="box-images">
                          <img loading="lazy" src="/images/menus/server-tools/Reverse_IP_Domain_Checker.svg" alt="">
                        </div>
                        <div class="box-text">
                          <p><a href="https://seotool.adminkit.net/reverse-ip-domain-checker">Reverse IP Domain Checker</a></p>
                        </div>
                      </div>
                    </div>
                    <div class="moreBox">
                      <div class="icon-box">
                        <div class="box-images">
                          <img loading="lazy" src="/images/menus/server-tools/Blacklist_Lookup.svg" alt="">
                        </div>
                        <div class="box-text">
                          <p><a href="https://seotool.adminkit.net/blacklist-lookup">Blacklist Lookup</a></p>
                        </div>
                      </div>
                    </div>
                    <div class="moreBox">
                      <div class="icon-box">
                        <div class="box-images">
                          <img loading="lazy" src="/images/menus/server-tools/Domain_Hosting_Checker.svg" alt="">
                        </div>
                        <div class="box-text">
                          <p><a href="https://seotool.adminkit.net/domain-hosting-checker">Domain Hosting Checker</a></p>
                        </div>
                      </div>
                    </div>
                    <div class="moreBox">
                      <div class="icon-box">
                        <div class="box-images">
                          <img loading="lazy" src="/images/menus/server-tools/Find_DNS_records.svg" alt="">
                        </div>
                        <div class="box-text">
                          <p><a href="https://seotool.adminkit.net/find-dns-records">Find DNS records</a></p>
                        </div>
                      </div>
                    </div>
                    <div class="moreBox">
                      <div class="icon-box">
                        <div class="box-images">
                          <img loading="lazy" src="/images/menus/server-tools/Domain_Authority_Checker.svg" alt="">
                        </div>
                        <div class="box-text">
                          <p><a href="https://seotool.adminkit.net/domain-authority-checker">Domain Authority Checker</a></p>
                        </div>
                      </div>
                    </div>
                    <div class="moreBox">
                      <div class="icon-box">
                        <div class="box-images">
                          <img loading="lazy" src="/images/menus/server-tools/Domain_intro_IP.svg" alt="">
                        </div>
                        <div class="box-text">
                          <p><a href="https://seotool.adminkit.net/domain-into-ip">Domain into IP</a></p>
                        </div>
                      </div>
                    </div>
                    <div class="box-more" style="display: none">
                      <a class="openMore" href="#seotool">Open More..</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-2dot4">
                <div class="menu-item">
                  <div class="txt-flex-title">
                    <h2 class="title-menu">Web Peformance Tools</h2>
                    <i class="ic-mobile">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                    <path d="M6 7.494L11.392 3l.608.729-6 5-6-5L.608 3z"></path>
                  </svg>
                  </i>
                </div>
                <div class="box-menu box-three">
                  <div class="moreBox">
                    <div class="icon-box">
                      <div class="box-images">
                        <img loading="lazy" src="/images/menus/web-tools/Webpage_Screen_Resolution.svg" alt="">
                      </div>
                      <div class="box-text">
                        <p><a href="https://seotool.adminkit.net/webpage-screen-resolution-simulator">Webpage Screen Resolution</a></p>
                      </div>
                    </div>
                  </div>
                  <div class="moreBox">
                    <div class="icon-box">
                      <div class="box-images">
                        <img loading="lazy" src="/images/menus/web-tools/Page_Size_Checker.svg" alt="">
                      </div>
                      <div class="box-text">
                        <p><a href="https://seotool.adminkit.net/page-size-checker">Page Size Checker</a></p>
                      </div>
                    </div>
                  </div>
                  <div class="moreBox">
                    <div class="icon-box">
                      <div class="box-images">
                        <img loading="lazy" src="/images/menus/web-tools/Website_Screenshot_Generator.svg" alt="">
                      </div>
                      <div class="box-text">
                        <p><a href="https://seotool.adminkit.net/website-screenshot-generator">Website Screenshot Generator</a></p>
                      </div>
                    </div>
                  </div>
                  <div class="moreBox">
                    <div class="icon-box">
                      <div class="box-images">
                        <img loading="lazy" src="/images/menus/web-tools/Get_Source_Code_of_Webpage.svg" alt="">
                      </div>
                      <div class="box-text">
                        <p><a href="https://seotool.adminkit.net/get-source-code-of-webpage">Get Source Code of Webpage</a></p>
                      </div>
                    </div>
                  </div>
                  <div class="moreBox">
                    <div class="icon-box">
                      <div class="box-images">
                        <img loading="lazy" src="/images/menus/web-tools/Page_Speed_Checker.svg" alt="">
                      </div>
                      <div class="box-text">
                        <p><a href="https://seotool.adminkit.net/page-speed-checker">Page Speed Checker</a></p>
                      </div>
                    </div>
                  </div>
                  <div class="moreBox">
                    <div class="icon-box">
                      <div class="box-images">
                        <img loading="lazy" src="/images/menus/web-tools/Code_to_Text_Ratio_Checker.svg" alt="">
                      </div>
                      <div class="box-text">
                        <p><a href="https://seotool.adminkit.net/code-to-text-ratio-checker">Code to Text Ratio Checker</a></p>
                      </div>
                    </div>
                  </div>
                  <div class="moreBox">
                    <div class="icon-box">
                      <div class="box-images">
                        <img loading="lazy" src="/images/menus/web-tools/Broken_Links_Finder.svg" alt="">
                      </div>
                      <div class="box-text">
                        <p><a href="https://seotool.adminkit.net/broken-links-finder">Broken Links Finder</a></p>
                      </div>
                    </div>
                  </div>
                  <div class="moreBox">
                    <div class="icon-box">
                      <div class="box-images">
                        <img loading="lazy" src="/images/menus/web-tools/Pagespeed_insights_Checker.svg" alt="">
                      </div>
                      <div class="box-text">
                        <p><a href="https://seotool.adminkit.net/pagespeed-insights-checker">Pagespeed Insights Checker</a></p>
                      </div>
                    </div>
                  </div>
                  <div class="moreBox">
                    <div class="icon-box">
                      <div class="box-images">
                        <img loading="lazy" src="/images/menus/web-tools/Get_HTTP_Headers.svg" alt="">
                      </div>
                      <div class="box-text">
                        <p><a href="https://seotool.adminkit.net/get-http-headers">Get HTTP Headers</a></p>
                      </div>
                    </div>
                  </div>
                  <div class="moreBox">
                    <div class="icon-box">
                      <div class="box-images">
                        <img loading="lazy" src="/images/menus/web-tools/Mobile_Friendly_Test.svg" alt="">
                      </div>
                      <div class="box-text">
                        <p><a href="https://seotool.adminkit.net/mobile-friendly-test">Mobile Friendly Test</a></p>
                      </div>
                    </div>
                  </div>
                  <div class="moreBox box-hidden">
                    <div class="icon-box">
                      <div class="box-images">
                        <img loading="lazy" src="/images/menus/web-tools/Website_Reviewer.svg" alt="">
                      </div>
                      <div class="box-text">
                        <p><a href="https://seotool.adminkit.net/website-reviewer">Website Reviewer</a></p>
                      </div>
                    </div>
                  </div>
                  <div class="moreBox box-hidden">
                    <div class="icon-box">
                      <div class="box-images">
                        <img loading="lazy" src="/images/menus/web-tools/Keyword_Density_Checker.svg" alt="">
                      </div>
                      <div class="box-text">
                        <p><a href="https://seotool.adminkit.net/keyword-density-checker">Keyword Density Checker</a></p>
                      </div>
                    </div>
                  </div>
                  <div class="moreBox box-hidden">
                    <div class="icon-box">
                      <div class="box-images">
                        <img loading="lazy" src="/images/menus/web-tools/Check_GZIP_compression.svg" alt="">
                      </div>
                      <div class="box-text">
                        <p><a href="https://seotool.adminkit.net/check-gzip-compression">Check GZIP compression</a></p>
                        <p>Check GZIP compression</p>
                      </div>
                    </div>
                  </div>
                  <div class="moreBox box-hidden">
                    <div class="icon-box">
                      <div class="box-images">
                        <img loading="lazy" src="/images/menus/web-tools/Class_C_Ip_Checker.svg" alt="">
                      </div>
                      <div class="box-text">
                        <p><a href="https://seotool.adminkit.net/class-c-ip-checker">Class C Ip Checker</a></p>
                      </div>
                    </div>
                  </div>
                  <div class="box-more">
                    <a class="openMore" href="#webPeforTools">Open More..</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-2dot4">
              <div class="menu-item">
                <div class="txt-flex-title">
                  <h2 class="title-menu">SEO Performance Tools</h2>
                  <i class="ic-mobile">
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                  <path d="M6 7.494L11.392 3l.608.729-6 5-6-5L.608 3z"></path>
                </svg>
                </i>
              </div>
              <div class="box-menu box-four">
                <div class="moreBox">
                  <div class="icon-box">
                    <div class="box-images">
                      <img loading="lazy" src="/images/menus/seo-tools/Article_Rewriter.svg" alt="">
                    </div>
                    <div class="box-text">
                      <p><a href="https://seotool.adminkit.net/article-rewriter">Article Rewriter</a></p>
                    </div>
                  </div>
                </div>
                <div class="moreBox">
                  <div class="icon-box">
                    <div class="box-images">
                      <img loading="lazy" src="/images/menus/seo-tools/Backlink_Checker.svg" alt="">
                    </div>
                    <div class="box-text">
                      <p><a href="https://seotool.adminkit.net/backlink-maker">Backlink Maker</a></p>
                    </div>
                  </div>
                </div>
                <div class="moreBox">
                  <div class="icon-box">
                    <div class="box-images">
                      <img loading="lazy" src="/images/menus/seo-tools/Meta_Tag_Generator.svg" alt="">
                    </div>
                    <div class="box-text">
                      <p><a href="https://seotool.adminkit.net/meta-tag-generator">Meta Tag Generator</a></p>
                    </div>
                  </div>
                </div>
                <div class="moreBox">
                  <div class="icon-box">
                    <div class="box-images">
                      <img loading="lazy" src="/images/menus/seo-tools/Meta_Tags_Analyzer.svg" alt="">
                    </div>
                    <div class="box-text">
                      <p><a href="https://seotool.adminkit.net/meta-tags-analyzer">Meta Tags Analyzer</a></p>
                    </div>
                  </div>
                </div>
                <div class="moreBox">
                  <div class="icon-box">
                    <div class="box-images">
                      <img loading="lazy" src="/images/menus/seo-tools/Keyword_Position_Checker.svg" alt="">
                    </div>
                    <div class="box-text">
                      <p><a href="https://seotool.adminkit.net/keyword-position-checker">Keyword Position Checker</a></p>
                    </div>
                  </div>
                </div>
                <div class="moreBox">
                  <div class="icon-box">
                    <div class="box-images">
                      <img loading="lazy" src="/images/menus/seo-tools/Robots.txt_Generator.svg" alt="">
                    </div>
                    <div class="box-text">
                      <p><a href="https://seotool.adminkit.net/robots-txt-generator">Robots.txt Generator</a></p>
                    </div>
                  </div>
                </div>
                <div class="moreBox">
                  <div class="icon-box">
                    <div class="box-images">
                      <img loading="lazy" src="/images/menus/seo-tools/XML_Sitemap_Generator.svg" alt="">
                    </div>
                    <div class="box-text">
                      <p><a href="https://seotool.adminkit.net/xml-sitemap-generator">XML Sitemap Generator</a></p>
                    </div>
                  </div>
                </div>
                <div class="moreBox">
                  <div class="icon-box">
                    <div class="box-images">
                      <img loading="lazy" src="/images/menus/seo-tools/Backlink_Maker.svg" alt="">
                    </div>
                    <div class="box-text">
                      <p><a href="https://seotool.adminkit.net/backlink-checker">Backlink Checker</a></p>
                    </div>
                  </div>
                </div>
                <div class="moreBox">
                  <div class="icon-box">
                    <div class="box-images">
                      <img loading="lazy" src="/images/menus/seo-tools/Alexa_Rank_Checker.svg" alt="">
                    </div>
                    <div class="box-text">
                      <p><a href="https://seotool.adminkit.net/alexa-rank-checker">Alexa Rank Checker</a></p>
                    </div>
                  </div>
                </div>
                <div class="moreBox">
                  <div class="icon-box">
                    <div class="box-images">
                      <img loading="lazy" src="/images/menus/seo-tools/URL_Rewriting_Tool.svg" alt="">
                    </div>
                    <div class="box-text">
                      <p><a href="https://seotool.adminkit.net/url-rewriting-tool">URL Rewriting Tool</a></p>
                    </div>
                  </div>
                </div>
                <div class="moreBox box-hidden">
                  <div class="icon-box">
                    <div class="box-images">
                      <img loading="lazy" src="/images/menus/seo-tools/Google_Index_Checker.svg" alt="">
                    </div>
                    <div class="box-text">
                      <p><a href="https://seotool.adminkit.net/google-index-checker">Google Index Checker</a></p>
                    </div>
                  </div>
                </div>
                <div class="moreBox box-hidden">
                  <div class="icon-box">
                    <div class="box-images">
                      <img loading="lazy" src="/images/menus/seo-tools/Website_Links_Count_Checker.svg" alt="">
                    </div>
                    <div class="box-text">
                      <p><a href="https://seotool.adminkit.net/website-links-count-checker">Website Links Count Checker</a></p>
                    </div>
                  </div>
                </div>
                <div class="moreBox box-hidden">
                  <div class="icon-box">
                    <div class="box-images">
                      <img loading="lazy" src="/images/menus/seo-tools/Search_Engine_Spider_Simulator.svg" alt="">
                    </div>
                    <div class="box-text">
                      <p><a href="https://seotool.adminkit.net/spider-simulator">Search Engine Spider Simulator</a></p>
                    </div>
                  </div>
                </div>
                <div class="moreBox box-hidden">
                  <div class="icon-box">
                    <div class="box-images">
                      <img loading="lazy" src="/images/menus/seo-tools/Keyword_Position_Checker.svg" alt="">
                    </div>
                    <div class="box-text">
                      <p><a href="https://seotool.adminkit.net/keywords-suggestion-tool">Keywords Suggestion Tool</a></p>
                    </div>
                  </div>
                </div>
                <div class="moreBox box-hidden">
                  <div class="icon-box">
                    <div class="box-images">
                      <img loading="lazy" src="/images/menus/seo-tools/Page_Authority_Checker.svg" alt="">
                    </div>
                    <div class="box-text">
                      <p><a href="https://seotool.adminkit.net/page-authority-checker">Page Authority Checker</a></p>
                    </div>
                  </div>
                </div>
                <div class="box-more">
                  <a class="openMore" href="#seotool">Open More..</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-2dot4">
            <div class="menu-item">
              <div class="txt-flex-title">
                <h2 class="title-menu">Other Tools</h2>
                <i class="ic-mobile">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                <path d="M6 7.494L11.392 3l.608.729-6 5-6-5L.608 3z"></path>
              </svg>
              </i>
            </div>
            <div class="box-menu box-five">
              <div class="moreBox">
                <div class="icon-box">
                  <div class="box-images">
                    <img loading="lazy" src="/images/menus/other-tools/Plagiarism_Checker.svg" alt="">
                  </div>
                  <div class="box-text">
                    <p><a href="https://seotool.adminkit.net/plagiarism-checker">Plagiarism Checker</a></p>
                  </div>
                </div>
              </div>
              <div class="moreBox">
                <div class="icon-box">
                  <div class="box-images">
                    <img loading="lazy" src="/images/menus/other-tools/Word_Counter.svg" alt="">
                  </div>
                  <div class="box-text">
                    <p><a href="https://seotool.adminkit.net/word-counter">Word Counter</a></p>
                  </div>
                </div>
              </div>
              <div class="moreBox">
                <div class="icon-box">
                  <div class="box-images">
                    <img loading="lazy" src="/images/menus/other-tools/Link_Analyzer.svg" alt="">
                  </div>
                  <div class="box-text">
                    <p><a href="https://seotool.adminkit.net/link-analyzer-tool">Link Analyzer</a></p>
                  </div>
                </div>
              </div>
              <div class="moreBox">
                <div class="icon-box">
                  <div class="box-images">
                    <img loading="lazy" src="/images/menus/other-tools/Domain_Age_Checker.svg" alt="">
                  </div>
                  <div class="box-text">
                    <p><a href="https://seotool.adminkit.net/domain-age-checker">Domain Age Checker</a></p>
                  </div>
                </div>
              </div>
              <div class="moreBox">
                <div class="icon-box">
                  <div class="box-images">
                    <img loading="lazy" src="/images/menus/other-tools/Whois_Checker.svg" alt="">
                  </div>
                  <div class="box-text">
                    <p><a href="https://seotool.adminkit.net/whois-checker">Whois Checker</a></p>
                  </div>
                </div>
              </div>
              <div class="moreBox">
                <div class="icon-box">
                  <div class="box-images">
                    <img loading="lazy" src="/images/menus/other-tools/www_Redirect_Checker.svg" alt="">
                  </div>
                  <div class="box-text">
                    <p><a href="https://seotool.adminkit.net/www-redirect-checker">www Redirect Checker</a></p>
                  </div>
                </div>
              </div>
              <div class="moreBox">
                <div class="icon-box">
                  <div class="box-images">
                    <img loading="lazy" src="/images/menus/other-tools/Mozrank_Checker.svg" alt="">
                  </div>
                  <div class="box-text">
                    <p><a href="https://seotool.adminkit.net/mozrank-checker">Mozrank Checker</a></p>
                  </div>
                </div>
              </div>
              <div class="moreBox">
                <div class="icon-box">
                  <div class="box-images">
                    <img loading="lazy" src="/images/menus/other-tools/URL_Encoder_Decoder.svg" alt="">
                  </div>
                  <div class="box-text">
                    <p><a href="https://seotool.adminkit.net/url-encoder-decoder">URL Encoder / Decoder</a></p>
                  </div>
                </div>
              </div>
              <div class="moreBox">
                <div class="icon-box">
                  <div class="box-images">
                    <img loading="lazy" src="/images/menus/other-tools/Suspicious_Domain_Checker.svg" alt="">
                  </div>
                  <div class="box-text">
                    <p><a href="https://seotool.adminkit.net/suspicious-domain-checker">Suspicious Domain Checker</a></p>
                  </div>
                </div>
              </div>
              <div class="moreBox">
                <div class="icon-box">
                  <div class="box-images">
                    <img loading="lazy" src="/images/menus/other-tools/Link_Price_Calculator.svg" alt="">
                  </div>
                  <div class="box-text">
                    <p><a href="https://seotool.adminkit.net/link-price-calculator">Link Price Calculator</a></p>
                  </div>
                </div>
              </div>
              <div class="moreBox box-hidden">
                <div class="icon-box">
                  <div class="box-images">
                    <img loading="lazy" src="/images/menus/other-tools/Online_Md5_Generator.svg" alt="">
                  </div>
                  <div class="box-text">
                    <p><a href="https://seotool.adminkit.net/online-md5-generator">Online Md5 Generator</a></p>
                  </div>
                </div>
              </div>
              <div class="moreBox box-hidden">
                <div class="icon-box">
                  <div class="box-images">
                    <img loading="lazy" src="/images/menus/other-tools/What_is_my_Browser.svg" alt="">
                  </div>
                  <div class="box-text">
                    <p><a href="https://seotool.adminkit.net/what-is-my-browser">What is my Browser</a></p>
                  </div>
                </div>
              </div>
              <div class="moreBox box-hidden">
                <div class="icon-box">
                  <div class="box-images">
                    <img loading="lazy" src="/images/menus/other-tools/Email_Privacy.svg" alt="">
                  </div>
                  <div class="box-text">
                    <p><a href="https://seotool.adminkit.net/email-privacy">Email Privacy</a></p>
                  </div>
                </div>
              </div>
              <div class="moreBox box-hidden">
                <div class="icon-box">
                  <div class="box-images">
                    <img loading="lazy" src="/images/menus/other-tools/Google_Cache_Checker.svg" alt="">
                  </div>
                  <div class="box-text">
                    <p><a href="https://seotool.adminkit.net/google-cache-checker">Google Cache Checker</a></p>
                  </div>
                </div>
              </div>
              <div class="moreBox box-hidden">
                <div class="icon-box">
                  <div class="box-images">
                    <img loading="lazy" src="/images/menus/other-tools/Bulk_Domain_Availability.svg" alt="">
                  </div>
                  <div class="box-text">
                    <p><a href="https://seotool.adminkit.net/domain-availability-checker">Bulk Domain Availability Checker</a></p>
                  </div>
                </div>
              </div>
              <div class="moreBox box-hidden">
                <div class="icon-box">
                  <div class="box-images">
                    <img loading="lazy" src="/images/menus/other-tools/Grammar_Checker.svg" alt="">
                  </div>
                  <div class="box-text">
                    <p><a href="https://seotool.adminkit.net/grammar-checker">Grammar Checker</a></p>
                    <p>Grammar Checker</p>
                  </div>
                </div>
              </div>
              <div class="moreBox box-hidden">
                <div class="icon-box">
                  <div class="box-images">
                    <img loading="lazy" src="/images/menus/other-tools/Flag_Counter.svg" alt="">
                  </div>
                  <div class="box-text">
                    <p><a href="https://seotool.adminkit.net/flag-counter">Flag Counter</a></p>
                  </div>
                </div>
              </div>
              <div class="moreBox box-hidden">
                <div class="icon-box">
                  <div class="box-images">
                    <img loading="lazy" src="/images/menus/other-tools/Google_Malware_Checker.svg" alt="">
                  </div>
                  <div class="box-text">
                    <p><a href="https://seotool.adminkit.net/google-malware-checker">Google Malware Checker</a></p>
                    <p>Google Malware Checker</p>
                  </div>
                </div>
              </div>
              <div class="box-more">
                <a class="openMore" href="#OtherTool">Open More..</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
<div class="search-wrapper span4l tm-right">
<div class="search-container">
<span id="btn-search-mobile">
<i class="iconm-search">
<svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M9.93945 0.931274C4.98074 0.931274 0.941406 4.9706 0.941406 9.92934C0.941406 14.888 4.98074 18.9352 9.93945 18.9352C12.0575 18.9352 14.0054 18.193 15.5449 16.9606L19.293 20.7067C19.4821 20.888 19.7347 20.988 19.9967 20.9853C20.2587 20.9827 20.5093 20.8775 20.6947 20.6924C20.8801 20.5072 20.9856 20.2569 20.9886 19.9949C20.9917 19.7329 20.892 19.4801 20.7109 19.2907L16.9629 15.5427C18.1963 14.0008 18.9395 12.0498 18.9395 9.92934C18.9395 4.9706 14.8982 0.931274 9.93945 0.931274ZM9.93945 2.9313C13.8173 2.9313 16.9375 6.0515 16.9375 9.92934C16.9375 13.8072 13.8173 16.9352 9.93945 16.9352C6.06162 16.9352 2.94141 13.8072 2.94141 9.92934C2.94141 6.0515 6.06162 2.9313 9.93945 2.9313Z" fill="#92A7B4"></path>
</svg>
</i>
</span>
<form autocomplete="off" action="#" id="searchForm" class="input">
<div class="autocomplete">
<div class="close-button">
<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M2 12L6 16L6 13L22 13L22 11L6 11L6 8L2 12Z" fill="#2E3A59"></path>
</svg>
</div>
<input class="search-main" type="text" name="my-search" id="myInput" placeholder="Enter Search Words">
</div>
<button class="btn-search-desktop" type="submit">
<i class="fa fa-search" aria-hidden="true"></i>
</button>
</form>
</div>
</div>
<div class="header-right span4l">
<ul class="nav-right">
<li class="link-to">
<a href="https://seotool.adminkit.net/analyze"><span>Analyze Website</span></a>
</li>
<li class="nav-button-account">
<a class="today-login">
  <img loading="lazy" src="/images/default_avarta.jpg" alt="">
</a>
</li>
</ul>
</div>
</div>
</nav>
<!-- mobile-nav -->
<div class="main-content">
<!-- desktop-nav -->
<div class="wrapper-header navbar-fixed-top">
<div class="container main-header" id="header">
</div>
</div>
<!-- desktop-nav b-ala-j-i -->
<?php if($controller == CON_MAIN){ ?>
<section class="headturbo" id="headturbo">
<div class="headturbo-wrap" id="headturbo-wrap">
<div class="texture-overlay"></div>
<div class="container">
<div class="row">
<div class="headturbo-img pull-right hidden-xs">
</div>
<div class="col-md-12 text-center">
<div class="headturbo-content">
<h1 class="pulse"><?php trans('Instantly Analyze Your SEO Issues',$lang['145']); ?></h1>
<h2><?php trans('Helps to identify your SEO mistakes and better optimize your site content.',$lang['146']); ?></h2>
<form class="turboform" method="POST" action="<?php createLink('domain'); ?>" onsubmit="return fixURL();">
<div class="input-group review">
  <input type="text" autocomplete="off" spellcheck="false" class="form-control" placeholder="Enter URL to Analyze" name="url" />
  <span class="input-group-btn"> <button class="btn btn-green" type="submit" id="review-btn">Analyze</button></span>
</div>
<p class="description-reviewer">
  Set side-by-side domain's comparisons: <a href="#">Site to site</a>
</p>
</form>
</div>
</div>
</div>
</div>
</div>
</section>
<?php } else { ?>
<div class="bg-primary-color page-block header-anylaze-nb">
<div class="container">
<div class="new-search-anylaze">
<h4 class="search-more-anylaze">Analyze your other web pages</h4>
<form method="POST" action="<?php createLink('domain'); ?>" onsubmit="return fixURL();">
<div class="input-group reviewBox">
<div class="input-container">
<input type="text" tabindex="1" placeholder="<?php trans('Website URL to review',$lang['37']); ?>" id="get-url" name="url" class="form-control reviewIn"/>
</div>
<div class="input-group-btn">
<button tabindex="2" type="submit" name="generate" class="btn btn-info url-lg"><?php trans('Analyze',$lang['36']); ?></button>
</div>
</div>
</form>
</div>
<!-- <h1 class="pageTitle text-center"><?php echo $pageTitle; ?></h1> -->
</div>
</div>
<?php } ?>
</div>