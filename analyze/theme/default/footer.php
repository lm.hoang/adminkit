<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
/*
* @author Balaji
* @Theme: Default Style
* @copyright 2019 ProThemes.Biz
*
*/
?>
<?php
require_once(THEME_DIR."includes/contactus.php");
?>
<section class="section-newsletter ">
	<div class="parallax-newsletter"></div>
	<div class="bg-newsletter"></div>
	<div class="wrap-letter">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12">
					<div class="box-text">
						<h2 class="title-letter">Join Our newsletter</h2>
						<p class="info-letter">Subscribe to our newsletter Today to receive updates and special offers</p>
						<form action="https://adminkit.us7.list-manage.com/subscribe/post?u=d3833a8a0a1d7a3687036b3fb&amp;id=19af294272" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate form-letter" target="_blank" novalidate="novalidate">
							<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" placeholder="Enter your email" aria-required="true">
							<button type="submit" name="subscribe" class="btn-letter">Subscribe</button>
						</form>
						<div id="mce-responses" class="clear">
							<div class="response" id="mce-error-response" style="display:none"></div>
							<div class="response" id="mce-success-response" style="display:none; color:white;"></div>
						</div>
						<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
						<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_d3833a8a0a1d7a3687036b3fb_19af294272" tabindex="-1" value=""></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="parallax-newsletter"></div>
</section>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[3]='ADDRESS';ftypes[3]='address';fnames[4]='PHONE';ftypes[4]='phone';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->
<!--section-footer./start-->
<section class="section-links mega-footer">
	<div class="fill bg-fill"></div>
	<div class="container">
		<div class="wrap-links accordion" id="accordionExample">
			<div class="container">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
						<div class="box-text">
							<div class="links-title">
								<i class="add-more"></i>
								<h2>Quick Links <span>(Explore popular categories)</span></h2>
							</div>
						</div>
					</div>
					<div class="show-links">
						<div class="row">
							<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2dot4">
								<div class="box-text">
									<h4>Server Performance Tools</h4>
									<p><a href="https://seotool.adminkit.net/my-ip-address">My IP Address</a></p>
									<p><a href="https://seotool.adminkit.net/server-status-checker">Server Status Checker</a></p>
									<p><a href="https://seotool.adminkit.net/reverse-ip-domain-checker">Reverse IP Domain Checker</a></p>
									<p><a href="https://seotool.adminkit.net/blacklist-lookup">Blacklist Lookup</a></p>
									<p><a href="https://seotool.adminkit.net/domain-hosting-checker">Domain Hosting Checker</a></p>
									<p><a href="https://seotool.adminkit.net/find-dns-records">Find DNS records</a></p>
									<p><a href="https://seotool.adminkit.net/domain-authority-checker">Domain Authority Checker</a></p>
									<p><a href="https://seotool.adminkit.net/domain-into-ip">Domain into IP</a></p>
								</div>
							</div>
							<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2dot4">
								<div class="box-text">
									<h4>SEO Performance Tools</h4>
									<p><a href="https://seotool.adminkit.net/article-rewriter">Article Rewriter</a></p>
									<p><a href="https://seotool.adminkit.net/backlink-maker">Backlink Maker</a></p>
									<p><a href="https://seotool.adminkit.net/meta-tag-generator">Meta Tag Generator</a></p>
									<p><a href="https://seotool.adminkit.net/meta-tags-analyzer">Meta Tags Analyzer</a></p>
									<p><a href="https://seotool.adminkit.net/keyword-position-checker">Keyword Position Checker</a></p>
									<p><a href="https://seotool.adminkit.net/robots-txt-generator">Robots.txt Generator</a></p>
									<p><a href="https://seotool.adminkit.net/xml-sitemap-generator">XML Sitemap Generator</a></p>
									<p><a href="https://seotool.adminkit.net/backlink-checker">Backlink Checker</a></p>
									<p><a href="https://seotool.adminkit.net/alexa-rank-checker">Alexa Rank Checker</a></p>
									<p><a href="https://seotool.adminkit.net/url-rewriting-tool">URL Rewriting Tool</a></p>
									<p><a href="https://seotool.adminkit.net/google-index-checker">Google Index Checker</a></p>
									<p><a href="https://seotool.adminkit.net/website-links-count-checker">Website Links Count Checker</a></p>
									<p><a href="https://seotool.adminkit.net/spider-simulator">Search Engine Spider Simulator</a></p>
									<p><a href="https://seotool.adminkit.net/keywords-suggestion-tool">Keywords Suggestion Tool</a></p>
									<p><a href="https://seotool.adminkit.net/page-authority-checker">Page Authority Checker</a></p>
								</div>
							</div>
							<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2dot4">
								<div class="box-text">
									<h4>Web Peformance Tools</h4>
									<p><a href="https://seotool.adminkit.net/page-size-checker">Page Size Checker</a></p>
									<p><a href="https://seotool.adminkit.net/website-screenshot-generator">Website Screenshot Generator</a></p>
									<p><a href="https://seotool.adminkit.net/get-source-code-of-webpage">Get Source Code of Webpage</a></p>
									<p><a href="https://seotool.adminkit.net/page-speed-checker">Page Speed Checker</a></p>
									<p><a href="https://seotool.adminkit.net/code-to-text-ratio-checker">Code to Text Ratio Checker</a></p>
									<p><a href="https://seotool.adminkit.net/broken-links-finder">Broken Links Finder</a></p>
									<p><a href="https://seotool.adminkit.net/pagespeed-insights-checker">Pagespeed Insights Checker</a></p>
									<p><a href="https://seotool.adminkit.net/get-http-headers">Get HTTP Headers</a></p>
									<p><a href="https://seotool.adminkit.net/mobile-friendly-test">Mobile Friendly Test</a></p>
									<p><a href="https://seotool.adminkit.net/website-reviewer">Website Reviewer</a></p>
									<p><a href="https://seotool.adminkit.net/keyword-density-checker">Keyword Density Checker</a></p>
									<p><a href="https://seotool.adminkit.net/check-gzip-compression">Check GZIP compression</a></p>
									<p><a href="https://seotool.adminkit.net/class-c-ip-checker">Class C Ip Checker</a></p>
								</div>
							</div>
							<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2dot4">
								<div class="box-text">
									<h4>Admin Tool Kits</h4>
									<p><a href="https://adminkit.net/dnsbl.aspx">BlackList Check</a></p>
									<p><a href="https://adminkit.net/mxlookup.aspx">MX Lookup</a></p>
									<p><a href="https://adminkit.net/dnsrecords.aspx">DNS Lookup</a></p>
									<p><a href="https://adminkit.net/telnet.aspx">Telnet</a></p>
									<p><a href="https://adminkit.net/ip2location.aspx">IP 2 Location</a></p>
									<p><a href="https://adminkit.net/ping.aspx">Ping</a></p>
									<p><a href="https://adminkit.net/dnsbl_monitor.aspx">BlackList Monitor</a></p>
									<p><a href="https://adminkit.net/smtp.aspx">SMTP Test Tool</a></p>
									<p><a href="https://adminkit.net/my_ip_address.aspx">My IP Adress</a></p>
									<p><a href="https://adminkit.net/traceroute.aspx">Trace route</a></p>
									<p><a href="https://adminkit.net/whois.aspx">Whois</a></p>
									<p><a href="https://adminkit.net/applications.aspx">Desktop ToolsOther Tools</a></p>
								</div>
							</div>
							<div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-2dot4">
								<div class="box-text">
									<h4>Other Tools</h4>
									<p><a href="https://seotool.adminkit.net/plagiarism-checker">Plagiarism Checker</a></p>
									<p><a href="https://seotool.adminkit.net/word-counter">Word Counter</a></p>
									<p><a href="https://seotool.adminkit.net/link-analyzer-tool">Link Analyzer</a></p>
									<p><a href="https://seotool.adminkit.net/domain-age-checker">Domain Age Checker</a></p>
									<p><a href="https://seotool.adminkit.net/whois-checker">Whois Checker</a></p>
									<p><a href="https://seotool.adminkit.net/www-redirect-checker">www Redirect Checker</a></p>
									<p><a href="https://seotool.adminkit.net/mozrank-checker">Mozrank Checker</a></p>
									<p><a href="https://seotool.adminkit.net/url-encoder-decoder">URL Encoder / Decoder</a></p>
									<p><a href="https://seotool.adminkit.net/suspicious-domain-checker">Suspicious Domain Checker</a></p>
									<p><a href="https://seotool.adminkit.net/link-price-calculator">Link Price Calculator</a></p>
									<p><a href="https://seotool.adminkit.net/online-md5-generator">Online Md5 Generator</a></p>
									<p><a href="https://seotool.adminkit.net/what-is-my-browser">What is my Browser</a></p>
									<p><a href="https://seotool.adminkit.net/email-privacy">Email Privacy</a></p>
									<p><a href="https://seotool.adminkit.net/google-cache-checker">Google Cache Checker</a></p>
									<p><a href="https://seotool.adminkit.net/social-stats-checker">Social Stats Checker</a></p>
									<p><a href="https://seotool.adminkit.net/domain-availability-checker">Bulk Domain Availability Checker</a></p>
									<p><a href="https://seotool.adminkit.net/grammar-checker">Grammar Checker</a></p>
									<p><a href="https://seotool.adminkit.net/flag-counter">Flag Counter</a></p>
									<p><a href="https://seotool.adminkit.net/google-malware-checker">Google Malware Checker</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>
<style>
#myBtn {
font-size: 14px;
border: none;
outline: none;
z-index: 9;
position: fixed;
right: 15px;
bottom: 100px;
background: #f7931d;
color: #fff;
width: 40px;
height: 40px;
padding-top: 9px;
text-align: center;
border-radius: 4px;
cursor: pointer;
line-height: 40px;
}
#myBtn:before {
content: "\f077";
font-family: 'FontAwesome';
position: absolute;
top: 8px;
left: 50%;
transform: translate(-50%, -50%);
}
</style>
<!-- Font-Awesome -->
<link href="<?php themeLink('css/font-awesome.min.css'); ?>" rel="stylesheet" />
<script src="/analyze/js/general_js.js"></script>
<script>
//Get the button
var mybutton = document.getElementById("myBtn");
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};
function scrollFunction() {
if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
mybutton.style.display = "block";
} else {
mybutton.style.display = "none";
}
}
// When the user clicks on the button, scroll to the top of the document
function topFunction() {
document.body.scrollTop = 0;
document.documentElement.scrollTop = 0;
}
</script>
<footer class="mega-footer aaaaa">
	<div class="fill bg-fill"></div>
	<div class="container">
		<div class="footer-top">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
					<div class="icon-box">
						<div class="box-images">
							<a href="#">
								<img style="width: 154px; height: 32px;" src="/images/footer-logo.png" alt="logo-footer">
							</a>
						</div>
						<div class="box-text">
							<p class="info-netbase">
								AdminKit help IT Admin to improve their website performance and IT system with our premium tools and remote support services. We provide admin from basic statistic to bigdata API about their project to come out the best consultancy report and work privately to improve the result.
							</p>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6 middle-pd">
					<div class="row">
						<div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
							<div class="box-text">
								<h4>Netbase JSC</h4>
								<p><a href="#">Terms</a></p>
								<p><a href="#">Licenses</a></p>
								<p><a href="#">Market API</a></p>
								<p><a href="#">Become an affiliate</a></p>
							</div>
						</div>
						<div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
							<div class="box-text">
								<h4>Netbase JSC</h4>
								<p><a href="#">Terms</a></p>
								<p><a href="#">Licenses</a></p>
								<p><a href="#">Market API</a></p>
								<p><a href="#">Become an affiliate</a></p>
							</div>
						</div>
						<div class="col-12 col-sm-4 col-md-4 col-lg-4 col-xl-4">
							<div class="box-text">
								<h4>Adminkit</h4>
								<p><a href="#">Terms</a></p>
								<p><a href="#">Licenses</a></p>
								<p><a href="#">Market API</a></p>
								<p><a href="#">Become an affiliate</a></p>
								<p><a href="https://adminkit.net/privacypolicy.aspx">Privacy</a></p>
								<p><a href="https://seotool.adminkit.net/contact">Contact us</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-bottom">
			<div class="row">
				<div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
					<div class="Library-images">
						<span class="icon-lib"></span>
						<span class="icon-lib"></span>
						<span class="icon-lib"></span>
						<span class="icon-lib"></span>
						<span class="icon-lib"></span>
					</div>
				</div>
				<div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">
					<div class="social-network">
						<span class="icon-social icon-skype"></span>
						<span class="icon-social icon-twitter"></span>
						<span class="icon-social icon-facebook"></span>
						<span class="icon-social icon-google"></span>
						<span class="icon-social icon-instagram"></span>
						<span class="icon-social icon-youtube"></span>
					</div>
				</div>
			</div>
			<div class="wrap-footer-bottom">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
						<div class="netbase-about">
							Netbase JSC, Copyright © 2010 - 2020 by <a href="#">Netbase</a>. All Rights Reserved
						</div>
					</div>
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
						<ul class="links-about">
							<li><a href="#">About Netbase JSC</a></li>
							<li><a href="#">Careers</a></li>
							<li><a href="#">Privacy</a></li>
							<li><a href="#">PolicySitemap</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<!--sidenav-fixed./start-->
<section class="sidenav-fixed sidenav_change">
	<div class="logo">
		<a href="#">
			<img class="img-logo-adminkit" src="/images/logo.png" alt="">
		</a>
	</div>
	<div id="sidenav_mobile">
		<i class="fa fa-caret-down" aria-hidden="true"></i>
	</div>
	<ul id="shortcuts-var3" class="change_show">
		<li title="" title-right="true">
			<a href="https://seotool.adminkit.net/" title="SEO Tools">
				<div class="sprite icon-nav"></div>
			</a>
		</li>
		<li title="" title-right="true">
			<a href="https://pingdom.adminkit.net/" title="Pingdom">
				<div class="sprite icon-nav"></div>
			</a>
		</li>
		<li title="" title-right="true">
			<a href="#" title="Website Reviewer">
				<div class="sprite icon-nav"></div>
			</a>
		</li>
		<li title="" title-right="true">
			<a href="#" title="Contract Sign">
				<div class="sprite icon-nav icon-nav"></div>
			</a>
		</li>
		<li title="" title-right="true">
			<a href="#" title="PDF Tools">
				<div class="sprite icon-nav icon-nav"></div>
			</a>
		</li>
		<li title="" title-right="true">
			<a href="#" title="File Sharings">
				<div class="sprite icon-nav icon-nav"></div>
			</a>
		</li>
		<li title="" title-right="true">
			<a href="#" title="Help Desk">
				<div class="sprite icon-nav icon-nav"></div>
			</a>
		</li>
		<li title="" title-right="true">
			<a href="https://netbasejsc.com/" title="Micro Projects">
				<div class="sprite icon-nav icon-nav"></div>
			</a>
		</li>
		<li title="" title-right="true">
			<a href="https://netbasejsc.com/" title="Domain &amp; Hosting">
				<div class="sprite icon-nav icon-nav"></div>
			</a>
		</li>
		<li title="" title-right="true">
			<a href="https://netbasejsc.com/" title="Admin Tools">
				<div class="sprite icon-nav icon-nav"></div>
			</a>
		</li>
	</ul>
</section>
<!-- section-login./start -->
<section class="section-login">
	<div class="bg-default"></div>
	<div class="wrap-login">
		<button class="btn-close-login">
		<div class="box-close-login">
			<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" fill="currentColor">
			<path d="M12 12.696L4.696 20L4 19.304L11.304 12L4 4.696L4.696 4L12 11.304L19.304 4L20 4.696L12.696 12L20 19.304L19.304 20L12 12.696Z"></path>
		</svg>
	</div>
	</button>
	<div class="main-login">
		<div class="box-main-login">
			<div class="title-login">
				<h2>Log in to Adminkit</h2>
			</div>
			<div class="title-Signup">
				<h2>Create an Account</h2>
			</div>
			<div class="title-forgot">
				<h2>Forgot your password?</h2>
			</div>
			<p class="intro-forgot">
				Enter your e-mail address below to begin the process of resetting your password.
			</p>
			<div class="intro-signup">
				Sign up today and get a suite of features and options to make optimizing your website speed clear and easy!
			</div>
			<div class="login-socical">
				<button class="icon-box">
				<div class="box-images">
					<div class="ic-login img-google">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
							<g fill="none" transform="translate(2 2)">
								<path
									fill="#4285F4"
									d="M19.5326718,10.1870992 C19.5326718,9.36770992 19.4661832,8.76977099 19.3222901,8.14969466 L9.96564885,8.14969466 L9.96564885,11.8480153 L15.4577863,11.8480153 C15.3470992,12.7670992 14.7491603,14.1512214 13.4203817,15.0812977 L13.4017557,15.2051145 L16.3601527,17.4969466 L16.5651145,17.5174046 C18.4474809,15.7789313 19.5326718,13.2210687 19.5326718,10.1870992"
								></path>
								<path
									fill="#34A853"
									d="M9.96564885,19.9312977 C12.6563359,19.9312977 14.9151908,19.0454198 16.5651145,17.5174046 L13.4203817,15.0812977 C12.578855,15.6681679 11.4493893,16.0778626 9.96564885,16.0778626 C7.33030534,16.0778626 5.09358779,14.3394656 4.29625954,11.9366412 L4.17938931,11.9465649 L1.10320611,14.3272519 L1.0629771,14.439084 C2.70175573,17.6945038 6.06793893,19.9312977 9.96564885,19.9312977"
								></path>
								<path
									fill="#FBBC05"
									d="M4.29625954,11.9366412 C4.08587786,11.3165649 3.96412214,10.6521374 3.96412214,9.96564885 C3.96412214,9.27908397 4.08587786,8.61473282 4.28519084,7.99465649 L4.27961832,7.86259542 L1.1648855,5.44366412 L1.0629771,5.4921374 C0.387557252,6.84305344 0,8.36007634 0,9.96564885 C0,11.5712214 0.387557252,13.0881679 1.0629771,14.439084 L4.29625954,11.9366412"
								></path>
								<path
									fill="#EB4335"
									d="M9.96564885,3.85335878 C11.8369466,3.85335878 13.0992366,4.66167939 13.8190076,5.33717557 L16.6315267,2.5910687 C14.9041985,0.985496183 12.6563359,0 9.96564885,0 C6.06793893,0 2.70175573,2.23671756 1.0629771,5.4921374 L4.28519084,7.99465649 C5.09358779,5.59183206 7.33030534,3.85335878 9.96564885,3.85335878"
								></path>
							</g>
						</svg>
					</div>
				</div>
				<div class="box-text">
					<div class="bg-login-btn bg-google"></div>
					<p class="txt-login">Log in with Google</p>
				</div>
				</button>
				<button class="icon-box">
				<div class="box-images">
					<div class="ic-login img-facebook">
						<div class="bg-ic-fb"></div>
						<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 24 24">
							<path
								d="M20.8965517,2 L3.10344828,2 C2.49403062,2 2,2.49403062 2,3.10344828 L2,20.8965517 C2,21.5059694 2.49403062,22 3.10344828,22 L12.6896552,22 L12.6896552,14.2655172 L10.0862069,14.2655172 L10.0862069,11.237931 L12.6896552,11.237931 L12.6896552,9.01034483 C12.6896552,6.42758621 14.2689655,5.02068966 16.5724138,5.02068966 C17.3486461,5.01899684 18.1244303,5.05813135 18.8965517,5.13793103 L18.8965517,7.83793103 L17.3103448,7.83793103 C16.0551724,7.83793103 15.8103448,8.43103448 15.8103448,9.30689655 L15.8103448,11.2344828 L18.8103448,11.2344828 L18.4206897,14.262069 L15.7931034,14.262069 L15.7931034,22 L20.8965517,22 C21.5059694,22 22,21.5059694 22,20.8965517 L22,3.10344828 C22,2.49403062 21.5059694,2 20.8965517,2 Z"
							></path>
						</svg>
					</div>
				</div>
				<div class="box-text">
					<div class="bg-login-btn bg-facebook"></div>
					<p class="txt-login">Log in with Facebook</p>
				</div>
				</button>
			</div>
			<div class="des-login">or</div>
			<form name="login">
				<div class="name-login">
					<div class="icon-box box-ip-login">
						<div class="box-text">
							<input required placeholder="Username" type="text" class="input-login" value="" />
						</div>
					</div>
				</div>
				<div class="icon-box box-ip-login">
					<div class="box-text">
						<input placeholder="Email" type="email" class="input-login" value="" required />
					</div>
				</div>
				<div class="pass-login">
					<div class="icon-box box-ip-login">
						<div class="box-text">
							<input required placeholder="Password" type="password" class="input-login" id="password" value="" />
							<div class="show-pass" id="showPassword">
								<div class="img-show-pass">
									<svg class="show-eye" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
										<path
											class="hide-eye"
											d="M12 19c-5.97 0-10-6.134-10-7 0-.866 4.03-7 10-7s10 6.134 10 7c0 .866-4.03 7-10 7zm0-1c2.365 0 4.614-1.087 6.556-2.905a13.285 13.285 0 0 0 1.893-2.196c.201-.298.362-.57.467-.787.058-.12.084-.197.084-.112s-.026.008-.084-.112a6.738 6.738 0 0 0-.467-.787 13.285 13.285 0 0 0-1.893-2.196C16.614 7.087 14.365 6 12 6S7.386 7.087 5.444 8.905a13.285 13.285 0 0 0-1.893 2.196c-.201.298-.362.57-.467.787-.058.12-.084.197-.084.112s.026-.008.084.112c.105.216.266.489.467.787a13.285 13.285 0 0 0 1.893 2.196C7.386 16.913 9.635 18 12 18zm0-2a4 4 0 1 1 0-8 4 4 0 0 1 0 8z"
										></path>
									</svg>
									<svg class="hide-eye" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
										<path
											d="M8.382 18.239l.768-.768A8.115 8.115 0 0 0 12 18c2.365 0 4.614-1.087 6.556-2.905a13.285 13.285 0 0 0 1.893-2.196c.201-.298.362-.57.467-.787.058-.12.084-.197.084-.112s-.026.008-.084-.112a6.738 6.738 0 0 0-.467-.787 13.285 13.285 0 0 0-2.336-2.593l.709-.708C20.832 9.54 22 11.534 22 12c0 .866-4.03 7-10 7a9.193 9.193 0 0 1-3.618-.761zm-2.655-1.587C3.382 14.822 2 12.507 2 12c0-.866 4.03-7 10-7 1.582 0 3.028.43 4.299 1.08l-.745.745C14.418 6.292 13.224 6 12 6 9.635 6 7.386 7.087 5.444 8.905a13.285 13.285 0 0 0-1.893 2.196c-.201.298-.362.57-.467.787-.058.12-.084.197-.084.112s.026-.008.084.112c.105.216.266.489.467.787a13.285 13.285 0 0 0 1.893 2.196c.324.302.655.585.995.845l-.712.712zm5.076-.834l5.015-5.015a4 4 0 0 1-5.015 5.015zM8.48 13.9a4 4 0 0 1 5.42-5.42L8.48 13.9zm10.739-9.325l.707.707L5.782 19.425l-.707-.707L19.218 4.575z"
										></path>
									</svg>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class=" confirm_pass">
					<div class="icon-box box-ip-login">
						<div class="box-text">
							<input required placeholder="Confirm Password" type="password" class="input-login" value="" />
							<div class="show-pass">
								<div class="img-show-pass">
									<svg class="show-eye" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
										<path
											class="hide-eye"
											d="M12 19c-5.97 0-10-6.134-10-7 0-.866 4.03-7 10-7s10 6.134 10 7c0 .866-4.03 7-10 7zm0-1c2.365 0 4.614-1.087 6.556-2.905a13.285 13.285 0 0 0 1.893-2.196c.201-.298.362-.57.467-.787.058-.12.084-.197.084-.112s-.026.008-.084-.112a6.738 6.738 0 0 0-.467-.787 13.285 13.285 0 0 0-1.893-2.196C16.614 7.087 14.365 6 12 6S7.386 7.087 5.444 8.905a13.285 13.285 0 0 0-1.893 2.196c-.201.298-.362.57-.467.787-.058.12-.084.197-.084.112s.026-.008.084.112c.105.216.266.489.467.787a13.285 13.285 0 0 0 1.893 2.196C7.386 16.913 9.635 18 12 18zm0-2a4 4 0 1 1 0-8 4 4 0 0 1 0 8z"
										></path>
									</svg>
									<svg class="hide-eye" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
										<path
											d="M8.382 18.239l.768-.768A8.115 8.115 0 0 0 12 18c2.365 0 4.614-1.087 6.556-2.905a13.285 13.285 0 0 0 1.893-2.196c.201-.298.362-.57.467-.787.058-.12.084-.197.084-.112s-.026.008-.084-.112a6.738 6.738 0 0 0-.467-.787 13.285 13.285 0 0 0-2.336-2.593l.709-.708C20.832 9.54 22 11.534 22 12c0 .866-4.03 7-10 7a9.193 9.193 0 0 1-3.618-.761zm-2.655-1.587C3.382 14.822 2 12.507 2 12c0-.866 4.03-7 10-7 1.582 0 3.028.43 4.299 1.08l-.745.745C14.418 6.292 13.224 6 12 6 9.635 6 7.386 7.087 5.444 8.905a13.285 13.285 0 0 0-1.893 2.196c-.201.298-.362.57-.467.787-.058.12-.084.197-.084.112s.026-.008.084.112c.105.216.266.489.467.787a13.285 13.285 0 0 0 1.893 2.196c.324.302.655.585.995.845l-.712.712zm5.076-.834l5.015-5.015a4 4 0 0 1-5.015 5.015zM8.48 13.9a4 4 0 0 1 5.42-5.42L8.48 13.9zm10.739-9.325l.707.707L5.782 19.425l-.707-.707L19.218 4.575z"
										></path>
									</svg>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="checkbox-login">
					<div class="icon-box">
						<label><input required class="cb-login" type="checkbox" />I agree to the <a href="#">Terms of Service </a>and<a href="#"> Privacy Policy</a></label>
					</div>
				</div>
				<div class="checkbox-signup">
					<div class="icon-box">
						<label><input required class="cb-login" type="checkbox" />Keep me logged in</label>
					</div>
				</div>
				<div class="box-login-popup">
					<input class="btn-login-popup" type="submit" value="Log in"/>
					
					<button class="btn-signup" type="submit">
					Create an Account
					</button>
					<button class="btn-forgot" type="submit">
					Send Password
					</button>
				</div>
			</form>
			<a class="blue-txt-login forgot-pass">Forgot your password?</a>
		</div>
		<div class="privacy-Terms">By creating an account you agree to our <a href="https://seotool.adminkit.net/theme/default/maintenance.php">Privacy & Terms</a></div>
		<div class="create-signup">Don't have an account? <span class="info-txt-login Create-signup-cb">Sign up</span></div>
		<div class="create-login">Already have an account? <span class="info-txt-login Create-login-cb">Log in</span></div>
		<div class="cmback-login">
			Got an account? <a class="Create-login-cb">Log in now!</a>
		</div>
		<div class ="cmbank-signup"> Sign up for Adminkit FREE? <a class ="Create-signup-cb">Create an Account</a></div>
	</div>
</div>
</section>
<!-- Bootstrap -->
<script src="<?php themeLink('js/bootstrap.min.js'); ?>" ></script>
<?php if($controller == CON_MAIN) { ?> <script type='text/javascript' src='<?php themeLink('js/particleground.min.js'); ?>'></script> <?php } ?>
<script type='text/javascript' src='<?php themeLink('js/sweetalert.min.js'); ?>'></script>
<!-- App JS -->
<script src="<?php themeLink('js/app.js'); ?>" ></script>
<!-- Master JS -->
<script src="<?php echo $baseURL.'rainbow/master-js'; ?>" ></script>
<?php if(isset($addtionalCodes)) echo $addtionalCodes;
if(isset($_SESSION['TWEB_CALLBACK_ERR'])){
$err = $_SESSION['TWEB_CALLBACK_ERR'];
unset($_SESSION['TWEB_CALLBACK_ERR']);
echo '<script>sweetAlert(oopsStr, "'. makeJavascriptStr($err) .'" , "error");</script>';
}
?>
<?php if($ga != ''){ ?>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', '<?php echo $ga; ?>', 'auto');
ga('send', 'pageview');
</script>
<?php } ?>
<!-- Sign in -->
<div class="modal loginme" id="signin" role="dialog" aria-hidden="true">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title"><?php trans('Sign In',$lang['93']); ?></h4>
		</div>
		<form method="POST" action="<?php createLink('account/login'); ?>" class="loginme-form">
			<div class="modal-body">
				<div class="alert alert-warning">
					<button type="button" class="close dismiss">&times;</button><span></span>
				</div>
				<?php if($enable_oauth){ ?>
				<div class="form-group connect-with">
					<div class="info"><?php trans('Sign in using social network',$lang['94']); ?></div>
					<a href="<?php createLink('facebook/login'); ?>" class="connect facebook" title="<?php trans('Sign in using Facebook',$lang['95']); ?>"><?php trans('Facebook',$lang['98']); ?></a>
					<a href="<?php createLink('google/login'); ?>" class="connect google" title="<?php trans('Sign in using Google',$lang['96']); ?>"><?php trans('Google',$lang['99']); ?></a>
					<a href="<?php createLink('twitter/login'); ?>" class="connect twitter" title="<?php trans('Sign in using Twitter',$lang['97']); ?>"><?php trans('Twitter',$lang['100']); ?></a>
				</div>
				<?php } ?>
				<div class="info"><?php trans('Sign in with your username',$lang['101']); ?></div>
				<div class="form-group">
					<label>
						<input placeholder="<?php trans('Username',$lang['102']); ?>" type="text" name="username" class="form-input width96" />
					</label>
				</div>
				<div class="form-group">
					<label> <br />
						<input placeholder="<?php trans('Password',$lang['103']); ?>" type="password" name="password" class="form-input width96" />
					</label>
				</div>
			</div>
			<div class="modal-footer"> <br />
				<button type="submit" class="btn btn-primary pull-left"><?php trans('Sign In',$lang['93']); ?></button>
				<div class="pull-right align-right">
					<a href="<?php createLink('account/forget'); ?>"><?php trans('Forgot Password',$lang['104']); ?></a><br />
					<a href="<?php createLink('account/resend'); ?>"><?php trans('Resend Activation Email',$lang['105']); ?></a>
				</div>
			</div>
			<input type="hidden" name="signin" value="<?php echo md5($date.$ip); ?>" />
			<input type="hidden" name="quick" value="<?php echo md5(randomPassword()); ?>" />
		</form>
	</div>
</div>
</div>
<!-- Sign up -->
<div class="modal loginme" id="signup" role="dialog" aria-hidden="true">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title"><?php trans('Sign Up',$lang['106']); ?></h4>
		</div>
		<form action="<?php createLink('account/register'); ?>" method="POST" class="loginme-form">
			<div class="modal-body">
				<div class="alert alert-warning">
					<button type="button" class="close dismiss">&times;</button><span></span>
				</div>
				<?php if($enable_oauth){ ?>
				<div class="form-group connect-with">
					<div class="info"><?php trans('Sign in using social network',$lang['94']); ?></div>
					<a href="<?php createLink('facebook/login'); ?>" class="connect facebook" title="<?php trans('Sign in using Facebook',$lang['95']); ?>"><?php trans('Facebook',$lang['98']); ?></a>
					<a href="<?php createLink('google/login'); ?>" class="connect google" title="<?php trans('Sign in using Google',$lang['96']); ?>"><?php trans('Google',$lang['99']); ?></a>
					<a href="<?php createLink('twitter/login'); ?>" class="connect twitter" title="<?php trans('Sign in using Twitter',$lang['97']); ?>"><?php trans('Twitter',$lang['100']); ?></a>
				</div>
				<?php } ?>
				<div class="info"><?php trans('Sign up with your email address',$lang['107']); ?></div>
				<div class="form-group">
					<label> <br />
						<input placeholder="<?php trans('Username',$lang['102']); ?>" type="text" name="username" class="form-input width96" />
					</label>
				</div>
				<div class="form-group">
					<label> <br />
						<input placeholder="<?php trans('Email',$lang['109']); ?>" type="text" name="email" class="form-input width96" />
					</label>
				</div>
				<div class="form-group">
					<label> <br />
						<input placeholder="<?php trans('Full Name',$lang['108']); ?>" type="text" name="full" class="form-input width96" />
					</label>
				</div>
				<div class="form-group">
					<label> <br />
						<input placeholder="<?php trans('Password',$lang['103']); ?>" type="password" name="password" class="form-input width96" />
					</label>
				</div>
			</div>
			<div class="modal-footer"> <br /> <br/>
				<button type="submit" class="btn btn-primary"><?php trans('Sign Up',$lang['106']); ?></button>
			</div>
			<input type="hidden" name="signup" value="<?php echo md5($date.$ip); ?>" />
			<input type="hidden" name="quick" value="<?php echo md5(randomPassword()); ?>" />
		</form>
	</div>
</div>
</div>
<!-- XD Box -->
<div class="modal fade loginme" id="xdBox" role="dialog" aria-hidden="true">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button id="xdClose" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title" id="xdTitle"></h4>
		</div>
		<div class="modal-body" id="xdContent">
		</div>
	</div>
</div>
</div>
<!-- FB chat -->
<!-- Pixel Code for https://leadee.ai/leadflows/ -->
<script async src="https://leadee.ai/leadflows/pixel/n83c40q1p4mw7od3ow2a5xot365hie99"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2P48XGXEX4"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'G-2P48XGXEX4');
</script>
</body>
</html>