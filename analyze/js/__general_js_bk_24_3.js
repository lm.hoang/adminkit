$(document).ready(function(){
  /*top-header*/
  $("#sidenav_mobile").on("click", function(){
    $("#shortcuts-var3").toggleClass('change_show');
    $(".sidenav-fixed").toggleClass('sidenav_change sidenav_rotate');
    $("body").toggleClass('theme_mega');
  });

  //Click open and close menu
  $(".menu-item").on("click", function(){
    $(this).toggleClass('change_menu');
    $(".menu-item").not(this).removeClass('change_menu');
  });

  $(".border-button-nav-gmenu").on("click", function(){
    $(".border-button-nav-gmenu").toggleClass("change_down-menu");
    $(".button-nav-gmenu").toggleClass("change-menu-table");
    $("#shortcuts-var3").addClass('change_show');
    $(".sidenav-fixed").addClass('sidenav_change');
    $(".sidenav-fixed").removeClass('sidenav_rotate');
    $("body").removeClass('theme_mega');

  });
  //click outsite menu arrown
  $(document).mouseup(e => {
     if (!$(".border-button-nav-gmenu").is(e.target) // if the target of the click isn't the container...
     && $(".menu-table").has(e.target).length === 0) // ... nor a descendant of the container
     {
        $(".border-button-nav-gmenu").removeClass("change_down-menu");
        $(".button-nav-gmenu").removeClass("change-menu-table");
      }
   });
  //search-moblie
  $("#btn-search-mobile").on("click", function(){
    $(".navbar").addClass('change_navbar');
  });
  $(".close-button").on("click", function(){
    $(".navbar").removeClass('change_navbar');
  });
  // reposive menu
  var rw = $(window).width();
  $(".moreBox").hide();
  if (rw >= 1430) {
    $(".box-one .moreBox").slice(0, 10).show();
    $(".box-two .moreBox").slice(0, 10).show();
    $(".box-three .moreBox").slice(0, 10).show();
    $(".box-four .moreBox").slice(0, 10).show();
    $(".box-five .moreBox").slice(0, 10).show();
  }
  if (rw < 1430) {
    $(".box-more").show();
    $(".box-one .moreBox").slice(0, 5).show();
    $(".box-two .moreBox").slice(0, 5).show();
    $(".box-three .moreBox").slice(0, 5).show();
    $(".box-four .moreBox").slice(0, 5).show();
    $(".box-five .moreBox").slice(0, 5).show();
  }
  if (rw < 992) {
    $(".box-more").show();
    $(".box-one .moreBox").slice(0, 10).show();
    $(".box-two .moreBox").slice(0, 10).show();
    $(".box-three .moreBox").slice(0, 10).show();
    $(".box-four .moreBox").slice(0, 10).show();
    $(".box-five .moreBox").slice(0, 10).show();
  }
  /*header-login*/
  $(".today-login").click(function() {
    $(".section-login").css({
      right: '0',
      transition: 'all 0.5s ease 0s'
    });
    $('.bg-default').addClass("bg-login");
    $('html').css('overflow', 'hidden');
    $('html').css('overflow', 'hidden');
  })
  $(".btn-close-login").click(function() {
    $(".section-login").css({
      right: '-100%',
    });
    $('html').css('overflow', 'auto');
    $('.bg-default').removeClass("bg-login");
  });

  $('.bg-default').on('click', function() {
    event.preventDefault();
    $('.btn-close-login').trigger('click');
  });
    // password
  $('#showPassword').on('click', function(){
      var passwordField = $('#password');
      var passwordFieldType = passwordField.attr('type');
      if(passwordFieldType == 'password')
      {
          passwordField.attr('type', 'text');
          $(this).val('Hide');
          $('.show-eye').hide();
          $('.hide-eye').show();
      } else {
          passwordField.attr('type', 'password');
          $(this).val('Show');
          $('.show-eye').show();
          $('.hide-eye').show();
      }

  });
  // Create-signup
  $('.Create-signup-cb').click(function() {
    $('.title-Signup').show();
    $('.intro-signup').show();
    $('.privacy-Terms').show();
    $('.btn-signup').show();
    $('.create-login').show();
    $('.confirm_pass').show();
    $('.pass-login').show();
    $('.checkbox-login').show();
    $('.name-login').show();

    $('.title-login').hide();
    $('.login-socical').hide();
    $('.btn-login-popup').hide();
    $('.forgot-pass').hide();
    $('.create-signup').hide();
    $('.cmback-login').hide();
    $('.cmbank-signup').hide();
    $('.btn-forgot').hide();
    $('.title-forgot').hide();
    $('.intro-forgot').hide();
    $('.checkbox-signup').hide();
    $('.des-login').hide();


  });

  //Create-login
  $('.Create-login-cb').click(function() {
    $('.title-Signup').hide();
    $('.intro-signup').hide();
    $('.privacy-Terms').hide();
    $('.btn-signup').hide();
    $('.create-login').hide();
    $('.checkbox-login ').hide();
    $('.confirm_pass').hide();
    $('.cmback-login').hide();
    $('.cmbank-signup').hide();
    $('.btn-forgot').hide();
    $('.title-forgot').hide();
    $('.intro-forgot').hide();
    $('.name-login').hide();
    $('.name-login').hide();

    $('.title-login').show();
    $('.login-socical').show();
    $('.btn-login-popup').show();
    $('.forgot-pass').show();
    $('.create-signup').show();
    $('.checkbox-signup').show();
    $('.pass-login').show();
    $('.pass-login').show();

  });

  $('.forgot-pass').on('click', function() {
    $('.title-forgot').show();
    $('.intro-forgot').show();
    $('.btn-forgot').show();
    $('.cmback-login').show();
    $('.cmbank-signup').show();


    $('.title-Signup').hide();
    $('.des-login').hide();
    $('.title-login').hide();
    $('.login-socical').hide();
    $('.name-login').hide();
    $('.pass-login').hide();
    $('.checkbox-login').hide();
    $('.forgot-pass').hide();
    $('.create-signup').hide();
    $('.btn-signup').hide();
    $('.btn-login-popup').hide();
    $('.checkbox-signup').hide();
  });
  /*style-js*/
   //add-more links-title
    $('.links-title').click(function() {
        $('.links-title .add-more').toggleClass('add-more-change');
        $('.show-links').toggleClass('show-links-change');
    });
});
function autocomplete(inp, arr) {
  var currentFocus;
  inp.addEventListener("input", function(e) {
      var a, b, i, val = this.value;
      closeAllLists();
      if (!val) { return false;}
      currentFocus = -1;
      a = document.createElement("DIV");
      a.setAttribute("class", "wrap-suggest");
      this.parentNode.appendChild(a);
      for (i = 0; i < arr.length; i++) {
        if (arr[i]['title'].substr(0, val.length).toUpperCase() == val.toUpperCase() || arr[i]['description'].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
          // create element a
          b = document.createElement("a");
          b.setAttribute("class", "icon-box");
          a.setAttribute("id", this.id + "autocomplete-list");
          var links_a = arr[i]['links-a'];
          b.setAttribute("href", links_a);
          b.innerHTML = '<div class="box-images"><img src="'+ arr[i]['links-img'] +'" alt=""></div>';
          if (arr[i]['title'].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
              b.innerHTML += '<div class="box-text"><h2 class="title-suggest"><strong>' + arr[i]["title"].substr(0, val.length) + '</strong>'+ arr[i]["title"].substr(val.length) +'</h2> <p class="desc-suggest">'+ arr[i]["description"]+'</p></div>';
          }else {
              b.innerHTML += '<div class="box-text"><h2 class="title-suggest">'+ arr[i]["title"]+'</h2> <p class="desc-suggest"><strong>' + arr[i]["description"].substr(0, val.length) + '</strong>'+ arr[i]["description"].substr(val.length) +'</p></div>';

          }
          b.innerHTML += "<input type='hidden' value='" + arr[i]['title'] + "'>";
          b.addEventListener("click", function(e) {
              inp.value = this.getElementsByTagName("input")[0].value;
              closeAllLists();
          });
          a.appendChild(b);
        }
      }
  });
  //event Keydown
  inp.addEventListener("keydown", function(e) {
      var x = document.getElementById(this.id + "autocomplete-list");
      if (x) x = x.getElementsByTagName("a");
      if (e.keyCode == 40) { //down
        currentFocus++;
        addActive(x);
        if(($(".autocomplete-active").offset().top + $("#" + this.id + "autocomplete-list").offset().top) > $( '#' +  "myInputautocomplete-list" ).height()){
            $( '#' +  "myInputautocomplete-list" ).scrollTop( ($("#" + this.id + "autocomplete-list").scrollTop() + $(".autocomplete-active").height()) + 20);
          }
        if (currentFocus == 0) {
            $('#' + "myInputautocomplete-list").animate({
                scrollTop: 0
            }, 'slow');
            return;
        }
      } else if (e.keyCode == 38) { //up
        currentFocus--;
        addActive(x);
        if($(".autocomplete-active").offset().top > $( '#' +  "myInputautocomplete-list" ).height()) {
           $('#' + "myInputautocomplete-list").animate({
                scrollTop: $(".autocomplete-active").offset().top
            }, 'slow');
        }
        if($(".autocomplete-active").offset().top < $("#" + this.id + "autocomplete-list").offset().top) {
          $("#" + this.id + "autocomplete-list").scrollTop(($("#" + this.id + "autocomplete-list").scrollTop() - $(".autocomplete-active").height() - 30 ));

        }
      } else if (e.keyCode == 13) { //enter
        e.preventDefault();
        if (currentFocus > -1) {
          if (x) x[currentFocus].click();
        }
      }
  });
  function addActive(x) {
    if (!x) return false;
    removeActive(x);
    if (currentFocus >= x.length) currentFocus = 0;
    if (currentFocus < 0) currentFocus = (x.length - 1);
    x[currentFocus].classList.add("autocomplete-active");
  }
  function removeActive(x) {
    for (var i = 0; i < x.length; i++) {
      x[i].classList.remove("autocomplete-active");
    }
  }
  function closeAllLists(elmnt) {
    var x = document.getElementsByClassName("wrap-suggest");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  document.addEventListener("click", function (e) {
      closeAllLists(e.target);
  });
}
function readTextFile(file) {
    var rawFile = new XMLHttpRequest();
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4)
        {
            if(rawFile.status === 200)
            {
                var allText = rawFile.responseText;
                var countries = JSON.parse(allText);
                autocomplete(document.getElementById("myInput"), countries);
            }
        }
    }
  rawFile.open("GET", file);
    rawFile.send(null);
}

readTextFile("../data/search.txt");