$(document).ready(function(){
	$("#sidenav_mobile").on("click", function(){
		$("#shortcuts-var3").toggleClass('change_show');
		$(".sidenav-fixed").toggleClass('sidenav_change sidenav_rotate');
		$("body").toggleClass('theme_mega');
	});

	//Click open and close menu
	$(".menu-item").on("click", function(){
		$(this).toggleClass('change_menu');
		$(".menu-item").not(this).removeClass('change_menu');
	});

	$(".border-button-nav-gmenu").on("click", function(){
		$(".border-button-nav-gmenu").toggleClass("change_down-menu");
		$(".button-nav-gmenu").toggleClass("change-menu-table");
		$("#shortcuts-var3").addClass('change_show');
		$(".sidenav-fixed").addClass('sidenav_change');
		$(".sidenav-fixed").removeClass('sidenav_rotate');
		$("body").removeClass('theme_mega');

	});
	//click outsite menu arrown
	$(document).mouseup(e => {
	   if (!$(".border-button-nav-gmenu").is(e.target) // if the target of the click isn't the container...
	   && $(".menu-table").has(e.target).length === 0) // ... nor a descendant of the container
	   {
	    	$(".border-button-nav-gmenu").removeClass("change_down-menu");
	    	$(".button-nav-gmenu").removeClass("change-menu-table");
	  	}
	 });
	//search-moblie
	$("#btn-search-mobile").on("click", function(){
		$(".navbar").addClass('change_navbar');
	});
	$(".close-button").on("click", function(){
		$(".navbar").removeClass('change_navbar');
	});
	// reposive menu
	var rw = $(window).width();
	$(".moreBox").hide();
	if (rw >= 1430) {
		$(".box-one .moreBox").slice(0, 10).show();
		$(".box-two .moreBox").slice(0, 10).show();
		$(".box-three .moreBox").slice(0, 10).show();
		$(".box-four .moreBox").slice(0, 10).show();
		$(".box-five .moreBox").slice(0, 10).show();
	}
	if (rw < 1430) {
		$(".box-more").show();
		$(".box-one .moreBox").slice(0, 5).show();
		$(".box-two .moreBox").slice(0, 5).show();
		$(".box-three .moreBox").slice(0, 5).show();
		$(".box-four .moreBox").slice(0, 5).show();
		$(".box-five .moreBox").slice(0, 5).show();
	}
	if (rw < 992) {
		$(".box-more").show();
		$(".box-one .moreBox").slice(0, 10).show();
		$(".box-two .moreBox").slice(0, 10).show();
		$(".box-three .moreBox").slice(0, 10).show();
		$(".box-four .moreBox").slice(0, 10).show();
		$(".box-five .moreBox").slice(0, 10).show();
	}
});