<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: Rainbow PHP Framework
 * @copyright 2020 ProThemes.Biz
 *
 */

$_SESSION['TWEB_HOMEPAGE'] = 1;

$perPage = 6; $count = 0;
$domainList = $onlyDomainName = array();
$screenShotCheck = true;

$result = mysqli_query($con, 'SELECT id,domain_name,other FROM recent_history ORDER BY id DESC LIMIT 20');
while($row = mysqli_fetch_array($result)) {
    $domainStr = strtolower($row['domain_name']);
    if(!in_array($domainStr,$onlyDomainName)){
        if($screenShotCheck){
            if (file_exists(HEL_DIR."site_snapshot/$domainStr.jpg")) {
                $other = decSerBase($row['other']);
                $onlyDomainName[] = $domainStr;
                $domainList[] = array($domainStr,$other[0],number_format($other[1]),$other[2]);
                $count++;
            }
        }else{
            $other = decSerBase($row['other']);
            $onlyDomainName[] = $domainStr;
            $domainList[] = array($domainStr,$other[0],number_format($other[1]),$other[2]);
            $count++;
        }
        if($count == $perPage)
            break;
    }
}
$count = 0;
require 'core/vendor/autoload.php';

use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;
//custom get categories
$categoryList = $onlyCategoryName = $subs = array();
$reslt = mysqli_query($con, 'SELECT * FROM domain_categories');
while($row = mysqli_fetch_array($reslt)) {
    if ($row['id'] != ''){
        $cats_name = $row['category_name'];
        if(!in_array($cats_name,$onlyCategoryName)){
            $subcats = unserialize($row['sub_categories']);
            foreach ($subcats as $key => $value) {
                $subs[$key] = $value['sub_name'];
            }
            $onlyCategoryName[] =  $cats_name;
            $categoryList[] = array($cats_name,$subs);
        }
    } else {
        $base_url = 'https://www.trustpilot.com';
        $category_url = $base_url.'/categories';
        $categories = getCat($category_url);
        if ($categories){
            foreach ($categories as $key => $item) {
                $id = $key+1;
                $category_name  = $item['cat_name'];
                $category_url   = $item['cat_link'];
                $sub_categories = serialize($item['sub_cat']);
                if ($_SESSION['TWEB_HOMEPAGE']){
                    $result = mysqli_query( $con, "INSERT INTO domain_categories (id,category_name,category_url,sub_categories) VALUES ('$id', '$category_name', '$category_url', '$sub_categories')");
                }
            }
        }
    }
}
//grandfather category
function getCat($cat_url){
    $client = new \GuzzleHttp\Client();
    $res = $client->request('GET', $cat_url);
    $html = ''.$res->getBody();
    $crawler = new Crawler($html);
    $categories = $crawler->filter('.categories_rightColumn__x8o71 > div.categories_subCategory__3OxUx')->each(function (Crawler $node, $i) {
        $name = $node->filter('h3.categories_subCategoryHeader__3Bd4c > a')->text();
        $link = $node->filter('a')->attr('href');
        $tag_link = substr(strrchr($link, "/"), 1);
        $tagUrl = prepareUrl('https://www.trustpilot.com',$link);
        //father category
        $subcategories = $node->filter('.categories_subCategoryItem__2Qwj8')->each(function(Crawler $node, $i) {
            $subname = $node->text();
            $sublink = $node->filter('a')->attr('href');
            $sub_tag = substr(strrchr($sublink, "/"), 1);
            $childUrl = prepareUrl('https://www.trustpilot.com',$sublink);
            if ($subname != ''){
                $subcat_items= [
                  'sub_name'  => $subname,
                  'sub_link'  => $childUrl,
                ];
                return $subcat_items;
            }
        });
        if ($name != ''){
            $cat_items = [
                'cat_name' => $name,
                'cat_link' => $tagUrl,
                'sub_cat'  => $subcategories
            ];
            return $cat_items;
        }
    });
    return $categories;
}
function prepareUrl($baseUrl, $urlPartToAppend, $currentUrl = null) {
    if(substr($urlPartToAppend, 0, 2) == '//') {
        return "http:" . $urlPartToAppend;
    }
    $baseUrl = rtrim($baseUrl, "/");
    if(!starts_with($urlPartToAppend, "http")) {
        if(starts_with($urlPartToAppend, "www")) return "http://" . $urlPartToAppend;
        if(starts_with($urlPartToAppend, "/")) {
            $urlPartToAppend = substr($urlPartToAppend, 1);
        } else {
            $currentUrl = $currentUrl ? $currentUrl : $baseUrl;
            if(!ends_with($currentUrl, "/") && !starts_with($urlPartToAppend, "?")) {
                $parts = explode("/", preg_replace("%^[^:]+://%", "", $currentUrl));
                if (sizeof($parts) > 1) {
                    $currentUrl = pathinfo($currentUrl, PATHINFO_DIRNAME);
                }
            } else {
            }
            $currentUrl = rtrim($currentUrl, "/");
            if(!starts_with($urlPartToAppend, "?")) $currentUrl .= "/";
            return $currentUrl . $urlPartToAppend;
        }
        return $baseUrl . "/" . $urlPartToAppend;
    }
    return $urlPartToAppend;
};
function starts_with($haystack, $needles){
    foreach ((array) $needles as $needle) {
        if ($needle !== '' && substr($haystack, 0, strlen($needle)) === (string) $needle) {
            return true;
        }
    }
    return false;
};
function ends_with($haystack, $needles){
    foreach ((array) $needles as $needle) {
        if (substr($haystack, -strlen($needle)) === (string) $needle) {
            return true;
        }
    }
    return false;
};