<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright © 2018 ProThemes.Biz
 *
 */
 
//AJAX ONLY 

//POST REQUEST Handler
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
       
    if(isset($_POST['order'])){
        
        //Get eToken Key
        $eToken = raino_trim($_POST['etoken']);
       
        //Check eToken
        if($_SESSION[N_APP.'eToken'] == $eToken){
            //Login Check - Check user logged stats
            if(isset($_SESSION[N_APP.'UserToken'])){
                //User Logged
                
                //Renew Token
                $eToken = randomPassword();
                $_SESSION[N_APP.'eToken'] = $eToken;
                
                //Get Username & sLevel
                $username = $_SESSION[N_APP.'Username'];
                $sLevel = $_SESSION[N_APP.'sLevel'];
                $nowDate = date('m/d/Y h:i:sA'); 
                $userID = getUserID($username,$con);
                
                if($sLevel == '1'){
                    //Get User Data
                    $firstname = escapeTrim($con, $_POST['firstname']);
                    $lastname = escapeTrim($con, $_POST['lastname']);
                    $company = escapeTrim($con, $_POST['company']);
                    $telephone = escapeTrim($con, $_POST['telephone']);
                    $country = escapeTrim($con, $_POST['country']);
                    $address1 = escapeTrim($con, $_POST['address1']);
                    $address2 = escapeTrim($con, $_POST['address2']);
                    $city = escapeTrim($con, $_POST['city']);
                    $postcode = escapeTrim($con, $_POST['postcode']);
                    $state = escapeTrim($con, $_POST['state']);
                    $stateStr = escapeTrim($con, $_POST['statestr']);
                    
                    //Null Value Check
                    if ($address1 != null && $city != null && $postcode != null && $state != null && $firstname != null && $telephone!=null && $country!= null){
                        if($userID !== false){    
                            $userCheckQuery = mysqli_query($con, "SELECT * FROM premium_users WHERE username='$username'");
                            $userData = base64_encode(serialize(array()));
                            if(mysqli_num_rows($userCheckQuery) > 0)
                                $query = "UPDATE premium_users SET firstname='$firstname', lastname='$lastname', company='$company', telephone='$telephone',address1='$address1', address2='$address2', city='$city', state='$state', statestr='$stateStr', postcode='$postcode', country='$country', userdata='$userData' WHERE client_id='$userID'"; //Already Premium User 
                            else
                                $query = "INSERT INTO premium_users (firstname,lastname,company,telephone,address1,address2,city,state,postcode,country,added_date,ip_address,client_id,username,statestr,userdata,pdf_data,pdf_limit) VALUES ('$firstname', '$lastname', '$company', '$telephone', '$address1', '$address2', '$city', '$state', '$postcode', '$country', '$nowDate', '$ip', '$userID', '$username', '$stateStr', '$userData', '', '')"; //New Premium User
                            
                            if (mysqli_query($con, $query)) {
                                $query = "UPDATE users SET firstname='$firstname', lastname='$lastname', company='$company', telephone='$telephone',address1='$address1', address2='$address2', city='$city', state='$state', statestr='$stateStr', postcode='$postcode', country='$country', userdata='$userData' WHERE id='$userID'";
                                mysqli_query($con, $query);
                                $_SESSION[N_APP.'sLevel'] = 2;
                                echo "1:::$eToken";
                                die();
                            }
                        }
                    }
                }
                
                if($sLevel == '2'){
                    //Get User Data
                    $paymentVal = escapeTrim($con, $_POST['paymentval']);
                    $totalAmount = escapeTrim($con, $_POST['totalamount']);
                    $amWithTax = escapeTrim($con, $_POST['WithTax']);
                    $recType = strtolower(escapeTrim($con, $_POST['recType']));
                    $billType = escapeTrim($con, $_POST['billtype']);
                    $planID = escapeTrim($con, $_POST['planid']);
                    $currencyType = escapeTrim($con,$_POST['currencyType']);
                    $taxAm = $recVal = 0;
                    $totalAmountwithTax = $totalAmount;
                    $premiumUserID = getPremiumUserID($username,$con);
                    $paymentName = getPaymentGateway($con,$paymentVal);
                    
                    //Null Value Check
                    if ($paymentVal != null && $totalAmount != null && $amWithTax != null && $billType != null && $planID != null){
                        //Plan ID Check
                        if($_SESSION[N_APP.'planID'] == $planID){
                            $query = mysqli_query($con, "SELECT * FROM premium_plans WHERE id='$planID'");
                            if (mysqli_num_rows($query) > 0){
                                $data = mysqli_fetch_array($query);
                                $plan_name = $data['plan_name'];
                                //Billing Type Check
                                if($data['payment_type'] == $billType){
                                    if($data['payment_type'] == '0'){
                                        $planAmount = calNewCurrencyAmount($currencyType,$data['one_time_fee'],$con);
                                    }else{
                                         $recurringPlanAmount = unserialize($data['recurrent_fee']);
                                         $recVal = getRecType($recType);
                                         $planAmount = calNewCurrencyAmount($currencyType,$recurringPlanAmount[$recVal][0],$con);
                                         $recCon = serialize(array('0','0','0','user','0'));
                                    }
                                    //Payment Amount Check
                                    if (abs((floatval($planAmount)-floatval($totalAmount))/floatval($totalAmount)) < 0.00001) {     
                                    //Calculate Tax
                                    $taxArr = getTaxData($ip,$con); 
                                    $taxDataStr = serialize($taxArr); 
                                        foreach($taxArr as $tax){
                                            if($tax[0])
                                                $taxAm = $tax[2];
                                            else
                                                $taxAm = $totalAmount * ($tax[2]/100); 
                                        $totalAmountwithTax = $totalAmountwithTax + $taxAm;       
                                        }
                                      //Tax Check
                                      if (abs((floatval($totalAmountwithTax)-floatval($amWithTax))/floatval($amWithTax)) < 0.00001) {
                                        #---User Data Passed---#
                                        //$amWithTax = con2money_format($amWithTax,$currencyType);
                                        $amWithTax = round($amWithTax,2);
                                        //Get Order Settings
                                        extract(orderSettings($con));
                                        //Generate Invoice Prefix
                                        $ordrNum = getInvoicePrefix($con);
                                        //Empty Order Comments
                                        $orderComments = base64_encode(serialize(array()));
                                        //Create the Order
                                        $query = "INSERT INTO premium_orders (plan_id, user_id, premium_user_id, invoice_prefix, billing_type, payment_type, status, amount, date, ip_address, rec_type, username, amount_tax, currency_type, plan_name, payment_status, tax_data, rec_data, order_comments, order_log) VALUES ('$planID', '$userID', '$premiumUserID', '$ordrNum', '$billType', '$paymentVal', '$order_status', '$totalAmount', '$nowDate', '$ip', '$recType', '$username', '$amWithTax', '$currencyType', '$plan_name', 'pending', '$taxDataStr', '$recCon', '$orderComments', '$orderComments')"; //New Order
                                        if (mysqli_query($con, $query)) {
                                            $_SESSION[N_APP.'sLevel'] = 3;
                                            $_SESSION[N_APP.'orderID'] = mysqli_insert_id($con);
                                            echo "1:::$eToken:::$paymentName[3]";
                                            die();
                                        }
                                      }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } 
      errOrder();
    }
    die();
}


//GET REQUEST Handler
if(isset($_GET['calTax'])){
    $taxData = ''; $totalAmount = 0;
    $amount = raino_trim($_GET['amount']);
    $currencyType = raino_trim($_GET['currencyType']);
    $currencySymbol = getCurrencySymbol($currencyType);
    $taxArr = getTaxData($ip,$con);   
    $totalAmount = $amount;
       
    $taxData .= '<div><span class="bold16 gray16">'.trans('Sub-Total', $lang['AD766'], true).':</span> <span id="subAm" class="bold14 gray16">'.$currencySymbol[0].con2money_format($amount,$currencyType).'</span></div>';
      
    foreach($taxArr as $tax){
        if($tax[0])
            $taxAm = $tax[2];
        else
            $taxAm = $amount * ($tax[2]/100); 
                   
    $totalAmount = $totalAmount + $taxAm;       
    $taxData .= '<div><span class="bold16 gray16">'.$tax[1].':</span> <span id="taxAm" class="bold14 gray16">'.$currencySymbol[0].con2money_format($taxAm,$currencyType).'</span></div>';
    }
    $taxData .= '<div><span class="bold17">'.trans('Total', $lang['AD767'], true).':</span> <span id="subAm" class="bold15 blue16">'.$currencySymbol[0].con2money_format($totalAmount,$currencyType).'</span></div>';
    $taxData .= '<input type="hidden" name="amWithTax" value="'.$totalAmount.'" />';
    die($taxData);
}

die();