<?php
defined('FLAG_COUNTER') or die(header('HTTP/1.0 403 Forbidden'));

/*
* @author Balaji
* @name: Flag Counter on A to Z SEO Tools
* @copyright � 2018 ProThemes.Biz
*
*/
    //Fix
    $server_path = 'h';
    $dataM = $data;
    
    //Load Flag Counter Settings
    $query =  "SELECT * FROM default_settings";
    $result = mysqli_query($con,$query);
            
    while($row = mysqli_fetch_array($result)) {
        $limit_results =  Trim($row['flag_limit']);
        $columns =   Trim($row['column_limit']);
        $flag_title =  Trim($row['top_label']);
        $background_color =   Trim($row['back_color']);
        $title_color =   Trim($row['title_color']);
        $textColor =   Trim($row['text_color']);
        $border_color =   Trim($row['border_color']);
        $country_code =   Trim($row['country_code']);
        $showPage =   Trim($row['showPage']);
        $flagSize = Trim($row['flagSize']);
        $default_flag_path = Trim($row['flag_path']);
        $bottom_padding = Trim($row['bottom_padding']);
        $title_bottom_padding = Trim($row['title_padding']);
        $side_buffer = Trim($row['side_space']);
    }
    $showPage = filter_var($showPage, FILTER_VALIDATE_BOOLEAN);
    $countryCode = filter_var($country_code, FILTER_VALIDATE_BOOLEAN);

    if($pointOut == 'load'){

    require_once (LIB_DIR."geoip.inc");   
        
    if ($flag_icon_path == '')
        $flag_icon_path = 'icons/logo_small.png';

    //Get user data from URL
    $code_id = raino_trim($route[2]);
    if (empty($code_id))
    {
        echo $lang['AD376'];
        die();
    }
    
    $code_id = str_replace("/", "", $code_id);
    $user_id = decode_URL($code_id);

//Check user code found on database
$qu = mysqli_query($con, "SELECT * FROM user_data WHERE id='$user_id'");
if (mysqli_num_rows($qu) > 0)
{
    // Current Date & User IP
    $date = date('jS F Y');
    $ip = $_SERVER['REMOTE_ADDR'];
    $user_browser = $_SERVER['HTTP_USER_AGENT'];

    $gi = geoip_open(LIB_DIR.'GeoIP.dat', GEOIP_MEMORY_CACHE);
    $user_country = geoip_country_code_by_addr($gi, $ip);
    geoip_close($gi);
    $user_country_code = (!empty($user_country)) ? $user_country : "unknown";
    $user_country = (!empty($user_country)) ? "code_" . $user_country :
        "code_unknown";

    //Get User Data as array
    $query = "SELECT * FROM user_data WHERE id='$user_id'";
    $result = mysqli_query($con, $query);

    $data = mysqli_fetch_array($result, MYSQLI_ASSOC);

    //Load all data
    $limit_results = (!empty($data['flag_limit'])) ? $data['flag_limit'] : $limit_results;
    $columns = (!empty($data['column_limit'])) ? $data['column_limit'] : $columns;
    $flag_title = (!empty($data['top_label'])) ? $data['top_label'] : $flag_title;
    $background_color = (!empty($data['back_color'])) ? $data['back_color'] : $background_color;
    $title_color = (!empty($data['title_color'])) ? $data['title_color'] : $title_color;
    $textColor = (!empty($data['text_color'])) ? $data['text_color'] : $textColor;
    $border_color = (!empty($data['border_color'])) ? $data['border_color'] : $border_color;
    $border_thick = (!empty($data['border_thick'])) ? $data['border_thick'] : $border_thick;
    $country_code = (isset($data['country_code'])) ? $data['country_code'] : $country_code;
    $country_code = filter_var($country_code, FILTER_VALIDATE_BOOLEAN);
    $showPage = (isset($data['showPage'])) ? $data['showPage'] : $showPage;
    $showPage = filter_var($showPage, FILTER_VALIDATE_BOOLEAN);
    $pageview = $data['pageview'];
    $in_code = $data[$user_country];
    $flagSize = $data['flagSize'];

    //Check IP already present on database or not
    $qu = mysqli_query($con, "SELECT * FROM table_$code_id WHERE ip='$ip'");
    if (mysqli_num_rows($qu) > 0)
    {
        //Present
        $pageview = $pageview + 1;
        $query = "UPDATE user_data SET pageview='$pageview' WHERE id='$user_id'";
        mysqli_query($con, $query);
    } else
    {
        //Not present

        //Increase pageview
        $pageview = $pageview + 1;
        $in_code = $in_code + 1;
        $now_time = mktime(date("H"), date("i"), date("s"), date("n"), date("j"), date("Y"));

        $query = "INSERT INTO table_$code_id (date,now_time,ip,country,browser) VALUES ('$date','$now_time','$ip','$user_country_code','$user_browser')";
        mysqli_query($con, $query);

        $query = "UPDATE user_data SET $user_country='$in_code', pageview='$pageview' WHERE id='$user_id'";
        mysqli_query($con, $query);

    }
} else
{
    echo $lang['AD376'];
    die();
}

//Get User Data as array
$query = "SELECT * FROM user_data WHERE id='$user_id'";
$result = mysqli_query($con, $query);

$data = mysqli_fetch_array($result, MYSQLI_ASSOC);

//Remove unwanted columns from array
unset($data['id']);
unset($data['user_ip']);
unset($data['date']);
unset($data['flag_limit']);
unset($data['column_limit']);
unset($data['top_label']);
unset($data['back_color']);
unset($data['title_color']);
unset($data['text_color']);
unset($data['border_color']);
unset($data['border_thick']);
unset($data['pageview']);
unset($data['country_code']);
unset($data['showPage']);
unset($data['flagSize']);
unset($data['passWord']);

//Detect the flag size
if (strtolower($flagSize) == 'large')
{
    $flag_size = '24';
    $default_flag_path = 'flags/default/24/';
    $flag_format = ".png";
    $flagHeight = 28;
    $flagOffset = 18;
    $column_width = 136;
    $textOffset = 28;
    $hitOffset = 53;
} elseif (strtolower($flagSize) == 'medium')
{
    $flag_size = '20';
    $default_flag_path = 'flags/default/20/';
    $flag_format = ".png";
    $flagHeight = 23;
    $flagOffset = 15;
    $column_width = 133;
    $textOffset = 24;
    $hitOffset = 49;
} else
{
    $flag_size = '16';
    $default_flag_path = 'flags/default/16/';
    $flag_format = ".png";
    $flagHeight = 18;
    $flagOffset = 12;
    $column_width = 130;
    $textOffset = 20;
    $hitOffset = 45;
}

//Set font for text
$title_font = 'core/fonts/verdana.ttf';
$main_font = 'core/fonts/verdana.ttf';

//Set font height
$fontHeight = 24;

//Set width and height for flag
$image_width = ($column_width * $columns) + $side_buffer;
$image_height = (ceil($limit_results / $columns) * $flagHeight) + $bottom_padding;

//Border thickness
$border_thick = '1';

if ($columns == '1')
{
    $image_height = $image_height + 6;
}

//Add space for title
if (!empty($flag_title))
{
    $image_height = $image_height + $fontHeight + $title_bottom_padding;
}

//Add Space for pagesviews
if ($showPage == true)
{
    $image_height = $image_height + 14;
}

// Create the image
$img = @imagecreatetruecolor($image_width, $image_height);

// Fill the rectangle
imagefilledrectangle($img, 0, 0, $image_width, $image_height, hextorgb($img, $background_color));

//Draw a Broder
$border_color = hextorgb($img, $border_color);
drawBorder($img, $border_color, $border_thick);


// Title for flag counter
if (!empty($flag_title))
{
    imagettftext($img, 16, 0, ceil($side_buffer / 2), $fontHeight, hextorgb($img, $title_color),
        $title_font, $flag_title);
    $start_flags = $fontHeight + $title_bottom_padding;
}

$column = 1;
$i = 1;
$start = 0;

//Sort the array
arsort($data);

foreach ($data as $country => $value)
{

    if ($start == $limit_results)
    {
        break;
    }
    $start++;
    if ($value == '0')
    {
        //Skip
    } else
    {

        //Flag data present
        $country = str_replace('code_', '', $country);

        $country = ($country == 'unknown') ? '??' : $country;

        //  Size of the Flag / Counts / Country Code

        $textX = (($column_width * $column) - $column_width) + ceil($side_buffer / 2) +
            $textOffset;

        $hitCountX = (($column_width * $column) - $column_width) + ceil($side_buffer / 2) +
            $hitOffset;

        $flagX = (($column_width * $column) - $column_width) + ceil($side_buffer / 2);

        $hitCountY = ((ceil($i / $columns)) * $flagHeight) + $start_flags;

        $flagY = ((ceil($i / $columns)) * $flagHeight) + $start_flags - $flagOffset;

        $textY = ((ceil($i / $columns)) * $flagHeight) + $start_flags;

        //Convert into Country Code into Country Name
        $my_flag_name = Trim(strtolower(country_code_to_country($country)));

        //Declare image location
        $flag = $default_flag_path . strtolower($my_flag_name) . $flag_format;

        // Create the flag counter image
        $src = @imagecreatefrompng($flag);

        // Size of the Flag
        @imagecopy($img, $src, $flagX, $flagY, 0, 0, $flag_size, $flag_size);

        //Check country code needed or not
        if ($country_code == true)
        {
            imagettftext($img, 10, 0, $textX, $textY, hextorgb($img, $textColor), $main_font,
                $country);
            imagettftext($img, 10, 0, $hitCountX, $hitCountY, hextorgb($img, $textColor), $main_font,
                number_format($value));
        } else
        {
            imagettftext($img, 10, 0, $textX + 4, $textY, hextorgb($img, $textColor), $main_font,
                number_format($value));
        }


        if ($column == $columns)
        {
            $column = 1;
        } else
        {
            $column++;
        }
        $i++;
    }

}
//Close the database conncetion
mysqli_close($con);

//Check PageView needed or not
if ($showPage == true)
{
    imagettftext($img, 10, 0, ceil($side_buffer / 2), $textY + 24, hextorgb($img, $textColor),
        $main_font, $lang['AD377'].": " . number_format($pageview));
}

//Show any Logo

//Declare beta logo
//$src = @imagecreatefrompng('icons/beta.png');

// Copy into image
// @imagecopy($img, $src, $image_width-35, 3, 0, 0, 32, 33);

//Declare brand logo
if (str_contains($flag_icon_path, ".png"))
{
    $src1 = @imagecreatefrompng($flag_icon_path);
} elseif (str_contains($flag_icon_path, ".jpg"))
{
    $src1 = @imagecreatefromjpeg($flag_icon_path);
} elseif (str_contains($flag_icon_path, ".jpeg"))
{
    $src1 = @imagecreatefromjpeg($flag_icon_path);
} else
{
    $src1 = @imagecreatefrompng('icons/logo_small.png');
}


// Copy into image
@imagecopy($img, $src1, $image_width - 100, $image_height - 34, 0, 0, 93, 33);

// Output the image
header("Content-type: image/png");
imagepng($img);
imagedestroy($img);
        
    }
    
    if ($pointOut == 'generate') {
        
        $flag_limit = raino_trim($_POST['maximumFlag']);
        $column_limit = raino_trim($_POST['columnsLimit']);
        $top_label = raino_trim($_POST['counterName']);
        $page_view = raino_trim($_POST['pageView']);
        $show_label = raino_trim($_POST['showLabel']);
        $flag_size = raino_trim($_POST['flag_size']);
        $back_color = raino_trim($_POST['backColor']);
        $back_color = str_replace("#",'',$back_color);
        $title_color = raino_trim($_POST['titleColor']);
        $title_color = str_replace("#",'',$title_color);
        $text_color = raino_trim($_POST['textColor']);
        $text_color =  str_replace("#",'',$text_color);
        $border_color = raino_trim($_POST['borderColor']);
        $border_color = str_replace("#",'',$border_color);
        $flag_size = raino_trim($_POST['flagSize']);
        $passWord = randomPassword();
        $page_view = ($page_view == '' ? 'false' : 'true');
        $show_label = ($show_label == '' ? 'false' : 'true');
        
        if (empty($flag_limit) || empty($column_limit) || empty($top_label) || empty($title_color) || empty($back_color) || empty($text_color) || empty($border_color))
        {
            die('Malformed request');
        }
        
        $query = "INSERT INTO user_data (date, user_ip, flag_limit, column_limit, top_label, back_color, title_color, text_color, border_color, border_thick, pageview, flagSize, country_code, showPage, passWord, code_unknown,code_AP,code_EU,code_AD,code_AE,code_AF,code_AG,code_AI,code_AL,code_AM,code_CW,code_AO,code_AQ,code_AR,code_AS,code_AT,code_AU,code_AW,code_AZ,code_BA,code_BB,code_BD,code_BE,code_BF,code_BG,code_BH,code_BI,code_BJ,code_BM,code_BN,code_BO,code_BR,code_BS,code_BT,code_BV,code_BW,code_BY,code_BZ,code_CA,code_CC,code_CD,code_CF,code_CG,code_CH,code_CI,code_CK,code_CL,code_CM,code_CN,code_CO,code_CR,code_CU,code_CV,code_CX,code_CY,code_CZ,code_DE,code_DJ,code_DK,code_DM,code_DO,code_DZ,code_EC,code_EE,code_EG,code_EH,code_ER,code_ES,code_ET,code_FI,code_FJ,code_FK,code_FM,code_FO,code_FR,code_SX,code_GA,code_GB,code_GD,code_GE,code_GF,code_GH,code_GI,code_GL,code_GM,code_GN,code_GP,code_GQ,code_GR,code_GS,code_GT,code_GU,code_GW,code_GY,code_HK,code_HM,code_HN,code_HR,code_HT,code_HU,code_ID,code_IE,code_IL,code_IN,code_IO,code_IQ,code_IR,code_IS,code_IT,code_JM,code_JO,code_JP,code_KE,code_KG,code_KH,code_KI,code_KM,code_KN,code_KP,code_KR,code_KW,code_KY,code_KZ,code_LA,code_LB,code_LC,code_LI,code_LK,code_LR,code_LS,code_LT,code_LU,code_LV,code_LY,code_MA,code_MC,code_MD,code_MG,code_MH,code_MK,code_ML,code_MM,code_MN,code_MO,code_MP,code_MQ,code_MR,code_MS,code_MT,code_MU,code_MV,code_MW,code_MX,code_MY,code_MZ,code_NA,code_NC,code_NE,code_NF,code_NG,code_NI,code_NL,code_NO,code_NP,code_NR,code_NU,code_NZ,code_OM,code_PA,code_PE,code_PF,code_PG,code_PH,code_PK,code_PL,code_PM,code_PN,code_PR,code_PS,code_PT,code_PW,code_PY,code_QA,code_RE,code_RO,code_RU,code_RW,code_SA,code_SB,code_SC,code_SD,code_SE,code_SG,code_SH,code_SI,code_SJ,code_SK,code_SL,code_SM,code_SN,code_SO,code_SR,code_ST,code_SV,code_SY,code_SZ,code_TC,code_TD,code_TF,code_TG,code_TH,code_TJ,code_TK,code_TM,code_TN,code_TO,code_TL,code_TR,code_TT,code_TV,code_TW,code_TZ,code_UA,code_UG,code_UM,code_US,code_UY,code_UZ,code_VA,code_VC,code_VE,code_VG,code_VI,code_VN,code_VU,code_WF,code_WS,code_YE,code_YT,code_RS,code_ZA,code_ZM,code_ME,code_ZW,code_A1,code_A2,code_O1,code_AX,code_GG,code_IM,code_JE,code_BL,code_MF,code_BQ,code_SS) VALUES (
        '$date','$ip','$flag_limit','$column_limit','$top_label','$back_color','$title_color','$text_color','$border_color','$border_thick','1','$flag_size','$show_label','$page_view','$passWord',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)"; 
        
        // Execute query
        if (mysqli_query($con,$query)) {
        
        // "New Data created successfully <br>";
        $last_id = get_last_id($con);
        $last_id = convert_URL($last_id);
          
        $sql = "CREATE TABLE table_$last_id 
        (
        id INT NOT NULL AUTO_INCREMENT,
        PRIMARY KEY(id),
        date text,
        now_time text,
        ip text,
        country text,
        browser text
        )";
        
        // Execute query
        if (mysqli_query($con,$sql)) {
        //echo "New Table created successfully <br>";
        $msg = $lang['AD378'];
        
        $image_link = $server_path.substr($toolURL, 1).'/load/'.$last_id.'/';
        $statics_link = $server_path.substr($toolURL, 1).'/details/'.$last_id.'/';
        $re_generate_link = $server_path.substr($toolURL, 1).'/regenerate/'.$last_id.'/';
        $html_code = '<a href="'.$server_path.substr($toolURL, 1).'/details/'.$last_id.'/"><img src="'.$image_link.'" alt="Flag Counter"></a>';
        $bb_code = '[URL='.$server_path.substr($toolURL, 1).'/details/'.$last_id.'/][IMG]'.$image_link.'[/IMG][/URL]';
        
        } else {
            $msg = $lang['AD379'];
            $error = '1';
        }
        
        } else {
            $msg = $lang['AD380'];
            $error = '1';
        }

    }
    
    if ($pointOut == 'details') {
        
    //Get user data from URL
    $code_id = raino_trim($route[2]);
    if (empty($code_id))
    {
        echo $lang['AD376'];
        die();
    }
    $code_id = str_replace("/", "", $code_id);
    $user_id = decode_URL($code_id);
    
    //Check user code found on database
    $qu = mysqli_query($con, "SELECT * FROM user_data WHERE id='$user_id'");
    if (mysqli_num_rows($qu) > 0)
    {
    
    // Sub Page Found
    if (isset($route[3]) && $route[3] != '') {
        
    $page_id = raino_trim($route[3]); 
    $page_id = str_replace("/","",$page_id);
    $pointOut = $page_id;
    if ($page_id == 'live-traffic')
    {
        $p_title = $lang['AD381'];
        $result = mysqli_query($con, "SELECT * FROM table_$code_id ORDER BY id DESC LIMIT 0 , 15");
        while ($row = mysqli_fetch_array($result))
        {
            $agent = $row['browser'];
            $exCountryCode = $row['country'];
            $agent = parse_user_agent($agent);
            extract($agent);
            $country[] = country_code_to_country($exCountryCode);
            $platform = ($platform == '' ? 'Unknown' : $platform);
            $browser = ($browser == '' ? 'Unknown' : $browser);
            $os[] = $platform;
            $browserArr[] = $browser;
            $nowtime = time();
            $oldtime = $row['now_time'];
            $visTime[] = conTime($nowtime-$oldtime);
            
        }
    }
    elseif ($page_id == 'today')
    {
        $p_title = $lang['AD382'];
        $result = mysqli_query($con, "SELECT * FROM table_$code_id WHERE date='$date'");
        while ($row = mysqli_fetch_array($result))
        {
          $ip = $row['ip'];
          $exCountryCode = $row['country'];
          $exCountry = country_code_to_country($exCountryCode);
          $agent = $row['browser'];
          $agent = parse_user_agent($agent);
          extract($agent);
          $countryCode[]=$exCountryCode;
          $Country[]=$exCountry;
          $os[] = $platform;
          $browserArr[] = $browser;
          $nowtime = time();
          $oldtime = $row['now_time'];
          $visTime[] = conTime($nowtime-$oldtime);
        }
        $dataCountry = array_count_values($countryCode);
        $totalCount = count($dataCountry);
        arsort($dataCountry);
    }
    elseif ($page_id == 'yesterday')
    {
        $p_title = $lang['AD383'];
        $date = date('jS F Y', strtotime(date('jS F Y'). ' -1 day'));  
        $result = mysqli_query($con, "SELECT * FROM table_$code_id WHERE date='$date'");
        while ($row = mysqli_fetch_array($result))
        {
          $ip = $row['ip'];
          $exCountryCode = $row['country'];
          $exCountry = country_code_to_country($exCountryCode);
          $agent = $row['browser'];
          $agent = parse_user_agent($agent);
          extract($agent);
          $countryCode[]=$exCountryCode;
          $Country[]=$exCountry;
          $os[] = $platform;
          $browserArr[] = $browser;
          $nowtime = time();
          $oldtime = $row['now_time'];
          $visTime[] = conTime($nowtime-$oldtime);
        }
        $dataCountry = array_count_values($countryCode);
        $totalCount = count($dataCountry);
        arsort($dataCountry);
    }
    elseif ($page_id == 'last-30-days')
    {
        $p_title = $lang['AD384'];
        $result = mysqli_query($con, "SELECT * FROM table_$code_id WHERE STR_TO_DATE( date , '%D %M %Y') BETWEEN SUBDATE(CURDATE(), INTERVAL 1 MONTH) AND NOW()");
        while ($row = mysqli_fetch_array($result))
        {
          $ip = $row['ip'];
          $exCountryCode = $row['country'];
          $exCountry = country_code_to_country($exCountryCode);
          $agent = $row['browser'];
          $agent = parse_user_agent($agent);
          extract($agent);
          $countryCode[]=$exCountryCode;
          $Country[]=$exCountry;
          $os[] = $platform;
          $browserArr[] = $browser;
          $nowtime = time();
          $oldtime = $row['now_time'];
          $visTime[] = conTime($nowtime-$oldtime);
        }
        $dataCountry = array_count_values($countryCode);
        $totalCount = count($dataCountry);
        arsort($dataCountry);
    }
    elseif ($page_id == 'all-details')
    {
        $p_title = $lang['AD385'];
    }
    }
    else
    {
        $p_title = 'Overview';
        $data=mysqli_fetch_array($qu,MYSQLI_ASSOC);
        $pageview = $data['pageview'];
        $result = mysqli_query($con, "SELECT * FROM table_$code_id");
        while ($row = mysqli_fetch_array($result))
        {
          $ip = $row['ip'];
          $exCountryCode = $row['country'];
          $exCountry = country_code_to_country($exCountryCode);
          $agent = $row['browser'];
          $agent = parse_user_agent($agent);
          extract($agent);
          $countryCode[]=$exCountryCode;
          $Country[]=$exCountry;
          $os[] = $platform;
          $browserArr[] = $browser;
        }
        $dataCountry = array_count_values($countryCode);
        $dataOs = array_count_values($os);
        $dataBrowser = array_count_values($browserArr);
        $totalCount = count($dataCountry);
        $totalHit =0;
        foreach($dataCountry as $country=>$hits)
        {
            $totalHit=$totalHit+(int)$hits;
        }
        $query = "SELECT @last_id := MAX(id) FROM table_$code_id";
        $gid = mysqli_query($con, $query);
        while ($row = mysqli_fetch_array($gid))
        {
            $last_id = $row['@last_id := MAX(id)'];
        }
        $res = mysqli_query($con, "SELECT * FROM table_$code_id WHERE id='$last_id'");
        while ($row = mysqli_fetch_array($res))
        {
            $lastCountryCode = $row['country'];
            $lastAgent = $row['browser'];
            $lastDate = $row['date'];
        }
        $lastCountry = country_code_to_country($lastCountryCode);
        $lastAgent = parse_user_agent($lastAgent);
        extract($lastAgent);
        $lastBrowser = $browser; 
        $lastOS = $platform;
          
        // Sort     
        arsort($dataCountry);
        arsort($dataOs);
        arsort($dataBrowser);
      }
    } else {
      $error = $lang['AD376'];
    }
    }
    
    if ($pointOut == 'regenerate') {
        
        //Get user data from URL
        $code_id = raino_trim($route[2]);
        if (empty($code_id))
        {
            echo $lang['AD376'];
            die();
        }
        
        $code_id = str_replace("/", "", $code_id);
        $user_id = decode_URL($code_id);
        
        //Check user code found on database
        $qu = mysqli_query($con, "SELECT * FROM user_data WHERE id='$user_id'");
        if (mysqli_num_rows($qu) > 0)
        {
            //Get User Data as array
            $query="SELECT * FROM user_data WHERE id='$user_id'";
            $result=mysqli_query($con,$query);
            $data=mysqli_fetch_array($result,MYSQLI_ASSOC);
            
            $userDate = $data['date'];
            $passWord = $data['passWord'];
            $flagLimit = $data['flag_limit'];
            $columnLimit = $data['column_limit'];
            $topLabel = $data['top_label'];
            $backColor = $data['back_color'];
            $titleColor = $data['title_color'];
            $borderColor = $data['border_color'];
            $textColor = $data['text_color'];
            $countryCode = $data['country_code'];
            $showPage = $data['showPage'];
            $showPage = filter_var($showPage, FILTER_VALIDATE_BOOLEAN);
            $countryCode = filter_var($countryCode, FILTER_VALIDATE_BOOLEAN);
            $flagSize = $data['flagSize'];
        }
        else
        {
            echo $lang['AD376'];
            die();
        }
    
    }
        $data = $dataM;
?>