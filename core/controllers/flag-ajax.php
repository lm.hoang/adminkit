<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: Rainbow PHP Framework 
 * @copyright � 2016 ProThemes.Biz
 *
 */

//AJAX ONLY 

//POST REQUEST Handler
if ($_SERVER['REQUEST_METHOD'] =='POST') {
    
    if($pointOut == 'ajaxDemo'){
        $flag_limit = raino_trim($_POST['flag_limit']);
        $column_limit = raino_trim($_POST['column_limit']);
        $top_label = raino_trim($_POST['top_label']);
        $page_view = raino_trim($_POST['page_view']);
        $show_label = raino_trim($_POST['show_label']);
        $flag_size = raino_trim($_POST['flag_size']);
        $back_color = raino_trim($_POST['back_color']);
        $back_color = str_replace("#",'',$back_color);
        $title_color = raino_trim($_POST['title_color']);
        $title_color = str_replace("#",'',$title_color);
        $text_color = raino_trim($_POST['text_color']);
        $text_color =  str_replace("#",'',$text_color);
        $border_color = raino_trim($_POST['border_color']);
        $border_color = str_replace("#",'',$border_color);
        $flag_size = raino_trim($_POST['flag_size']);
        
        $query = "UPDATE user_data SET flagSize='$flag_size', country_code='$show_label', showPage='$page_view', top_label='$top_label', flag_limit='$flag_limit', column_limit='$column_limit', back_color='$back_color', title_color='$title_color', text_color='$text_color', border_color='$border_color' WHERE id='2'"; 
        mysqli_query($con,$query); 
              
        if (mysqli_errno($con)) {   
        die('0');     
        }
        else
        {
        die('1');
        }
    }
    
    if($pointOut == 'ajaxSave'){
        
        $user_id = raino_trim($_POST['user_id']);
        $passWord = raino_trim($_POST['passWord']);
        $flag_limit = raino_trim($_POST['flag_limit']);
        $column_limit = raino_trim($_POST['column_limit']);
        $top_label = raino_trim($_POST['top_label']);
        $page_view = raino_trim($_POST['page_view']);
        $show_label = raino_trim($_POST['show_label']);
        $flag_size = raino_trim($_POST['flag_size']);
        $back_color = raino_trim($_POST['back_color']);
        $back_color = str_replace("#",'',$back_color);
        $title_color = raino_trim($_POST['title_color']);
        $title_color = str_replace("#",'',$title_color);
        $text_color = raino_trim($_POST['text_color']);
        $text_color =  str_replace("#",'',$text_color);
        $border_color = raino_trim($_POST['border_color']);
        $border_color = str_replace("#",'',$border_color);
        $flag_size = raino_trim($_POST['flag_size']);
        
        //Check user code found on database
        $qu = mysqli_query($con, "SELECT * FROM user_data WHERE id='$user_id'");
        if (mysqli_num_rows($qu) > 0)
        {
        
        while($row = mysqli_fetch_array($qu)) {
            $user_password =  Trim($row['password']);
        }
        
        if ($user_password == $password)
        {
        $query = "UPDATE user_data SET flagSize='$flag_size', country_code='$show_label', showPage='$page_view', top_label='$top_label', flag_limit='$flag_limit', column_limit='$column_limit', back_color='$back_color', title_color='$title_color', text_color='$text_color', border_color='$border_color' WHERE id='$user_id'"; 
        mysqli_query($con,$query); 
              
        if (mysqli_errno($con)) {   
            echo $lang['AD380'];
            die();     
        }
        else
        {
        die('1');
        }
        }
        else
        {
            echo $lang['AD380'];
            die();     
        }
        }
        else
        {
            echo $lang['AD376'];
            die();
        }

    }
    
    if($pointOut == 'ajaxPass'){
        
        $user_passWord = raino_trim($_POST['password']);
        $user_id = raino_trim($_POST['user_id']);
        
        //Get User Data as array
        $query="SELECT * FROM user_data WHERE id='$user_id'";
        $result=mysqli_query($con,$query);
        $data=mysqli_fetch_array($result,MYSQLI_ASSOC);
        $passWord = $data['passWord'];
        
        if ($user_passWord == $passWord)
        {
            die('1');
        }
        else
        {
            echo $lang['AD386'];
            die();
        }

    }
    
}

//GET REQUEST Handler
if(isset($_GET['hello'])) {
    echo 'Hello';
    die();
}


if($pointOut == 'ajax-data'){
    
    //Get Request data
    $code_id = raino_trim($_GET['code_id']);
    $date = raino_trim($_GET['date']);
    
    // DB table to use
    $table = "table_$code_id";
    
    // Table's primary key
    $primaryKey = 'id';
    
    // Array of database columns which should be read and sent back to DataTables.
    // The `db` parameter represents the column name in the database, while the `dt`
    // parameter represents the DataTables column identifier. In this case simple
    // indexes
    $columns = array(
    	array( 'db' => 'id', 'dt' => 0 ),
    	array( 'db' => 'date',  'dt' => 1 ),
    	array( 'db' => 'now_time',   'dt' => 2 ),
    	array( 'db' => 'country',   'dt' => 3 ),
    	array( 'db' => 'browser',   'dt' => 4 )
    );
    
    $columns2 = array(
    	array( 'db' => 'id', 'dt' => 0 ),
    	array( 'db' => 'country',   'dt' => 1 ),
    	array( 'db' => 'browser',   'dt' => 2 ),
    	array( 'db' => 'platform',  'dt' => 3 ),
    	array( 'db' => 'time',   'dt' => 4)
    );
    
    
    // SQL server connection information
    $sql_details = array(
    	'user' => $dbUser,
    	'pass' => $dbPass,
    	'db'   => $dbName,
    	'host' => $dbHost
    );
    
    
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * If you just want to use the basic configuration for DataTables with PHP
     * server-side, there is no need to edit below this line.
     */
        
    echo json_encode(
    	SSPFLAGDATA::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $columns2, $date )
    );
    die();
}

if($pointOut == 'ajax-all-data'){
    
    //Get Request data
    $code_id = raino_trim($_GET['code_id']);
    
    // DB table to use
    $table = "table_$code_id";
    
    // Table's primary key
    $primaryKey = 'id';
    
    // Array of database columns which should be read and sent back to DataTables.
    // The `db` parameter represents the column name in the database, while the `dt`
    // parameter represents the DataTables column identifier. In this case simple
    // indexes
    $columns = array(
    	array( 'db' => 'id', 'dt' => 0 ),
    	array( 'db' => 'date',  'dt' => 1 ),
    	array( 'db' => 'now_time',   'dt' => 2 ),
    	array( 'db' => 'country',   'dt' => 3 ),
    	array( 'db' => 'browser',   'dt' => 4 )
    );
    
    $columns2 = array(
    	array( 'db' => 'id', 'dt' => 0 ),
    	array( 'db' => 'date', 'dt' => 1 ),
    	array( 'db' => 'country',   'dt' => 2 ),
    	array( 'db' => 'browser',   'dt' => 3 ),
    	array( 'db' => 'platform',  'dt' => 4 ),
    	array( 'db' => 'time',   'dt' => 5)
    );
    
    
    // SQL server connection information
    $sql_details = array(
    	'user' => $dbUser,
    	'pass' => $dbPass,
    	'db'   => $dbName,
    	'host' => $dbHost
    );
    
    
    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
     * If you just want to use the basic configuration for DataTables with PHP
     * server-side, there is no need to edit below this line.
     */
        
    echo json_encode(
    	SSPFLAGDATAALL::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $columns2 )
    );
    
    die();
}

if($pointOut == 'ajax-traffic'){
    
$code_id = raino_trim($_POST['code_id']);

    $result = mysqli_query($con, "SELECT * FROM table_$code_id ORDER BY id DESC LIMIT 0 , 15");
    while ($row = mysqli_fetch_array($result))
    {
        $agent = $row['browser'];
        $exCountryCode = $row['country'];
        $agent = parse_user_agent($agent);
        extract($agent);
        $country[] = country_code_to_country($exCountryCode);
        $platform = ($platform == '' ? 'Unknown' : $platform);
        $browser = ($browser == '' ? 'Unknown' : $browser);
        $os[] = $platform;
        $browserArr[] = $browser;
        $nowtime = time();
        $oldtime = $row['now_time'];
        $visTime[] = conTime($nowtime-$oldtime);
        
    }
?>
          <table class="table table-bordered">
                  <thead>
                  <tr>
                  <th>#</th>
                  <th>Country</th>
                  <th>Browser</th>
                  <th style="width: 120px;">Platform</th>
                  <th>Time</th>
                  </tr>
                  </thead>
                  <tbody>
                        <?php
                        $loop = 1;
                        $arrLoop = 0;
                        foreach($country as $user_country)
                        {
                            $countryFile = 'flags/default/16/'.strtolower(Trim($user_country)).'.png';
                            if (!file_exists(ROOT_DIR.$countryFile))
                            $countryFile = 'icons/unknown.png';
                            $browser = $browserArr[$arrLoop];
                            if ($browser == 'Googlebot-Image')
                            $browser = 'Googlebot';
                            if ($browser == 'MSIE')
                            $browser = 'IE';
                            $browserFile = 'icons/'.strtolower(Trim($browser)).'.png';
                            if (!file_exists(ROOT_DIR.$browserFile))
                            $browserFile = 'icons/unknown.png';
                            $osFile = 'icons/'.strtolower(Trim($os[$arrLoop])).'.png';
                            if (!file_exists(ROOT_DIR.$osFile))
                            $osFile = 'icons/unknown.png';
                            echo'<tr>
                             <td style="width: 50px">'.$loop.'.</td>
                             <td><img alt="'.$user_country.'" src="'.createLink($countryFile,true,true).'">  '.$user_country.'</td>
                             <td><img alt="'.$browser.'" src="'.createLink($browserFile,true,true).'">  '.ucfirst($browser).'</td>
                             <td><img alt="'.$os[$arrLoop].'" src="'.createLink($osFile,true,true).'">  '.ucfirst($os[$arrLoop]).'</td>
                             <td>'.$visTime[$arrLoop].'</td>
                             </tr>';
                             $arrLoop++;
                             $loop++;
                        }
                        ?>
        
                                                   
            
                                    </tbody></table>
                                    
<?php 

die(); 

} 

die();
?>