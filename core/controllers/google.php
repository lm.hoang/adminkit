<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: Rainbow PHP Framework
 * @copyright © 2017 ProThemes.Biz
 *
 */
 
$pageTitle = $lang['RF85'];  //'Google Oauth';

if(isset($_SESSION[N_APP.'Username'])){
  redirectTo(createLink('',true));
  die();
}

if(!$enable_reg){
    header("Location: ". createLink('',true));
    exit();  
}

if(!$enable_oauth){
    header("Location: ". createLink('',true));
    exit();  
}

if($oauth_keys['oauth']['g_redirect_uri'] == '')
    $oauth_keys['oauth']['g_redirect_uri'] = $baseURL. '?route=google';
    
// Oauth Google 
define('G_Client_ID', $oauth_keys['oauth']['g_client_id']);  // Enter your google api application id
define('G_Client_Secret', $oauth_keys['oauth']['g_client_secret']); // Enter your google api application secret code
define('G_Redirect_Uri', $oauth_keys['oauth']['g_redirect_uri']);
define('G_Application_Name', 'Rainbow_PHP_By_Balaji');

//Google Oauth Library
require_once (LIB_DIR . 'Google/Client.php');

$client = new Google_Client();
$client->setScopes(array(
    "https://www.googleapis.com/auth/userinfo.profile",
    "https://www.googleapis.com/auth/userinfo.email"
));

if (isset($_GET['code'])) {
  $client->authenticate($_GET['code']);
  $_SESSION['access_token'] = $client->getAccessToken();
  $redirect = $baseURL . $_SERVER['PHP_SELF'];
  header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
}

if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
  $client->setAccessToken($_SESSION['access_token']);
  $access_token = json_decode($_SESSION['access_token'], 1);
  $access_token = $access_token['access_token'];
  $resp = file_get_contents('https://www.googleapis.com/oauth2/v1/userinfo?access_token='.$access_token);
  $user = json_decode($resp, 1);  
  $client_email = filter_var($user['email'], FILTER_SANITIZE_EMAIL);
  $client_name = filter_var($user['name'], FILTER_SANITIZE_STRING);
  $client_id = filter_var($user['id']);
  $client_plat = "Google";
  $client_pic = $user['picture'];
  $content = $user;
  $token = $client->getAccessToken();
} else {
  $authUrl = $client->createAuthUrl();
}
if ($client->getAccessToken() && isset($_GET['url'])) {

  $_SESSION['access_token'] = $client->getAccessToken();
}


if (isset($client_email)) {
$row = mysqliPreparedQuery($con, "SELECT * FROM users WHERE oauth_uid=?",'s',array($client_id));
if($row !== false){
  $user_username  = $row['username'];
  $db_verified  = $row['verified'];
  
  if ($db_verified == "2"){
      die($lang['RF44']);
  } else {
      //Premium Membership Settings
      if(file_exists(CON_DIR.'premium.php')){
          $subArr = subscriptionCheck($row['username'],$con);
          if($subArr[0]){
              if($subArr[1]){
                  //Premium Active User
                  $dataPlan = getPlanInfo($subArr[4],$con);
                  if($dataPlan[0]){
                      //Plan Found
                      $_SESSION[N_APP.'premiumClient'] = 1;
                      $_SESSION[N_APP.'premiumToken'] = array($subArr[2],$subArr[3],$subArr[4],$dataPlan[1],$dataPlan[2],$dataPlan[3],$dataPlan[4],$dataPlan[5],$subArr[6],$dataPlan[6]);
                  }else{
                      //Plan Not Found!
                      $_SESSION[N_APP.'premiumClient'] = 1;
                      $_SESSION[N_APP.'premiumError'] = $lang['AD735'].' "'.$subArr[5].'" '.$lang['AD736'].' <br> '.str_replace('[contact-link]','<a href="'.createLink('contact',true).'">'.$lang['AD738'].'</a>',$lang['AD737']).'<br>';
                  }
              }else{
                  //Premium Non-Active User
                  $_SESSION[N_APP.'premiumError'] = $lang['AD731'].' <br> '.$lang['AD732'].' '.$subArr[3].' <br> '.$lang['AD733'].' <a href="'.createLink('invoice/'.$subArr[4],true).'">'.$lang['AD734'].'</a>.<br>';
              }
          }
      }
      $_SESSION[N_APP.'Username'] = $user_username;
      $_SESSION[N_APP.'Token'] = $token;
      $_SESSION[N_APP.'Oauth_uid'] = $client_id;
      $_SESSION[N_APP.'UserToken'] = passwordHash($db_id . $username);
      $old_user =1;
      header("Location: ". createLink('',true));
      exit();
  }

} else {
  $new_user= 1;
  $last_id = getLastID($con, 'users');
  if ($last_id== '' || $last_id==null){
      $username = "User1";
  } else {
      $last_id = $last_id+1;  
      $username = "User$last_id";
  }
  $_SESSION[N_APP.'Username'] = $username;
  $_SESSION[N_APP.'Token'] = $token;
  $_SESSION[N_APP.'Oauth_uid'] = $client_id;
  $_SESSION[N_APP.'UserToken'] = passwordHash($db_id . $username);
  $nowDate = date('m/d/Y h:i:sA'); 
  $sDate = date('m/d/Y'); 
  $res = insertToDbPrepared($con, 'users', array(
        'oauth_uid' => $client_id, 
        'username' => $username, 
        'email_id' => $client_email, 
        'full_name' => $client_name, 
        'platform' => $client_plat, 
        'password' => $password, 
        'verified' => '1', 
        'picture' => 'NONE', 
        'date' => $sDate, 
        'added_date' => $nowDate, 
        'ip' => $ip
    ));
  header("Location: ".createLink('',true));
  exit();
}

} else {
    if($pointOut == 'login') {
        header('Location: '.$authUrl);
        exit();
    }
}
die();
?>