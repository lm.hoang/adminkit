<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright � 2018 ProThemes.Biz
 *
 */
 
$p_title = trans('My Subscription', $lang['AD746'], true);

if(!isset($_SESSION[N_APP.'premiumClient']))
    die($lang['97']);

//Get Username
$username = $_SESSION[N_APP.'Username'];
$premiumUserInfo = getPremiumUserInfo($username,$con);
$arrPDFLimit = decSerBase($premiumUserInfo['pdf_limit']);
$dbDate = $arrPDFLimit[0];
$dbCount = (int)$arrPDFLimit[1];
$todayDate = date('m/d/Y');
$totalLimit = (int)$_SESSION[N_APP.'premiumToken'][5];

//Check PDF Limit
if($totalLimit != '0'){
if($dbDate != '' || $dbCount != ''){
    if($dbDate == $todayDate){
        if($dbCount <= $totalLimit)
            $remCountMsg = ($totalLimit - $dbCount). ' '.trans('PDF Reports', $lang['AD747'], true);
        else
            $remCountMsg = '<b style="color: #d14233;">'.trans('Today Limit Reached!', $lang['AD748'], true).'</b>';
    }else{
        //Insert New Date
        newPDFLimit($con,$username);
        $remCountMsg = $totalLimit.' '.trans('PDF Reports', $lang['AD747'], true);
    }
}else{
    //No data found - Insert New Date
    newPDFLimit($con,$username);
    $remCountMsg = $totalLimit.' '.trans('PDF Reports', $lang['AD747'], true);
}
}

$subID = $_SESSION[N_APP.'premiumToken'][8];
$orderInfo = subInfo($subID,$con);
$cancelBtn = createLink($controller.'/cancel',true);

if($orderInfo['billing_type'] == '0') {
    //One Time Fee Plans
    $expDate = trans('Lifetime', $lang['AD749'], true);
    $billType = trans('One Time Fee', $lang['AD750'], true);
    $renewBtn = '-';
    $recbilling_type = false;
}else{
    //Recurring Fee Plans
    $recbilling_type = true;
    $recCon = unserialize($orderInfo['rec_data']); 
    
    if($recCon[4] == 'paid')
        $orderInfo = subInfo($recCon[1],$con);

    $subDate = $orderInfo['date'];
    
    if($orderInfo['rec_type'] == 'rec1'){
        $billType = trans('Monthly', $lang['AD751'], true);
        $expDate = date('m/d/Y h:i:sA', getUnixTimestamp($subDate,'1M')); 
    }elseif ($orderInfo['rec_type'] == 'rec2'){
        $billType = trans('Every 3 Months', $lang['AD752'], true);
        $expDate = date('m/d/Y h:i:sA', getUnixTimestamp($subDate,'3M')); 
    }elseif ($orderInfo['rec_type'] == 'rec3'){
        $billType = trans('Every 6 Months', $lang['AD753'], true);
        $expDate = date('m/d/Y h:i:sA', getUnixTimestamp($subDate,'6M')); 
    }elseif ($orderInfo['rec_type'] == 'rec4'){
        $billType = trans('Yearly', $lang['AD754'], true);
        $expDate = date('m/d/Y h:i:sA', getUnixTimestamp($subDate,'1Y')); 
    }
    
    $query = mysqli_query($con, "SELECT * FROM premium_orders WHERE username='$username' AND status='rec_pending'");
    if(mysqli_num_rows($query) > 0){
        $renewInfo = mysqli_fetch_array($query);
        $renewBtn = '<a href="'.createLink('invoice/'.$renewInfo['id'],true).'" class="btn btn-success">'.trans('Renew', $lang['AD755'], true).'</a>';
    }else{
        $renewBtn = '-';
    }
}

$planID = $_SESSION[N_APP.'premiumToken'][2];
$planName = $_SESSION[N_APP.'premiumToken'][3];
$allowPdf = filter_var($_SESSION[N_APP.'premiumToken'][4], FILTER_VALIDATE_BOOLEAN);
$pdfLimit = (int)$_SESSION[N_APP.'premiumToken'][5];
$brandPDF = filter_var($_SESSION[N_APP.'premiumToken'][6], FILTER_VALIDATE_BOOLEAN);

$premiumUserTools = unserialize($_SESSION[N_APP.'premiumToken'][7]);

if($allowPdf){
    if($pdfLimit == '0')
        $allowPdf = trans('Unlimited', $lang['AD756'], true);
    else
        $allowPdf = $pdfLimit.' '.trans('Reports/per day', $lang['AD757'], true);
        
}else{
    $allowPdf = trans('Not Allowed', $lang['AD758'], true);
}

if($brandPDF)
    $brandPDF = trans('Allowed', $lang['AD759'], true);
else
    $brandPDF = trans('Not Allowed', $lang['AD758'], true); 
    
$tools = array();
$result = mysqli_query($con,"SELECT * FROM seo_tools");
while ($row = mysqli_fetch_array($result)) {
    $toolId = $row['id'];
    
    if (in_array($toolId, $premiumUserTools))
        $tools[] = array(shortCodeFilter($row['tool_name']),createLink($row['tool_url'],true),$row['icon_name'],$row['tool_no']);
}

//Cancel Subscription
if(isset($_POST['cancel'])){
    $msg = raino_trim($_POST['can_msg']);
    if($recbilling_type){
        subCancel($username,$msg,$con);
    }else{
        $id = $_SESSION[N_APP.'premiumToken'][8];
        subCancelByID($id,$msg,$con);
    }
    session_destroy();
}
?>