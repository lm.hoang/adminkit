<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright � 2018 ProThemes.Biz
 *
 */
 
$pageTitle = $lang['AD730'];
$des = $keyword = '';

if(!isset($_SESSION[N_APP.'premiumClient']))
    die($lang['97']);

//Get Username
$username = $_SESSION[N_APP.'Username'];
$premiumUserInfo = getPremiumUserInfo($username,$con);

$tools = array();
$result = mysqli_query($con, "SELECT * FROM seo_tools ORDER BY CAST(tool_no AS UNSIGNED) ASC");

while ($row = mysqli_fetch_array($result)) {
    $toolID = $row['id'];
    $premiumUserTools = unserialize($_SESSION[N_APP.'premiumToken'][7]);

    if (in_array($toolID, $premiumUserTools))          
        $tools[] = array(shortCodeFilter($row['tool_name']),createLink($row['tool_url'],true),$row['icon_name'],$row['tool_show'],$row['tool_no']);
}

?>