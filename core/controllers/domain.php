<?php

defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
define('TEMP_DIR',APP_DIR.'temp'.D_S);
    
/*
 * @author Balaji
 * @name: Rainbow PHP Framework
 * @copyright 2018 ProThemes.Biz
 *
 */

//POST REQUEST Handler
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if(isset($_POST['url'])){
        $myUrl = parse_url('http://'.clean_url(raino_trim($_POST['url'])));
        $myUrlHost = strtolower(str_replace("www.","",$myUrl['host']));
        redirectTo(createLink($controller.'/'.$myUrlHost,true));
    }else{
        die($lang['327']);
    }
}

//Default Value
$domainFound = false;
$updateFound = false;
$isOnline = '0';
$username = $_SESSION[N_APP.'Username'];
$nowDate = date('m/d/Y h:i:sA');
$disDate = date('F, j Y h:i:s A');

//True (or) False Image
$true = '<img src="'.themeLink('img/true.png',true).'" alt="'.$lang['AN24'].'" />';
$false = '<img src="'.themeLink('img/false.png',true).'" alt="'.$lang['AN23'].'" />';

//Check User Request
if ($pointOut == '')   
    die($lang['327']);
    
if(isset($route[2])){
   $updateCheck = strtolower(raino_trim($route[2]));
   if($updateCheck == 'update')
       $updateFound = true;
}

//Get User Request
$my_url = raino_trim($pointOut);
$my_url = 'http://'.clean_url($my_url);

//Parse Host
$my_url_parse = parse_url($my_url);
$inputHost = $my_url_parse['scheme'] . "://" . $my_url_parse['host'];
$my_url_host = str_replace("www.","",$my_url_parse['host']);
$my_url_path = $my_url_parse['path'];
$my_url_query = $my_url_parse['query']; 
$domainStr = strtolower($my_url_host);
$pageTitle = ucfirst($my_url_host);

//Check Valid Host
if($my_url_host == '')
    die($lang['327']);

//Premium Access Check
if(!isset($_SESSION[N_APP.'premiumClient'])){
    $orderData = orderSettings($con);
    $orderData['reviewer_list'] = unserialize($orderData['reviewer_list']);
    $freeLimit = (int)$orderData['free_limit'];
    $pdfUrl = $updateUrl = createLink('premium',true);
}else{
    $updateUrl = createLink($controller.'/'.$domainStr.'/'.'update',true);
    $pdfUrl = createLink('genpdf/'.$domainStr,true);
    addUserRecentSites($domainStr,$username,$con);
}

//Check Domain Name Exists
$query = mysqli_query($con, "SELECT * FROM domains_data WHERE domain='$domainStr'");
if(mysqli_num_rows($query) > 0){
    $data = mysqli_fetch_array($query);
        if($data['completed'] == 'yes'){
            $domainFound = true;
        }else{
            $updateFound = true;
            $domainFound = false;
        }
}else{
    $updateFound = true;
    $domainFound = false;
    
    //Create the Domain
    $query = "INSERT INTO domains_data (domain,date) VALUES ('$domainStr', '$nowDate')";
    if (!mysqli_query($con, $query))
        $error = trans('Database Error - Contact Support!', $lang['AD773'], true);
}

if($updateFound){
    $queryx = "UPDATE domains_data SET date='$nowDate'";
    if (!mysqli_query($con, $queryx)) 
        $error = trans('Database Error - Contact Support!', $lang['AD773'], true);
}

//Hash Code
$hashCode = md5($my_url_host);
$filename = TEMP_DIR.$hashCode.'.tdata';

//Get Data of the URL
if($updateFound){
    $sourceData = curlGET($my_url);
    
    if($sourceData == ''){
        //Second try with Curl
        $sourceData = getMyData($my_url);
        
        if($sourceData == '')
            $error  = $lang['327']; //'Input Site is not valid!';
    }
    
    if(!isset($error)){
        $isOnline = '1';
        putMyData($filename,$sourceData);
    }
}

if(!isset($error)){
    if($updateFound){
        //New or Update the data
        
        //Free Users
        if(!isset($_SESSION[N_APP.'premiumClient'])){
            if(isset($_SESSION[N_APP.'FREE_LIMIT'])){
                $limitUsed = (int)$_SESSION[N_APP.'FREE_LIMIT'];
                if($limitUsed == $freeLimit){
                    redirectTo(createLink('premium',true));
                }else{
                    $limitUsed++;
                    $_SESSION[N_APP.'FREE_LIMIT'] = $limitUsed;
                }
            }else{
                $_SESSION[N_APP.'FREE_LIMIT'] = 1;
            }
        }
    }else{
        //Extract DB Data
        define('DB_DOMAIN',true);
        require(CON_DIR.'db-domain.php');
    }
}else{
    die($error);
}
?>