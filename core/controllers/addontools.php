<?php

defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

if(!defined('SEO_ADDON_TOOLS')){
    $controller = "error";
    require(CON_DIR. $controller . '.php');
}

/*
 * @author Balaji
 * @name: A to Z SEO Tools - PHP Script
 * @copyright � 2017 ProThemes.Biz
 *
 */



//PR56 - Website Reviewer
elseif($toolUid == 'PR56') {
    
   $controller = 'reviewer';

    if ($pointOut == 'output') {
        if (!isset($_POST['url']))
            die($lang['4']); //Malformed Request!
        if (!isset($error)) {
            $my_url = raino_trim($_POST['url']);
            $my_url = 'http://'.clean_url($my_url);
            $my_url_parse = parse_url($my_url);
            $my_url_host = str_replace("www.","",$my_url_parse['host']);
            header('Location: '.$toolURL.'/'.$my_url_host);
            die();
        }
    }
    
    if ($pointOut != '') {            
        define('TEMP_DIR',APP_DIR.'temp'.D_S);
        $isOnline = '0';
        $my_url = raino_trim($pointOut);
        $pointOut = 'output';
        $my_url = 'http://'.clean_url($my_url);
        //Parse Host
        $my_url_parse = parse_url($my_url);
        $inputHost = $my_url_parse['scheme'] . "://" . $my_url_parse['host'];
        $my_url_host = str_replace("www.","",$my_url_parse['host']);
        $my_url_path = $my_url_parse['path'];
        $my_url_query = $my_url_parse['query']; 
        
        $hashCode = md5($my_url_host);
        $filename = TEMP_DIR.$hashCode.'.tdata';
        
        //Get Data of the URL
        $sourceData = curlGET($my_url);
        
        if($sourceData == ""){
           
            //Second try
            $sourceData = getMyData($my_url);
            
            if($sourceData == "")
                $error  = 'Input Site is not valid!';
        }
        
        if(!isset($error)){
            $isOnline = '1';
            putMyData($filename,$sourceData);
        }
    }
    
}


//PR51 - Get HTTP Headers
if($toolUid == 'PR51') {
        
    $controller = 'output'.D_S.'get_http_headers';
    
    if ($pointOut == 'output') {
        if (!isset($_POST['url']))
        die($lang['4']); //Malformed Request!
        if (!isset($error)) {
            $my_url = raino_trim($_POST['url']);
            $my_url = "http://".clean_with_www($my_url);
            if (filter_var($my_url, FILTER_VALIDATE_URL) === false) {
            $error = $lang['327'];
            }else {
            $regUserInput = $my_url;
            $outData = getHeaders($my_url);
            if($outData=="")
            $error = $lang['327'];
            }
        }
    }
}


//GS01 - Grammar Checker
if($toolUid == 'GS01') {
        
    $controller = 'output'.D_S.'grammar_checker';
    
    if(!nullCheck($pointOut))
        redirectTo($toolURL);
}


//PR57 - Bulk Domain Availability Checker
elseif($toolUid == 'PR57') {
        
        
    $controller = 'output'.D_S.'domain_availability_checker';
    
    if ($pointOut == 'whois') {
        $route = escapeTrim($con,$_GET['route']); 
        $route = explode('/',$route);
        if(!isset($route[2]) || Trim($route[2]) == '' || Trim($route[3]) == '' || !isset($_SESSION['whoToken']))
            die($lang['4']);
        $url = raino_trim($route[2]);
        $whoToken = raino_trim($route[3]);
        
        if($_SESSION['whoToken'] != $whoToken)
            die($lang['4']);
        
        $url = parse_url(Trim('http://'.clean_with_www($url))); 
        $host =  str_replace('www.','',$url['host']);
        $myHost = ucfirst($host);
        $whois= new whois;
        $site = $whois->cleanUrl($host);
        $whois_data = $whois->whoislookup($site);
        $whoisData = $whois_data[0];
    }
    
    if ($pointOut == 'output') {
        if (!isset($_POST['data']))
        die($lang['4']); //Malformed Request!
        if (!isset($error)) {
        $userInput = raino_trim($_POST['data']);
        $regUserInput = truncate($userInput,30,150);
        $array = explode("\n", $userInput);
        $count = 0;
        $statusColor = $whoisLink = $domainAvailabilityStats = $statusMsg = '';
        $statusColors = $whoisLinks = array();
        
        //Server List Path
        $path = LIB_DIR.'domainAvailabilityservers.tdata';
            
        if (file_exists($path)) {
            $contents = file_get_contents($path);
            $serverList = json_decode($contents, true);
        }
        
        $whoToken = randomPassword();
        $_SESSION['whoToken'] = $whoToken;
           
        foreach ($array as $url) {
            $url = clean_with_www($url); $url = Trim("http://$url");
            if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
            if($count == 20)
                break;
            $count++;
            $my_url[] = Trim($url);
            $url = parse_url(Trim($url));
            $host =  str_replace('www.','',$url['host']);
            $myHost[] = ucfirst($host);
                       
            //Get the status of domain name
            $domainAvailabilityChecker = new domainAvailability($serverList);
            $domainAvailabilityStats = $domainAvailabilityChecker->isAvailable($host);
            
            //Response Code - Reason
            //2 - Domain is already taken!
            //3 - Domain is available
            //4 - No WHOIS entry was found for that TLD
            //5 - WHOIS Query failed

            if($domainAvailabilityStats == 2){
                $statusMsg = $lang['AD69'];
                $statusColor = '#e74c3c';
                $whoisLink = '<a href="'.$toolURL.'/whois/'.$host.'/'.$whoToken.'" target="_blank" rel="nofollow" title="'.$lang['AD70'].'">'.$lang['AD70'].'</a>';
            } elseif($domainAvailabilityStats == 3){
                $statusMsg = $lang['AD66'];
                $statusColor = '#27ae60';
                $whoisLink = '-';
            } elseif($domainAvailabilityStats == 4){ 
                $statusMsg = $lang['AD67'];
                $statusColor = '#ec5e00';
                $whoisLink = '-';
            } else {
                $statusMsg = $lang['AD68'];
                $statusColor = '#ec5e00';
                $whoisLink = '-';
            }
            $whoisLinks[] = $whoisLink;
            $statusColors[] = $statusColor;
            $stats[] = $statusMsg;
            }
        }
        }
    }
}


//IDS29 - Flag Counter
if($toolUid == 'IDS29') {
        
    $controller = 'output'.D_S.'flag-counter';
    
    define('FLAG_COUNTER','1');
    require CON_DIR.'flag-counter.php';
}


//PR53 - Social Stats Checker
if($toolUid == 'PR53') {
        
    $controller = 'output'.D_S.'social_stats_checker';
    
    if ($pointOut == 'output') {
        if (!isset($_POST['url']))
        die($lang['4']); //Malformed Request!
        if (!isset($error)) {
            $my_url = raino_trim($_POST['url']);
            $my_url = "http://".clean_with_www($my_url);
            if (filter_var($my_url, FILTER_VALIDATE_URL) === false) {
            $error = $lang['327'];
            }else {
                $regUserInput = $my_url;
                $my_url = parse_url($my_url);
                $host = $my_url['host'];
                $myHost = ucfirst($host);
                
                $sourceData = curlGET('http://'.$host);
                $socialData = getSocialData($sourceData);
                
            }
        }
    }
}


//PR52 - Mobile Friendly Test
if($toolUid == 'PR52') {
        
    $controller = 'output'.D_S.'mobile_friendly_test';
    
    if(!nullCheck($pointOut))
        redirectTo($toolURL);
}


//PR54 - Check GZIP compression
if($toolUid == 'PR54') {
        
    $controller = 'output'.D_S.'check_gzip_compression';

    if ($pointOut == 'output') {
        if (!isset($_POST['url']))
        die($lang['4']); //Malformed Request!
        if (!isset($error)) {
            $my_url = raino_trim($_POST['url']);
            $my_url = "http://".clean_with_www($my_url);
            if (filter_var($my_url, FILTER_VALIDATE_URL) === false) {
            $error = $lang['327'];
            }else {
            $regUserInput = $my_url;
            $my_host = parse_url($my_url);
            $my_host = $my_host['host'];
            $myHost = ucfirst(clean_url($my_host));
            $outData = compressionTest($my_host);
            $comSize = $outData[0];
            $unComSize = $outData[1];
            $isGzip = $outData[2];
            $gzdataSize = $outData[3];
            $header = $outData[4];
            $body = Trim($outData[5]);
            if($body == ""){
                $error = $lang['97'];
            }else{
            if($isGzip){
                $percentage = round(((((int)$unComSize - (int)$comSize) / (int)$unComSize) * 100),1);
            }else{
                $percentage = round(((((int)$unComSize - (int)$gzdataSize) / (int)$unComSize) * 100),1);
            }
            }
            }
        }
    }
}
