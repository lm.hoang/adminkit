<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
define('PAYMODE','1');

/*
 * @author Balaji
 * @name: Rainbow PHP Framework
 * @copyright © 2018 ProThemes.Biz
 *
 */

$actionStop = false;
$nowDate = date('m/d/Y h:i:sA'); 
$username = $_SESSION[N_APP.'Username'];
$paymentGateway = strtolower($route[1]);
$paymentAction = strtolower($route[2]);
$paymentData = '';
extract(orderSettings($con));

if ($paymentGateway == null && $paymentAction == null)
     $actionStop = true;

if(isset($route[3]))
    $eToken = $route[3];
    
if($paymentAction == 'process'){
    //Check eToken
    if($_SESSION[N_APP.'eToken'] == $eToken){
        //Login Check - Check user logged stats
        if(isset($_SESSION[N_APP.'UserToken'])){
            //User Logged
            
            //Renew Token
            $eToken = randomPassword();
            $_SESSION[N_APP.'eToken'] = $eToken;
            
            //Check sLevel    
            if($_SESSION[N_APP.'sLevel'] == '3'){
                $orderID = $_SESSION[N_APP.'orderID'];
                $query = mysqli_query($con, "SELECT * FROM premium_orders WHERE id='".$orderID."'");
                $orderInfo = mysqli_fetch_array($query);
                extract($orderInfo);
                extract(getPremiumUserInfo($username,$con));
                $userInfo = getUserInfo($username,$con);
                $payerEmail = Trim($userInfo[3]);
                $amount_tax = removeFormatting($amount_tax);
            }else{
                $actionStop = true;
            }
        }else{
            $actionStop = true;
        }
    }else{ 
        $actionStop = true;
    }
}

if($actionStop)
    die($lang['97']);


#---Load Payment Gateway---

$paymentModule = PLG_DIR.'payments'.D_S.$paymentGateway.'.php';

if(file_exists($paymentModule))
    require($paymentModule);
else
    die($lang['AD760']); //Selected payment module not found!


die();