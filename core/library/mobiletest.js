
var myURL,authCode;

function startTask(auth){
    authCode = auth;
    jQuery("#mainbox").fadeOut();
    jQuery("#resultBox").css({"display":"block"});
    jQuery("#resultBox").show();
    jQuery("#resultBox").fadeIn();
    jQuery(".percentimg").css({"display":"block"});
    jQuery(".percentimg").show();
    jQuery(".percentimg").fadeIn();
    jQuery.post(axPath,{getMobileFriendly:'1', url:myURL, authcode:authCode},function(outData){
    jQuery("#results").html(outData);
    $('.knob').knob();
    jQuery(".percentimg").fadeOut();
    jQuery(".percentimg").css({"display":"none"});
    jQuery(".percentimg").hide();
    }); 
}

jQuery(document).ready(function(){
    jQuery("#checkButton").click(function(){
        myURL = $.trim($("#myurl").val());
    	if (myURL.indexOf("https://") == 0){myURL=myURL.substring(8);}
        if (myURL.indexOf("http://") == 0){myURL=myURL.substring(7);}
    	if (myURL.indexOf("/") != -1){var xGH=myURL.indexOf("/");myURL=myURL.substring(0,xGH);}
    	if (myURL.indexOf(".") == -1 ){myURL+=".com";}
    	if (myURL.indexOf(".") == (myURL.length-1)){myURL+="com";}
        var regular = /^([www\.]*)+(([a-zA-Z0-9_\-\.])+\.)+([a-zA-Z0-9]{2,4})+$/;
    	if(!regular.test(myURL)){
    	    sweetAlert(oopsStr, msgDomain , "error");
    		return;
    	}else{
            validateCaptcha();
        }
    });
});

/*
 * @author Balaji
 * @name: A to Z SEO Tools v2 - PHP Script
 * @copyright � 2018 ProThemes.Biz
 *
 */