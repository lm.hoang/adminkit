<?php

/*
 * @author Balaji
 * @name: Rainbow PHP Framework
 * @copyright 2018 ProThemes.Biz
 *
 */
 
define('CRON','_1');
define('ROOT_DIR', realpath(dirname(dirname(__FILE__))) .DIRECTORY_SEPARATOR);
define('APP_DIR', ROOT_DIR .'core'.DIRECTORY_SEPARATOR);
define('CONFIG_DIR', APP_DIR .'config'.DIRECTORY_SEPARATOR);

//Load required functions
require CONFIG_DIR.'config.php';
require APP_DIR.'functions.php';

//Database Connection
$con = dbConncet($dbHost,$dbUser,$dbPass,$dbName);

//Get site Info
$siteInfo =  mysqli_query($con, "SELECT * FROM site_info where id='1'");
$row = mysqli_fetch_array($siteInfo);
$site_name =   Trim($row['site_name']);
$admin_mail =   Trim($row['email']);
$admin_name = $site_name;
$copyright =  htmlspecialchars_decode(Trim($row['copyright']));

//Load Mail Settings
extract(loadMailSettings($con));
$mail_type = $protocol;

//Invoice not paid more than a day
$query =  "SELECT * FROM premium_orders WHERE payment_status='pending'";
$result = mysqli_query($con,$query);
        
while($row = mysqli_fetch_array($result)) {
    
    if(getUnixTimestamp($row['date'],'1D') < getUnixTimestamp()) {
       cronSubCancel($row['id'],'Auto Cancellation by CronJob',$con); 
       break;
    }
}

//Generate new invoice for recurring payments
$query =  "SELECT * FROM premium_orders WHERE status='completed'";
$result = mysqli_query($con,$query);
        
while($row = mysqli_fetch_array($result)) {
    if($row['billing_type'] == '1'){
        $orderRecCon = unserialize($row['rec_data']);
        $subDate = $row['date']; 
        if($orderRecCon[1] == '0'){
            $recType = '1M';
            if($row['rec_type'] == 'rec1'){
                //Monthly
                $expDate = date('m/d/Y h:i:sA', getUnixTimestamp($subDate,'1M')); 
                $recType = '1M';
            }elseif ($row['rec_type'] == 'rec2'){
                //3 Months
                $expDate = date('m/d/Y h:i:sA', getUnixTimestamp($subDate,'3M')); 
                $recType = '3M';
            }elseif ($row['rec_type'] == 'rec3'){
                //6 Months
                $expDate = date('m/d/Y h:i:sA', getUnixTimestamp($subDate,'6M')); 
                $recType = '6M';
            }elseif ($row['rec_type'] == 'rec4'){
                //1 Year
                $expDate = date('m/d/Y h:i:sA', getUnixTimestamp($subDate,'1Y')); 
                $recType = '1Y';
            }
            
            if(getUnixTimestamp($expDate,'-5D') < getUnixTimestamp()) {
                //New Invoice
                $invoicePrefix = getInvoicePrefix($con);
                $recCon = serialize(array('0','0','0','auto','0'));   
                                     
                $query = "INSERT INTO premium_orders (plan_id, user_id, premium_user_id, invoice_prefix, billing_type, payment_type, status, amount, date, ip_address, rec_type, username, amount_tax, currency_type, plan_name, payment_status, tax_data, rec_data) VALUES ('".$row['plan_id']."', '".$row['user_id']."', '".$row['premium_user_id']."', '".$invoicePrefix."', '".$row['billing_type']."', '".$row['payment_type']."', 'rec_pending', '".$row['amount']."', '".$expDate."', '".$row['ip_address']."', '".$row['rec_type']."', '".$row['username']."', '".$row['amount_tax']."', '".$row['currency_type']."', '".$row['plan_name']."', 'pending', '".$row['tax_data']."', '".$recCon."')";
                if (mysqli_query($con,$query)){
                    $orderID = mysqli_insert_id($con);
                    if(recSubReg($row['id'],'1',$orderID,$invoicePrefix,$orderRecCon[3],$con)){
                        //First Reminder  

                        $result = mysqli_query($con,"SELECT * FROM premium_orders WHERE id='$orderID'");
                        $row = mysqli_fetch_array($result);

                        //Customer Invoice Mail Code
                        $query =  "SELECT * FROM premium_mails WHERE code='reminder'";
                        $result = mysqli_query($con,$query);
                        $arrData = mysqli_fetch_array($result);
                        $arrData['subject'] = base64_decode($arrData['subject']);
                        $arrData['body'] = base64_decode($arrData['body']);
    
                        $userInfo = getUserInfo($row['username'],$con);
                        $premiumInfo = getPremiumUserInfo($row['username'],$con);
                        $sent_mail =  $userInfo['email_id'];
                        $currencyType =  $row['currency_type'];
                        
                        $date_raw = date_create(Trim($row['date']));
                        $subDate = date_format($date_raw,"jS F Y");
                        
                        if($row['billing_type'] == '1'){
                            $recCon = unserialize($row['rec_data']);
                            if($recCon[3] == 'auto'){
                                $invoiceDate = $dueDate = $subDate;
                                $invoiceDate = date_create($invoiceDate);
                                date_sub($invoiceDate, date_interval_create_from_date_string('5 days'));
                                $invoiceDate = date_format($invoiceDate, 'jS F Y');
                            }else{
                                $invoiceDate = $dueDate = $subDate;
                            }
                        }else{
                            $invoiceDate = $dueDate = $subDate;
                        }
                        $gatewayData = getPaymentGateway($con,$row['payment_type']);
                        
                        $replacementCode = array(
                        '{FirstName}' => $premiumInfo['firstname'],
                        '{InvoiceNo}' => $row['invoice_prefix'],
                        '{InvoiceDate}' => $invoiceDate,
                        '{DueDate}' => $dueDate,
                        '{GatewayName}' => $gatewayData['gateway_title'],
                        '{AmountWithTax}' => $row['amount_tax'].' '. $currencyType,
                        '{SiteName}' => $site_name,
                        '{InvoiceLink}' => createLink('invoice/'.$row['id'].'/view',true)
                        );
  
                        $subject = html_entity_decode(stripslashes(str_replace(array_keys($replacementCode),array_values($replacementCode),$arrData['subject'])));
                        $body = html_entity_decode(html_entity_decode(stripslashes(str_replace(array_keys($replacementCode),array_values($replacementCode),$arrData['body']))));
          
                        if ($mail_type == '1')
                            default_mail ($admin_mail,$admin_name,$sent_mail,$subject,$body);
                        else
                            smtp_mail($smtp_host,$smtp_port,$smtp_auth,$smtp_username,$smtp_password,$smtp_socket,$admin_mail,$admin_name,$sent_mail,$subject,$body);
                            
                        break;
                    }else{
                        //Something Went Wrong!
                        break;
                    }
                }else{
                    //Something Went Wrong!
                    break;
                }
            }
        }else{
            //Already Invoice Generated
            
            //Check Invoice Paid
            if($orderRecCon[4] != 'paid'){
                //Invoice Not Paid 
                
                //Calculate Expire Date
                if($row['rec_type'] == 'rec1'){
                    //Monthly
                    $expDate = date('m/d/Y h:i:sA', getUnixTimestamp($subDate,'1M')); 
                }elseif ($row['rec_type'] == 'rec2'){
                    //3 Months
                    $expDate = date('m/d/Y h:i:sA', getUnixTimestamp($subDate,'3M')); 
                }elseif ($row['rec_type'] == 'rec3'){
                    //6 Months
                    $expDate = date('m/d/Y h:i:sA', getUnixTimestamp($subDate,'6M')); 
                }elseif ($row['rec_type'] == 'rec4'){
                    //1 Year
                    $expDate = date('m/d/Y h:i:sA', getUnixTimestamp($subDate,'1Y')); 
                }
               
               //Check Notification Level      
               if($orderRecCon[0] != '2'){
                   //Second Reminder                  
                   if(getUnixTimestamp($expDate,'-2D') < getUnixTimestamp()) {
                    
                    //Update Notification Level
                    recSubReg2($row['id'],$con);

                    $result = mysqli_query($con,"SELECT * FROM premium_orders WHERE id='".$orderRecCon[1]."'");
                    $row = mysqli_fetch_array($result);

                    //Invoice Payment Reminder Mail Code
                    $query =  "SELECT * FROM premium_mails WHERE code='reminder'";
                    $result = mysqli_query($con,$query);
                    $arrData = mysqli_fetch_array($result);
                    $arrData['subject'] = base64_decode($arrData['subject']);
                    $arrData['body'] = base64_decode($arrData['body']);

                    $userInfo = getUserInfo($row['username'],$con);
                    $premiumInfo = getPremiumUserInfo($row['username'],$con);
                    $sent_mail =  $userInfo['email_id'];
                    $currencyType =  $row['currency_type'];
                    $date_raw = date_create(Trim($row['date']));
                    $subDate = date_format($date_raw,"jS F Y");
                    
                    if($row['billing_type'] == '1'){
                        $recCon = unserialize($row['rec_data']);
                        if($recCon[3] == 'auto'){
                            $invoiceDate = $dueDate = $subDate;
                            $invoiceDate = date_create($invoiceDate);
                            date_sub($invoiceDate, date_interval_create_from_date_string('5 days'));
                            $invoiceDate = date_format($invoiceDate, 'jS F Y');
                        }else{
                            $invoiceDate = $dueDate = $subDate;
                        }
                    }else{
                        $invoiceDate = $dueDate = $subDate;
                    }
                    $gatewayData = getPaymentGateway($con,$row['payment_type']);
                    
                    $replacementCode = array(
                    '{FirstName}' => $premiumInfo['firstname'],
                    '{InvoiceNo}' => $row['invoice_prefix'],
                    '{InvoiceDate}' => $invoiceDate,
                    '{DueDate}' => $dueDate,
                    '{GatewayName}' => $gatewayData['gateway_title'],
                    '{AmountWithTax}' => $row['amount_tax'].' '. $currencyType,
                    '{SiteName}' => $site_name,
                    '{InvoiceLink}' => createLink('invoice/'.$row['id'].'/view',true)
                    );

                    $subject = html_entity_decode(stripslashes(str_replace(array_keys($replacementCode),array_values($replacementCode),$arrData['subject'].'- 2')));
                    $body = html_entity_decode(html_entity_decode(stripslashes(str_replace(array_keys($replacementCode),array_values($replacementCode),$arrData['body']))));

                    if ($mail_type == '1')
                        default_mail ($admin_mail,$admin_name,$sent_mail,$subject,$body);
                    else
                        smtp_mail($smtp_host,$smtp_port,$smtp_auth,$smtp_username,$smtp_password,$smtp_socket,$admin_mail,$admin_name,$sent_mail,$subject,$body);
                        
                    break;
                    
                   }
               }elseif($orderRecCon[0] == '2'){
                //Still invoice not "Paid" after Second Payment Reminder!
                
                //Plan Expired more than 10 days
                if(getUnixTimestamp($expDate,'10D') < getUnixTimestamp()) {
                  if(recSubCompleted($row['id'],$con)){
                      //Update the status to cancel
                      cronSubCancel(Trim($orderRecCon[1]),'Auto Cancellation by CronJob',$con); 
                      break;
                  }
                }
               }                
            }
        }
        
    }
}
?>