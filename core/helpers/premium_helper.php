<?php

/*
* @author Balaji
* @name A to Z SEO Tools - PHP Script
* @copyright 2020 ProThemes.Biz
*
*/

if(isset($_GET['logout'])){
    unset($_SESSION[N_APP.'premiumClient']);
    unset($_SESSION[N_APP.'premiumToken']);
    unset($_SESSION[N_APP.'premiumError']);
    unset($_SESSION[N_APP.'Token']);
    unset($_SESSION[N_APP.'Oauth_uid']);
    unset($_SESSION[N_APP.'Username']);
    unset($_SESSION[N_APP.'Pic']);
    unset($_SESSION[N_APP.'UserToken']);
    header('Location: '.$baseURL);
    exit();
}

function errOrder(){
    echo '0:::Error';
    die();
}

function getTaxData($userIP,$con){
    $taxArr = array();
    $query = mysqli_query($con, "SELECT * FROM order_settings WHERE id='1'");
    $data = mysqli_fetch_array($query);
    if(filter_var($data['enable_tax'], FILTER_VALIDATE_BOOLEAN)){
        //User Country Tax
        require_once (LIB_DIR . 'geoip.inc');
        $gi = geoip_open(LIB_DIR.'GeoIP.dat', GEOIP_MEMORY_CACHE);
        $userCountryCode = geoip_country_code_by_addr($gi,$userIP);
        geoip_close($gi);

        if($userCountryCode != ''){
            $query = "SELECT * FROM premium_tax WHERE country='$userCountryCode'";
            $result = mysqli_query($con, $query);
            while ($row = mysqli_fetch_array($result))
            {
                $taxTitle = Trim($row['tax_title']);
                $taxRate = Trim($row['tax_rate']);
                $taxID = Trim($row['id']);
                $taxStatus = filter_var(Trim($row['status']), FILTER_VALIDATE_BOOLEAN);
                $isFixed = filter_var(Trim($row['rate_fixed']), FILTER_VALIDATE_BOOLEAN);
                if($taxStatus)
                    $taxArr[] = array($isFixed,$taxTitle,$taxRate);
            }
        }

        //All Country Tax
        $query = "SELECT * FROM premium_tax WHERE country='all'";
        $result = mysqli_query($con, $query);
        while ($row = mysqli_fetch_array($result))
        {
            $taxTitle = Trim($row['tax_title']);
            $taxRate = Trim($row['tax_rate']);
            $taxID = Trim($row['id']);
            $taxStatus = filter_var(Trim($row['status']), FILTER_VALIDATE_BOOLEAN);
            $isFixed = filter_var(Trim($row['rate_fixed']), FILTER_VALIDATE_BOOLEAN);
            if($taxStatus)
                $taxArr[] = array($isFixed,$taxTitle,$taxRate);
        }

    }
    return $taxArr;
}

function removeFormatting($amount) {
    return preg_replace("/([^0-9\\.])/i", "", $amount);
}

function orderSettings($con){
    $query = mysqli_query($con, "SELECT * FROM order_settings WHERE id='1'");
    return mysqli_fetch_array($query);
}

function defaultCurrency($con){
    $query = mysqli_query($con, "SELECT * FROM order_settings WHERE id='1'");
    $cur = mysqli_fetch_array($query);
    $currencyType = strtoupper($cur["currency_type"]);
    return $currencyType;
}

function getPaymentGateways($con){
    $paymentGateways = array();
    $query =  "SELECT * FROM payment_gateways";
    $result = mysqli_query($con,$query);

    while($row = mysqli_fetch_array($result)) {
        extract($row);
        $paymentGateways[] = array($sort_order,$gateway_title,$id,$gateway_name,$supported_currency,$active);
    }
    return $paymentGateways;
}

if(!function_exists('unqFile')){
    function unqFile($path,$filename){
        if (file_exists($path.$filename)) {
            $filename = rand(1, 99999999) . "_" . $filename;
            return unqFile($path,$filename);
        }else{
            return $filename;
        }
    }
}

if(!function_exists('strEOL')){
    function strEOL($str){
        return str_replace(array('\r\n','\n','\r'),PHP_EOL,$str);
    }
}

function text2array($str) {
    $keys = array();
    $values = array();
    $output = array();

    if( substr($str, 0, 5) == 'Array' ) {

        $array_contents = substr($str, 7, -2);
        $array_contents = str_replace(array('[', ']', '=>'), array('#!#', '#?#', ''), $array_contents);
        $array_fields = explode("#!#", $array_contents);

        for($i = 0; $i < count($array_fields); $i++ ) {

            if( $i != 0 ) {

                $bits = explode('#?#', $array_fields[$i]);
                if( $bits[0] != '' ) $output[$bits[0]] = $bits[1];

            }
        }

        return $output;

    } else {

        return false;

    }
}

function getPaymentGateway($con,$id=null,$gatewayName=null){
    if($id === null)
        $query = mysqli_query($con, "SELECT * FROM payment_gateways WHERE gateway_name='$gatewayName'");
    else
        $query = mysqli_query($con, "SELECT * FROM payment_gateways WHERE id='$id'");

    return mysqli_fetch_array($query);
}

function getInvoicePrefix($con){
    $invoice_prefix = '';
    $query = mysqli_query($con, "SELECT * FROM premium_orders order by id desc limit 1");
    if (mysqli_num_rows($query) > 0){
        $data = mysqli_fetch_array($query);
        $invoice_prefix = $data['invoice_prefix'];
        $invoice_prefix = ++$invoice_prefix;
    }else{
        $orderSettings = orderSettings($con);
        $invoice_prefix = $orderSettings['invoice_prefix'];
        $invoice_prefix = ++$invoice_prefix;
    }
    return $invoice_prefix;
}

function invoiceConfirmationMail($id,$con){
    //Get site Info
    $query =  "SELECT * FROM site_info";
    $result = mysqli_query($con,$query);

    while($row = mysqli_fetch_array($result)) {
        $site_name =   Trim($row['site_name']);
        $admin_mail =   Trim($row['email']);
        $admin_name = $site_name;
        $copyright =  htmlspecialchars_decode(Trim($row['copyright']));
    }

    //SMTP information 
    $query =  "SELECT * FROM mail WHERE id='1'";
    $result = mysqli_query($con,$query);

    while($row = mysqli_fetch_array($result)) {
        $smtp_host =   Trim($row['smtp_host']);
        $smtp_user =  Trim($row['smtp_username']);
        $smtp_pass =  Trim($row['smtp_password']);
        $smtp_port =  Trim($row['smtp_port']);
        $protocol =  Trim($row['protocol']);
        $smtp_auth =  Trim($row['auth']);
        $smtp_sec =  Trim($row['socket']);
    }

    $query =  "SELECT * FROM premium_mails WHERE code='invoice'";
    $result = mysqli_query($con,$query);
    $arrData = mysqli_fetch_array($result);
    $arrData['subject'] = base64_decode($arrData['subject']);
    $arrData['body'] = base64_decode($arrData['body']);

    $mail_type = $protocol;

    $orderInfo = getOrderInfo($id,$con);

    $userInfo = getUserInfo($orderInfo['username'],$con);
    $premiumInfo = getPremiumUserInfo($orderInfo['username'],$con);
    $sent_mail =  $userInfo['email_id'];
    $currencyType =  $orderInfo['currency_type'];

    $replacementCode = array(
        '{OrderId}' => '#'.$id,
        '{FirstName}' => $premiumInfo['firstname'],
        '{InvoiceNo}' => $orderInfo['invoice_prefix'],
        '{InvoiceDate}' => $orderInfo['date'],
        '{AmountWithTax}' => $orderInfo['amount_tax'].' '.$currencyType,
        '{PaymentStats}' => ucfirst($orderInfo['payment_status']),
        '{TransactionId}' => $orderInfo['transaction_id'],
        '{SiteName}' => $site_name,
        '{InvoiceLink}' => createLink('invoice/'.$id.'/view',true)
    );

    $subject = html_entity_decode(stripslashes(str_replace(array_keys($replacementCode),array_values($replacementCode),$arrData['subject'])));
    $body = html_entity_decode(html_entity_decode(stripslashes(str_replace(array_keys($replacementCode),array_values($replacementCode),$arrData['body']))));

    if ($mail_type == '1')
        default_mail ($admin_mail,$admin_name,$admin_mail,$admin_name,$sent_mail,$subject,$body);
    else
        smtp_mail($smtp_host,$smtp_port,$smtp_auth,$smtp_user,$smtp_pass,$smtp_sec,$admin_mail,$admin_name,$admin_mail,$admin_name,$sent_mail,$subject,$body);
    return true;
}

function calNewCurrencyAmount($currencyCode,$amount,$con){
    $currencyCode = Trim(strtoupper($currencyCode));
    $query = mysqli_query($con, "SELECT * FROM premium_currency WHERE currency_code='$currencyCode'");
    if(mysqli_num_rows($query) > 0){
        $arrData = mysqli_fetch_array($query);
        $currencyName = $arrData["currency_name"];
        $currencyRate = $arrData["currency_rate"];
        $amount = $amount * $currencyRate;
        return $amount;
    }else{
        return $amount;
    }
}

function orderConfirmationMail($id,$con){
    //Get site Info
    $query =  "SELECT * FROM site_info";
    $result = mysqli_query($con,$query);

    while($row = mysqli_fetch_array($result)) {
        $site_name =   Trim($row['site_name']);
        $admin_mail =   Trim($row['email']);
        $admin_name = $site_name;
        $copyright =  htmlspecialchars_decode(Trim($row['copyright']));
    }

    //SMTP information
    $query =  "SELECT * FROM mail WHERE id='1'";
    $result = mysqli_query($con,$query);

    while($row = mysqli_fetch_array($result)) {
        $smtp_host =   Trim($row['smtp_host']);
        $smtp_user =  Trim($row['smtp_username']);
        $smtp_pass =  Trim($row['smtp_password']);
        $smtp_port =  Trim($row['smtp_port']);
        $protocol =  Trim($row['protocol']);
        $smtp_auth =  Trim($row['auth']);
        $smtp_sec =  Trim($row['socket']);
    }

    $query =  "SELECT * FROM premium_mails WHERE code='confirmation'";
    $result = mysqli_query($con,$query);
    $arrData = mysqli_fetch_array($result);
    $arrData['subject'] = base64_decode($arrData['subject']);
    $arrData['body'] = base64_decode($arrData['body']);

    $mail_type = $protocol;

    $orderInfo = getOrderInfo($id,$con);

    $userInfo = getUserInfo($orderInfo['username'],$con);
    $premiumInfo = getPremiumUserInfo($orderInfo['username'],$con);
    $sent_mail =  $userInfo['email_id'];
    $currencyType =  $orderInfo['currency_type'];

    $replacementCode = array(
        '{OrderId}' => '#'.$id,
        '{FirstName}' => $premiumInfo['firstname'],
        '{InvoiceNo}' => $orderInfo['invoice_prefix'],
        '{AmountWithTax}' => $orderInfo['amount_tax'].' '.$currencyType,
        '{PlanName}' => $orderInfo['plan_name'],
        '{TransactionId}' => $orderInfo['transaction_id'],
        '{SiteName}' => $site_name,
        '{InvoiceLink}' => createLink('invoice/'.$id.'/view',true)
    );

    $subject = html_entity_decode(stripslashes(str_replace(array_keys($replacementCode),array_values($replacementCode),$arrData['subject'])));
    $body = html_entity_decode(html_entity_decode(stripslashes(str_replace(array_keys($replacementCode),array_values($replacementCode),$arrData['body']))));
    if ($mail_type == '1')
        default_mail ($admin_mail,$admin_name,$admin_mail,$admin_name,$sent_mail,$subject,$body);
    else
        smtp_mail($smtp_host,$smtp_port,$smtp_auth,$smtp_user,$smtp_pass,$smtp_sec,$admin_mail,$admin_name,$admin_mail,$admin_name,$sent_mail,$subject,$body);
    return true;
}

function getRecType($val){
    $rVal = 0;
    if($val == 'rec1')
        $rVal = 0;
    elseif($val == 'rec2')
        $rVal = 1;
    elseif($val == 'rec3')
        $rVal = 2;
    elseif($val == 'rec4')
        $rVal = 3;
    return $rVal;
}

function getPremiumUserInfo($username,$con){

    $query = mysqli_query($con, "SELECT * FROM premium_users WHERE username='$username'");
    if(mysqli_num_rows($query) > 0){
        return mysqli_fetch_array($query);
    }
    return false;
}

function getPlanList($con){
    $planList = array();
    $result = mysqli_query($con, "SELECT * FROM premium_plans");
    while ($row = mysqli_fetch_array($result)){
        $planList[] = array($row['id'], $row['plan_name']);
    }
    return $planList;
}

function getPlanInfo($plan_id,$con){
    $plan_id = Trim($plan_id);
    $query = mysqli_query($con, "SELECT * FROM premium_plans WHERE id='$plan_id'");
    if(mysqli_num_rows($query) > 0){
        $planInfo = mysqli_fetch_array($query);
        $captcha = filter_var(Trim($planInfo['captcha']), FILTER_VALIDATE_BOOLEAN);
        return array(true,$planInfo['plan_name'],$planInfo['allow_pdf'],$planInfo['projects'],$planInfo['brand_pdf'],$planInfo['premium_tools'],$captcha);
    }else{
        return array(false);
    }
}

function getComPlanInfo($plan_id,$con){
    $query = mysqli_query($con, "SELECT * FROM premium_plans WHERE id='$plan_id'");
    return mysqli_fetch_array($query);
}

function getOrderInfo($order_id,$con){
    $order_id = Trim($order_id);
    $query = mysqli_query($con, "SELECT * FROM premium_orders WHERE id='$order_id'");
    if(mysqli_num_rows($query) > 0){
        return mysqli_fetch_array($query);
    }else{
        return false;
    }
}

function subscriptionCheck($username,$con){

    $query = mysqli_query($con, "SELECT * FROM premium_orders WHERE username='$username' AND status='completed' ORDER BY id DESC");
    if(mysqli_num_rows($query) > 0){
        $orderInfo = mysqli_fetch_array($query);
        $subDate = $orderInfo['date'];
        if($orderInfo['billing_type'] == '0') {
            //One Time Fee Plans
            $expDate = 'Lifetime';
        }else{
            //Recurring Fee Plans
            $recType = '1M';
            $orderRecCon = unserialize($orderInfo['rec_data']);
            if($orderInfo['rec_type'] == 'rec1'){
                //Monthly
                $expDate = date('m/d/Y h:i:sA', getUnixTimestamp($subDate,'1M'));
                $recType = '1M';
            }elseif ($orderInfo['rec_type'] == 'rec2'){
                //3 Months
                $expDate = date('m/d/Y h:i:sA', getUnixTimestamp($subDate,'3M'));
                $recType = '3M';
            }elseif ($orderInfo['rec_type'] == 'rec3'){
                //6 Months
                $expDate = date('m/d/Y h:i:sA', getUnixTimestamp($subDate,'6M'));
                $recType = '6M';
            }elseif ($orderInfo['rec_type'] == 'rec4'){
                //1 Year
                $expDate = date('m/d/Y h:i:sA', getUnixTimestamp($subDate,'1Y'));
                $recType = '1Y';
            }

            if(getUnixTimestamp($subDate,$recType) < getUnixTimestamp()) {
                //Plan Expired

                //Check New Invoice Status
                $recCon = unserialize($orderInfo['rec_data']);

                if($recCon[4] == 'paid'){
                    //Recurring Invoice Paid
                    if(recSubCompleted($orderInfo['id'],$con)){
                        //Update the Status
                        subscriptionCheck($username,$con);
                    }else{
                        //Update Failed
                        die($lang['97']);
                    }
                }else{
                    //Recurring Invoice Not Paid
                    return array(true,false,$subDate,$expDate,$recCon[1]);
                }
            }
        }

        //Everything Fine 
        return array(true,true,$subDate,$expDate,$orderInfo['plan_id'],$orderInfo['plan_name'],$orderInfo['id']);
    }
    return array(false,false);
}

function recSubPaid($id,$tryLevel,$orderID,$invoicePrefix,$type,$con){

    $recCon = serialize(array($tryLevel,$orderID,$invoicePrefix,$type,'paid'));

    $query = "UPDATE premium_orders SET rec_data='$recCon' WHERE id='$id'";
    if (!mysqli_query($con,$query))
        return false;
    else
        return true;
}

function recSubPaidbyID($id,$con){

    $query = mysqli_query($con, "SELECT * FROM premium_orders WHERE id='$id'");
    if(mysqli_num_rows($query) > 0){
        $orderInfo = mysqli_fetch_array($query);
        $recCon = unserialize($orderInfo['rec_data']);

        $recCon = serialize(array($recCon[0],$recCon[1],$recCon[2],$recCon[3],'paid'));

        $query = "UPDATE premium_orders SET rec_data='$recCon' WHERE id='$id'";
        if (!mysqli_query($con,$query))
            return false;
        else
            return true;
    }else{
        return false;
    }
}

function recSubReg($id,$tryLevel,$orderID,$invoicePrefix,$type,$con){

    $recCon = serialize(array($tryLevel,$orderID,$invoicePrefix,$type,'0'));

    $query = "UPDATE premium_orders SET rec_data='$recCon' WHERE id='$id'";
    if (!mysqli_query($con,$query))
        return false;
    else
        return true;
}

function recSubReg2($id,$con){

    $query = mysqli_query($con, "SELECT * FROM premium_orders WHERE id='$id'");
    if(mysqli_num_rows($query) > 0){
        $orderInfo = mysqli_fetch_array($query);
        $recCon = unserialize($orderInfo['rec_data']);

        $recCon = serialize(array('2',$recCon[1],$recCon[2],$recCon[3],$recCon[4]));

        $query = "UPDATE premium_orders SET rec_data='$recCon' WHERE id='$id'";
        if (!mysqli_query($con,$query))
            return false;
        else
            return true;
    }else{
        return false;
    }
}

function recSubCompleted($id,$con){

    $query = "UPDATE premium_orders SET status='rec_completed' WHERE id='$id'";
    if (!mysqli_query($con,$query))
        return false;
    else
        return true;
}

function subInfo($id,$con){
    $query = mysqli_query($con, "SELECT * FROM premium_orders WHERE id='$id'");

    if(mysqli_num_rows($query) > 0)
        return mysqli_fetch_array($query);
    else
        return false;
}

function getOrderComments($id,$con){
    $query = mysqli_query($con, "SELECT * FROM premium_orders WHERE id='$id'");
    if(mysqli_num_rows($query) > 0){
        $orderInfo = mysqli_fetch_array($query);
        return unserialize(base64_decode($orderInfo['order_comments']));
    }
    return false;
}

function getOrderLog($id,$con){
    $query = mysqli_query($con, "SELECT * FROM premium_orders WHERE id='$id'");
    if(mysqli_num_rows($query) > 0){
        $orderInfo = mysqli_fetch_array($query);
        return unserialize(base64_decode($orderInfo['order_log']));
    }
    return false;
}

function addtoOrderLog($id,$msg=null,$con){
    $msg = date('m/d/Y h:i:sA').'|:| '.$msg;
    $query = mysqli_query($con, "SELECT * FROM premium_orders WHERE id='$id'");
    if(mysqli_num_rows($query) > 0){
        $orderInfo = mysqli_fetch_array($query);
        $order_log =  unserialize(base64_decode($orderInfo['order_log']));
        array_push($order_log,$msg);
        $order_log = base64_encode(serialize($order_log));
        $query = "UPDATE premium_orders SET order_log='$order_log' WHERE id='$id'";
        mysqli_query($con,$query);
    }
    return false;
}

function addtoOrderComment($id,$msg=null,$con){
    $msg = date('m/d/Y h:i:sA').'|:| '.$msg;
    $query = mysqli_query($con, "SELECT * FROM premium_orders WHERE id='$id'");
    if(mysqli_num_rows($query) > 0){
        $orderInfo = mysqli_fetch_array($query);
        $order_comments =  unserialize(base64_decode($orderInfo['order_comments']));
        array_push($order_comments,$msg);
        $order_comments = base64_encode(serialize($order_comments));
        $query = "UPDATE premium_orders SET order_comments='$order_comments' WHERE id='$id'";
        mysqli_query($con,$query);
        return true;
    }
    return false;
}

function checkAndCancelRecSub($username,$msg=null,$con){
    $msg = date('m/d/Y h:i:sA').'|:| '. 'Subscription Cancellation Message: '.$msg;
    $query = mysqli_query($con, "SELECT * FROM premium_orders WHERE username='$username' AND status='rec_pending'");
    if(mysqli_num_rows($query) > 0){
        $orderInfo = mysqli_fetch_array($query);
        $id = $orderInfo['id'];
        $order_comments =  unserialize(base64_decode($orderInfo['order_comments']));
        array_push($order_comments,$msg);
        $order_comments = base64_encode(serialize($order_comments));
        $query = "UPDATE premium_orders SET status='canceled', order_comments='$order_comments' WHERE id='$id'";
        mysqli_query($con,$query);
    }
}

function subCancel($username,$msg=null,$con){
    $msg = date('m/d/Y h:i:sA').'|:| '. 'Subscription Cancellation Message: '.$msg;
    $query = mysqli_query($con, "SELECT * FROM premium_orders WHERE username='$username' AND status='rec_pending'");
    if(mysqli_num_rows($query) > 0){
        $orderInfo = mysqli_fetch_array($query);
        $id = $orderInfo['id'];
        $order_comments =  unserialize(base64_decode($orderInfo['order_comments']));
        array_push($order_comments,$msg);
        $order_comments = base64_encode(serialize($order_comments));
        $query = "UPDATE premium_orders SET status='canceled', order_comments='$order_comments' WHERE id='$id'";
        mysqli_query($con,$query);
    }

    $query =  "SELECT * FROM premium_orders WHERE username='$username' AND status='completed'";
    $result = mysqli_query($con,$query);

    while($row = mysqli_fetch_array($result)) {
        $id =  $row['id'];
        $order_comments =  unserialize(base64_decode($row['order_comments']));
        array_push($order_comments,$msg);
        $order_comments = base64_encode(serialize($order_comments));
        $query = "UPDATE premium_orders SET status='canceled', order_comments='$order_comments' WHERE id='$id'";
        mysqli_query($con,$query);
    }
    return true;
}

function subCancelByID($id,$msg=null,$con){

    $msg = date('m/d/Y h:i:sA').'|:| '. 'Subscription Cancellation Message: '.$msg;
    $query = mysqli_query($con, "SELECT * FROM premium_orders WHERE id='$id'");
    if(mysqli_num_rows($query) > 0){
        $orderInfo = mysqli_fetch_array($query);
        $id = $orderInfo['id'];
        $order_comments =  unserialize(base64_decode($orderInfo['order_comments']));
        array_push($order_comments,$msg);
        $order_comments = base64_encode(serialize($order_comments));
        $query = "UPDATE premium_orders SET status='canceled', order_comments='$order_comments' WHERE id='$id'";
        mysqli_query($con,$query);
    }
    return true;
}

function cronSubCancel($id,$msg=null,$con){

    $msg = date('m/d/Y h:i:sA').'|:| '. 'Subscription Cancellation Message: '.$msg;
    $query = mysqli_query($con, "SELECT * FROM premium_orders WHERE id='$id'");
    if(mysqli_num_rows($query) > 0){
        $orderInfo = mysqli_fetch_array($query);
        $id = $orderInfo['id'];
        $order_comments =  unserialize(base64_decode($orderInfo['order_comments']));
        array_push($order_comments,$msg);
        $order_comments = base64_encode(serialize($order_comments));
        $query = "UPDATE premium_orders SET status='canceled', payment_status='canceled', order_comments='$order_comments' WHERE id='$id'";
        mysqli_query($con,$query);
    }
    return true;
}

function getUnixTimestamp($date=null,$expire=null){

    if($date === null)
        $date = date('m/d/Y h:i:sA');

    $date_raw = date_create(Trim($date));

    $year = date_format($date_raw,"Y");
    $month = date_format($date_raw,"n");
    $day = date_format($date_raw,"j");

    $hour = date_format($date_raw,"H");
    $min = date_format($date_raw,"i");
    $sec = date_format($date_raw,"s");

    if($expire === null)
        return mktime($hour,$min,$sec,$month,$day,$year);
    elseif($expire == '1M')
        return mktime($hour,$min,$sec,$month+"1",$day,$year);
    elseif($expire == '3M')
        return mktime($hour,$min,$sec,$month+"3",$day,$year);
    elseif($expire == '6M')
        return mktime($hour,$min,$sec,$month+"6",$day,$year);
    elseif($expire == '1Y')
        return mktime($hour,$min,$sec,$month,$day,$year+"1");
    elseif($expire == '1D')
        return mktime($hour,$min,$sec,$month,$day+"1",$year);
    elseif($expire == '-2D')
        return mktime($hour,$min,$sec,$month,$day-"2",$year);
    elseif($expire == '5D')
        return mktime($hour,$min,$sec,$month,$day+"5",$year);
    elseif($expire == '-5D')
        return mktime($hour,$min,$sec,$month,$day-"5",$year);
    elseif($expire == '10D')
        return mktime($hour,$min,$sec,$month,$day+"10",$year);

    return false;
}

function getPremiumUserID($username,$con){

    $query = mysqli_query($con, "SELECT * FROM premium_users WHERE username='$username'");
    if(mysqli_num_rows($query) > 0){
        //Username found
        $data = mysqli_fetch_array($query);
        $userID = Trim($data['id']);
        return $userID;
    }else{
        return false;
    }
    return false;
}

function add2Sitemap($con,$path){

    $query = mysqli_query($con,"select * from sitemap_options WHERE id='1'");
    $data = mysqli_fetch_array($query);
    $sitemapDate = date('Y-m-d');
    $sitemapPath = ROOT_DIR.'sitemap.xml';

    if(file_exists($sitemapPath)){

        $doc = new DomDocument();
        $doc->load($sitemapPath);

        $node = $doc->getElementsByTagName('urlset')->item(0);

        //if($nodelist->length === 0){
        //  $nodelist = $doc->createElement("urlset");
        //  $doc->appendChild($nodelist); 
        //}

        //Insert New URL
        $element = $doc->createElement("url");
        $urlNode = $node->appendChild($element);

        $loc = $urlNode->appendChild($doc->createElement("loc"));
        $loc->appendChild($doc->createTextNode($path));

        $priority = $urlNode->appendChild($doc->createElement("priority"));
        $priority->appendChild($doc->createTextNode($data['priority']));

        $changefreq = $urlNode->appendChild($doc->createElement("changefreq"));
        $changefreq->appendChild($doc->createTextNode($data['changefreq']));

        $lastmod = $urlNode->appendChild($doc->createElement("lastmod"));
        $lastmod->appendChild($doc->createTextNode($sitemapDate));

        $doc->formatOutput = true;
        $doc->save($sitemapPath);

        return true;
    }else{
        return false;
    }
}

function getUserRecentSites($con){
    $domains = array();
    $username = $_SESSION[N_APP.'Username'];
    $userData = getPremiumUserInfo($username,$con);
    $domains = decSerBase($userData['userdata']);
    return $domains;
}

function addUserRecentSites($domain,$username,$con){
    $domains = array();
    $userData = getPremiumUserInfo($username,$con);
    $domains = decSerBase($userData['userdata']);
    array_unshift($domains,$domain);
    $domains = array_splice(array_unique($domains), 0, 10);
    $domainStr = serBase($domains);
    $query = "UPDATE premium_users SET userdata='$domainStr' WHERE username='$username'";
    mysqli_query($con,$query);
    return true;
}

function isAllowedStats($con,$reviewer){
    $orderData = orderSettings($con);
    $orderData['reviewer_list'] = unserialize($orderData['reviewer_list']);

    if (in_array($reviewer, $orderData['reviewer_list']))
        return false;
    else
        return true;
}

function newPDFLimit($con,$username){
    $todayDate = date('m/d/Y');
    $data = serBase(array($todayDate,1));
    $query = "UPDATE premium_users SET pdf_limit='$data' WHERE username='$username'";
    if (!mysqli_query($con, $query))
        return false;
    else
        return true;
}

function getReviewerList(){

    $arr = array(
        'seoBox1' => 'Meta Data Information',
        'seoBox4' => 'Headings',
        'seoBox5' => 'Google Preview',
        'seoBox6' => 'Alt Attribute',
        'seoBox7' => 'Keywords Cloud',
        'seoBox8' => 'Keyword Consistency',
        'seoBox9' => 'Text/HTML Ratio',
        'seoBox10' => 'GZIP Compression',
        'seoBox11' => 'WWW Resolve',
        'seoBox12' => 'IP Canonicalization',
        'seoBox13' => 'In-Page Links',
        'seoBox14' => 'Broken Links',
        'seoBox15' => 'XML Sitemap',
        'seoBox16' => 'Robots.txt',
        'seoBox17' => 'URL Rewrite',
        'seoBox18' => 'Underscores in the URLs',
        'seoBox19' => 'Embedded Objects',
        'seoBox20' => 'Iframe',
        'seoBox21' => 'Domain Registration',
        'seoBox22' => 'WHOIS Data',
        'seoBox23' => 'Mobile Friendliness',
        'seoBox24' => 'Mobile View',
        'seoBox25' => 'Mobile Compatibility',
        'seoBox28' => 'Custom 404 Page',
        'seoBox29' => 'Page Size',
        'seoBox30' => 'Load Time',
        'seoBox31' => 'Language',
        'seoBox32' => 'Domain Availability',
        'seoBox33' => 'Typo Availability',
        'seoBox34' => 'Email Privacy',
        'seoBox35' => 'Safe Browsing',
        'seoBox36' => 'Server IP',
        'seoBox37' => 'Speed Tips',
        'seoBox38' => 'Analytics',
        'seoBox39' => 'W3C Validity',
        'seoBox40' => 'Doc Type',
        'seoBox41' => 'Encoding',
        'seoBox42' => 'Indexed Pages',
        'seoBox43' => 'Backlinks Counter',
        'seoBox44' => 'Social Data',
        'seoBox45' => 'Estimated Worth',
        'seoBox46' => 'Traffic Rank',
        'seoBox47' => 'Visitors Localization'
    );
    return $arr;
}

function colorCode($status){
    $status = strtolower($status);
    if($status == 'completed')
        return 'success';
    elseif($status == 'rec_completed')
        return 'success';
    elseif($status == 'pending')
        return 'warning';
    elseif($status == 'rec_pending')
        return 'info';
    elseif($status == 'canceled')
        return 'default';
    elseif($status == 'refunded')
        return 'info';
    elseif($status == 'fraud')
        return 'danger';
    elseif($status == 'chargeback')
        return 'danger';
    elseif($status == 'reversed')
        return 'danger';
    else
        return 'default';
}


function colorCodeText($status){
    if($status == 'completed')
        return '#00A65A';
    elseif($status == 'pending')
        return '#F39C12';
    elseif($status == 'canceled')
        return '#ADADAD';
    elseif($status == 'refunded')
        return '#8C63D0';
    elseif($status == 'fraud')
        return '#DD6566';
    elseif($status == 'chargeback')
        return '#DD6566';
    elseif($status == 'reversed')
        return '#DD6566';
    else
        return '#ADADAD';
}

//-------------------------PDF-------------------------

function imgTag($path,$echo=true,$alt=null){
    if(is_null($alt)){
        if($echo)
            echo '<img src="'.$path.'" />';
        else
            return '<img src="'.$path.'" />';
    }else{
        if($echo)
            echo '<img alt="'.$alt.'" src="'.$path.'" />';
        else
            return '<img alt="'.$alt.' src="'.$path.'" />';
    }
}

function pdfOutBox($title,array $starData, $type, $mainData, $sugClass, array $sugData, $sugMsg,$exData=null){
    $exCode = '';
    if($type == 1)
        $star = $starData[0];
    elseif($type == 2)
        $star = $starData[1];
    elseif($type == 3)
        $star = $starData[2];
    else
        $star = $starData[3];

    if($sugClass == 'passedBox')
        $sugType =  $sugData[0];
    elseif($sugClass == 'improveBox')
        $sugType =  $sugData[1];
    elseif($sugClass == 'errorBox')
        $sugType =  $sugData[2];
    else
        $sugType =  $sugData[3];


    if(!is_null($exData)){
        $exCode = $exData;
    }

    $outData = '<br /><br /><bookmark title="'.$title.'" level="2" ></bookmark>   
    <table style="width: 100%;" class="icons">
	   <tr class="tableBody">
		<td style="width: 4.5%;"> '.$sugType.' </td>
		<td class="sideHead" style="width: 20%;">
            '.$title.' <br />
            '.$star.'
        </td>
		<td style="width: 75.5%;">'.$mainData.'</td>      
 	  </tr>                       
     </table>
      '.$exCode.'
    <div class="suggestionBox '.$sugClass.'">
            '.$sugMsg.'
    </div>';
    echo $outData;

}

function pdfHeadBox($leftTitle,$rightTitle){

    $outData = '<page_header>
        <bookmark title="'.$leftTitle.'" level="1" ></bookmark>
        <table class="page_header">
            <tr>
                <td style="width: 50%; text-align: left"> '.$leftTitle.' </td>
                <td style="width: 47%; text-align: right"> '.$rightTitle.' </td>
            </tr>
        </table>
    </page_header>';
    return $outData;
}