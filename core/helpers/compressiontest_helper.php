<?php

/*
 * @author Balaji
 * @name A to Z SEO Tools - PHP Script
 * @copyright © 2018 ProThemes.Biz
 *
 */

function compressionTest($site) {
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    curl_setopt($ch, CURLOPT_URL, $site);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
   	curl_setopt($ch, CURLOPT_ENCODING , "gzip");
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:52.0) Gecko/20100101 Firefox/52.0');
    $response=curl_exec($ch);
    $comSize = curl_getinfo($ch, CURLINFO_SIZE_DOWNLOAD);
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $header = substr($response, 0, $header_size);
    $body = substr($response, $header_size);
    $unComSize = strlen($body);
    $gzdataSize = strlen(gzencode($body, 9));
    curl_close($ch);
    
    if(str_contains($header,"gzip"))
        $isGzip = true;
    else
        $isGzip = false;
        
    return array($comSize,$unComSize,$isGzip,$gzdataSize,$header,$body);
}

?>