<?php

/*
* @author Balaji
* @name Rainbow PHP Framework
* @copyright � 2018 ProThemes.Biz
*
*/

if(!function_exists('strEOL')){
    function strEOL($str){
        return str_replace(array('\r\n','\n','\r'),PHP_EOL,$str);
    }
}

?>