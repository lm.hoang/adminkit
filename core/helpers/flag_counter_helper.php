<?php

/*
* @author Balaji
* @name: Flag Counter on A to Z SEO Tools
* @copyright � 2018 ProThemes.Biz
*
*/

if(!function_exists('conTime')){
    function conTime($secs) {
        $bit = array(
            ' year' => $secs / 31556926 % 12,
            ' week' => $secs / 604800 % 52,
            ' day' => $secs / 86400 % 7,
            ' hour' => $secs / 3600 % 24,
            ' min' => $secs / 60 % 60,
            ' sec' => $secs % 60);
    
        foreach ($bit as $k => $v)
        {
            if ($v > 1)
                $ret[] = $v . $k . 's';
            if ($v == 1)
                $ret[] = $v . $k;
        }
        array_splice($ret, count($ret) - 1, 0, 'and');
        $ret[] = 'ago';
    
        $val = join(' ', $ret);
        if (str_contains($val, "week"))
        {
        } else
        {
            $val = str_replace("and", "", $val);
        }
        if (Trim($val) == "ago")
        {
            $val = "1 sec ago";
        }
        return $val;
    }
}

function decode_URL($src){
    $src = (string )$src;
    $srcAlphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $dstAlphabet = '0123456789';
    $srcBase = strlen($srcAlphabet);
    $dstBase = strlen($dstAlphabet);

    $wet = $src;
    $val = 0;
    $mlt = 1;

    while ($l = strlen($wet))
    {
        $digit = $wet[$l - 1];
        $val += $mlt * strpos($srcAlphabet, $digit);
        $wet = substr($wet, 0, $l - 1);
        $mlt *= $srcBase;
    }

    $wet = $val;
    $dst = '';

    while ($wet >= $dstBase)
    {
        $digitVal = $wet % $dstBase;
        $digit = $dstAlphabet[$digitVal];
        $dst = $digit . $dst;
        $wet /= $dstBase;
    }

    $digit = $dstAlphabet[$wet];
    $dst = $digit . $dst;
    $dst = (int)$dst - 500;
    return $dst;
}

function convert_URL($src){
    $src = (int)$src + 500;
    $src = (string )$src;
    $srcAlphabet = '0123456789';
    $dstAlphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $srcBase = strlen($srcAlphabet);
    $dstBase = strlen($dstAlphabet);

    $wet = $src;
    $val = 0;
    $mlt = 1;

    while ($l = strlen($wet))
    {
        $digit = $wet[$l - 1];
        $val += $mlt * strpos($srcAlphabet, $digit);
        $wet = substr($wet, 0, $l - 1);
        $mlt *= $srcBase;
    }

    $wet = $val;
    $dst = '';

    while ($wet >= $dstBase)
    {
        $digitVal = $wet % $dstBase;
        $digit = $dstAlphabet[$digitVal];
        $dst = $digit . $dst;
        $wet /= $dstBase;
    }

    $digit = $dstAlphabet[$wet];
    $dst = $digit . $dst;

    return $dst;
}

function hex2rgbFlag($hex){
    $hex = str_replace("#", "", $hex);
    if (strlen($hex) == 3)
    {
        $r = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
        $g = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
        $b = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
    } else
    {
        $r = hexdec(substr($hex, 0, 2));
        $g = hexdec(substr($hex, 2, 2));
        $b = hexdec(substr($hex, 4, 2));
    }
    $rgb ="$r,$g,$b";
    return $rgb;
}

if(!function_exists('rgb2hex')){
    function rgb2hex($rgb){
        $hex = "#";
        $hex .= str_pad(dechex($rgb[0]), 2, "0", STR_PAD_LEFT);
        $hex .= str_pad(dechex($rgb[1]), 2, "0", STR_PAD_LEFT);
        $hex .= str_pad(dechex($rgb[2]), 2, "0", STR_PAD_LEFT);
        return $hex;
    }
}

if(!function_exists('hextorgb')){
    function hextorgb($im, $color){
        $hex = str_replace("#", "", $hex);
        return imagecolorallocate($im, hexdec('0x' . $color{0} . $color{1}), hexdec('0x' .
            $color{2} . $color{3}), hexdec('0x' . $color{4} . $color{5}));
    
    
    }
}

if(!function_exists('drawBorder')){
    function drawBorder(&$img, &$color, $thickness = 1){
        $x1 = 0;
        $y1 = 0;
        $x2 = ImageSX($img) - 1;
        $y2 = ImageSY($img) - 1;
    
        for ($i = 0; $i < $thickness; $i++)
        {
            ImageRectangle($img, $x1++, $y1++, $x2--, $y2--, $color);
        }
    }
}

if(!function_exists('get_last_id')){
    function get_last_id($con){
        $query = "SELECT @last_id := MAX(id) FROM user_data";
        $result = mysqli_query($con, $query);
        while ($row = mysqli_fetch_array($result))
        {
            $last_id = $row['@last_id := MAX(id)'];
            return $last_id;
        }
    }
}

?>