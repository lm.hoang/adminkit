<?php

/*
* @author Balaji
* @name Rainbow PHP Framework v1.3
* @copyright © 2018 ProThemes.Biz
*
*/

function getCurrencyListv2(){
    $currencyData =array(
    "USD" => array("USA Dollars","$"),
    "BGN" => array("Bulgarian lev","Лв."),
    "CZK" => array("Czech koruna","Kč"),
    "HUF" => array("Hungarian forint","Ft"),
    "PLN" => array("Polish zloty","zł"),
    "RON" => array("Romanian leu","RON"),
    "CHF" => array("Swiss franc","Fr."),
    "ISK" => array("Icelandic krona","Íkr"),
    "NOK" => array("Norwegian krone","kr"),
    "HRK" => array("Croatian kuna","kn"),
    "HKD" => array("Hong Kong dollar","HK$"),
    "IDR" => array("Indonesian rupiah","Rp"),
    "MYR" => array("Malaysian ringgit","RM"),
    "AUD" => array("Australia Dollars","$"),
    "BRL" => array("Brazil Reais","R$"),
    "GBP" => array("Britain (United Kingdom) Pounds","£"),
    "CAD" => array("Canada Dollars","C$"),
    "CNY" => array("China Yuan Renminbi","¥"),
    "DKK" => array("Denmark Kroner","kr"),
    "EUR" => array("Euro","€"),
    "INR" => array("Indian Rupees","₹"),
    "ILS" => array("Israel New Shekels","₪"),
    "JPY" => array("Japan Yen","¥"),
    "KRW" => array("Korea (South) Won","₩"),
    "MXN" => array("Mexico Pesos","$"),
    "NPR" => array("Nepal Rupees","₨"),
    "NZD" => array("New Zealand Dollars","$"),
    "PHP" => array("Philippines Pesos","₱"),
    "QAR" => array("Qatar Rials","﷼"),
    "RUB" => array("Russia Rubles","руб"),
    "SAR" => array("Saudi Arabia Riyals","﷼"),
    "SGD" => array("Singapore Dollars","$"),
    "ZAR" => array("South Africa Rand","R"),
    "LKR" => array("Sri Lanka Rupees","₨"),
    "SEK" => array("Sweden Kronor","kr"),
    "SYP" => array("Syria Pounds","£"),
    "TWD" => array("Taiwan New Dollars","NT$"),
    "THB" => array("Thailand Baht","฿"),
    "TRY" => array("Turkey New Lira",html_entity_decode('&#8378;' , ENT_COMPAT, 'UTF-8'))
    );
    return $currencyData;
}

function getCurrencyList(){
    $currencyData =array(
    "USD" => array("USA Dollars","$"),
	"ARS" => array("Argentina Pesos","$"),
	"AWG" => array("Aruba Guilders (also called Florins)","ƒ"),
	"AUD" => array("Australia Dollars","$"),
	"BSD" => array("Bahamas Dollars","$"),
	"BBD" => array("Barbados Dollars","$"),
	"BEF" => array("Belgium Francs","₣"),
	"BZD" => array("Belize Dollars","$"),
	"BMD" => array("Bermuda Dollars","$"),
	"BOB" => array("Bolivia Bolivianos","$"),
	"BRL" => array("Brazil Reais","R$"),
	"BRC" => array("Brazil Cruzeiros","₢"),
	"GBP" => array("Britain (United Kingdom) Pounds","£"),
	"BND" => array("Brunei Darussalam Dollars","$"),
	"KHR" => array("Cambodia Riels","៛"),
	"CAD" => array("Canada Dollars","C$"),
	"KYD" => array("Cayman Islands Dollars","$"),
	"CLP" => array("Chile Pesos","$"),
	"CNY" => array("China Yuan Renminbi","¥"),
	"COP" => array("Colombia Pesos","$"),
	"CRC" => array("Costa Rica Colón","₡"),
	"CUP" => array("Cuba Pesos","₱"),
	"CYP" => array("Cyprus Pounds","£C"),
	"DKK" => array("Denmark Kroner","kr"),
	"DOP" => array("Dominican Republic Pesos","RD$"),
	"XCD" => array("East Caribbean Dollars","$"),
	"EGP" => array("Egypt Pounds","£"),
	"SVC" => array("El Salvador Colón","$"),
	"EUR" => array("Euro","€"),
	"XEU" => array("European Currency Unit","₠"),
	"FKP" => array("Falkland Islands Pounds","FK£"),
	"FJD" => array("Fiji Dollars","FJ$"),
	"FRF" => array("France Francs","₣"),
	"GIP" => array("Gibraltar Pounds","£"),
	"GRD" => array("Greece Drachmae","₯"),
	"GGP" => array("Guernsey Pounds","£"),
	"GYD" => array("Guyana Dollars","$"),
	"INR" => array("Indian Rupees","₹"),
	"IRR" => array("Iran Rials","﷼"),
	"IEP" => array("Ireland Punt","IR£"),
	"IMP" => array("Isle of Man Pounds","£"),
	"ILS" => array("Israel New Shekels","₪"),
	"ITL" => array("Italy Lire","₤"),
	"JMD" => array("Jamaica Dollars","JA$"),
	"JPY" => array("Japan Yen","¥"),
	"JEP" => array("Jersey Pounds","£"),
	"KPW" => array("Korea (North) Won","₩"),
	"KRW" => array("Korea (South) Won","₩"),
	"LAK" => array("Laos Kips","₭"),
	"LBP" => array("Lebanon Pounds","£"),
	"LRD" => array("Liberia Dollars","$"),
	"LUF" => array("Luxembourg Francs","₣"),
	"MTL" => array("Malta Liri","₤"),
	"MUR" => array("Mauritius Rupees","₨"),
	"MXN" => array("Mexico Pesos","$"),
	"MNT" => array("Mongolia Tugriks","₮"),
	"NAD" => array("Namibia Dollars","$"),
	"NPR" => array("Nepal Rupees","₨"),
	"ANG" => array("Netherlands Antilles Guilders (also called Florins)","ƒ"),
	"NLG" => array("Netherlands Guilders","ƒ"),
	"NZD" => array("New Zealand Dollars","$"),
	"NGN" => array("Nigeria Nairas","₦"),
	"OMR" => array("Oman Rials","﷼"),
	"PKR" => array("Pakistan Rupees","₨"),
	"PEN" => array("Peru Nuevos Soles","S/."),
	"PHP" => array("Philippines Pesos","₱"),
	"QAR" => array("Qatar Rials","﷼"),
	"RUB" => array("Russia Rubles","руб"),
	"SHP" => array("Saint Helena Pounds","£"),
	"SAR" => array("Saudi Arabia Riyals","﷼"),
	"SCR" => array("Seychelles Rupees","₨"),
	"SGD" => array("Singapore Dollars","$"),
	"SBD" => array("Solomon Islands Dollars","$"),
	"ZAR" => array("South Africa Rand","R"),
	"KRW" => array("South Korea Won","₩"),
	"ESP" => array("Spain Pesetas","Pts"),
	"LKR" => array("Sri Lanka Rupees","₨"),
	"SEK" => array("Sweden Kronor","kr"),
	"SRD" => array("Suriname Dollars","$"),
	"SYP" => array("Syria Pounds","£"),
	"TWD" => array("Taiwan New Dollars","NT$"),
	"THB" => array("Thailand Baht","฿"),
	"TTD" => array("Trinidad and Tobago Dollars","TT$"),
	"TRY" => array("Turkey New Lira",html_entity_decode('&#8378;' , ENT_COMPAT, 'UTF-8')),
	"TRL" => array("Turkey Liras",html_entity_decode('&#8378;' , ENT_COMPAT, 'UTF-8')),
	"TVD" => array("Tuvalu Dollars","$")
    );
    return $currencyData;
}

function getCurrencySymbol($currencyCode='USD',$htmlentities=false){
    $currencyCode = Trim(strtoupper($currencyCode));
    $currencyData =array(
    "USD" => array("America (United States of America) Dollars","$"),
	"ARS" => array("Argentina Pesos","$"),
	"AWG" => array("Aruba Guilders (also called Florins)","ƒ"),
	"AUD" => array("Australia Dollars","$"),
	"BSD" => array("Bahamas Dollars","$"),
	"BBD" => array("Barbados Dollars","$"),
	"BEF" => array("Belgium Francs","₣"),
	"BZD" => array("Belize Dollars","$"),
	"BMD" => array("Bermuda Dollars","$"),
	"BOB" => array("Bolivia Bolivianos","$"),
	"BRL" => array("Brazil Reais","R$"),
	"BRC" => array("Brazil Cruzeiros","₢"),
	"GBP" => array("Britain (United Kingdom) Pounds","£"),
	"BND" => array("Brunei Darussalam Dollars","$"),
	"KHR" => array("Cambodia Riels","៛"),
	"CAD" => array("Canada Dollars","C$"),
	"KYD" => array("Cayman Islands Dollars","$"),
	"CLP" => array("Chile Pesos","$"),
	"CNY" => array("China Yuan Renminbi","¥"),
	"COP" => array("Colombia Pesos","$"),
	"CRC" => array("Costa Rica Colón","₡"),
	"CUP" => array("Cuba Pesos","₱"),
	"CYP" => array("Cyprus Pounds","£C"),
	"DKK" => array("Denmark Kroner","kr"),
	"DOP" => array("Dominican Republic Pesos","RD$"),
	"XCD" => array("East Caribbean Dollars","$"),
	"EGP" => array("Egypt Pounds","£"),
	"SVC" => array("El Salvador Colón","$"),
	"EUR" => array("Euro","€"),
	"XEU" => array("European Currency Unit","₠"),
	"FKP" => array("Falkland Islands Pounds","FK£"),
	"FJD" => array("Fiji Dollars","FJ$"),
	"FRF" => array("France Francs","₣"),
	"GIP" => array("Gibraltar Pounds","£"),
	"GRD" => array("Greece Drachmae","₯"),
	"GGP" => array("Guernsey Pounds","£"),
	"GYD" => array("Guyana Dollars","$"),
	"INR" => array("India Rupees","₹"),
	"IRR" => array("Iran Rials","﷼"),
	"IEP" => array("Ireland Punt","IR£"),
	"IMP" => array("Isle of Man Pounds","£"),
	"ILS" => array("Israel New Shekels","₪"),
	"ITL" => array("Italy Lire","₤"),
	"JMD" => array("Jamaica Dollars","JA$"),
	"JPY" => array("Japan Yen","¥"),
	"JEP" => array("Jersey Pounds","£"),
	"KPW" => array("Korea (North) Won","₩"),
	"KRW" => array("Korea (South) Won","₩"),
	"LAK" => array("Laos Kips","₭"),
	"LBP" => array("Lebanon Pounds","£"),
	"LRD" => array("Liberia Dollars","$"),
	"LUF" => array("Luxembourg Francs","₣"),
	"MTL" => array("Malta Liri","₤"),
	"MUR" => array("Mauritius Rupees","₨"),
	"MXN" => array("Mexico Pesos","$"),
	"MNT" => array("Mongolia Tugriks","₮"),
	"NAD" => array("Namibia Dollars","$"),
	"NPR" => array("Nepal Rupees","₨"),
	"ANG" => array("Netherlands Antilles Guilders (also called Florins)","ƒ"),
	"NLG" => array("Netherlands Guilders","ƒ"),
	"NZD" => array("New Zealand Dollars","$"),
	"NGN" => array("Nigeria Nairas","₦"),
	"OMR" => array("Oman Rials","﷼"),
	"PKR" => array("Pakistan Rupees","₨"),
	"PEN" => array("Peru Nuevos Soles","S/."),
	"PHP" => array("Philippines Pesos","₱"),
	"QAR" => array("Qatar Rials","﷼"),
	"RUB" => array("Russia Rubles","руб"),
	"SHP" => array("Saint Helena Pounds","£"),
	"SAR" => array("Saudi Arabia Riyals","﷼"),
	"SCR" => array("Seychelles Rupees","₨"),
	"SGD" => array("Singapore Dollars","$"),
	"SBD" => array("Solomon Islands Dollars","$"),
	"ZAR" => array("South Africa Rand","R"),
	"KRW" => array("South Korea Won","₩"),
	"ESP" => array("Spain Pesetas","Pts"),
	"LKR" => array("Sri Lanka Rupees","₨"),
	"SEK" => array("Sweden Kronor","kr"),
	"SRD" => array("Suriname Dollars","$"),
	"SYP" => array("Syria Pounds","£"),
	"TWD" => array("Taiwan New Dollars","NT$"),
	"THB" => array("Thailand Baht","฿"),
	"TTD" => array("Trinidad and Tobago Dollars","TT$"),
	"TRY" => array("Turkey New Lira",html_entity_decode('&#8378;' , ENT_COMPAT, 'UTF-8')),
	"TRL" => array("Turkey Liras",html_entity_decode('&#8378;' , ENT_COMPAT, 'UTF-8')),
	"TVD" => array("Tuvalu Dollars","$")
    );
    if(array_key_exists($currencyCode,$currencyData)){
        if($htmlentities)
            return array(htmlentities($currencyData[$currencyCode][1]),$currencyData[$currencyCode][0]);
        else
           return array($currencyData[$currencyCode][1],$currencyData[$currencyCode][0]); 
    }else{
        return array('!','Unknown');
    }
}

function con2money_format($floatcurr, $curr = 'USD') {
    $currencies['ARS'] = array(2, ',', '.');          //  Argentine Peso
    $currencies['AMD'] = array(2, '.', ',');          //  Armenian Dram
    $currencies['AWG'] = array(2, '.', ',');          //  Aruban Guilder
    $currencies['AUD'] = array(2, '.', ' ');          //  Australian Dollar
    $currencies['BSD'] = array(2, '.', ',');          //  Bahamian Dollar
    $currencies['BHD'] = array(3, '.', ',');          //  Bahraini Dinar
    $currencies['BDT'] = array(2, '.', ',');          //  Bangladesh, Taka
    $currencies['BZD'] = array(2, '.', ',');          //  Belize Dollar
    $currencies['BMD'] = array(2, '.', ',');          //  Bermudian Dollar
    $currencies['BOB'] = array(2, '.', ',');          //  Bolivia, Boliviano
    $currencies['BAM'] = array(2, '.', ',');          //  Bosnia and Herzegovina, Convertible Marks
    $currencies['BWP'] = array(2, '.', ',');          //  Botswana, Pula
    $currencies['BRL'] = array(2, ',', '.');          //  Brazilian Real
    $currencies['BND'] = array(2, '.', ',');          //  Brunei Dollar
    $currencies['CAD'] = array(2, '.', ',');          //  Canadian Dollar
    $currencies['KYD'] = array(2, '.', ',');          //  Cayman Islands Dollar
    $currencies['CLP'] = array(0,  '', '.');          //  Chilean Peso
    $currencies['CNY'] = array(2, '.', ',');          //  China Yuan Renminbi
    $currencies['COP'] = array(2, ',', '.');          //  Colombian Peso
    $currencies['CRC'] = array(2, ',', '.');          //  Costa Rican Colon
    $currencies['HRK'] = array(2, ',', '.');          //  Croatian Kuna
    $currencies['CUC'] = array(2, '.', ',');          //  Cuban Convertible Peso
    $currencies['CUP'] = array(2, '.', ',');          //  Cuban Peso
    $currencies['CYP'] = array(2, '.', ',');          //  Cyprus Pound
    $currencies['CZK'] = array(2, '.', ',');          //  Czech Koruna
    $currencies['DKK'] = array(2, ',', '.');          //  Danish Krone
    $currencies['DOP'] = array(2, '.', ',');          //  Dominican Peso
    $currencies['XCD'] = array(2, '.', ',');          //  East Caribbean Dollar
    $currencies['EGP'] = array(2, '.', ',');          //  Egyptian Pound
    $currencies['SVC'] = array(2, '.', ',');          //  El Salvador Colon
    $currencies['ATS'] = array(2, ',', '.');          //  Euro
    $currencies['BEF'] = array(2, ',', '.');          //  Euro
    $currencies['DEM'] = array(2, ',', '.');          //  Euro
    $currencies['EEK'] = array(2, ',', '.');          //  Euro
    $currencies['ESP'] = array(2, ',', '.');          //  Euro
    $currencies['EUR'] = array(2, ',', '.');          //  Euro
    $currencies['FIM'] = array(2, ',', '.');          //  Euro
    $currencies['FRF'] = array(2, ',', '.');          //  Euro
    $currencies['GRD'] = array(2, ',', '.');          //  Euro
    $currencies['IEP'] = array(2, ',', '.');          //  Euro
    $currencies['ITL'] = array(2, ',', '.');          //  Euro
    $currencies['LUF'] = array(2, ',', '.');          //  Euro
    $currencies['NLG'] = array(2, ',', '.');          //  Euro
    $currencies['PTE'] = array(2, ',', '.');          //  Euro
    $currencies['GHC'] = array(2, '.', ',');          //  Ghana, Cedi
    $currencies['GIP'] = array(2, '.', ',');          //  Gibraltar Pound
    $currencies['GTQ'] = array(2, '.', ',');          //  Guatemala, Quetzal
    $currencies['HNL'] = array(2, '.', ',');          //  Honduras, Lempira
    $currencies['HKD'] = array(2, '.', ',');          //  Hong Kong Dollar
    $currencies['HUF'] = array(0,  '', '.');          //  Hungary, Forint
    $currencies['ISK'] = array(0,  '', '.');          //  Iceland Krona
    $currencies['INR'] = array(2, '.', ',');          //  Indian Rupee
    $currencies['IDR'] = array(2, ',', '.');          //  Indonesia, Rupiah
    $currencies['IRR'] = array(2, '.', ',');          //  Iranian Rial
    $currencies['JMD'] = array(2, '.', ',');          //  Jamaican Dollar
    $currencies['JPY'] = array(0,  '', ',');          //  Japan, Yen
    $currencies['JOD'] = array(3, '.', ',');          //  Jordanian Dinar
    $currencies['KES'] = array(2, '.', ',');          //  Kenyan Shilling
    $currencies['KWD'] = array(3, '.', ',');          //  Kuwaiti Dinar
    $currencies['LVL'] = array(2, '.', ',');          //  Latvian Lats
    $currencies['LBP'] = array(0,  '', ' ');          //  Lebanese Pound
    $currencies['LTL'] = array(2, ',', ' ');          //  Lithuanian Litas
    $currencies['MKD'] = array(2, '.', ',');          //  Macedonia, Denar
    $currencies['MYR'] = array(2, '.', ',');          //  Malaysian Ringgit
    $currencies['MTL'] = array(2, '.', ',');          //  Maltese Lira
    $currencies['MUR'] = array(0,  '', ',');          //  Mauritius Rupee
    $currencies['MXN'] = array(2, '.', ',');          //  Mexican Peso
    $currencies['MZM'] = array(2, ',', '.');          //  Mozambique Metical
    $currencies['NPR'] = array(2, '.', ',');          //  Nepalese Rupee
    $currencies['ANG'] = array(2, '.', ',');          //  Netherlands Antillian Guilder
    $currencies['ILS'] = array(2, '.', ',');          //  New Israeli Shekel
    $currencies['TRY'] = array(2, '.', ',');          //  New Turkish Lira
    $currencies['NZD'] = array(2, '.', ',');          //  New Zealand Dollar
    $currencies['NOK'] = array(2, ',', '.');          //  Norwegian Krone
    $currencies['PKR'] = array(2, '.', ',');          //  Pakistan Rupee
    $currencies['PEN'] = array(2, '.', ',');          //  Peru, Nuevo Sol
    $currencies['UYU'] = array(2, ',', '.');          //  Peso Uruguayo
    $currencies['PHP'] = array(2, '.', ',');          //  Philippine Peso
    $currencies['PLN'] = array(2, '.', ' ');          //  Poland, Zloty
    $currencies['GBP'] = array(2, '.', ',');          //  Pound Sterling
    $currencies['OMR'] = array(3, '.', ',');          //  Rial Omani
    $currencies['RON'] = array(2, ',', '.');          //  Romania, New Leu
    $currencies['ROL'] = array(2, ',', '.');          //  Romania, Old Leu
    $currencies['RUB'] = array(2, ',', '.');          //  Russian Ruble
    $currencies['SAR'] = array(2, '.', ',');          //  Saudi Riyal
    $currencies['SGD'] = array(2, '.', ',');          //  Singapore Dollar
    $currencies['SKK'] = array(2, ',', ' ');          //  Slovak Koruna
    $currencies['SIT'] = array(2, ',', '.');          //  Slovenia, Tolar
    $currencies['ZAR'] = array(2, '.', ' ');          //  South Africa, Rand
    $currencies['KRW'] = array(0,  '', ',');          //  South Korea, Won
    $currencies['SZL'] = array(2, '.', ', ');         //  Swaziland, Lilangeni
    $currencies['SEK'] = array(2, ',', '.');          //  Swedish Krona
    $currencies['CHF'] = array(2, '.', '\'');         //  Swiss Franc
    $currencies['TZS'] = array(2, '.', ',');          //  Tanzanian Shilling
    $currencies['THB'] = array(2, '.', ',');          //  Thailand, Baht
    $currencies['TOP'] = array(2, '.', ',');          //  Tonga, Paanga
    $currencies['AED'] = array(2, '.', ',');          //  UAE Dirham
    $currencies['UAH'] = array(2, ',', ' ');          //  Ukraine, Hryvnia
    $currencies['USD'] = array(2, '.', ',');          //  US Dollar
    $currencies['VUV'] = array(0,  '', ',');          //  Vanuatu, Vatu
    $currencies['VEF'] = array(2, ',', '.');          //  Venezuela Bolivares Fuertes
    $currencies['VEB'] = array(2, ',', '.');          //  Venezuela, Bolivar
    $currencies['VND'] = array(0,  '', '.');          //  Viet Nam, Dong
    $currencies['ZWD'] = array(2, '.', ' ');          //  Zimbabwe Dollar
    if ($curr == "INR") {
        return formatinr($floatcurr);
    } else {
        return number_format($floatcurr, $currencies[$curr][0], $currencies[$curr][1], $currencies[$curr][2]);
    }
}

function formatinr($input) {
    $dec = "";
    $pos = strpos($input, ".");
    if ($pos === FALSE)
    {
        //no decimals
    }
    else
    {
        //decimals
        $dec   = substr(round(substr($input, $pos), 2), 1);
        $input = substr($input, 0, $pos);
    }
    $num   = substr($input, -3);    // get the last 3 digits
    $input = substr($input, 0, -3); // omit the last 3 digits already stored in $num
    // loop the process - further get digits 2 by 2
    while (strlen($input) > 0)
    {
        $num   = substr($input, -2).",".$num;
        $input = substr($input, 0, -2);
    }
    return $num.$dec;
}

?>