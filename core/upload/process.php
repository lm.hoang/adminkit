<?php

/*
 * @author Balaji
 * @name: A to Z SEO Tools - PHP Script
 * @copyright � 2014 ProThemes.Biz
 *
 */
 
// Disable Errors
error_reporting(1);

require_once('../../core/functions.php');
require('class.txtConversion.php');

$filePath = 'files/'.raino_trim($_POST['fileName']);

$docObj = new txtConversion($filePath);
$return = $docObj->convertToText();
unlink($filePath);
echo $return;  
?>