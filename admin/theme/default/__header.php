<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: Rainbow PHP Framework
 * @copyright © 2017 ProThemes.Biz
 *
 */
 
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <link rel="icon" type="image/png" href="<?php themeLink('dist/img/favicon.png'); ?>" />
    <title><?php echo $pageTitle .' | '. APP_NAME; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport' />
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php themeLink('bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="<?php themeLink('plugins/morris/morris.css'); ?>" rel="stylesheet" type="text/css" />
    <!-- DATA TABLES -->
    <link href="<?php themeLink('plugins/datatables/dataTables.bootstrap.css'); ?>" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <!-- daterange picker -->
    <link href="<?php themeLink('plugins/daterangepicker/daterangepicker.css'); ?>" rel="stylesheet" type="text/css" />
    <!-- colorpicker -->
    <link href="<?php themeLink('plugins/colorpicker/bootstrap-colorpicker.min.css'); ?>" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php themeLink('dist/css/AdminLTE.min.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php themeLink('dist/css/custom.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?php themeLink('dist/css/skins/skin-blue.min.css'); ?>" rel="stylesheet" type="text/css" />
    
    <!-- iCheck -->
    <link href="<?php themeLink('plugins/iCheck/square/blue.css'); ?>" rel="stylesheet" type="text/css" />
<!-- custom -->
     <!--  google font  -->
     <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
<!-- custom -->
    <!-- Select2 -->
    <link rel="stylesheet" href="<?php themeLink('plugins/select2/select2.min.css'); ?>" />
    
    <!-- jQuery 2.1.4 -->
    <?php scriptLink('plugins/jQuery/jQuery-2.1.4.min.js'); ?>
    
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body class="skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">
<!-- custom -->
          <nav class="navbar" role="navigation" aria-label="main navigation">
              <div class="navbar-brand span4l">
                  <a class="navbar-item" href="<?php createLink(); ?>">
                      <span class="logo-lg"><?php echo HTML_APP_NAME; ?></span>
                  </a>
                  <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                      <span aria-hidden="true"></span>
                      <span aria-hidden="true"></span>
                      <span aria-hidden="true"></span>
                  </a>
              </div>

              <div class="search-wrapper span4l">
                  <div class="search-container">
                      <form id="searchForm" class="ng-pristine ng-valid ng-valid-maxlength">

                          <div class="input">

                              <i class="iconf-search"></i>

                              <input name="s" maxlength="50" type="text" placeholder="" class="header-search" autocomplete="off">



                              <input type="submit">

                          </div>

                          <ul id="search-results"></ul>
                      </form>
                  </div>
              </div>


              <div class="header-right span4l">
                  <ul class="nav-right">
                      <li class="notifications">
                          <a href="#">
                              <i class="iconf-bookmark"></i>
                          </a>
                      </li>
                      <li class="notifications">
                          <a href="#">
                              <i class="iconf-alert"></i>
                          </a>
                      </li>
                      <li class="user-header-menu">
                          <a href="<?php adminLink('?logout'); ?>" title="Logout"><i class="glyphicon glyphicon-off"></i></a>
                      </li>
                  </ul>
              </div>
          </nav>
<!-- custom -->
      </header>
<!-- custom -->
        <div class="sidenav-fixed">
            <div class="logo">
                <img src="https://staging.printcart.com/wp-content/themes/printcart-child/assets/images/logo.png" alt="">
            </div>
            <ul id="shortcuts-var3">
                <li title="" title-right="true">
                    <a href="#">
                        <div class="sprite Group837"></div>
                    </a>
                </li>
                <li title="" title-right="true">
                    <a href="#">
                        <div class="sprite Group826"></div>
                    </a>
                </li>
                <li title="" title-right="true">
                    <div class="sprite Group827"></div>
                </li>
                <li title="" title-right="true">
                    <div class="sprite Group828"></div>
                </li>
                <li title="" title-right="true">
                    <div class="sprite Group829"></div>
                </li>
                <li title="" title-right="true">
                    <div class="sprite Group830"></div>
                </li>
                <li title="" title-right="true">
                    <div class="sprite Group831"></div>
                </li>
                <li title="" title-right="true">
                    <div class="sprite Group832"></div>
                </li>
                <li title="" title-right="true">
                    <div class="sprite Group833"></div>
                </li>
                <li title="" title-right="true">
                    <div class="sprite Group834"></div>
                </li>
                <li title="" title-right="true">
                    <div class="sprite Group835"></div>
                </li>
                <li title="" title-right="true">
                    <div class="sprite Group825"></div>
                </li>
            </ul>
        </div>
<!-- custom -->
            <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
            <!-- Optionally, you can add icons to the links -->
            <?php
            define('ADMIN_LINKS',true);
            require ADMIN_CON_DIR.'links.php';
            ksort($menuBarLinks);
            foreach($menuBarLinks as $menuBarLink){
                $isActive = $subMenuLinkData = '';
                $subMenuConNames = array();
                if($menuBarLink[0]){
                    if(isset($menuBarLink[4])){
                        foreach($menuBarLink[4] as $subMenuLink){
                            if(!isset($subMenuLink[3]))
                                $subMenuLinkData .= '<li><a href="'.$adminBaseURL.$subMenuLink[1].'"><i class="'.$subMenuLink[2].'"></i>'.$subMenuLink[0].'</a></li>';
                            $subMenuConNames[] = $subMenuLink[1];
                        }
                        if (in_array($controller,$subMenuConNames)) $isActive = 'active';
                        echo '<li class="treeview '.$isActive.'">
                          <a href="#"><i class="'.$menuBarLink[3].'"></i> <span> '.$menuBarLink[1].'</span> <i class="fa fa-angle-left pull-right"></i></a>
                          <ul class="treeview-menu">
                              '.$subMenuLinkData.'
                          </ul>
                        </li>';
                    } else {
                        if($controller == $menuBarLink[2]) $isActive = 'active';
                        if($menuBarLink[2] == 'dashboard') $menuBarLink[2] = '';
                        if($menuBarLink[2] == 'header-li')
                            echo '<li class="header">'.$menuBarLink[1].'</li>';
                        else
                            echo '<li class="'.$isActive.'"><a href="'.$adminBaseURL.$menuBarLink[2].'"><i class="'.$menuBarLink[3].'"></i> <span> '.$menuBarLink[1].'</span></a></li>';
                    }
                }
            }
            ?>
          </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>