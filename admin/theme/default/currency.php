<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright © 2018 ProThemes.Biz
 *
 */
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $pageTitle; ?>  
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php adminLink(); ?>"><i class="<?php getAdminMenuIcon($controller,$menuBarLinks); ?>"></i> Admin</a></li>
        <li class="active"><a href="<?php adminLink($controller); ?>"><?php echo $pageTitle; ?></a> </li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $headTitle; ?></h3>
                <div style="position:absolute; top:4px; right:15px;">
                  <a href="<?php adminLink('currency&new-rule');  ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i> New Currency Rule </a>
                </div>
            </div><!-- /.box-header ba-la-ji -->
            <!-- form start -->

                <form action="#" method="POST">
                    <div class="box-body" style="width: 60%;">
                <?php
                if(isset($msg)){
                    echo $msg.'<br>';
                } if($newPage) {?>
                
                    <div class="form-group">
                      <label for="currency_name">Currency Name</label>
                      <input type="text" placeholder="Enter your currency name" value="<?php echo $currency_name; ?>" name="currency_name" class="form-control" />
                    </div>
                    
                    <div class="form-group">
                    <label for="currency_rate">Conversion Rate <small>(1 USD = Your Currency)</small></label>
                      <input type="text" placeholder="Enter your conversion rate" value="<?php echo $currency_rate; ?>" name="currency_rate" class="form-control" />
                    </div>
                    
                    <div class="form-group">
                      <label for="currency_code">Currency Code / Symbol</label>
                        <select class="form-control" name="currency_code">
                        <?php
                        foreach($curDataList as $curData=>$curDataVal){
                            if($curData == $currency_code){
                                echo '<option selected value="'.$curData.'">'.$curData . ' - ' . $curDataVal[1].'</option>';
                                $selCurType = $curDataVal[1];
                            }else
                                echo '<option value="'.$curData.'">'.$curData . ' - ' . $curDataVal[1].'</option>';
                        }
                        ?>
                        </select>
                    </div>
                    
                    <?php if(isset($_GET['new-rule'])) { ?>
                        <input type="hidden" value="1" name="newCurrency" />
                    <?php } else { ?>
                        <input type="hidden" value="1" name="editCurrency" />
                        <input type="hidden" value="<?php echo $currency_id; ?>" name="id" />
                    <?php } ?>
                
                <?php  } else { ?>
                        <div class="form-group">
                        <label for="default_currency">Default Currency Symbol</label>
                        <select class="form-control" name="default_currency">
                            <?php echo $defaultCurrencyData; ?>
    					</select>
                        </div>
                        <input type="hidden" value="1" name="currencyEnable" />
                        
                        <div class="alert alert-warning">
                        Make sure that your selected currency is compatible with your payment gateway. 
                        Otherwise, the cart value shows incorrect or payment gateway throws errors.
                        </div>
                      <?php } ?>                  
                            <button type="submit" class="btn btn-primary">Save</button>
                         </div><!-- /.box-body -->
                    </form>
                </div>
           <?php  if(!$newPage) {     ?>
         <div class="box box-success">
        <div class="box-header with-border">
            <!-- tools box -->

            <h3 class="box-title">
                Currency List
            </h3>
        </div>

        <div class="box-body">

        <table id="currencyTable" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Currency Name</th>
            <th>Currency Code</th>
            <th>Currency Symbol</th>
            <th>Conversion Rate</th>
            <th>Edit</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
        <?php foreach($currencyList as $currency){

            echo '<tr>
            <td>'.$currency[0].'</td>
            <td>'.$currency[1].'</td>
            <td>'.$currency[2].'</td>
            <td>'.$currency[3].'</td>
            <td>'.$currency[4].'</td>
            <td>'.$currency[5].'</td>
          </tr>';
        }
        ?>

        </tbody>
      </table>


        </div><!-- /.box-body -->

        <div class="box-footer">

        </div>
    </div>
    
    <?php } ?>
      
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      
      <script type="text/javascript">
      $(function () {
        $('#currencyTable').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": false
        });
      });
    </script>