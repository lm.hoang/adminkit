<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: Turbo Website Reviewer
 * @copyright © 2017 ProThemes.Biz
 *
 */
?>
  <link rel="stylesheet" href="<?php themeLink('plugins/fancybox/jquery.fancybox.min.css'); ?>" />
  
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $pageTitle; ?>  
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php adminLink(); ?>"><i class="<?php getAdminMenuIcon($controller,$menuBarLinks); ?>"></i> Admin</a></li>
        <li class="active"><a href="<?php adminLink($controller); ?>"><?php echo $pageTitle; ?></a> </li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo $subTitle; ?></h3>
        </div><!-- /.box-header ba-la-ji -->
        <form action="#" method="POST">
        <div class="box-body">
      
        <?php if(isset($msg)) echo $msg; ?><br />
              <div class="row">
                <div class="col-md-6">
                
                <div class="form-group">
                    <label>Blog Status:</label>
                    <select name="blog_status" class="form-control"> 
                        <option <?php isSelected($blog_status, true, '1'); ?> value="on">Enabled</option>
                        <option <?php isSelected($blog_status, false, '1'); ?> value="off">Disable</option>
                    </select>             
                </div>  
                
                <div class="form-group">
                  <label for="meta_des">Meta Description:</label>
                  <textarea placeholder="Description must be within 150 Characters" rows="3" name="meta_des" class="form-control"><?php echo $meta_des; ?></textarea>
               </div>
                                                 
               <div class="form-group">
                    <label for="maximum_posts">Maximum number of post shown on per page: <small>(For Blog Index Page)</small></label>
                    <input type="number" placeholder="Enter your maximum posts per page" id="maximum_posts" name="maximum_posts" value="<?php echo $maximum_posts; ?>" class="form-control">
               </div>
               
               <div class="form-group">
                    <label for="posted_by">Default Posted By:</label>
                    <input type="text" placeholder="Enter who posted by as default" id="posted_by" name="posted_by" value="<?php echo $posted_by; ?>" class="form-control">
               </div>
               
               <div class="form-group">
                    <label>Enable Related Posts:</label>
                    <select name="enable_related" class="form-control"> 
                        <option <?php isSelected($enable_related, true, '1'); ?> value="on">Yes</option>
                        <option <?php isSelected($enable_related, false, '1'); ?> value="off">No</option>
                    </select>             
                </div>  
                                    
               </div><!-- /.col-md-6 -->
                
                <div class="col-md-6">
                
               <div class="form-group">
                    <label>Enable Comments: <small>(Powered by Disqus)</small></label>
                    <select name="enable_comments" class="form-control"> 
                        <option <?php isSelected($enable_comments, true, '1'); ?> value="on">Yes</option>
                        <option <?php isSelected($enable_comments, false, '1'); ?> value="off">No</option>
                    </select>             
                </div>  
                                    
                <div class="form-group">
                  <label for="meta_tags">Meta Keywords: <small>(Separate with commas)</small></label>
                  <textarea placeholder="keywords1, keywords2, keywords3" rows="3" name="meta_tags" class="form-control"><?php echo $meta_tags; ?></textarea>
               </div>
               
               <div class="form-group">
                    <label for="truncate">Content Truncate Length: <small>(For Blog Index Page)</small></label>
                    <input type="number" placeholder="Enter your truncate length" id="truncate" name="truncate" value="<?php echo $truncate; ?>" class="form-control">
               </div>
                                  
               <div class="form-group">
               <label for="no_image">Default Featured Image: </label>
               <div class="input-group">
                    <input type="text" id="no_image" value="<?php echo $no_image; ?>" class="form-control" name="no_image" />                                    
                    <span class="input-group-addon">
                    <a class="fa fa-picture-o iframe-btn" href="<?php echo $baseURL; ?>core/library/filemanager/dialog.php?type=1&field_id=no_image"></a>
                    </span>
               </div>
               </div> 
               
               <div class="form-group">
                    <label for="related_count">Related posts per page:</label>
                    <input type="number" placeholder="Enter related posts per page" name="related_count" value="<?php echo $related_count; ?>" class="form-control">
               </div> 

                </div>
            </div><!-- /.row -->
            
            <div class="row">
             
             <div class="form-group" style="margin: 12px;">
                  <label for="disqus_id">Put your Disqus Universal Code - Comment System: (<a href="https://disqus.com/admin/create/" target="_blank" title="Disqus Site">Create Disqus Comment</a>)</label>
                  <textarea name="disqus_id" id="disqus_id" class="form-control" rows="6"><?php echo stripcslashes(strEOL($disqus_id)); ?></textarea>
               </div>
             
             </div>
                
            <input type="submit" name="save" value="Save" class="btn btn-primary"/>
            <br /> <br />
            
            </div><!-- /.box-body -->
            </form>
          </div><!-- /.box -->
  
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
<?php
$linkjs = themeLink('plugins/fancybox/jquery.fancybox.min.js',true); 
$footerAddArr[] = <<<EOD
<script src="$linkjs"></script>
<script type="text/javascript">

  function relatedCheck(val){
    if(val == 'off')
        $('input[name=related_count]').attr('disabled', 'disabled');
    else
        $('input[name=related_count]').removeAttr('disabled');
  }
  function commentCheck(val){
    if(val == 'off')
        $('#disqus_id').attr('disabled', 'disabled');
    else
        $('#disqus_id').removeAttr('disabled');
  }
   
  var selVal;  
  $(function () {
    selVal = jQuery('select[name="enable_related"]').val();
    relatedCheck(selVal);
    selVal = jQuery('select[name="enable_comments"]').val();
    commentCheck(selVal);
  });      
  $('select[name="enable_related"]').on('change', function() {
    selVal = jQuery('select[name="enable_related"]').val();
    relatedCheck(selVal);
  }); 
  $('select[name="enable_comments"]').on('change', function() {
    selVal = jQuery('select[name="enable_comments"]').val();
    commentCheck(selVal);
  });  
 jQuery(document).ready(function ($) {   
      $('.iframe-btn').fancybox({
        'width': '75%',
        'height': '90%',
        'autoScale': false,
        'autoDimensions': false,
        'transitionIn': 'none',
        'transitionOut': 'none',
        'type': 'iframe'
    });
});
</script>
<style>.fancybox-content{ height: 500px !important; }</style>
EOD;
?>