<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright � 2018 ProThemes.Biz
 *
 */
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $pageTitle; ?>  
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php adminLink(); ?>"><i class="<?php getAdminMenuIcon($controller,$menuBarLinks); ?>"></i> Admin</a></li>
        <li class="active"><a href="<?php adminLink($controller); ?>"><?php echo $pageTitle; ?></a> </li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $subTitle; ?></h3>
            </div><!-- /.box-header ba-la-ji -->

            <div class="box-body">
             <?php
            if(isset($msg)){
                echo $msg;
            }?>
            
            <div class="row">
                
               <?php if($orderSuc) { ?>
                 
                <div class="text-center">  
                <h2>Order created successfully!</h2>
                <h6><a href="<?php adminLink('orders&view='.$orderID);  ?>" class="btn btn-primary" target="_blank">View Order</a></h6>
                </div> 
                
               <?php } else { ?>
               <form action="#" method="POST">
                <div class="col-md-6">
                                    
                <div class="form-group">
                <label for="amount">Customer:<small>(Username)</small></label>
                <input placeholder="Type your customer username (autocomplete)" type="text" name="username" id="username" class="form-control" />
                </div>
            
                <div class="form-group">
                <label for="emailID">Email ID:</label>
                <input disabled="" type="text" name="emailID" id="emailID" class="form-control" />
                </div>
                
                <div class="form-group">
                <label for="plan">Plan Name:</label>
                <input placeholder="Type your plan name (autocomplete)" type="text" name="plan" id="plan" class="form-control" />
                </div>
                
                <div class="form-group">
                <label for="billDis">Billing Type:</label>
                <input disabled="" type="text" id="billDis" name="billDis" class="form-control" />
                </div>
                
                <div class="form-group">
                <label for="payType">Payment Method</label>
                <select class="form-control" name="payType">
                    <?php echo $paymentGatewaysData; ?>
				</select>
                </div>
                
                <div class="form-group">
                <label for="orderStatus">Order Status</label>
                <select class="form-control" name="orderStatus">
                    <option value="completed">Completed</option>
                    <option value="rec_completed">Recurring Completed</option>
                    <option value="pending">Pending</option>
                    <option value="rec_pending">Recurring Pending</option>
                    <option value="canceled">Canceled</option>
				</select>
                </div>
                
                </div><!-- /.col-md-6 -->
                
                <div class="col-md-6">
                
                <div class="form-group">
                <label for="userID">Customer ID:</label>
                <input disabled="" type="text" name="userID" id="userID" class="form-control" />
                </div>
                
                <div class="form-group">
                <label for="since">Member Since:</label>
                <input disabled="" type="text" name="since" id="since" class="form-control" />
                </div>
                
                <div class="form-group" id="recTypeBox" style="display: none;">
                <label for="recType">Recurring Type:</label>
                <select class="form-control" id="recType" name="recType"></select>
                </div>  
                
                <div class="form-group">
                <label for="amount">Plan Amount:</label>
                <input type="text" id="amount" name="amount" class="form-control" />
                </div>
                
                <div class="form-group">
                <label for="total">Total Amount:</label>
                <input type="text" id="total" name="total" class="form-control" />
                </div>
                
                <div class="form-group">
                <label for="payStatus">Payment Status</label>
                <select class="form-control" name="payStatus">
                    <option value="completed">Paid</option>
                    <option value="pending">Unpaid</option>
                    <option value="fraud">Fraud</option>
                    <option value="refunded">Refunded</option>
                    <option value="chargeback">Chargeback</option>
                    <option value="reversed">Reversed</option>
                    <option value="canceled">Canceled</option>
				</select>
                </div>
         
                </div>
                <br />
                 <input type="hidden" name="rec1Am" id="rec1Am" />
                 <input type="hidden" name="rec2Am" id="rec2Am" />
                 <input type="hidden" name="rec3Am" id="rec3Am" />
                 <input type="hidden" name="rec4Am" id="rec4Am" />
                 
                 <input type="hidden" name="billType" id="billType" />
                 <input type="hidden" name="planID" id="planID" />
                 <div class="col-md-12">    <br />
                <input type="submit" name="save" value="Create Order" class="btn btn-primary"/>
                <br /><br />
                </div>
                 </form>
                 <?php } ?>
            </div><!-- /.row -->
            
            </div><!-- /.box-body -->
   
        </div>
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
  
<?php 
$link1 = adminLink('premium_ajax&getPlans',true);
$link2 = adminLink('premium_ajax&getCustomers',true);
$footerAddArr[] = <<<EOD

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" />
<style>
.ui-autocomplete {
z-index: 100 !important;
}
</style>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript">
$(document).ready (function(){
$('#plan').autocomplete({
	source: function( request, response ) {
  		$.ajax({
  			url : '$link1',
  			dataType: "json",
			data: {
			   term: request.term,
			   type: 'plan_table'
			},
			 success: function( data ) {
				 response( $.map( data, function( item ) {
				 	var code = item.split("|");
					return {
						label: code[0],
						value: code[0],
						data : item
					}
				}));
			}
  		});
  	},
  	autoFocus: true,	      	
  	minLength: 0,
  	select: function( event, ui ) {
		var names = ui.item.data.split("|");						
		$('#planID').val(names[1]);
        if(names[2] == '0'){
            $('#billDis').val('One Time Billing');
      		$('#billType').val('0');
      		$('#amount').val(names[3]);
      		$('#total').val(names[3]);
            jQuery("#recTypeBox").css({"display":"none"});
        }else{
            jQuery("#recTypeBox").css({"display":"block"});
            $('#billDis').val('Recurring Billing');
      		$('#billType').val('1');

            if(names[4] != '0'){
          		$('#recType').append('<option value="'+names[4]+'">Monthly</option>');  
                $('#rec1Am').val(names[5]);
            }
            if(names[6] != '0'){
          		$('#recType').append('<option value="'+names[6]+'">Every 3 months</option>');  
                $('#rec2Am').val(names[7]);
            }
            if(names[8] != '0'){
          		$('#recType').append('<option value="'+names[8]+'">Every 6 months</option>');
                $('#rec3Am').val(names[9]);  
            }
            if(names[10] != '0'){
          		$('#recType').append('<option value="'+names[10]+'">Every year</option>');  
                $('#rec4Am').val(names[11]);  
            }
            
            $("#recType option:first").attr('selected','selected');
            var myAmount = 0;
            var myTotal = 0;
            if($("#recType").val()== 'rec1'){
                myAmount = myTotal =  $('#rec1Am').val();
            }
            if($("#recType").val()== 'rec2'){
                myAmount = myTotal =  $('#rec2Am').val();
            }
            if($("#recType").val()== 'rec3'){
                myAmount = myTotal =  $('#rec3Am').val();
            }
            if($("#recType").val()== 'rec4'){
                myAmount = myTotal =  $('#rec4Am').val();
            }
            $('#amount').val(myAmount);
      		$('#total').val(myTotal);
        }
	}		      	
});

$(document).on('change','#recType',function(){
    var myAmount = 0;
    var myTotal = 0;
    if($("#recType").val()== 'rec1'){
        myAmount = myTotal =  $('#rec1Am').val();
    }
    if($("#recType").val()== 'rec2'){
        myAmount = myTotal =  $('#rec2Am').val();
    }
    if($("#recType").val()== 'rec3'){
        myAmount = myTotal =  $('#rec3Am').val();
    }
    if($("#recType").val()== 'rec4'){
        myAmount = myTotal =  $('#rec4Am').val();
    }
    $('#amount').val(myAmount);
	$('#total').val(myTotal);
});

$('#username').autocomplete({
	source: function( request, response ) {
  		$.ajax({
  			url : '$link2',
  			dataType: "json",
			data: {
			   term: request.term,
			   type: 'users_table'
			},
			 success: function( data ) {
				 response( $.map( data, function( item ) {
				 	var code = item.split("|");
					return {
						label: code[0],
						value: code[0],
						data : item
					}
				}));
			}
  		});
  	},
  	autoFocus: true,	      	
  	minLength: 2,
  	select: function( event, ui ) {
		var names = ui.item.data.split("|");						
		$('#emailID').val(names[1]);
		$('#since').val(names[2]);
		$('#userID').val(names[3]);
	}		      	
});
});
</script>
EOD;
?>