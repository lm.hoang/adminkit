<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright © 2018 ProThemes.Biz
 *
 */
?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script type="text/javascript" language="javascript" src="<?php themeLink('dist/js/jquery.dataTables.js'); ?>"></script>

<script type="text/javascript" language="javascript" class="init">

$(document).ready(function() {
	$('#mySitesTable').dataTable( {
		"processing": true,
        "aaSorting": [[0, 'desc']],
		"serverSide": true,
		"ajax": "<?php adminLink('premium_ajax&manageInvoices');  ?>"
	} );
} );

</script>
    
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo $pageTitle; ?>  
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php adminLink(); ?>"><i class="<?php getAdminMenuIcon($controller,$menuBarLinks); ?>"></i> Admin</a></li>
            <li class="active"><a href="<?php adminLink($controller); ?>"><?php echo $pageTitle; ?></a> </li>
          </ol>
        </section>
    
        <!-- Main content -->
        <section class="content">
            
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#settings" data-toggle="tab">Settings</a></li>
                  <li><a href="#manageInvoices" data-toggle="tab">Manage Invoices</a></li>
                </ul>
                <div class="tab-content">
                
                <div class="tab-pane active" id="settings" >
                
                <form action="#" method="POST" enctype="multipart/form-data">
                <div class="box-body">
                 <?php
                if(isset($msg)){
                    echo $msg;
                }?>
                
                <div class="row">
                    <div class="col-md-8">
                    <div class="form-group">
                      <label for="invoice_prefix">Invoice Prefix Format <small>(Eg. PR-2016-00)</small></label>
                      <input type="text" placeholder="Enter your invoice prefix" value="<?php echo $invoice_prefix; ?>" name="invoice_prefix" class="form-control" />
                    </div>
                    
                    <div class="form-group">
                      <label for="company_add">Your Company Address</label>
                      <textarea placeholder="Enter your Company Address for billing (HTML/CSS Supported)" rows="3" name="company_add" class="form-control"><?php echo strEOL($company_add); ?></textarea>
                   </div>
                   
                   <div class="form-group">
                      <label for="invoice_footer">Invoice Footer <small>(For adding copyright text, signature, VAT no. etc..)</small></label>
                      <textarea placeholder="Enter any additonal information here (HTML/CSS Supported)" rows="3" name="invoice_footer" class="form-control"><?php echo strEOL($invoice_footer); ?></textarea>
                   </div>
                   
                    <div class="form-group">
                        <label for="invoiceLogoBox">Invoice Header Logo: </label><br />
                        <img class="invoiceLogoBox" id="invoiceLogoBox" alt="Header Logo" src="<?php echo $invoice_logo; ?>" />   
                        <br />
                        Recommended Size: (PNG 180x82px)
                        <input type="file" name="invoiceUpload" id="invoiceUpload" class="btn btn-default" />
                   </div>               
                    </div><!-- /.col-md-8 -->
                   
                </div><!-- /.row -->
                <input type="submit" name="save" value="Save" class="btn btn-primary"/>
                <br />
                
                </div><!-- /.box-body -->
                </form>
                </div>
                
                <div class="tab-pane" id="manageInvoices" >
                <br />
                <?php
                if(isset($msg)){
                    echo $msg;
                }?>
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="mySitesTable">
                	<thead>
                		<tr>
                              <th>Invoice #</th>
                              <th>Invoice Date</th>
                              <th>Due Date</th>
                              <th>Detail</th>
                              <th>Total</th>
                              <th>Status</th>
                              <th>View</th>
                		</tr>
                	</thead>         
                    <tbody>                        
                    </tbody>
                </table>
                
                </div>
            
                </div>
            </div>

      
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->