<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright � 2018 ProThemes.Biz
 *
 */
?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    
    <script type="text/javascript" language="javascript" src="<?php themeLink('dist/js/jquery.dataTables.js'); ?>"></script>
    
    <script type="text/javascript" language="javascript" class="init">
    
    $(document).ready(function() {
    	$('#mySitesTable').dataTable( {
    		"processing": true,
    		"serverSide": true,
    		"ajax": "<?php adminLink('?route=premium_ajax&manageCustomers');  ?>"
    	} );
    } );
    
    </script>
    
    <link href="<?php createLink('theme/default/premium/css/bootstrap-formhelpers.min.css',false,true);  ?>" rel="stylesheet" type="text/css" />
    
    <script src="<?php createLink('theme/default/premium/js/bootstrap-formhelpers.min.js',false,true);  ?>" type="text/javascript"></script>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <?php echo $pageTitle; ?>  
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php adminLink(); ?>"><i class="<?php getAdminMenuIcon($controller,$menuBarLinks); ?>"></i> Admin</a></li>
            <li class="active"><a href="<?php adminLink($controller); ?>"><?php echo $pageTitle; ?></a> </li>
          </ol>
        </section>
    
        <!-- Main content -->
        <section class="content">

              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"><?php echo $headTitle; ?></h3>
                    <div style="position:absolute; top:4px; right:15px;">
                        <a href="<?php adminLink('premium-clients&newUser');  ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Add New Customer </a>
                    </div>
                </div><!-- /.box-header -->

                <div class="box-body">
                <?php
                if(isset($msg)){
                    echo $msg;
                }?>
                <br />
                
                <?php if(isset($_GET['details'])){ ?>
                <div class="widget widget-table action-table">
                    <div class="widget-header"> <i class="icon-th-list"></i>
                      <h3><?php echo $user_username.' Details'; ?></h3>
                    </div>
                    <!-- /widget-header -->
                    <div class="widget-content">
                      <table class="table table-striped table-bordered">
                        <tbody>
                          <tr>
                            <td style="width: 30%;">  Username </td>
                                        <td> <?php echo $user_username; ?> </td>   
                          </tr>
                                        <tr>
                            <td style="width: 200px;"> Email ID </td>
                            <td> <?php echo $user_email_id; ?> </td>
                          </tr>
                          <tr>
                            <td style="width: 200px;"> Platform </td>
                             <td> <?php echo $user_platform; ?> </td>
                          </tr>
                          
                          <tr>
                            <td style="width: 200px;"> Oauth ID </td>
                            <td> <?php echo $user_oauth_uid; ?> </td>
                          </tr>
                                    
                                        <tr>
                            <td style="width: 200px;"> Current Status </td>
                             <td> <?php echo $user_verified; ?> </td>
                          </tr>
                                        <tr>
                            <td style="width: 200px;"> User IP </td>
                             <td> <?php echo $user_ip; ?> </td>
                          </tr>
                          <tr>
                            <td style="width: 200px;"> User Since </td>
                             <td> <?php echo $user_date; ?> </td>
                          </tr>        
                        
                        </tbody>
                      </table>
                      
              <?php if($addInfo) { ?>
                <div class="clear" style="margin-bottom: 10px;"></div>

                    <div class="widget-header"> <i class="icon-th-list"></i>
                      <h3>Personal Information:</h3>
                    </div>

                      <table class="table table-striped table-bordered">
                    <tbody>
                        <tr>
                            <td style="width: 30%;">First Name</td>
                            <td><span><?php echo $firstname; ?></span></td>
                        </tr> 
                        <tr>
                            <td style="width: 200px;">Last Name</td>
                            <td><span><?php echo $lastname; ?></span></td>         
                        </tr> 
                        <?php if($company != '') { ?>
                        <tr>
                            <td style="width: 200px;">Company</td>
                            <td><span><?php echo $company; ?></span></td>         
                        </tr> 
                        <?php } ?>
                        <tr>
                            <td style="width: 200px;">Address Line 1</td>
                            <td><span><?php echo $address1; ?></span></td>         
                        </tr> 
                        <?php if($address2 != '') { ?>
                        <tr>
                            <td style="width: 200px;">Address Line 2</td>
                            <td><span><?php echo $address2; ?></span></td>         
                        </tr> 
                        <?php } ?>
                        <tr>
                            <td style="width: 200px;">City</td>
                            <td><span><?php echo $city; ?></span></td>         
                        </tr>
                        <tr>
                            <td style="width: 200px;">State</td>
                            <td><span><?php echo $statestr; ?></span></td>         
                        </tr> 
                        <tr>
                            <td style="width: 200px;">Country</td>
                            <td><span><?php echo country_code_to_country($country); ?></span></td>         
                        </tr>  
                        <tr>
                            <td style="width: 200px;">Post Code</td>
                            <td><span><?php echo $postcode; ?></span></td>         
                        </tr> 
                        <tr>
                            <td style="width: 200px;">Telephone</td>
                            <td><span><?php echo $telephone; ?></span></td>         
                        </tr> 
                    </tbody>
                    </table>
                <?php } ?>
                
                      <div>
                        <a href="<?php adminLink('premium-clients');  ?>" class="btn btn-primary">Go Back</a>
                      </div>
                    </div>
                    <!-- /widget-content --> 
                  </div>    
 

                <?php } elseif (isset($_GET['newUser'])) { ?>
                
                <form method="POST" action="#" onclick="return fixState();"> 
                <div class="col-md-6">

				<div class="form-group">
					Username <br />
						<input type="text" class="form-control" name="username" />
				</div>	
				
                <div class="form-group">
					Email <br />
						<input type="text" class="form-control" name="email" />
				</div>

                 <div class="form-group">
					First Name <br />
					<input value="<?php echo $firstname; ?>" placeholder="Enter your first name" type="text" name="firstname" class="form-control"  style="width: 96%;"/>
				</div>	
                
                 <div class="form-group">
					Last Name <br />
					<input value="<?php echo $lastname; ?>" placeholder="Enter your last name" type="text" name="lastname" class="form-control"  style="width: 96%;"/>
				</div>
                
                <div class="form-group">
			         Company <br />
					 <input value="<?php echo $company; ?>" placeholder="Enter your company name (optional)" type="text" name="company" class="form-control"  style="width: 96%;"/>
				</div>  
                
                <div class="form-group">
			          Telephone <br />
					 <input value="<?php echo $telephone; ?>" placeholder="Enter your phone no." type="text" name="telephone" class="form-control bfh-phone" data-country="country" style="width: 96%;" />
				</div>
                
                <div class="form-group">
			         Country <br />
					 <select id="country" name="country" class="form-control bfh-countries" data-country="<?php echo $country != '' ? $country : 'IN'; ?>" style="width: 96%;"></select>
				</div>
                
        </div><!-- /.col-md-6 -->
        
        <div class="col-md-6">
           
				<div class="form-group">
					Full Name <br />
						<input type="text" class="form-control" name="full" />
				</div>
				
                <div class="form-group">
					Password <br />
						<input type="password" class="form-control" name="password" />
				</div>
                
            <div class="form-group">
		          Address 1 <br />
				 <input value="<?php echo $address1; ?>" placeholder="Enter your home address" type="text" name="address1" class="form-control"  style="width: 96%;"/>
			</div>    
            
            <div class="form-group">
		         Address 2 <br />
				 <input value="<?php echo $address2; ?>" placeholder="Address line 2 (optional)" type="text" name="address2" class="form-control"  style="width: 96%;"/>
			</div> 
            
            <div class="form-group">
		        City <br />
	 	        <input value="<?php echo $city; ?>" placeholder="Enter your city" type="text" name="city" class="form-control"  style="width: 96%;"/>
			</div>
            
            <div class="form-group">
		         Post Code <br />
				 <input value="<?php echo $postcode; ?>" placeholder="Enter your postal code" type="text" name="postcode" class="form-control"  style="width: 96%;"/>
			</div>  
            
            <div class="form-group">
		          Region / State <br />
                  <?php $state = ($state != '' ? 'data-state="'.$state.'"' : ''); ?>
				 <select name="state" class="form-control bfh-states" data-country="country" <?php echo $state; ?> style="width: 96%;"></select>
			</div>
                
        </div><!-- /.col-md-6 --> 
        <div class="col-md-12">
                        <br /> 
                <input type="hidden" value="1" name="addUser" />
                <input type="hidden" value="No State" name="stateStr" id="stateStr" />
                <input class="btn btn-primary" type="submit" value="Save" /> 
                <br /> <br />
        </div>
        </form> 
                    
                <?php }else { ?>
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="mySitesTable">
                	<thead>
                		<tr>
                              <th>Username</th>
                              <th>Email ID</th>
                              <th>User Since</th>
                              <th>First Name</th>
                              <th>Last Name</th>
                              <th>Country</th>
                              <th>Ban User</th>
                              <th>Actions</th>
                		</tr>
                	</thead>         
                    <tbody>                        
                    </tbody>
                </table>
                <?php } ?>
                <br />
                
                </div><!-- /.box-body -->

              </div><!-- /.box -->
      
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<script>
function fixState(){
    var stateStr = $.trim($('select[name=state] option:selected').text());
    $('#stateStr').val(stateStr);
    return true;
}
</script>