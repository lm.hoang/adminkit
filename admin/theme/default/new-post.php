<?php

defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: Turbo Website Reviewer
 * @copyright � 2017 ProThemes.Biz
 *
 */
?>
  <link rel="stylesheet" href="<?php themeLink('plugins/fancybox/jquery.fancybox.min.css'); ?>" />

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $pageTitle; ?>  
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php adminLink(); ?>"><i class="<?php getAdminMenuIcon($controller,$menuBarLinks); ?>"></i> Admin</a></li>
        <li class="active"><a href="<?php adminLink($controller); ?>"><?php echo $pageTitle; ?></a> </li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $subTitle; ?></h3>
            </div><!-- /.box-header ba-la-ji -->
            <form action="#" method="POST" onsubmit="return finalFixedLink();">
            <div class="box-body">
          
            <?php if(isset($msg)) echo $msg; ?><br />
                            
                <div class="row">
                    <div class="col-md-6">
                    <div class="form-group">
                      <label for="post_title">Post Title</label>
                      <input type="text" placeholder="Enter your post title" value="<?php echo $post_title; ?>" name="post_title" class="form-control" />
                    </div>
                    <div class="form-group">
                      <label for="post_url">Post URL</label><small id="linkBox"> (<?php echo $baseURL .'blog/'; ?>) </small>
                      <input type="text" id="pageUrlBox" placeholder="Enter your post url" value="<?php echo $post_url; ?>" name="post_url" class="form-control" />
                    </div>
                    
                    <div class="form-group">
                      <label for="meta_des">Meta Description</label>
                      <textarea placeholder="Description must be within 150 Characters" rows="3" name="meta_des" class="form-control"><?php echo $meta_des; ?></textarea>
                   </div>
                   
                   <div class="form-group">
                   <label for="thumbnail">Featured Image</label>
                   <div class="input-group">
                        <input type="text" id="thumbnail" value="<?php echo $featured_image; ?>" class="form-control" name="thumbnail" />                                    
                        <span class="input-group-addon">
                        <a class="fa fa-picture-o iframe-btn" href="<?php echo $baseURL; ?>core/library/filemanager/dialog.php?type=1&field_id=thumbnail"></a>
                        </span>
                   </div>
                   </div>   
                              
                   <div class="form-group">
                        <label>Status</label>
                        <select name="post_enable" class="form-control"> 
                            <option <?php isSelected($post_enable, true, '1'); ?> value="on">Published</option>
                            <option <?php isSelected($post_enable, false, '1'); ?> value="off">Unpublished</option>
                        </select>             
                    </div>
                    
                   <div class="form-group"> 
                      <label for="loginreq">Who can access this page?</label>
                      <select class="form-control" name="loginreq">
                        <option <?php isSelected($loginreq,true,'1','all'); ?> value="all">All Users</option>
                        <option <?php isSelected($loginreq,true,'1','registered'); ?> value="registered">Only Registered Users</option>
                      </select>
                   </div> 
                                                              
                    </div><!-- /.col-md-6 -->
                    
                    <div class="col-md-6">
                    
                    <div class="form-group">
                      <label for="category">Category</label>
                      <input type="text" placeholder="Enter the category name" value="<?php echo $category; ?>" name="category" class="form-control" />
                    </div>
                    
                    <div class="form-group">
                      <label for="posted_date">Posted Date</label>

                      <?php if($posted_date == '') { ?>
                      <input type="text" placeholder="Enter your posted date" id="postedDate" name="posted_date" class="form-control" />
                      <?php } else { ?>
                      <input type="text" placeholder="Enter your posted date" value="<?php echo $posted_date; ?>" id="postedDate" name="posted_date" class="form-control" />
                      <?php } ?>
                    </div>
                    
                    <div class="form-group">
                      <label for="meta_tags">Meta Keywords (Separate with commas)</label>
                      <textarea placeholder="keywords1, keywords2, keywords3" rows="3" name="meta_tags" class="form-control"><?php echo $meta_tags; ?></textarea>
                   </div>
                   
                    <div class="form-group">
                      <label for="posted_by">Posted By</label>
                      <input type="text" placeholder="Enter the posted by" value="<?php echo $posted_by; ?>" name="posted_by" class="form-control" />
                    </div>
                    
                    <div class="form-group">
                        <label>Allow comment</label>
                        <select name="allow_comment" class="form-control"> 
                            <option <?php isSelected($allow_comment, true, '1'); ?> value="on">Yes</option>
                            <option <?php isSelected($allow_comment, false, '1'); ?> value="off">No</option>
                        </select>             
                    </div> 
                    
                   <div class="form-group"> 
                      <label for="pageLangCode">Show the page for all language users ?</label>
                      <select class="form-control" name="pageLangCode">
                        <option value="all">All Languages</option>
                        <option disabled="">Available Languages</option>
                        <?php foreach(getAvailableLanguages($con) as $langBalaji){
                            echo '<option '. isSelected($pageLangCode,true,'1',$langBalaji[2],true).' value="'.$langBalaji[2].'">'.$langBalaji[3].' Only</option>';
                        }
                        ?>
                      </select>
                   </div>

                    </div>
                </div><!-- /.row -->
                
                <div class="row">
                 
                 <div class="form-group" style="margin: 12px;">
                      <label for="post_content">Page Content</label>
                      <textarea id="editor1" name="post_content" class="form-control"><?php echo $post_content; ?></textarea>
                   </div>
                 
                 </div>
                 <?php if(isset($_GET['edit'])){ ?>
                 <input type="hidden" name="editPage" value="1" />
                 <input type="hidden" name="editID" value="<?php echo $editID; ?>" />
                 <?php } else { ?>
                 <input type="hidden" name="newPage" value="1" />
                 <?php } ?>
                <input type="submit" name="save" value="Save" class="btn btn-primary"/>
                <br />
                
                </div><!-- /.box-body -->
            </form>

              </div><!-- /.box -->
      
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<?php
$blogLink = createLink('blog/',true);
$filebrowserBrowseUrl = createLink('core/library/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',true);
$filebrowserUploadUrl = createLink('core/library/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',true);
$filebrowserImageBrowseUrl = createLink('core/library/filemanager/dialog.php?type=1&editor=ckeditor&fldr=',true);
$linkjs = themeLink('plugins/fancybox/jquery.fancybox.min.js',true); 
$footerAddArr[] = <<<EOD
<script>
var mainLink = "$blogLink";

$("#pageUrlBox").focus(function (){
    fixLinkBox()
    });
$("#pageUrlBox").keypress(function (){
    fixLinkBox()
    });
$("#pageUrlBox").blur(function (){
    fixLinkBox(); 
    });
$("#pageUrlBox").click(function (){
    fixLinkBox()
    });
    
function fixLinkBox(){
    var pageUrl= jQuery.trim($('input[name=post_url]').val());
    var ref = pageUrl.toLowerCase().replace(/[^a-z0-9]+/g,'-');
    $("#linkBox").html(" (" + mainLink + ref + ") "); 
}

function finalFixedLink(){
    var pageUrl= jQuery.trim($('input[name=post_url]').val());
    var ref = pageUrl.toLowerCase().replace(/[^a-z0-9]+/g,'-');
    $("#pageUrlBox").val(ref); 
    return true;
}

</script>

<script type="text/javascript">
$(function () {
CKEDITOR.replace('editor1',{ filebrowserBrowseUrl : '$filebrowserBrowseUrl', filebrowserUploadUrl : '$filebrowserUploadUrl', filebrowserImageBrowseUrl : '$filebrowserImageBrowseUrl' });
CKEDITOR.on( 'dialogDefinition', function( ev ){

var dialogName = ev.data.name;
var dialogDefinition = ev.data.definition;

if ( dialogName == 'link' || dialogName == 'image' ){
dialogDefinition.removeContents( 'Upload' );
}
});

});
</script>
<script src="$linkjs"></script>
<script type="text/javascript">
 jQuery(document).ready(function ($) {
    
    $('#postedDate').daterangepicker({locale: {
    format: 'MM/DD/YYYY h:mm A'
    },singleDatePicker: true, timePicker: true, format: 'MM/DD/YYYY h:mm A'});
            
      $('.iframe-btn').fancybox({
        'width': '75%',
        'height': '90%',
        'autoScale': false,
        'autoDimensions': false,
        'transitionIn': 'none',
        'transitionOut': 'none',
        'type': 'iframe'
    });
});
</script>
<style>.fancybox-content{ height: 500px !important; }</style>
EOD;
?>