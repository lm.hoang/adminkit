<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright © 2018 ProThemes.Biz
 *
 */
?>  
<style>
.alert-success {
    background-color: #dff0d8 !important;
    border-color: #d6e9c6 !important;
    color: #3c763d !important;
}
.lineCode{
    padding: 5px;
}
</style> 

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $pageTitle; ?>  
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php adminLink(); ?>"><i class="<?php getAdminMenuIcon($controller,$menuBarLinks); ?>"></i> Admin</a></li>
        <li class="active"><a href="<?php adminLink($controller); ?>"><?php echo $pageTitle; ?></a> </li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Templates</h3>                  
            </div><!-- /.box-header -->

            <form action="#" method="POST">
            <div class="box-body">
             <?php
            if(isset($msg)){
                echo $msg;
            }?>
            
            <div class="row">
                
                <div class="col-md-12">
                <br /> <h4>Order Confirmation Mail Template</h4>
                </div>
                
                <div class="col-md-8">
                    <div class="form-group">
                      <label for="confirmationSub">Subject</label>
                      <input class="form-control" type="text" name="confirmationSub" value="<?php echo $confirmationSub; ?>" />
                    </div>
                    
                    <div class="form-group">
                      <label for="confirmation">Mail Content</label>
                      <textarea id="editor1" name="confirmation" class="form-control"><?php echo $confirmationMail; ?></textarea>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <label>Replacement Codes</label>
                    
                    <div class="well alert-success">
                        <div class="lineCode"><b>{OrderId}</b> - Returns order id<br /></div>
                        <div class="lineCode"><b>{FirstName}</b> - Returns customers name<br /></div>
                        <div class="lineCode"><b>{InvoiceNo}</b> - Returns invoice number<br /></div>
                        <div class="lineCode"><b>{AmountWithTax}</b> - Returns total amount with tax<br /></div>
                        <div class="lineCode"><b>{PlanName}</b> - Returns plan name<br /></div>
                        <div class="lineCode"><b>{TransactionId}</b> - Returns transaction number<br /></div>
                        <div class="lineCode"><b>{SiteName}</b> - Returns your site name<br /></div>
                        <div class="lineCode"><b>{InvoiceLink}</b> - Returns invoice link<br /></div>
                    </div>
                    
                </div>
               
                <div class="col-md-12">
                    <br /><hr /><br /><h4>Invoice Payment Reminder Mail Template</h4><br />
                </div>
                                    
                <div class="col-md-8">
                    <div class="form-group">
                      <label for="reminderSub">Subject</label>
                      <input class="form-control" type="text" name="reminderSub" value="<?php echo $reminderSub; ?>" />
                    </div>
                    
                    <div class="form-group">
                      <label for="reminder">Mail Content</label>
                      <textarea id="reminder" name="reminder" class="form-control"><?php echo $reminderMail; ?></textarea>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <label>Replacement Codes</label>
                    
                    <div class="well alert-success">
                        <div class="lineCode"><b>{FirstName}</b> - Returns customers name<br /></div>
                        <div class="lineCode"><b>{InvoiceNo}</b> - Returns invoice number<br /></div>
                        <div class="lineCode"><b>{InvoiceDate}</b> - Returns invoice date<br /></div>
                        <div class="lineCode"><b>{DueDate}</b> - Returns total amount due<br /></div>
                        <div class="lineCode"><b>{GatewayName}</b> - Returns payment gateway name<br /></div>
                        <div class="lineCode"><b>{AmountWithTax}</b> - Returns total amount with tax<br /></div>
                        <div class="lineCode"><b>{SiteName}</b> - Returns your site name<br /></div>
                        <div class="lineCode"><b>{InvoiceLink}</b> - Returns invoice link<br /></div>
                    </div>
                    
                </div>
                
                <div class="col-md-12">
                    <br /><hr /><br /><h4>Invoice Payment Confirmation Mail Template</h4><br />
                </div>
                                    
                <div class="col-md-8">
                    <div class="form-group">
                      <label for="invoiceSub">Subject</label>
                      <input class="form-control" type="text" name="invoiceSub" value="<?php echo $invoiceSub; ?>" />
                    </div>
                    
                    <div class="form-group">
                      <label for="invoice">Mail Content</label>
                      <textarea id="invoice" name="invoice" class="form-control"><?php echo $invoiceMail; ?></textarea>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <label>Replacement Codes</label>
                    
                    <div class="well alert-success">
                        <div class="lineCode"><b>{FirstName}</b> - Returns customers name<br /></div>
                        <div class="lineCode"><b>{PaymentStats}</b> - Returns payment status<br /></div>
                        <div class="lineCode"><b>{InvoiceNo}</b> - Returns invoice no<br /></div>
                        <div class="lineCode"><b>{InvoiceDate}</b> - Returns invoice date<br /></div>
                        <div class="lineCode"><b>{AmountWithTax}</b> - Returns total amount with tax<br /></div>
                        <div class="lineCode"><b>{SiteName}</b> - Returns your site name<br /></div>
                        <div class="lineCode"><b>{InvoiceLink}</b> - Returns invoice link<br /></div>
                        <div class="lineCode"><b>{TransactionId}</b> - Returns transaction id<br /></div>
                    </div>
                    
                </div>
            
                <div class="col-md-12">
                    <br /><br />
                    <input type="submit" name="save" value="Update Templates" class="btn btn-primary"/>
                    <br /><br />
                </div>
            </div>
            </div><!-- /.box-body -->
            </form>

          </div><!-- /.box -->
  
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->
<?php
$filebrowserBrowseUrl = createLink('core/library/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',true);
$filebrowserUploadUrl = createLink('core/library/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',true);
$filebrowserImageBrowseUrl = createLink('core/library/filemanager/dialog.php?type=1&editor=ckeditor&fldr=',true);
$footerAddArr[] = <<<EOD
<script type="text/javascript">
    $(function () {
    CKEDITOR.replace('editor1',{ filebrowserBrowseUrl : '$filebrowserBrowseUrl', filebrowserUploadUrl : '$filebrowserUploadUrl', filebrowserImageBrowseUrl : '$filebrowserImageBrowseUrl', toolbar : 'Basic' });
    CKEDITOR.on( 'dialogDefinition', function( ev ) {
      var dialogName = ev.data.name;
      var dialogDefinition = ev.data.definition;
      if ( dialogName == 'link' || dialogName == 'image' ){
         dialogDefinition.removeContents( 'Upload' );
      }
   });
    CKEDITOR.replace('reminder',{ filebrowserBrowseUrl : '$filebrowserBrowseUrl', filebrowserUploadUrl : '$filebrowserUploadUrl', filebrowserImageBrowseUrl : '$filebrowserImageBrowseUrl', toolbar : 'Basic' });
    CKEDITOR.on( 'dialogDefinition', function( ev ) {
      var dialogName = ev.data.name;
      var dialogDefinition = ev.data.definition;
      if ( dialogName == 'link' || dialogName == 'image' ){
         dialogDefinition.removeContents( 'Upload' );
      }
   });
    CKEDITOR.replace('invoice',{ filebrowserBrowseUrl : '$filebrowserBrowseUrl', filebrowserUploadUrl : '$filebrowserUploadUrl', filebrowserImageBrowseUrl : '$filebrowserImageBrowseUrl', toolbar : 'Basic' });
    CKEDITOR.on( 'dialogDefinition', function( ev ) {
      var dialogName = ev.data.name;
      var dialogDefinition = ev.data.definition;
      if ( dialogName == 'link' || dialogName == 'image' ){
         dialogDefinition.removeContents( 'Upload' );
      }
   });
   });
</script>
EOD;
?>