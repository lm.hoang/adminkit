<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright 2018 ProThemes.Biz
 *
 */
?>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    
    <script type="text/javascript" language="javascript" src="<?php themeLink('dist/js/jquery.dataTables.js'); ?>"></script>
    
    <script type="text/javascript" language="javascript" class="init">
    
    $(document).ready(function() {
    	$('#mySitesTable').dataTable( {
    		"processing": true,
    		"serverSide": true,
    		"ajax": "<?php adminLink('?route=premium_ajax&managePlans');  ?>"
    	} );
    } );
    
    </script>
    
          <!-- Content Wrapper. Contains page content -->
          <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1>
                <?php echo $pageTitle; ?>  
                <small>Control panel</small>
              </h1>
              <ol class="breadcrumb">
                <li><a href="<?php adminLink(); ?>"><i class="<?php getAdminMenuIcon($controller,$menuBarLinks); ?>"></i> Admin</a></li>
                <li class="active"><a href="<?php adminLink($controller); ?>"><?php echo $pageTitle; ?></a> </li>
              </ol>
            </section>
        
            <!-- Main content -->
            <section class="content">

              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Plan List</h3>
                  
                  <div style="position:absolute; top:4px; right:15px;">
                  <a href="<?php adminLink('add-plan');  ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Add Plan</a>
                  </div>
                  
                </div><!-- /.box-header -->

                <div class="box-body">
                <?php
                if(isset($msg)){
                    echo $msg;
                }?>

                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="mySitesTable">
                	<thead>
                		<tr>
                              <th>Plan Name</th>
                              <th>Payment Type</th>
                              <th>Premium Tools</th>
                              <th>PDF Reports</th>
                              <th>Status</th>
                              <th>Actions</th>
                		</tr>
                	</thead>         
                    <tbody>                        
                    </tbody>
                </table>
                <br />
                
                </div><!-- /.box-body -->

              </div><!-- /.box -->
      
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->