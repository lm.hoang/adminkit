<?php

defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: Turbo Website Reviewer
 * @copyright © 2017 ProThemes.Biz
 *
 */

$fullLayout = 1;
$pageTitle = 'Manage Blog Posts';
$subTitle = 'Post List'; $footerAdd = true; $footerAddArr = array();

if($pointOut == 'postSuccess')
    $msg = successMsgAdmin('New post added successfully!');

if($pointOut == 'editSuccess')
    $msg = successMsgAdmin('Edited post saved successfully!');

if($pointOut == 'status'){
        
    if(isset($args[0]) && isset($args[2])){
        
    $post_status = escapeTrim($con, $args[0]);
    $post_id = escapeTrim($con, intval($args[2]));

    if($post_status == '2')
        $post_enable = 'off';  
    else
        $post_enable = 'on';  
    
    $query = "UPDATE blog_content SET post_enable='$post_enable' WHERE id='$post_id'";
    mysqli_query($con, $query);

    if (mysqli_errno($con))
        $msg = errorMsgAdmin(mysqli_error($con));
    else
        $msg = successMsgAdmin('Post settings saved successfully');
        
    }
}


if ($pointOut == 'delete'){
    
    if(isset($args[0])){ 
        $delete = escapeTrim($con, intval($args[0]));
        $result = mysqli_query($con, "DELETE FROM blog_content WHERE id=$delete");
    
        if (mysqli_errno($con))
            $msg = errorMsgAdmin(mysqli_error($con));
        else
            $msg = successMsgAdmin('Post deleted from database successfully.');
    }
}

if(isset($_GET['manageBlogPosts'])){
    
    $table = 'blog_content'; $primaryKey = 'id';
    
    $columns = array(
    	array( 'db' => 'id',   'dt' => 0 ),
    	array( 'db' => 'post_title', 'dt' => 1 ),
    	array( 'db' => 'category',  'dt' => 2 ),
    	array( 'db' => 'date',  'dt' => 3),
    	array( 'db' => 'post_enable',   'dt' => 4 ),
    	array( 'db' => 'pageview',   'dt' => 5 ),
    	array( 'db' => 'post_url',   'dt' => 6 )
    );
    
    $columns2 = array(
    	array( 'db' => 'post_title', 'dt' => 0 ),
    	array( 'db' => 'category',  'dt' => 1 ),
    	array( 'db' => 'date',  'dt' => 2),
    	array( 'db' => 'pageview',   'dt' => 3 ),
    	array( 'db' => 'post_enable',   'dt' => 4 ),
    	array( 'db' => 'action',   'dt' => 5)
    );
    
    $sql_details = array( 'user' => $dbUser, 'pass' => $dbPass, 'db'   => $dbName, 'host' => $dbHost );
    
    echo json_encode(SSPBLOG::simple($_GET, $sql_details, $table, $primaryKey, $columns, $columns2));
    
    die();   
}

?>