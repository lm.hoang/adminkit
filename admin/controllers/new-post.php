<?php

defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: Turbo Website Reviewer
 * @copyright © 2017 ProThemes.Biz
 *
 */

$pageTitle = 'Create Blog Post';
$subTitle = 'New Post';
$fullLayout = 1; $footerAdd = $status = $post_enable = $allow_comment = true; $footerAddArr = array();
$page_id = $editID = $post_title = $post_url = $meta_des = $meta_tags = $featured_image = $category = '';
$posted_date = $post_content = $posted_by = $loginreq = $pageLangCode = '';
    
//Load Blog Settings
$result = mysqli_query($con, "SELECT * FROM blog where id='1'");
$row = mysqli_fetch_array($result);
$posted_by = $row['posted_by'];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    
    if (!isset($_POST['editPage'])) {
        $post_title = escapeTrim($con, $_POST['post_title']);
        $post_url = escapeTrim($con, $_POST['post_url']);
        $meta_des = escapeTrim($con, $_POST['meta_des']);
        $featured_image = escapeTrim($con, $_POST['thumbnail']);
        $post_enable = escapeTrim($con, $_POST['post_enable']);
        $category = strtolower(escapeTrim($con, $_POST['category']));
        $meta_tags = escapeTrim($con, $_POST['meta_tags']);
        $posted_date = escapeTrim($con, $_POST['posted_date']);
        $post_content = escapeTrim($con, $_POST['post_content']);
        $posted_by = escapeTrim($con, $_POST['posted_by']);
        $allow_comment = escapeTrim($con, $_POST['allow_comment']);
        $pageLangCode = escapeTrim($con,$_POST['pageLangCode']);
        $loginreq = escapeTrim($con,$_POST['loginreq']);
        
        $query = "INSERT INTO blog_content (post_title,post_url,meta_des,featured_image,post_enable,category,meta_tags,date,post_content,posted_by,allow_comment,pageview,lang,access) VALUES ('$post_title', '$post_url', '$meta_des', '$featured_image', '$post_enable', '$category', '$meta_tags', '$posted_date', '$post_content', '$posted_by', '$allow_comment', '0', '$pageLangCode', '$loginreq')";

        if (!mysqli_query($con, $query)){
            $msg = errorMsgAdmin('Something Went Wrong!');
        } else {
            header('Location: '. adminLink('manage-blog/postSuccess',true),true,302);
            exit();
        }
        
    }else{
        $post_title = escapeTrim($con, $_POST['post_title']);
        $post_url = escapeTrim($con, $_POST['post_url']);
        $meta_des = escapeTrim($con, $_POST['meta_des']);
        $featured_image = escapeTrim($con, $_POST['thumbnail']);
        $post_enable = escapeTrim($con, $_POST['post_enable']);
        $category = strtolower(escapeTrim($con, $_POST['category']));
        $meta_tags = escapeTrim($con, $_POST['meta_tags']);
        $posted_date = escapeTrim($con, $_POST['posted_date']);
        $post_content = escapeTrim($con, $_POST['post_content']);
        $posted_by = escapeTrim($con, $_POST['posted_by']);
        $allow_comment = escapeTrim($con, $_POST['allow_comment']);
        $editID = escapeTrim($con, $_POST['editID']);
        $pageLangCode = escapeTrim($con,$_POST['pageLangCode']);
        $loginreq = escapeTrim($con,$_POST['loginreq']);
        
        $query = "UPDATE blog_content SET post_title='$post_title', post_url= '$post_url', meta_des= '$meta_des',featured_image='$featured_image', post_enable='$post_enable',category='$category',meta_tags='$meta_tags',date='$posted_date',post_content='$post_content',posted_by='$posted_by',allow_comment='$allow_comment',lang='$pageLangCode', access='$loginreq' WHERE id='$editID'";
    
        if (!mysqli_query($con, $query)){
            $msg = errorMsgAdmin('Something Went Wrong!');
        } else{
            header('Location: '. adminLink('manage-blog/editSuccess',true),true,302);
            exit();
        }
    }
}

if(isset($_GET['edit'])){
    $page_id = escapeTrim($con, intval($_GET['edit']));
    $result = mysqli_query($con, "SELECT * FROM blog_content where id='$page_id'");
    $row = mysqli_fetch_array($result);
    $editID = $row['id'];
    $post_title = $row['post_title'];
    $post_url = $row['post_url'];
    $meta_des = $row['meta_des'];
    $meta_tags = $row['meta_tags'];
    $featured_image = $row['featured_image'];
    $post_enable = filter_var($row['post_enable'], FILTER_VALIDATE_BOOLEAN);
    $category = $row['category'];
    $posted_date = $row['date'];
    $post_content = $row['post_content'];
    $posted_by = $row['posted_by'];
    $allow_comment = filter_var($row['allow_comment'], FILTER_VALIDATE_BOOLEAN);
    $loginreq =  $row['access'];
    $pageLangCode = trim($row['lang']);
}
?>