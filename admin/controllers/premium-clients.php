<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright © 2018 ProThemes.Biz
 *
 */
 
$fullLayout = 1;
$pageTitle = 'Premium Clients';
$headTitle = 'Premium Customer List';

if (isset($_GET['success'])) {
     $msg = '<div class="alert alert-success alert-dismissable">
     <strong>Alert!</strong> New User added successfully!
     </div>';
}

if (isset($_GET['newUser'])) {
    if (isset($_POST['addUser'])) {
        $username = raino_trim($_POST['username']);
        $email = raino_trim($_POST['email']);
        $full = raino_trim($_POST['full']);
        $password = passwordHash(raino_trim($_POST['password']));
        $ip = getUserIP();
        $date = date('jS F Y');
       
        $firstname = raino_trim($_POST['firstname']);
        $lastname = raino_trim($_POST['lastname']);
        $company = raino_trim($_POST['company']);
        $telephone = raino_trim($_POST['telephone']);
        $country = raino_trim($_POST['country']);
        $address1 = raino_trim($_POST['address1']);
        $address2 = raino_trim($_POST['address2']);
        $city = raino_trim($_POST['city']);
        $postcode = raino_trim($_POST['postcode']);
        $state = raino_trim($_POST['state']);
        $stateStr = raino_trim($_POST['stateStr']);
        $userData = base64_encode(serialize(array()));
        $nowDate = date('m/d/Y h:i:sA'); 

        $query = "INSERT INTO users (oauth_uid,username,email_id,full_name,platform,password,verified,picture,date,ip,firstname,lastname,company,telephone,address1,address2,city,state,postcode,country,statestr,userdata,added_date) VALUES ('0','$username','$email','$full','Direct','$password','1','NONE','$date','$ip', '$firstname', '$lastname', '$company', '$telephone', '$address1', '$address2', '$city', '$state', '$postcode', '$country', '$stateStr', '$userData', '$nowDate')"; 
        mysqli_query($con,$query); 
        
        if (mysqli_error($con)){
            //Error
            $msg = '<div class="alert alert-danger alert-dismissable">
             <strong>Alert!</strong> ' . mysqli_error($con) . '
             </div>';
        }else{
            //Pass
            $userID = getUserID($username,$con);
            $query = "INSERT INTO premium_users (firstname,lastname,company,telephone,address1,address2,city,state,postcode,country,added_date,ip_address,client_id,username,statestr,userdata,pdf_data,pdf_limit) VALUES ('$firstname', '$lastname', '$company', '$telephone', '$address1', '$address2', '$city', '$state', '$postcode', '$country', '$nowDate', '$ip', '$userID', '$username', '$stateStr', '$userData', '', '')"; //New Premium User        
            if (mysqli_query($con, $query)) {
                header('Location: '.adminLink('premium-clients&success',true));
            }
        }
        
    }
}

if (isset($_GET['delete'])) {
    $user_id = raino_trim($_GET['delete']);
    $premium_id = raino_trim($_GET['id']);
    $query = "DELETE FROM users WHERE id=$user_id";
    
    $result = mysqli_query($con, $query);
    if (mysqli_errno($con))
    {
        $msg = '<div class="alert alert-danger alert-dismissable">
         <strong>Alert!</strong> ' . mysqli_error($con) . '
         </div>';
    } else {
        
        $query = "DELETE FROM premium_users WHERE id=$premium_id";
        $result = mysqli_query($con, $query);
        if (mysqli_errno($con))
        {
            $msg = '<div class="alert alert-danger alert-dismissable">
             <strong>Alert!</strong> ' . mysqli_error($con) . '
             </div>';
        }else{
            $msg = '<div class="alert alert-success alert-dismissable">
             <strong>Alert!</strong> User deleted from database successfully
             </div>';
        }
    }

}

if (isset($_GET['ban'])) {
    $ban_id = raino_trim($_GET['ban']);
    $query = "UPDATE users SET verified='2' WHERE id='$ban_id'";
    $result = mysqli_query($con, $query);
    if (mysqli_errno($con))
    {
        $msg = '<div class="alert alert-danger alert-dismissable">
     <strong>Alert!</strong> ' . mysqli_error($con) . '
     </div>';
    } else
    {
        $msg = '<div class="alert alert-success alert-dismissable">
         <strong>Alert!</strong> User banned successfully
         </div>';
    }

}

if (isset($_GET['unban'])) {
    $ban_id = raino_trim($_GET['unban']);
    $query = "UPDATE users SET verified='1' WHERE id='$ban_id'";
    $result = mysqli_query($con, $query);
    if (mysqli_errno($con))
    {
        $msg = '<div class="alert alert-danger alert-dismissable">
     <strong>Alert!</strong> ' . mysqli_error($con) . '
     </div>';
    } else
    {
        $msg = '<div class="alert alert-success alert-dismissable">
         <strong>Alert!</strong> User un-banned successfully
         </div>';
    }

}

if (isset($_GET['details'])) {
    $headTitle = 'Premium Customer Details';
    $detail_id = raino_trim($_GET['details']);
    $premium_id = raino_trim($_GET['id']);
    $query = "SELECT * FROM users WHERE id='$detail_id'";
    $result = mysqli_query($con, $query);
    while ($row = mysqli_fetch_array($result))
    {
        $user_oauth_uid = $row['oauth_uid'];
        $user_username = $row['username'];
        $user_email_id = $row['email_id'];
        $user_full_name = $row['full_name'];
        $user_platform = Trim($row['platform']);
        $user_verified = $row['verified'];
        $user_date = $row['date'];
        $user_ip = $row['ip'];
    }
    if ($user_oauth_uid == '0') {
        $user_oauth_uid = "None";
    }
    if ($user_verified == '0') {
        $user_verified = "Not verfied user";
    } elseif ($user_verified == '1') {
        $user_verified = "Verfied User / Active";
    } elseif ($user_verified == '2') {
        $user_verified = "Banned User";
    }
    $addInfo = true;
    extract(getPremiumUserInfo($user_username,$con));
}
?>