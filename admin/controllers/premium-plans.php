<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright � 2016 ProThemes.Biz
 *
 */
 
$fullLayout = 1;
$pageTitle = "Premium Plans";
$subTitle = '';

if(isset($_GET['postSuccess'])){
            $msg = '<div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
            <b>Alert!</b> New Plan added successfully!
            </div>
            ';
}

if(isset($_GET['editSuccess'])){
            $msg = '<div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
            <b>Alert!</b> Edited Plan saved successfully!
            </div>
            ';   
}

if(isset($_GET['status'])){
    $post_status = escapeTrim($con, $_GET['status']);
    $plan_id = escapeTrim($con, $_GET['id']);

    if($post_status == "2")
    $plan_status = "off";  
    else
    $plan_status = "on";  
    
    $query = "UPDATE premium_plans SET status='$plan_status' WHERE id='$plan_id'";
    mysqli_query($con, $query);

    if (mysqli_errno($con))
    {
        $msg = '<div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                        <b>Alert!</b> ' . mysqli_error($con) . '
                                    </div>';
    } else
    {
        $msg = '
        <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                        <b>Alert!</b> Plan settings saved successfully
                                    </div>';
    }
}


if (isset($_GET{'delete'}))
{
    $delete = raino_trim($_GET['delete']);
    $query = "DELETE FROM premium_plans WHERE id=$delete";
    $result = mysqli_query($con, $query);

    if (mysqli_errno($con))
    {
        $msg = '<div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                        <b>Alert!</b> ' . mysqli_error($con) . '
                                    </div>';
    } else
    {
        $msg = '
        <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                        <b>Alert!</b> Plan deleted from database successfully.
                                    </div>';
    }
}

?>