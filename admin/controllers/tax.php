<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright © 2018 ProThemes.Biz
 *
 */
 
$fullLayout = 1;
$pageTitle = 'Tax';
$newPage = false;
$headTitle = 'Settings';

if(isset($_GET['status'])) {
    
    $tax_status = escapeTrim($con, $_GET['status']);
    $tax_id = escapeTrim($con, $_GET['id']);

    if($tax_status == "2")
        $tax_enable = "false";  
    else
        $tax_enable = "true";  
    
    $query = "UPDATE premium_tax SET status='$tax_enable' WHERE id='$tax_id'";
    mysqli_query($con, $query);

    if (mysqli_errno($con)) {
        $msg = '<div class="alert alert-danger alert-dismissable">
                <i class="fa fa-ban"></i>
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                <b>Alert!</b> ' . mysqli_error($con) . '
            </div>';
    } else {
        $msg = '
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
            <b>Alert!</b> Tax settings saved successfully
            </div>';
    }
}

if(isset($_GET['delete'])) {
    
    $tax_id = escapeTrim($con, $_GET['id']);
    
    $query = "DELETE FROM premium_tax WHERE id='$tax_id'";
    mysqli_query($con, $query);

    if (mysqli_errno($con)) {
        $msg = '<div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                        <b>Alert!</b> ' . mysqli_error($con) . '
                                    </div>';
    } else {
        $msg = '
        <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                        <b>Alert!</b> Deleted successfully
                                    </div>';
    }
}

if(isset($_GET['new-rule'])) {
    $newPage = true;
    $headTitle = 'New Tax Rule';
    $countriesList = getCountries();
}

if(isset($_GET['edit'])) {
    $newPage = true;
    $headTitle = 'Edit Tax Rule';
    $countriesList = getCountries();
    
    $tax_id = escapeTrim($con, $_REQUEST['id']);
    $query = mysqli_query($con, "SELECT * FROM premium_tax WHERE id='$tax_id'");
    extract(mysqli_fetch_array($query));
    $rate_fixed = filter_var($rate_fixed, FILTER_VALIDATE_BOOLEAN);
    $dbCountryCode = $country;
}

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    
    if(isset($_POST['taxEnable'])){
        
        $tax_enable = raino_trim($_POST['tax_enable']);
        
        $query = "UPDATE order_settings SET enable_tax='$tax_enable' WHERE id='1'";
    
        if (!mysqli_query($con, $query)) {
            $msg = '<div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                    <b>Alert!</b> Something Went Wrong!
                    </div>';
        } else {
            $msg = '<div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                    <b>Alert!</b> Settings saved successfully!
                    </div>
                    ';
        }
        
    }
    
    if(isset($_POST['editTax'])){
        $tax_title = raino_trim($_POST['tax_title']);
        $tax_rate = raino_trim($_POST['tax_rate']);
        $rate_fixed = raino_trim($_POST['rate_fixed']);
        $dbCountryCode = raino_trim($_POST['country']);
        $status = raino_trim($_POST['tax_status']);
        
        $query = "UPDATE premium_tax SET tax_title='$tax_title', tax_rate='$tax_rate', country='$dbCountryCode', rate_fixed='$rate_fixed', status='$status' WHERE id='$tax_id'";
    
        if (!mysqli_query($con, $query)) {
            $msg = '<div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                    <b>Alert!</b> Something Went Wrong!
                    </div>';
        } else {
            $msg = '<div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                    <b>Alert!</b> Tax Settings saved successfully!
                    </div>
                    ';
        }
        $rate_fixed = filter_var($rate_fixed, FILTER_VALIDATE_BOOLEAN);
        
    }
    
    if(isset($_POST['newTax'])){
        
        $tax_title = raino_trim($_POST['tax_title']);
        $tax_rate = raino_trim($_POST['tax_rate']);
        $rate_fixed = raino_trim($_POST['rate_fixed']);
        $dbCountryCode = raino_trim($_POST['country']);
        $status = raino_trim($_POST['tax_status']);
        
        $query = "INSERT INTO premium_tax (tax_title,tax_rate,country,rate_fixed,status) VALUES ('$tax_title', '$tax_rate', '$dbCountryCode', '$rate_fixed', '$status')";
                            
        if (!mysqli_query($con, $query)) {
            $msg = '<div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                    <b>Alert!</b> Something Went Wrong!
                    </div>';
        } else {
            $msg = '<div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                    <b>Alert!</b> New Tax Rule added successfully!
                    </div>
                    <meta http-equiv="refresh" content="1,URL='.adminLink('tax',true).'">
                    ';
        }
        $rate_fixed = filter_var($rate_fixed, FILTER_VALIDATE_BOOLEAN);
        
    }
}

$query =  "SELECT * FROM premium_tax";
$result = mysqli_query($con,$query);
        
while($row = mysqli_fetch_array($result)){
    $taxActive = filter_var($row["status"], FILTER_VALIDATE_BOOLEAN);
    $rateFixed = filter_var($row["rate_fixed"], FILTER_VALIDATE_BOOLEAN);
    $taxName = $row["tax_title"];
    
    if($taxActive){
     $taxActive = '<a href="'.adminLink('tax&status=2&id='.$row["id"],true).'" class="label label-success">Active</a>';
    }else{
       $taxActive = '<a href="'.adminLink('tax&status=1&id='.$row["id"],true).'" class="label label-danger">Disabled</a>';
    }
    
    if($rateFixed){
        $curType = defaultCurrency($con);
        $currencySymbol = getCurrencySymbol($curType);
        $taxRate = $currencySymbol[0] . $row["tax_rate"];
        $taxType = 'Fixed Rate';
    }else{
       $taxRate = $row["tax_rate"] .' %';
       $taxType = 'Percentage';
    }
    
    if(strtolower($row["country"]) == 'all'){
        $taxCountry = 'All Countries';
    }else{
        $taxCountry = country_code_to_country(strtoupper($row["country"]));
    }
    $editLink = '<a class="btn btn-sm btn-primary" href="'.adminLink('tax&id='.$row["id"],true).'&edit">Edit</a>';
    $deleteLink = '<a class="btn btn-sm btn-danger" href="'.adminLink('tax&id='.$row["id"].'&delete',true).'">Delete</a>';
    
  $taxList[] = array($taxName,$taxRate,$taxType,$taxCountry,$taxActive,$editLink,$deleteLink);  
}

$orderData = orderSettings($con);
$tax  = filter_var($orderData['enable_tax'], FILTER_VALIDATE_BOOLEAN);

?>