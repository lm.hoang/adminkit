<?php

defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: Turbo Website Reviewer
 * @copyright © 2017 ProThemes.Biz
 *
 */

$pageTitle = 'Blog Settings';
$subTitle = 'Settings';
$fullLayout = 1; $footerAdd = true; $footerAddArr = array();

$result = mysqli_query($con, "SELECT * FROM blog where id='1'");
$row = mysqli_fetch_array($result);

$meta_des = $row['meta_des'];
$meta_tags = $row['meta_tags'];
$maximum_posts = $row['maximum_posts'];
$truncate = $row['truncate'];
$no_image = $row['no_image'];
$posted_by = $row['posted_by'];
$enable_comments = $row['show_comment'];
$blog_status = $row['blog_enable'];
$disqus_id = $row['discuss_id'];
$relatedArr = dbStrToArr($row['related_data']);
$enable_related = $relatedArr[0];
$related_count = $relatedArr[1];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    
    $related_data = '';
    $blog_status = escapeTrim($con, $_POST['blog_status']);
    $maximum_posts = escapeTrim($con, $_POST['maximum_posts']);
    $meta_des = escapeTrim($con, $_POST['meta_des']);
    $posted_by = escapeTrim($con, $_POST['posted_by']);
    $enable_comments = escapeTrim($con, $_POST['enable_comments']);
    $truncate = escapeTrim($con, $_POST['truncate']);
    $meta_tags = escapeTrim($con, $_POST['meta_tags']);
    $no_image = escapeTrim($con, $_POST['no_image']);
    $enable_related = escapeTrim($con, $_POST['enable_related']);
        
    if(isset($_POST['related_count']))
        $related_count = escapeTrim($con, $_POST['related_count']);
        
    if(isset($_POST['disqus_id']))
        $disqus_id = escapeTrim($con, $_POST['disqus_id']);
    else
        $disqus_id = escapeMe($con, $disqus_id);
    
    $related_data = arrToDbStr($con, array($enable_related,$related_count));
    
    $query = "UPDATE blog SET related_data='$related_data', meta_des='$meta_des', meta_tags='$meta_tags', maximum_posts='$maximum_posts', truncate='$truncate', no_image='$no_image', posted_by='$posted_by', show_comment='$enable_comments', discuss_id='$disqus_id', blog_enable='$blog_status' WHERE id='1'";

    if (!mysqli_query($con, $query)){
        echo mysqli_error($con); die(); //$msg = errorMsgAdmin('Something Went Wrong!');
    }else
        $msg = successMsgAdmin('Blog Settings saved successfully!');

}

$enable_comments = filter_var($enable_comments, FILTER_VALIDATE_BOOLEAN);
$blog_status = filter_var($blog_status, FILTER_VALIDATE_BOOLEAN);
$enable_related = filter_var($enable_related, FILTER_VALIDATE_BOOLEAN);
?>