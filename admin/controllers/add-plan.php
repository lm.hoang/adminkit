<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright � 2018 ProThemes.Biz
 *
 */

$pageTitle = "Add Premium Membership Plan";
$subTitle = "Plan Settings";

 $footerAdd = true; $footerAddArr = array();
$premiumTools = array();
$planAmount = $rec1 = $rec2 = $rec3 = $rec4 = '0.0';
$oneTimeSel = $recTimeSel = $rec1Sel = $rec2Sel = $rec3Sel = $rec4Sel = '';
$curDataList = getCurrencyList();
$selCurType = '$';
$plan_price = '0';
$plan_sub = 'Sub-Title';
$plan_type = 'Monthly';

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    
    $premium_tools = array();
    $recpays = array();
    $recpayAmt = array();
    
    $addedDate = date('m/d/Y h:i:sA');
    $plan_name = escapeTrim($con,$_POST['plan_name']);
    $plan_url = escapeTrim($con,$_POST['plan_url']);
    $plan_act = escapeTrim($con,$_POST['plan_act']);
    $allow_pdf = escapeTrim($con,$_POST['allow_pdf']);
    $plan_no = escapeTrim($con,$_POST['plan_no']);
    $plan_title = escapeTrim($con,$_POST['plan_title']);
    $plan_des = escapeTrim($con,$_POST['plan_des']);
    $brand_pdf = escapeTrim($con,$_POST['brand_pdf']);
    $plan_stats = escapeTrim($con,$_POST['plan_stats']);
    $plan_hide = escapeTrim($con,$_POST['plan_hide']);
    $featured = escapeTrim($con,$_POST['featured']);
    $max_pdf = escapeTrim($con,$_POST['max_pdf']);
    $payment_type = escapeTrim($con,$_POST['payment_type']);
    $onepay_cost = escapeTrim($con,$_POST['onepay_cost']);
    $captcha_free = escapeTrim($con,$_POST['captcha_free']);
    
    $plan_sub = escapeTrim($con,$_POST['plan_sub']);
    $cur_type = escapeTrim($con,$_POST['cur_type']); 
    $plan_price = escapeTrim($con,$_POST['plan_price']);
    $plan_type = escapeTrim($con,$_POST['plan_type']);
    $plan_features = escapeTrim($con,$_POST['plan_features']);
    $pricing_box = base64_encode(serialize(array($plan_sub,$cur_type,$plan_price,$plan_type,$plan_features)));

    foreach($_POST['premium_tools'] as $premium_tool)
       $premium_tools[] = escapeTrim($con,$premium_tool); 
    $premium_toolsStr = serialize($premium_tools);
    
    foreach($_POST['recpay'] as $recpay)
       $recpayAmt[] = escapeTrim($con,$recpay); 
       
    $recPayLoop = 0;
    $defaults = array_fill(0, 3, 'off');
    
    if($_POST['recpayCheck'] === null)
        $_POST['recpayCheck'] = array();
   
    $recpayChecks = $_POST['recpayCheck'] + $defaults;
    ksort($recpayChecks);

    foreach($recpayChecks as $recpayCheck){
       if(filter_var(escapeTrim($con,$recpayCheck), FILTER_VALIDATE_BOOLEAN)){
            $recpays[] = array($recpayAmt[$recPayLoop],true);
       }else{
            $recpays[] = array('0',false);
       }
       $recPayLoop++;
    }
    $recpaysStr = serialize($recpays);
   
    if(isset($_GET['edit'])){
        $editID = raino_trim($_GET['edit']);
        $query = "UPDATE premium_plans SET plan_name='$plan_name', title='$plan_title', plan_des='$plan_des', url='$plan_url', status='$plan_stats', hidden='$plan_hide', featured='$featured', activation='$plan_act', premium_tools='$premium_toolsStr', allow_pdf='$allow_pdf', brand_pdf='$brand_pdf', projects='$max_pdf', payment_type='$payment_type', one_time_fee='$onepay_cost', recurrent_fee='$recpaysStr', plan_id='$plan_no', captcha='$captcha_free', pricing_box='$pricing_box' WHERE id='$editID'";
    }else{
        $query = "INSERT INTO premium_plans (plan_name, added_date, title, plan_des, url, status, hidden, featured, activation, premium_tools, allow_pdf, brand_pdf, projects, payment_type, one_time_fee, recurrent_fee, plan_id, captcha, pricing_box) VALUES ('$plan_name', '$addedDate', '$plan_title', '$plan_des', '$plan_url', '$plan_stats', '$plan_hide', '$featured', '$plan_act', '$premium_toolsStr', '$allow_pdf', '$brand_pdf', '$max_pdf', '$payment_type', '$onepay_cost', '$recpaysStr', '$plan_no', '$captcha_free', '$pricing_box')";
    }
    
    if (!mysqli_query($con,$query))
    {
        $msg = '<div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
        <b>Alert!</b> Something Went Wrong!
        </div>';
    }else{
        if(isset($_GET['edit'])){
        $msg = '<div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
        <b>Alert!</b> Changes updated successfully!
        </div>
        <meta http-equiv="refresh" content="1;url='.adminLink('premium-plans',true).'">
        ';
        }else{
        $msg = '<div class="alert alert-success alert-dismissable">
        <i class="fa fa-check"></i>
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
        <b>Alert!</b> New premium plan added successfully!
        </div>
        <meta http-equiv="refresh" content="1;url='.adminLink('premium-plans',true).'">
        ';
        }
    }
}

if(isset($_GET['edit'])){
    $editID = raino_trim($_GET['edit']);
    
    $query = mysqli_query($con, "SELECT * FROM premium_plans WHERE id='$editID'");
    if(mysqli_num_rows($query) > 0){
        $data = mysqli_fetch_array($query);
        $plan_name = Trim($data['plan_name']);
        $plan_url = Trim($data['url']);
        $plan_des = Trim($data['plan_des']);
        $plan_no = Trim($data['plan_id']);
        $plan_title = Trim($data['title']);
        $max_pdf = Trim($data['projects']);
    
        $pricing_box = unserialize(base64_decode($data['pricing_box']));
        $plan_sub = $pricing_box[0];
        $cur_type = $pricing_box[1];
        $plan_price = $pricing_box[2];
        $plan_type = $pricing_box[3];
        $plan_features = strEOL($pricing_box[4]);
        $plan_featuresArr = explode(PHP_EOL,$plan_features);
        
        $data['premium_tools'] = unserialize($data['premium_tools']);
        
        if($data['payment_type'] == '0'){
            //One time Payment
            $oneTimeSel = 'checked=""';
            $planAmount = $data['one_time_fee'];
        }else{
            //Recurring Payment
            $recTimeSel = 'checked=""';
            $recurringPlanAmount = unserialize($data['recurrent_fee']);
            
            $rec1 = $recurringPlanAmount[0][0];
            $rec2 = $recurringPlanAmount[1][0];
            $rec3 = $recurringPlanAmount[2][0];
            $rec4 = $recurringPlanAmount[3][0];
            
            if($recurringPlanAmount[0][1])
                $rec1Sel = 'checked=""'; //Monthly
                
            if($recurringPlanAmount[1][1])
                $rec2Sel = 'checked=""'; //Every 3 months
          
            if($recurringPlanAmount[2][1])
                $rec3Sel = 'checked=""'; //Every 6 months
          
            if($recurringPlanAmount[3][1])
                $rec4Sel = 'checked=""'; //Every year
        }
                           
        if(filter_var($data['captcha'], FILTER_VALIDATE_BOOLEAN))
            $captcha_free_yes = 'selected';
        else
            $captcha_free_no = 'selected';
        
        if(filter_var($data['activation'], FILTER_VALIDATE_BOOLEAN))
            $plan_act_yes = 'selected';
        else
            $plan_act_no = 'selected';
            
        if(filter_var($data['allow_pdf'], FILTER_VALIDATE_BOOLEAN))
            $allow_pdf_yes = 'selected';
        else
            $allow_pdf_no = 'selected';
            
        if(filter_var($data['brand_pdf'], FILTER_VALIDATE_BOOLEAN))
            $brand_pdf_yes = 'selected';
        else
            $brand_pdf_no = 'selected';
            
        if(filter_var($data['status'], FILTER_VALIDATE_BOOLEAN))
            $plan_stats_yes = 'selected';
        else
            $plan_stats_no = 'selected';
        
        if(filter_var($data['hidden'], FILTER_VALIDATE_BOOLEAN))
            $plan_hide_yes = 'selected';
        else
            $plan_hide_no = 'selected';     
            
        if(filter_var($data['featured'], FILTER_VALIDATE_BOOLEAN))
            $featured_yes = 'selected';
        else
            $featured_no = 'selected';  
                       
    }else{
        $msg = '<div class="alert alert-danger alert-dismissable">
        <i class="fa fa-ban"></i>
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
        <b>Alert!</b> Plan not Found!
        </div>
        <meta http-equiv="refresh" content="1;url='.adminLink('premium-plans',true).'">
        ';
    }
}

$result = mysqli_query($con,"SELECT * FROM seo_tools WHERE tool_login='premium'");

while ($row = mysqli_fetch_array($result)){
    $premiumTools[] = array($row['id'],$row['tool_name']);
}

$fullLayout = 1;

?>