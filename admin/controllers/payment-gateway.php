<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));
define('ADMIN_PLG_DIR', ADMIN_DIR.'plugins'.DIRECTORY_SEPARATOR);

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright © 2018 ProThemes.Biz
 *
 */
 
$fullLayout = 1;
$pageTitle = "Payment Gateways";
$boxTitle = 'Gateways List';

if(isset($_GET['status'])) {
    
    $pay_status = escapeTrim($con, $_GET['status']);
    $pay_id = escapeTrim($con, $_GET['id']);

    if($pay_status == "2")
        $pay_enable = "off";  
    else
        $pay_enable = "on";  
    
    $query = "UPDATE payment_gateways SET active='$pay_enable' WHERE id='$pay_id'";
    mysqli_query($con, $query);

    if (mysqli_errno($con)) {
        $msg = '<div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                        <b>Alert!</b> ' . mysqli_error($con) . '
                                    </div>';
    } else {
        $msg = '
        <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                        <b>Alert!</b> Gateway settings saved successfully
                                    </div>';
    }
}

if(isset($_GET['settings'])){
    $pay_id = escapeTrim($con, $_GET['id']);
    $arrData = getPaymentGateway($con,$pay_id);
    $boxTitle = 'General Settings';
    $controlPath = ADMIN_PLG_DIR.'payments'.DIRECTORY_SEPARATOR.$arrData['gateway_name'].DIRECTORY_SEPARATOR.'control.php'; 
    if(file_exists($controlPath)){
        require($controlPath);
    }else{
        $pay_id = escapeTrim($con, $_GET['id']);
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $gateway = escapeTrim($con, $_POST['gateway']);
            $gateway_title = escapeTrim($con, $_POST['gateway_title']);
            $sort_order = escapeTrim($con, $_POST['sort_order']);

        $query = "UPDATE payment_gateways SET gateway_title='$gateway_title', sort_order='$sort_order', active='$gateway' WHERE id='$pay_id'";
    
        if (!mysqli_query($con, $query)) {
            $msg = '<div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                    <b>Alert!</b> Something Went Wrong!
                    </div>';
        } else {
            $msg = '<div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                    <b>Alert!</b> Settings saved successfully!
                    </div>
                    ';
        }
        }
        extract(getPaymentGateway($con,$pay_id));
    }
    $gateway = filter_var($active, FILTER_VALIDATE_BOOLEAN);
}

$paymentGatewayData = getPaymentGateways($con);
?>