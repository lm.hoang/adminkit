<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright © 2018 ProThemes.Biz
 *
 */
 
$fullLayout = 1;
$pageTitle = 'Orders';
$headTitle = 'Manage Orders';
$orderSettings = orderSettings($con);
$cur_type = $orderSettings['currency_type'];
$checkout_terms = unserialize($orderSettings['checkout_terms']);
$checkout = filter_var($checkout_terms[0], FILTER_VALIDATE_BOOLEAN);
$checkoutPage = Trim($checkout_terms[1]);

$defaultCurrency = defaultCurrency($con);
$defaultCurrencyData = '';

$query =  "SELECT * FROM premium_currency";
$result = mysqli_query($con,$query);
        
while($row = mysqli_fetch_array($result)){
    $currencyName = $row["currency_name"];
    $currencyCode = $row["currency_code"];
    $currencyRate = $row["currency_rate"];
    
    if($defaultCurrency == $currencyCode){
        $defaultCurrencyData .= '<option value="'.$currencyCode.'" selected="">'.$currencyName.' - '.$currencyCode.'</option>';
    }else{
        $defaultCurrencyData .= '<option value="'.$currencyCode.'">'.$currencyName.' - '.$currencyCode.'</option>';
    }
}

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    
    if(isset($_POST['addComment'])){
        $notify = filter_var(escapeTrim($con,$_POST['notify']), FILTER_VALIDATE_BOOLEAN);
        $myComment = escapeTrim($con,$_POST['myComment']);
        $orderCommentID = escapeTrim($con,$_POST['addCommentID']);
        $orderCommentDate = escapeTrim($con,$_POST['addCommentDate']);
        $sent_mail = escapeTrim($con,$_POST['addCommentMail']);
        
        $checkBack = addtoOrderComment($orderCommentID,$myComment,$con);
        if($checkBack === false){
             $msg = '<div class="alert alert-danger alert-dismissable">
            <i class="fa fa-ban"></i>
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
            <b>Alert!</b> Something Went Wrong!
            </div>'; 
        }else{
            $msg = '
            <div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
            <b>Alert!</b> Comment added successfully
            </div>';
        }
        
        if($notify){
            //Get site Info
            $query =  "SELECT * FROM site_info";
            $result = mysqli_query($con,$query);
                  
            while($row = mysqli_fetch_array($result)) {
                $site_name =   Trim($row['site_name']);
                $admin_mail =   Trim($row['email']);
                $admin_name = $site_name;
                $copyright =  htmlspecialchars_decode(Trim($row['copyright']));
            }
            
            //SMTP information 
            $query =  "SELECT * FROM mail WHERE id='1'";
            $result = mysqli_query($con,$query);
                    
            while($row = mysqli_fetch_array($result)) {
                    $smtp_host =   Trim($row['smtp_host']);
                    $smtp_user =  Trim($row['smtp_username']);
                    $smtp_pass =  Trim($row['smtp_password']);
                    $smtp_port =  Trim($row['smtp_port']);
                    $protocol =  Trim($row['protocol']);
                    $smtp_auth =  Trim($row['auth']);
                    $smtp_sec =  Trim($row['socket']);
            }
            
            $mail_type = $protocol;
            $subject = '['.$site_name.'] - Order Update '.$orderCommentID;
            $myComment = '
            Order ID: '.$orderCommentID.'
            Date Ordered: '.$orderCommentDate.'
                
            The comments for your order are:
            '.$myComment;
            
            $body = nl2br(html_entity_decode($myComment));
            
            if ($mail_type == '1')
                default_mail ($admin_mail,$admin_name,$sent_mail,$subject,$body);
            else
                smtp_mail($smtp_host,$smtp_port,$smtp_auth,$smtp_user,$smtp_pass,$smtp_sec,$admin_mail,$admin_name,$sent_mail,$subject,$body);

        }
    }
    
    if(isset($_POST['orderSettings'])){
        $checkout = escapeTrim($con,$_POST['checkout']);
        $checkoutPage = escapeTrim($con,$_POST['checkoutPage']);
        $cur_type = escapeTrim($con,$_POST['cur_type']);
        $checkout_terms = serialize(array($checkout,$checkoutPage));
        
        $query = "UPDATE order_settings SET currency_type='$cur_type', checkout_terms='$checkout_terms' WHERE id='1'";

        if (!mysqli_query($con, $query)) {
            $msg = '<div class="alert alert-danger alert-dismissable">
                    <i class="fa fa-ban"></i>
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                    <b>Alert!</b> Something Went Wrong!
                    </div>';
        } else {
            $msg = '<div class="alert alert-success alert-dismissable">
                    <i class="fa fa-check"></i>
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">&times;</button>
                    <b>Alert!</b> Order Settings saved successfully!
                    </div>
                    ';
        }
    }
    
    if(isset($_POST['editOrder'])){

        $orderID = escapeTrim($con, $_GET['edit']);
        $orderStatus = escapeTrim($con,$_POST['orderStatus']);
        $payStatus = escapeTrim($con,$_POST['payStatus']);
        $plan = escapeTrim($con,$_POST['plan']);
        $billType = escapeTrim($con,$_POST['billType']);
        $amount = escapeTrim($con,$_POST['amount']);
        $total = escapeTrim($con,$_POST['total']);
    
        $query = "UPDATE premium_orders SET billing_type='$billType', plan_id='$plan', amount_tax='$total', amount='$amount', payment_status='$payStatus', status='$orderStatus' WHERE id='$orderID'";        
        if (!mysqli_query($con,$query)) {
            $msg = '<div class="alert alert-danger alert-dismissable">
                                            <i class="fa fa-ban"></i>
                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                            <b>Alert!</b> ' . mysqli_error($con) . '
                                        </div>';
        } else {
            $msg = '
            <div class="alert alert-success alert-dismissable">
                                            <i class="fa fa-check"></i>
                                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                            <b>Alert!</b> Order details updated successfully
                                        </div>';
        }
    }
    
}

if(isset($_GET['delete'])) {
    
    $order_id = escapeTrim($con, $_GET['delete']);
    
    $query = "DELETE FROM premium_orders WHERE id='$order_id'";
    mysqli_query($con, $query);

    if (mysqli_errno($con)) {
        $msg = '<div class="alert alert-danger alert-dismissable">
                                        <i class="fa fa-ban"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                        <b>Alert!</b> ' . mysqli_error($con) . '
                                    </div>';
    } else {
        $msg = '
        <div class="alert alert-success alert-dismissable">
                                        <i class="fa fa-check"></i>
                                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                                        <b>Alert!</b> Order Deleted successfully
                                    </div>';
    }
}

if(isset($_GET['view']) || isset($_GET['edit'])) {
    
    if(isset($_GET['view'])){
        $order_id = escapeTrim($con, $_GET['view']);
        $headTitle = 'Order Information';
    }
    if(isset($_GET['edit'])){
        $order_id = escapeTrim($con, $_GET['edit']);
        $headTitle = 'Edit Order Information';
    }
    $billType = '';
    
    $query = mysqli_query($con, "SELECT * FROM premium_orders WHERE id='$order_id'");
    if(mysqli_num_rows($query) > 0){
        $orderInfo = mysqli_fetch_array($query);
        $premiumUserInfo = getPremiumUserInfo($orderInfo['username'],$con);
        $userInfo = getUserInfo($orderInfo['username'],$con);
        $paymentInfo = getPaymentGateway($con,$orderInfo['payment_type']);
        $userCountry = country_code_to_country($premiumUserInfo['country']);
        
        $orderComments = getOrderComments($orderInfo['id'],$con);
        $orderLogs = getOrderLog($orderInfo['id'],$con);
                
        $currencySymbol = getCurrencySymbol($orderInfo['currency_type']);
        $orderInfo['amount'] = con2money_format($orderInfo['amount'],$orderInfo['currency_type']);

        if($orderInfo['payment_status'] == 'completed')
            $viewPayStatus = 'Paid';
        elseif($orderInfo['payment_status'] == 'pending')
            $viewPayStatus = 'Unpaid';
        else
            $viewPayStatus = ucfirst($orderInfo['payment_status']);
        
        $subDate = $orderInfo['date'];     
        $oneBillType = $recBillType = '';
        
        if($orderInfo['billing_type'] == '0') {
            //One Time Fee Plans
            $billType = 'One Time Fee';
            $expDate = 'Lifetime';
            $oneBillType = 'selected=""';
        }else{
            //Recurring Fee Plans
            $billType = 'Recurring Fee';
            $recBillType = 'selected=""';
            
            if($orderInfo['rec_type'] == 'rec1'){
                //Monthly
                $expDate = date('m/d/Y h:i:sA', getUnixTimestamp($subDate,'1M')); 
                $billType .= '(Monthly)';
            }elseif ($orderInfo['rec_type'] == 'rec2'){
                //3 Months
                $expDate = date('m/d/Y h:i:sA', getUnixTimestamp($subDate,'3M')); 
                $billType .= '(3 Months)';
            }elseif ($orderInfo['rec_type'] == 'rec3'){
                //6 Months
                $expDate = date('m/d/Y h:i:sA', getUnixTimestamp($subDate,'6M')); 
                $billType .= '(6 Months)';
            }elseif ($orderInfo['rec_type'] == 'rec4'){
                //1 Year
                $expDate = date('m/d/Y h:i:sA', getUnixTimestamp($subDate,'1Y')); 
                $billType .= '(1 Year)';
            }
        }
        
    }
    
    if(isset($_GET['edit'])){
        $planList  = getPlanList($con);
        $sel = $planListData = '';
        foreach($planList as $plan){
            if($plan[0] == Trim($orderInfo['plan_id']))
                $sel = 'selected=""';
            else
                $sel = '';
            $planListData .= '<option '.$sel.' value="'.$plan[0].'">'.$plan[1].'</option>';
        }
        
        $payStatus1 = $payStatus2 = $payStatus3 = $payStatus4 = $payStatus5 = $payStatus6 = $payStatus7 = '';
        $orderStatus1 = $orderStatus2 = $orderStatus3 = $orderStatus4 = $orderStatus5 = '';
        
        
        if($orderInfo['status'] == 'completed')
            $orderStatus1 =  'selected=""';
        elseif($orderInfo['status'] == 'rec_completed')
            $orderStatus2 =  'selected=""';
        elseif($orderInfo['status'] == 'pending')
            $orderStatus3 =  'selected=""';
        elseif($orderInfo['status'] == 'rec_pending')
            $orderStatus4 =  'selected=""';
        elseif($orderInfo['status'] == 'canceled')
            $orderStatus5 =  'selected=""';

        if($orderInfo['payment_status'] == 'completed')
            $payStatus1 =  'selected=""';
        elseif($orderInfo['payment_status'] == 'pending')
            $payStatus2 =  'selected=""';
        elseif($orderInfo['payment_status'] == 'fraud')
            $payStatus3 =  'selected=""';
        elseif($orderInfo['payment_status'] == 'refunded')
            $payStatus4 =  'selected=""';
        elseif($orderInfo['payment_status'] == 'chargeback')
            $payStatus5 =  'selected=""';
        elseif($orderInfo['payment_status'] == 'reversed')
            $payStatus6 =  'selected=""';
        elseif($orderInfo['payment_status'] == 'canceled')
            $payStatus7 =  'selected=""';
    }
}

?>