<?php
defined('APP_NAME') or die(header('HTTP/1.0 403 Forbidden'));

/*
 * @author Balaji
 * @name: A to Z SEO Tools
 * @copyright © 2018 ProThemes.Biz
 *
 */
?>
                <div class="row">
                    <div class="col-md-8">
                                           
                       <br /><div class="box-header with-border">
                        <h3 class="box-title"><?php echo $arrData['gateway_title']. ' Settings'; ?></h3>
                       </div><br />
                       
                        <div class="form-group">
                          <label for="paypal_id">PayPal ID</label>
                          <input type="text" placeholder="Enter your paypal id" value="<?php echo $paypal_id; ?>" name="paypal_id" class="form-control" />
                        </div>
                        
                        <div class="form-group">
                        <label for="sandbox">Enable PayPal sandbox mode </label>
                        <select class="form-control" name="sandbox">
    						<option <?php echo $sandbox ? 'selected=""' : ''; ?> value="yes">Yes</option>
    						<option <?php echo $sandbox ? '' : 'selected=""'; ?> value="no">No</option>
    					</select>
                        </div> 
                        
                        <div class="form-group">
                          <label for="ipnUrl">IPN callback URL:</label>
                          <input type="text" disabled="" value="<?php createLink('payments/paypal/ipn',false,true); ?>" name="ipnUrl" class="form-control" />
                        </div>
                        
                        <div class="form-group">
                        <label for="log">Log IPN callback results into database</label>
                        <select class="form-control" name="log">
    						<option <?php echo $log ? 'selected=""' : ''; ?> value="yes">Yes</option>
    						<option <?php echo $log ? '' : 'selected=""'; ?> value="no">No</option>
    					</select>
                        </div>
                        
                        <div class="form-group">
                            <label for="paypalLogoBox">Header Logo: <small>(For PayPal Payment Page)</small></label><br />
                            <img class="paypalLogoBox" id="paypalLogoBox" alt="PayPal Logo" src="<?php echo $paypalFilePath; ?>" />   
                            <br />
                            Recommended Size: (PNG 150x79px)
                            <input type="file" name="paypalUpload" id="paypalUpload" class="btn btn-default" />
                       </div>    
                       
                       <br /><div class="box-header with-border">
                        <h3 class="box-title">Fraud Prevention</h3>
                       </div><br />
                        
                       <div class="form-group">
                        <label for="amount">Detect amount mismatch</label>
                        <select class="form-control" name="amount">
    						<option <?php echo $amount ? 'selected=""' : ''; ?> value="yes">Yes</option>
    						<option <?php echo $amount ? '' : 'selected=""'; ?> value="no">No</option>
    					</select>
                        </div> 
                        
                        <div class="form-group">
                        <label for="idMismatch">Detect PayPal ID mismatch</label>
                        <select class="form-control" name="idMismatch">
    						<option <?php echo $idMismatch ? 'selected=""' : ''; ?> value="yes">Yes</option>
    						<option <?php echo $idMismatch ? '' : 'selected=""'; ?> value="no">No</option>
    					</select>
                        </div>
                                            

                    </div>
                </div>